#
# I used these graphs for the MOREHIST 3 seminar in Toulouse (2015)
library(igraph)

# Erdos-Renyi
n <- 25
p <- 1
g <- sample_gnp(n, p)

la <- layout_in_circle(g)
plot(g,
     layout = la, 
     vertex.label = "",
     vertex.color = "black",
     vertex.size = 1 + 0.7 * log( graph.strength(g), 3 ), 
     edge.width = 1.5, 
     asp=9/16,
     margin=-0.15)

# Barabasi-Albert
n <- 1000
p <- 1.5
g <- sample_pa(n, power=p, directed=FALSE)
la <- layout_with_fr(g)
#la <- layout_with_lgl(g)
#la <- layout_as_star(g, center=1)
plot(g,
     layout = la, 
     vertex.label = "",
     vertex.color = "black",
     vertex.size = 1, #+ 0.7 * log( graph.strength(g), 3 ), 
     edge.width = 1.5, 
     #asp=9/16,
     margin=-0.15)

la <- layout_with_fr(g, grid="nogrid")
plot(as.undirected(g),
     layout = la, 
     vertex.label = "",
     vertex.color = "black",
     vertex.size = 1.5 + 1.5 * log( graph.strength(g), 3 ), 
     edge.width = 1.5, 
     asp=9/16,
     margin=-0.15)