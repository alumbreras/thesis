 %\documentclass[11pt]{amsart}
\documentclass[11pt]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{Variational inference for role-based regressions}
\author{Alberto Lumbreras}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
\section{Introduction}
In this document, I elaborate the equations for the variational inference algorithm applied to the model of role-based regressions.

\section{Notation}

Let $\mathbf{P}$ (observed participations matrix) be an $U \times T$ matrix where every column represents a thread. $p_{ij} = 1$ if user $i$ participates in thread $j$ and $0$ otherwise. 
\[
\mathbf{P}_{U \times T} =
\begin{bmatrix}
p_{11} & p_{12} &\cdots& p_{1T} \\
p_{21} & p_{22} &\cdots& p_{2T} \\
\vdots &  & & \vdots \\
p_{U1} & p_{U2} &\cdots& p_{UT} \\
\end{bmatrix}
\]

Let $\mathbf{C}$ (latent census matrix) be an $R \times T$ matrix where every column represents a thread and $c_{ij}$ indicates the percentage of users that hold role $i$ in thread $j$.
\[
\mathbf{C}_{R \times T} =
\begin{bmatrix}
c_{11} & c_{12} &\cdots& c_{1T} \\
c_{21} & c_{22} &\cdots& c_{2T} \\
\vdots &  & & \vdots \\
c_{R1} & c_{R2} &\cdots& c_{RT} \\
\end{bmatrix}
\]

Let $\mathbf{Z}$ (latent roles matrix) be an $R \times U$ matrix where every column represents a users and $z_{ij} = 1$ if user $j$ holds role $i$ and $0$ otherwise. A user only holds one role.
\[
\mathbf{Z}_{R \times U} =
\begin{bmatrix}
z_{11} & z_{12} &\cdots& z_{1U} \\
z_{21} & z_{22} &\cdots& z_{2U} \\
\vdots &  & & \vdots \\
z_{R1} & z_{R2} &\cdots& z_{RU} \\
\end{bmatrix}
\]

\section{Generative model}

For a given thread $t$, we model its success as a linear combination of its census:
\begin{align}
y^{(t)} = \sum_{i=1}^R c_{it} w_i+ e_t
\end{align}
which for the set of all the threads is:
\begin{align}
\mathbf{y} = \mathbf{C^T} \mathbf{w}+\mathbf{e}
\end{align}
Assuming that the noise is gaussian, we can express it in probabilistic terms as:
\begin{equation}
\mathbf{y} \sim \mathcal{N}(\mathbf{C^T} \mathbf{w}, \mathbf{Ie})
\end{equation}

$\mathbf{C}$ is the census matrix computed as the product between the roles matrix $\mathbf{Z}$ (latent) and the participations matrix $\mathbf{P}$ (observed): 
\begin{equation}
\mathbf{C=ZP}
\end{equation}

To fully specify the model, we place some priors in $\mathbf{z}$, getting to the following generative process:
\begin{align}
y_u | \mathbf{z}, \mathbf{w} &\sim \mathcal{N}(\mathbf{C^T w}, \sigma)\\
\mathbf{w} &\sim \mathcal{N}(0, \Sigma)\\
z_u &\sim \mbox{Multinomial}(\pi)\\
\pi &\sim \mbox{Dirchlet}(\alpha)
\end{align}

The process is as follows:
\begin{itemize}

   \item Draw the role coefficients, $\mathbf{w} \sim \mathcal{N}(0, \Sigma)$.   
   \item For every user $ u \in \mathcal{U}$:
   \begin{itemize}
      \item Draw a role, $z_i \sim \mbox{Categorical}(\pi)$.
   \end{itemize}

   \item For every thread $ t \in \mathcal{T}$:
   \begin{itemize}
      \item Draw a length, $y_t \sim  \mathcal{N}(\mathbf{Cw}, \sigma_t)$.
   \end{itemize}

\end{itemize}

The joint probability distribution of the model is:
\begin{align}
p(\mathbf{y, w, z}) = 
\prod_{i=1}^U p(z_i) 
\prod_{t=1}^T p(y_t | \mathbf{z_{(t)}, w})
p(\mathbf{w})
\label{full_joint}
\end{align}

where $z_{(t)}$ is the set of $z_i$ such that user $i$ participates in thread $t$.

\section{Mean fields variational inference}
We want to maximize the posterior w.r.t $\mathbf{z, w}$
\begin{align}
p(\mathbf{w, z | y})&=
\frac{p(\mathbf{y, w, z})}{p(\mathbf{y})}
\end{align}
where:
\begin{align}
p(\mathbf{y}) &= 
\int_{\mathbf{w}} \sum_{\mathbf{z}} p(\mathbf{y, w, z})
\end{align}
which we cannot solve analytically,  due to dependencies between the different $z$, and we cannot solve computationally due to the high number of $z$. Monte Carlo methods (e.g.: Gibbs sampling, Metropolis-Hastings, Hamiltonian Monte Carlo) are a natural solution for this problem. 

The rationale behind Gibbs sampler is the following. Solving the full posterior is hard, but sometimes we can easily take samples from the  individual conditionals since there formulas can be found easier, at least up to a proportionality constant. In our model, this means sampling from every $z_u$ given all the other variables, $p(z_u|\mathbf{z_{-u}, w, y})$ and sampling from $\mathbf{w}$ given all the other variables $p(\mathbf{w} | \mathbf{z, y})$. The variables in every conditional are those sampled in previous steps. We repeat this sequence many times, storing every sample we got. In the end the samples gotten from $p(z_u|\mathbf{z_{-u}, w, y})$ (or any other sampled variable) will approximate its posterior $p(z_u | \mathbf{y})$.

In the next section, we derive the conditional distributions of the variables in our model.


\subsection{Roles assignments $p(z_u | \mathbf{z_{-u}, w, y})$}

\begin{align}
p(z_u | \mathbf{z_{-u}, w, y})&=
\frac
{p(\mathbf{z, w, y})}
{\sum_{z_u=1}^R p(\mathbf{z, w, y})}\\
&=
\frac
{
\prod_{i=1}^U p(z_i) 
\prod_{t=1}^T p(y_t | \mathbf{z_{(t)}, w})
p(\mathbf{w})}
{
\sum_{z_u=1}^R 
\prod_{i=1}^U p(z_i) 
\prod_{t=1}^T p(y_t | \mathbf{z_{(t)}, w})
p(\mathbf{w})
}
\end{align}

Note that $p(\mathbf{w})$ does not depend on the value of $z_u$ and therefore is equal in both numerator and denominator. Same happens for $p(z_i)$ where $i \neq u$. As for the threads, the only $y_t$ that depends on $z_u$ are those where user $u$ participates. By canceling all these factors we arrive to:
\begin{align}
p(z_u | \mathbf{z_{-u}, w, y})&=
\frac
{
p(z_u) 
\prod_{t \in T_u} p(y_t | \mathbf{z_{(t)}, w})
}
{
\sum_{z_u=1}^R 
p(z_u) 
\prod_{t \in T_u} p(y_t | \mathbf{z_{(t)}, w})
}
\end{align}
where we use $T_u$ to denote the set of threads where user $u$ participates. And since the denominator is just a normalization factor:
\begin{align}
p(z_u | \mathbf{z_{-u}, w, y})&\propto p(z_u) p(\mathbf{y_T} | \mathbf{z_{(T)}, w})
\end{align}
where the right term is actually a product of likelihoods that, since these are univariate gaussians, can be computed as a single multivariate gaussian.

In case we marginalize the joint distribution with respect to $\pi$ (see Appendix \ref{appendix_pi}), the different $z$ will not be independent anymore, and we will have:
\begin{align}
p(z_u | \mathbf{z_{-u}, w, y})&\propto p(z_u | \mathbf{z_{-u}}) p(\mathbf{y_T} | \mathbf{z_{(T)}, w})
\end{align}
Derivating the left factor we have:
\begin{align}
p(z_u | \mathbf{z_{-u}, w, y})&\propto \frac{\alpha + q_x -1}{R\alpha + U -1} p(\mathbf{y_T} | \mathbf{z_{(T)}, w})
\end{align}

\subsection{Role coefficients $p(\mathbf{w} | \mathbf{z, y})$}
\begin{align}
p(\mathbf{w} | \mathbf{z, y}) &=
\frac
{p(\mathbf{z, w, y})}
{\int_{w_r} p(\mathbf{z, w, y})}
&=
\frac
{
\prod_{i=1}^U p(z_i) 
\prod_{t=1}^T p(y_t | \mathbf{z_{(t)}, w})
p(\mathbf{w} | \mathbf{w_0}, \Sigma_0))}
{
\int_{\mathbf{w}}
\prod_{i=1}^U p(z_i) 
\prod_{t=1}^T p(y_t | \mathbf{z_{(t)}, w})
p(\mathbf{w} | \mathbf{w_0}, \Sigma_0))
}
\end{align}
Note that $p(z_i)$ does not depend on $w_r$. Therefore we cancel these factors out:
\begin{align}
p(\mathbf{w}| \mathbf{z, y})
&=
\frac
{
\prod_{t=1}^T p(y_t | \mathbf{z_{(t)}, w})
p(\mathbf{w} | \mathbf{w_0}, \Sigma_0)}
{
\int_{w_r}
\prod_{t=1}^T p(y_t | \mathbf{z_{(t)}, w})
p(\mathbf{w} | \mathbf{w_0}, \Sigma_0)
}
\end{align}
Since $p(w_r)$ and $p(y_ |\mathbf{z_{(t)}, w} )$ are both gaussians we know that the posterior $p(\mathbf{w} | \mathbf{z, y})$ is gaussian too. The denominator is just a normalization factor that we can ignore as long as the final distribution be properly normalized.

So, we find the parameters of the posterior gaussian $w_{r_p}, \sigma_{r_p}$. For the sake of notation simplicity, we will drop the $r$ subindex and will talk about $w_p$ and $\sigma_{p}$ from now on. Ignoring the normalization factor we get:
\begin{equation}
p(\mathbf{w} | \mathbf{z, y})
\propto
\prod_{t=1}^T p(y_t | \mathbf{c^t w}, \sigma)p(w_r)
= 
\prod_{t=1}^T\mathcal{N}(y_t | \mathbf{c_{(t)}^T w}, \sigma) \mathcal{N}(w_r | w_0, \sigma_0)
\label{posterior_wr1}
\end{equation}
As we know that the result is a gaussian like:
\begin{align}
\mathcal{N}(\mathbf{w_p}, \Sigma_p) 
&\propto 
\exp
\left \{ \frac{1}{2}
(\mathbf{w} - \mathbf{w_p})^T \Sigma^{-1} (\mathbf{w} - \mathbf{w_p})
\right \}\\
&=
\exp
\left \{ \frac{1}{2}
(
\mathbf{w}^T  \Sigma^{-1}\mathbf{w}
+
\mathbf{w_p}^T\Sigma^{-1}\mathbf{w_p}
-2(\mathbf{w}^T\Sigma^{-1} \mathbf{w_p}
)
\right \}
\label{posterior_template}
\end{align}
we sum the two exponents in the gaussians of equation \ref{posterior_wr1} and identify $\mathbf{w_p}$ and $\Sigma_p$ in the equation \ref{posterior_template}. 
Expanding the exponents of equation \ref{posterior_wr1} we get:
\begin{align}
&=
\frac{1}{2\sigma^2}\sum_{t=1}^T(y_t - \mathbf{c_{(t)}^T w})^2
+
\frac{1}{2}(\mathbf{w - w_0})^T \Lambda_0(\mathbf{w - w_0}))\\
&=
\frac{1}{2\sigma^2}(\mathbf{y - C^Tw})^T (\mathbf{y - C^Tw})
+
\frac{1}{2}(\mathbf{w - w_0})^T \Lambda_0 (\mathbf{w - w_0}))\\
&=
\frac{1}{2\sigma^2}\left(\mathbf{y^Ty + w^TCC^Tw -2 yC^Tw}\right)
+
\frac{1}{2}\left(\mathbf{w^T \Lambda_0 w + w_0^T\Lambda_0 w_0 -2w^T\Lambda_0 w_0} \right)\\
&\propto
\frac{1}{\sigma^2}\left(\mathbf{w^TCC^Tw - 2yC^Tw} \right)
+ \left( \mathbf{w^T \Lambda_0 w - 2w^T \Lambda_0 w_0} \right)\\
&=
\mathbf{w^T (\frac{C C^T}{\sigma^2} + \Lambda_0) w} - 
2\mathbf{
w^T \left(
\frac{Cy^T}{\sigma^2}+ \Lambda_0 w_0
\right)
}
\end{align}
From this we can identify:
\begin{align}
\mathbf{\Lambda_p} &=  \mathbf{\frac{C C^T}{\sigma^2} + \Lambda_0}\\
\mathbf{w_p} &= \mathbf{\Lambda_0^{-1}}
\left( 
\mathbf{
\frac{C y^T}{\sigma^2} + \Lambda_0 w_0 
}
\right)
\end{align}

\section{MCMC with Tempered Transitions}
Tempered transitions MCMC is a technique that performs Metropolis-Hastings updates in a simulated annealing way. As in Metropolis-Hastings, we need to define the joint probability and a transition kernel. The transition kernel performs multi-jump where every jump is made at a different temperature of the density (that is, raising the density to a power). Every multi-jump (tempered transition) will be accepted or rejected in such a way that the final result respects the original distribution. The general description of the algorithm is as follows:
\begin{itemize}
\item Create flattened versions of $p_0$: $p_1$, $p_2$... $p_m$. $p_m$ is the more uniform distribution, and therefore the easiest to sample from.
\item Sample from $p_0$, then from $p_1$... then from $p_m$. Then $p_{m-1}$... $p_0$. Forget intermediate samples and keep only those sampled from $p_0$. The intermediate steps allow us to make larger jumps and escape from local modes.
\item The new sample from $p_0$ is submitted to a a Metropolis-Hasting accept/reject so that it is garanteed that the stationary distribution is $p_0$.
\end{itemize}

The joint probability distribution of the model is:
\begin{align}
p(\mathbf{y, w, z}) = 
\prod_{i=1}^U p(z_i) 
\prod_{t=1}^T p(y_t | \mathbf{z_{(t)}, w})
p(\mathbf{w})
\label{full_joint}
\end{align}

\section{References}
http://www.cs.princeton.edu/courses/archive/fall11/cos597C/lectures/variational-inference-i.pdf

\appendix




\end{document} 

