\chapter{Contributions and perspectives}\label{ch:six}
%\epigraph{The philosophers have only interpreted the world, in various ways. The point, however, is to change it}{Karl Marx}
% http://www.stats.org.uk/quotes/
%\minitoc% 

\section{Contributions}

In this thesis we set out to automatically detect roles in online discussion forums. We tried to stick as much as possible to the sociological definition of role as the \textit{behavioral repertoire characteristic of a person or a position} \citep{Biddle1979}. Following this idea, we focused our efforts on conversational behaviors since conversations are the constituent behaviors of a forum. First, we have considered that two users hold the same role if they tend to participate in the same type of conversation. By taking into account their structural position in the motif of the conversation that embeds a post (a neighborhood), we aimed to capture the different ways users contribute to conversations where they participate. Indeed, we were able to showed that we can detect different types of conversationalists.

Secondly, in order to take a more functional approach---in the sense of capturing the functions that drive user behaviors--- we considered that two users hold the same role if their replying behavior---their criteria for choosing a post to reply--- can be modeled by the same function. Or, more specifically, if their choices can be modeled by the same probability distribution. We showed that not all users behave similarly and that indeed modeling users as members of different subpopulations with different behavioral functions leads not only to a better fit of the data compared to a model that does not consider differences between the individuals ---the model is more flexible--- but also to better likelihoods in unobserved behaviors. Yet we also showed that this improvement is not enough to make better predictions, which leaves room for improvement of the model.

In an attempt to integrate the former two approaches, we proposed a third \textit{dual-view} method that opens the door to the integration of features and behavioral functions. In particular, we modeled users features and behaviors as generated from a mixture model of roles, where users in the same component ---the same role--- have similar features and similar behaviors. We used synthetic data to show the properties of the model. In particular, we showed that a dual-view model can learn with fewer examples than models that use one single view (features as in our first definition, or behaviors as in our second one.).

%The expectation from the community in online forums does not hold. There is not a closed community, but an huge society of users where only a few are active. Hence, we concentrated our efforts in trying to find regularities in the user behaviors, regardless of the community.

%\bertrandbox{Dire très clairment que c'est une approche data based favorisé par le fait que c'est une thèse CIFRE (fortaleza)}

\section{Roles or not roles?}
The aim of this thesis was to detect roles and to prove that these roles can be used as predictors of a user behavior. We showed that conversational behaviors, regarded from a merely structural point of view, are different for different groups of users. We stated that, in order to give these clusters the category of role, they needed to prove their predictive power. There is some evidence in favor of this---an increase in the likelihood of unobserved behaviors---but the evidence is not strong enough and thus we do not dare to claim that we found \textit{roles} from a grounded sociological basis.

Therefore, the question remains open. Are there \textit{predictive} roles in online forums? There are certainly some users with a consistent extreme behavior, such as \textit{trolls}, and specific methods have been proposed by some authors to detect them. Yet a finer-grained taxonomy might not be possible. One can blame the object---users are too random in online conversations, they have too much variance---or the measuring instrument---the method does not capture enough the non-random part. Or one can blame both.

As for the object, it might be good news that humans are not \textit{always} that predictable---although, in many aspects, they are; we would have otherwise an army of jobless sociologists, statisticians and machine learners. As for the measure instrument, we propose some possible improvements in the following section.

%Lastly, we would like to emphasize the importance of an interdisplicinar approach that involves sociologists. Because the concept of role lays in the sociological plane, computer scientists should collaborate with sociologists to avoid applying the term of role \textit{with impunity to almost any purpose}, as Biddle criticized. Some remarkable examples of this interdisciplinarity are \cite{Viegas2004},\cite{Fisher2006} \cite{Welser2007,Welser2011}, \cite{Himelboim2009} \cite{Glea2009}---the co-authorship graph of these papers is actually a connected component.



\section{Perspectives}
As mentioned before, the role detection machinery can be improved in many ways. These are left as paths for future research. We divide them in methodological and technical.

\subsection{Technical}

\subsubsection{Clustering based on count data}
In Chapter~\ref{ch:three}, we chose hierarchical clustering to compare the clusters for each type of neighborhood and forum. The main object was a matrix of discrete distributions, where each distribution indicates the tendency of a user towards each type of conversation. We recall that, before obtaining that matrix, we had a matrix of counts ---how many times a user was seen in each motif. As such, some specific methods can be used to deal with count data. For instance, we might model the vectors of counts as draws from a mixture of multinomial distribution. Then, we would infer clusters of users where users in the same cluster draw their vectors of counts from the same multinomial distribution. This model-based clustering can be easily used as the feature branch of our dual-view model. 

\subsubsection{A scalable dual-view model}
In Chapter~\ref{ch:five}, we faced a serious problem of scalability in our dual-view model. The reason was two-fold. On the one hand, the need to compute the inverse of a matrix with as many rows and columns as users. This was required during the Gibbs Sampling to get the covariance matrix of the Normal distribution that generates the user coefficients (the matrix $\boldsymbol{\Lambda'}^{-1}$ to sample from  $p(b_u | \cdot)$ in the Appendix \ref{sec:appendix_coefficients}). This problem can be easily avoided if we choose to model individual behaviors instead of collective behaviors (the length of a thread), since the inference of a user parameters would be conditionally independent, given the clusters, of the other users. The behavioral model presented in \ref{ch:four} holds this condition. Unfortunately, its likelihood function has no conjugate priors and thus we could not use Gibbs Sampling to sample the behavioral coefficients ($\alpha, \beta, \tau$). Instead, we may use other sampling methods that do not require conjugacy.

On the other hand, Gibbs Sampling and Monte-Carlo methods are easily beaten in speed by Variational Inference methods. Replacing our Gibbs Sampler by a Variational Inference would likely boost the speed considerably. Although we would obtain approximations to the posterior---Variational Inference approximates the posterior with a function that is analytically tractable---the gain in scalability might be worth it.  

\subsection{Methodological}

\subsubsection{A dictionary of conversations}
We showed in Chapter~\ref{ch:three} that the different neighborhood definitions gave motifs that correspond, without much doubt, to the same type  of conversation. Thus, some of them might be considered isomorphic. Even if we might apply a more agressive pruning, we think that a more reasonable solution is to do this task manually so that we obtain a dictionary of meaningful conversational motifs. In other words, an orthogonal---as much as possible---base of conversations.

\subsubsection{A better tree growth model}
%%%If the community was dense, we might apply a blockmodeling to infer the tendency of users of one group to reply to users of another group. We could 

Our model in Chapter~\ref{ch:four} considers that each group of users behaves following this behavioral function, borrowed from \cite{Gomez2012}:

\begin{equation}
p(\pi_t = i |  \boldsymbol{\pi}_{1:(t-1)}) \propto \alpha_{z(a(t))} d_i + \beta_{z(a(t))} r_i+ \tau_{z(a(t))}^{l_i}
\end{equation}

This model assumes that when a user chooses to reply to a post $i$ over the set of posts in a thread, they consider the popularity $i$, its recency and whether $i$ is the root of the thread. It seems reasonable to assume that the choice also depends on who the author of $i$ is. Some authors, for instance, might have the ability to write particularly interesting posts. Thus, we might consider that clusters are also associated to an \textit{interestingness} factor and that users in the same cluster write posts with similar levels of \textit{interestingness}. The likelihood would be: 
\begin{equation}
p(\pi_t = i | \boldsymbol{\pi}_{1:(t-1)}) \propto \alpha_{z(a(t))} d_i + \beta_{z(a(t))} r_i+ \tau_{z(a(t))}^{l_i} + \eta_{z(i)}
\end{equation}
where $\gamma_k$ would be the interestingness of users in cluster $k$. Note, however, that this model would increase in $K$ (the number of clusters) the number of parameters.

Moreover, we might apply the idea of blockmodeling and consider that the interestingness depends on the groups of both users (the replier and the replied). For instance, everyone might be attracted by posts written by experts, and experts might only be attracted by posts from other experts. If we denote as $\eta_{k,k'}$ the interestingness of posts from group $k'$ from the point of view of users in group $k$, we might express the likelihood as:

\begin{equation}
p(\pi_t = i | \boldsymbol{\pi}_{1:(t-1)}) \propto \alpha_{z(a(t))} d_i + \beta_{z(a(t))} r_i+ \tau_{z(a(t))}^{l_i} + \eta_{z(a(t)), z(a(i))}
\end{equation}
where the number of new parameters is $K\times K$.

\subsubsection{Combining structure and text}
In this thesis, we took into consideration only the structural aspect of conversations. We claimed that the subtleties of user language are ---often--- difficult to capture by current algorithms. Yet some textual analysis might be helpful if we combine it  with structural analysis presented in this thesis.

We might, for instance, detect some basic textual features of the post and add them to the growth model of Chapter \ref{ch:four}. An off-topic post, for instance, might be less attractive than a post that uses the key words of a particular forum. The length of a post might also be a good predictor. A basic sentiment  analysis of posts to classify them into negative, neutral, and positive may give another feature. In Chapter \ref{ch:three}, we might also add textual features to (carefully) increase our dictionary of motifs.
