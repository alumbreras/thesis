%\documentclass[11pt]{amsart}
\documentclass[11pt]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{Cluster-based regression}
\author{Alberto Lumbreras}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
\section{Introduction}
Clustering assigns a set of of observation or data points into subsets, called clusters, so that observations in the same cluster are similar according to some similarity metric. Regression, on the other hand, finds the coefficients of this equation:
\begin{equation}
y^{(i)} = X^T w + \epsilon
\label{regression}
\end{equation}

We propose the following learning problem. Given some observations $y$, and some users belonging to latent clusters $C$ so that $n$ is a vector indicating the number of users in every cluster, model $y$ as a linear combination of $n$:

\begin{equation}
y^{(i)} = n^T w + \epsilon
\label{interdependence_belbin}
\end{equation}

\section{Notation}
\begin{itemize}
\item $\mathbf{y}$ is the vector of outputs of dimensions $n \times D$.
\item Following a machine learning convention, the inputs are represented in a matrix $\mathbf{X}$ where instances are columns.
\end{itemize}

\section{Linear regression}
A linear regression model follows the equation
\begin{equation}
\mathbf{y} = \mathbf{X^T}\mathbf{w} + \mathbf{e}
\end{equation}
where $\mathbf{e}$ is a vector with the noises of every instance ${\epsilon_1,...\epsilon_n}$ and $\mathbf{w}$ is the vectors of the coefficients to be found.

Given the inputs $\mathbf{X}$ and the outputs $\mathbf{y}$, the task is to find the vector $\mathbf{w}$ that better fits the data. Different fitness metrics and different methods to find the parameters that maximize the fitness give place to different regression algorithms
\subsection{Ordinary Least Squares}
One possible fitness measure is the Sum of Squared Errors (SSE), given by
\begin{equation*}
SSE = e^Te = (\mathbf{y}-\mathbf{X^T}\mathbf{w})^T (\mathbf{y}-\mathbf{X^T}\mathbf{w}) 
=
\mathbf{y^T}\mathbf{y} + \mathbf{w^T}\mathbf{X}\mathbf{X^T}\mathbf{w} - 2\mathbf{w^T}\mathbf{X}y
\end{equation*}
The most basic way to compute the weights of a linear regression model is the Ordinary Least Squares (OLS). OLS minimize the sum of the squared errors.

The first factor is contanst, the third factor is a first order derivative and the second factor derivative has the following form:
\begin{align*}
\frac{\partial \mathbf{w^T} \mathbf{B}\mathbf{w}}{\partial \mathbf{w}}
= (\mathbf{B} + \mathbf{B}^T) \mathbf{w}
\end{align*}

Therefore the expression of the derivative is:
\begin{equation*}
\frac{\partial SSE}{ \partial \mathbf{w}} =  2\mathbf{X}\mathbf{X^T}\mathbf{w} - 2\mathbf{X}\mathbf{y}
\end{equation*}

And the weights $w$ that minimize the SSE are:
\begin{align*}
2\mathbf{X}\mathbf{X^T}\mathbf{w} - 2\mathbf{X}\mathbf{y} &= 0 \\
\mathbf{X}\mathbf{X^T}\mathbf{w} &= \mathbf{X}\mathbf{y} \\
(\mathbf{X}\mathbf{X^T})^{-1}\mathbf{X}\mathbf{X^T}\mathbf{w} &= (\mathbf{X}\mathbf{X^T})^{-1}\mathbf{X}\mathbf{y}\\
\mathbf{w} &= (\mathbf{X}\mathbf{X^T})^{-1}\mathbf{X}\mathbf{y}
\end{align*}

\subsection{Maximum Likelihood Estimation}
The problem can be expressed in probability terms. If the noise $\mathbf{e}$
follows a normal distribution of variance $\sigma_\epsilon^2$, the equation of the linear regression can be expressed as:

\begin{equation}
\mathbf{y} \sim \mathcal{N}(\mathbf{X^Tw}, \sigma_\epsilon^2)
\end{equation}

The maximum likelihood estimation (MLE) of  $\mathbf{w}$ searches the coefficients that maximize:
\begin{align*}
max_w p (y | w ) = \mathcal{N}(\mathbf{y} | \mathbf{X^Tw}, \sigma_\epsilon^2) = C \exp(\frac{1}{\sigma}(\mathbf{y-X^T})^T(\mathbf{y-X^T})
\end{align*}
which is equivalent to minimize the exponent:
\begin{align}
(\mathbf{y}-\mathbf{X^T}\mathbf{w})^T (\mathbf{y}-\mathbf{X^T}\mathbf{w}) 
\end{align}
and therefore OLS is equivalent to MLE when the noise is normally distributed.
\subsection{Bayesian estimation}
Bayesian estimation considers that $\mathbf{w}$ is a random variable with some \textit{prior} distribution.
\begin{align*}
\mathbf{y} &\sim \mathcal{N}(\mathbf{X^Tw}, \sigma_\epsilon^2) \\
\mathbf{w} &\sim \mathcal{N}(0, \Sigma_0)
\end{align*}
Since $\mathbf{w}$ is now a random variable and not a fixed parameters, bayesian estimation finds its posterior distribution, that is, its updated distribution after seeing the data:
\begin{align}
p(\mathbf{w | y}) \propto p(\mathbf{y |w}) p(\mathbf{w})
\end{align}
Since both $p(\mathbf{y |w})$ (the likelihood) and $p(\mathbf{w})$ (the prior) are normal distributions, their product is another normal distribution with a mean $\mathbf{m_p}$ and variance $\Sigma_p$.

\section{Cluster-based regression}

Imagine an discussion forum with $T$ threads and a population of $U$ users that participate along the different threads. Previous research has suggested that users can be classified by roles, that is, they can be grouped according to some behavioral or structural similarity in the network. Our hypothesis is that the success of a thread can be modeled as a linear combination of the number of users in every role. Moreover, we define roles merely in terms of contribution to success. Thus, users with similar contribution levels are considered to play the same role.

Let $\mathbf{P}$ (observed participations matrix) be an $U \times T$ matrix where every column represents a thread. $p_{ij} = 1$ if user $i$ participates in thread $j$ and $0$ otherwise. 
\[
\mathbf{P}_{U \times T} =
\begin{bmatrix}
p_{11} & p_{12} &\cdots& p_{1T} \\
p_{21} & p_{22} &\cdots& p_{2T} \\
\vdots &  & & \vdots \\
p_{U1} & p_{U2} &\cdots& p_{UT} \\
\end{bmatrix}
\]


Let $\mathbf{C}$ (latent census matrix) be an $R \times T$ matrix where every column represents a thread and $c_{ij}$ indicates the percentage of users that hold role $i$ in thread $j$.

\[
\mathbf{C}_{R \times T} =
\begin{bmatrix}
c_{11} & c_{12} &\cdots& c_{1T} \\
c_{21} & c_{22} &\cdots& c_{2T} \\
\vdots &  & & \vdots \\
c_{R1} & c_{R2} &\cdots& c_{RT} \\
\end{bmatrix}
\]


Let $\mathbf{Z}$ (latent roles matrix) be an $R \times U$ matrix where every column represents a users and $p_{ij} = 1$ if user $i$ holds role $j$ and $0$ otherwise. A user only holds one role.
\[
\mathbf{Z}_{R \times U} =
\begin{bmatrix}
z_{11} & z_{12} &\cdots& z_{1U} \\
z_{21} & z_{22} &\cdots& z_{2U} \\
\vdots &  & & \vdots \\
z_{R1} & z_{R2} &\cdots& z_{RU} \\
\end{bmatrix}
\]

For a given thread $t$, we model its success as a linear combination of its census:
\begin{align*}
y^{(t)} = \sum_{i=1}^R c_{it} w_i+ e_t
\end{align*}
and we can express all threads together:
\begin{align}
\mathbf{y} = \mathbf{C^T} \mathbf{w}+\mathbf{e}
\end{align}
Unlike a regular linear regression where we have to find the optima $\mathbf{w}$, in our model we have find $\mathbf{C}$ as well. $\mathbf{C}$ can be decomposed as the product $\mathbf{ZP}$. Then, replacing in the former equation, 
\begin{align*}
\mathbf{y} = \mathbf{(ZP)^T} \mathbf{w}+\mathbf{e}
\end{align*}

that can be rearranged as:
\begin{align}
\mathbf{y} = \mathbf{P^T}(\mathbf{Z^T} \mathbf{w})+\mathbf{e}
\end{align}
and by naming $\mathbf{Z^Tw} = \mathbf{a}$ we have:
\begin{equation}
\mathbf{y} = \mathbf{P^T}\mathbf{a}+\mathbf{e}
\end{equation}

that is a classic linear regression and therefore its maximum likelihood $\mathbf{a}$ is:
\begin{equation}
\mathbf{a} = \mathbf{(P P^T)}^{-1}\mathbf{P}\mathbf{y}
\end{equation}
Finally, we have the equation:
\begin{equation*}
\mathbf{Z^Tw} = \mathbf{a}
\end{equation*}
which looks like
\[
\begin{bmatrix}
z_{11} & z_{21} &\cdots& z_{R1} \\
z_{12} & z_{22} &\cdots& z_{R2} \\
\vdots &  & & \vdots \\
z_{1U} & z &\cdots& z_{RU} \\
\end{bmatrix}
\begin{bmatrix}
w_1\\
w_2\\
\vdots\\
w_R
\end{bmatrix}
= 
\begin{bmatrix}
a_1\\
a_2\\
\vdots\\
a_U
\end{bmatrix}
\]
Note that every row in $\mathbf{Z^T}$ represents a single variable; let $z_i$ denote the position of the only 1 in the row, that is, the identifier of the role hold by user $i$. It is easy to see that the left side of the equation is a vector of size $U$ where row $i$ contains the  weight associated to the role $z_i$:
\[
\begin{bmatrix}
w_{z_1}\\
w_{z_2}\\
\vdots\\
w_{z_U}
\end{bmatrix}
=
\begin{bmatrix}
a_1\\
a_2\\
\vdots\\
a_U
\end{bmatrix}
\]
The system has U+R variables. Therefore, for the equation to have a unique solution, the number of roles will be chosen as $rank(\mathbf{a})$ which in this case is just the number of different $a_i$. Every $a_i$ is the weight $w$ associated to the role $z_i$; thus, vector $\mathbf{a}$ gives us the role partition that uses the minimum number of possible roles. Two users $i$ and $j$ have the same role if and only if $a_i=a_j$.

Unfortunately, the most likely is that $rank(\mathbf{a})$ be almost $U$, since it is unlikely that two users participate in identical threads, with same census $\mathbf{c}$ and same success $y$. This issue is a consequence of our implicit definition of role: two users have the same role if they contribute exactly the same to the success of threads. We will soften this constraining definition in the following section.

\subsection{Bayesian estimation}

In the last section, we ended up with a vector $\mathbf{a}$ that indicates how much users contribute to the success of the threads they participate in.
Two users, we implicitly said, hold the same role if they both contribute the same to their respective threads. Nonetheless, this definition is limiting and tends to assign a different role to every user, making the analysis useless.

To alleviate this consider every coefficient $w_{z_i}$ to be drawn from a gaussian distribution associated to its role $\mathcal{N}(m_r, \sigma)$. Intuitively, we will seek coefficients that seems to be drawn from the same distribution and which therefore belong to the same latent role. This is a well known model called Gaussian Mixture Model.

To formalize it, we consider the following generative process:
\begin{itemize}
\item Draw a $R$ dimensional vector with the probabilities of every role,  $
\pi \sim \mbox{Dirichlet}(\alpha)$.
\item For every role $r \in \mathcal{R}$:
\begin{itemize}
\item Draw the means of its normal distribution, $m_u \sim \mathcal{N}(0, s)$.
\end{itemize}
\item For every user $ u \in \mathcal{U}$:
\begin{itemize}
\item Draw a role, $z_i \sim \mbox{Categorical}(\pi)$.
\item Draw a coefficient $a_u$, $a_u \sim \mathcal{N}(m_{z_i}, \sigma)$.
\end{itemize}
\end{itemize}

The random variables of this process are:
\begin{align*}
a_u | \mathbf{z}, \mathbf{m} &\sim \mathcal{N}(m_{z_u}, \sigma)\\
m_r &\sim \mathcal{N}(0, \Sigma)\\
z_u &\sim Cat(\pi)\\
\pi &\sim Dir(\alpha)
\end{align*}

The joint probability distribution of the weights is:
\begin{align}
p(\mathbf{a, z, m, \pi}) = 
\prod_{r=1}^R p(\pi | \alpha )
\prod_{u=1}^U p(z_u | \pi ) 
\prod_{u=1}^U p(a_u | \mathbf{z, m})
\prod_{r=1}^R p(m_r)
\end{align}

Our primary goal is to find $p(\mathbf{z, m | a})$. Since we are not interested on the latent variable $\pi$, we need to marginalize it out from the joint distribution:
\begin{align}
p(\mathbf{a, z, m}) = \int_\pi p(\mathbf{a, z, m, \pi})
\label{int_pi}
\end{align}
Then, the posterior probability distribution of $\mathbf{z, m}$ is:
\begin{equation}
p(\mathbf{z, m | \mathbf{a}}) = \frac{p(\mathbf{a, z, m})}{\int_{z,m} p(\mathbf{a, z, m})}
\end{equation}
The numerator is actually an integral over $U+R$ variables (every $z_u$ and every $m_r$) which cannot be solved analytically due to the dependencies between them. A common alternative is to sample from the posterior, instead of computing it analytically, using Monte Carlo methods such as Metropolis Hastings or Gibbs sampling. In the next section we will develop a Gibbs sampler for this posterior.

\subsubsection{Integrate out $\pi$}
First, we integrate out $\pi$ since we are not interested on it (equation \ref{int_pi}):
\begin{equation*}
\int_\pi p(\pi | \alpha) \prod_u^U p(z_u | \pi) 
= \int_\pi \frac{\Gamma(\sum_{r=1}^R \alpha_r)}{\prod_{r=1}^R \Gamma(\alpha_r)}\prod_{r=1}^R \pi_r^{\alpha_r -1} \prod_{u=1}^U \pi_r^{[z_u=r]} \\
\end{equation*}
Let $q_r$ be the number of users $u \in \mathcal{U}$ such that $z_u=r$. The we can reduce the equation to: 
\begin{equation*}
\int_\pi \frac{\Gamma(\sum_{r=1}^R \alpha_r)}{\prod_{r=1}^R \Gamma(\alpha_r)}\prod_{r=1}^R \pi_r^{\alpha_r + q_r -1} 
\end{equation*}
which is almost the Dirichlet distribution. If we multiply and divide by the normalization factor of Dirichlet$(\alpha + q)$ we get:
\begin{equation*}
\int_\pi 
\frac
{\Gamma(\sum_{r=1}^R \alpha_r)}
{\prod_{r=1}^R \Gamma(\alpha_r)}
\prod_{r=1}^R \pi_r^{\alpha_r + q_r -1} 
\frac
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
\end{equation*}
which is:
\begin{equation*}
\frac
{\Gamma(\sum_{r=1}^R \alpha_r)}
{\prod_{r=1}^R \Gamma(\alpha_r)}
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
\int_\pi 
\mbox{Dirichlet}(\alpha + q)
\end{equation*}
and finally, since the integral of a density function is one:
\begin{equation*}
\frac
{\Gamma(\sum_{r=1}^R \alpha_r)}
{\prod_{r=1}^R \Gamma(\alpha_r)}
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
\end{equation*}
Since $\alpha_r$ is a fixed parameter, the left factor does not change the shape of the final distribution, and therefore we can drop it into a proportionallity constant. The final expression is then:

\begin{equation}
\int_\pi p(\pi | \alpha) \prod_u^U p(z_u | \pi) =  \prod_u p(z_u | \alpha) 
\propto
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
\end{equation}

\subsubsection{Conditional $p(z_u | \mathbf{z_{-u}, m, a})$}
For the Gibbs sampler, we need to find out the conditional distribution of a role assignment given all the other latent variables being sampled (the rest of the role assignments and the vector $\mathbf{w}$)
\begin{align}
p(z_u | \mathbf{z_{-u}, m, a}) 
&=
\frac
{\prod_u p(z_u | \alpha)  \prod p(a_j | z_j, m_{z_j})\prod p(m_r)}
{\sum_{z_u=1}^R \prod_u p(z_u | \alpha)  \prod p(a_j | z_j, m_{z_j})\prod p(m_r)}\\
\end{align}
Note that the denominator is constant with respect to $z_u$, acting just as a normalization factor.  The posterior distribution of $z_u$ can be found by computing the numerator at every $z_u$ and then normalizing so that it sums up to one. However, if the denominator has common factors with the numerator we can simplify the numerator and speed up the computations. For instance, we can simplify to:
\begin{align}
\frac
{p(z_u | \alpha) \prod p(a_j | z_j, m_{z_j})}
{\sum_{z_u=1}^Rp(z_u | \alpha) \prod p(a_j | z_j, m_{z_j})}\\
\end{align}
%% TODO: sacar fuera el factor que dependa de z_x para proceder con la simplificaci�n.
%% En cualquier caso, ya podria tirar con el numerador, solo que computacionalmente ser� mas costoso porque tendr� que calcular todo lo que hay en el numerador.
%% OTRA opci�n es integrar m.
Let us try an easier way. Using $p(z_u | z_{-u}, \mathbf{a}) \propto p(\mathbf{a|z})p(\mathbf{z_u | z_{-u}})$, then:
\begin{align*}
p(\mathbf{z_u | z_{-u}}) &= \frac{p(\mathbf{z})}{\mathbf{z_{-u}}} \\
&=\frac{DirMult(\alpha, k, z)}{DirMult(\alpha, k, z_{-u})}\\
&=\frac{\prod \sum(\alpha / R + c_r) / \Gamma(\alpha + U)}
{\prod \sum(\alpha / R + c_{r}^{(-u)}) / \Gamma(\alpha + U-1)}\\
&=\frac
{\Gamma(\alpha/R + c_x)\prod_{u \neq x}\Gamma(\alpha / R + c_u)/ \Gamma(\alpha+U)}
{\Gamma(\alpha/R + c_x-1)\prod_{u \neq x}\Gamma(\alpha / R + c_u)/ \Gamma(\alpha+U-1)}\\
&=\frac
{\Gamma(\alpha/R+c_x)}
{\Gamma(\alpha/R+c_x-1)}
\frac
{\Gamma(\alpha + N-1)}
{\Gamma(\alpha + N)}\\
&=
\frac{\alpha/R + c_x -1}{\alpha+N-1}
\end{align*}

So finally we have:
\begin{align}
p(z_u | \mathbf{z_{-u}, a}) 
\propto 
\frac{\alpha/R + c_x -1}{\alpha + U - 1}
p(\mathbf{y} | z_u)
\end{align}


\subsubsection{Conditional $p(m_r |\mathbf{m_{-r}, z})$}
\begin{align}
p(m_r | \mathbf{a, z}) \propto p(\mathbf{a}| m_r) p(m_r)
\end{align}
since this is a product of gaussians, we  $p(m_r | \mathbf{a,z})$ is also a gaussian. Multiplying the two factors we have:
\begin{align}
\exp\left( \mathbf{\frac{1}{\sigma}(a-m_r)\frac{m_r}{\Sigma}}\right)
\end{align}
Now we re-arrange the exponent by \textit{completing the square} so that is has the form:
\begin{align}
\exp\left( \mathbf{\frac{1}{\sigma}(m_r - m_p)}\right)
\end{align}


\begin{align}
\Sigma_{p}^{-1} &= \Sigma_0^{-1} + n \Sigma^{-1}\\
m_{r_p} &= \Sigma_{p}^{-1}(m_0 \Sigma_0^{-1} + m n \Sigma^{-1})
\end{align}

\subsubsection{Gibbs sampler}

\subsubsection{Experiments}
\end{document} 