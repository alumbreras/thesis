\documentclass[../main/main.tex]{subfiles}

\begin{document}

\chapter{Clustering discussions}
In this chapter we analyze the types of conversations that emerge in online forums. We use a tree edit distance metric to compute the similarity between every pair of trees and then we perform a cluster analysis.

\section{Tree edit distance}
Tree Edit Distance is a metric to evaluate how different are two trees. The idea is to see one tree as the result of a set of editions over the other tree. The possible editions are \textit{insert}, \textit{delete} and \textit{rename}.

\section{Pruning}
Online discussions can be represented as tree graphs and thus the computation of their tree edit distance is straightforward. Nonetheless, the same type of discussion can appear in different sizes, which obliges us to adapt the algorithm so that size is not that important.
See, for instance, the three trees of Figure~\ref{fig:trees}. It is clear that the first two trees 
\subref{fig_long_cascade} and
\subref{fig_short_cascade} are cascades, usually provoked by two users engaging in a debate. The third tree \subref{fig_short_star} is quantitative different and it represents the star shape usually observed when someone asks a question the others answer with no debate among them. Although we would like the two cascades to be considered similar, the edit distance between the small cascade and the small star is smaller than the distance between the two cascades.

% Prunning
In order to make distance independent of size, we prune the trees so that we get a summary tree with only the relevant moments. Instead of comparing trees, we will compare their summaries. In this regard, pruning is a sort of normalization. Our pruning consists on: 


\begin{itemize}
	\item \textit{nodes with many children}: for every node, keep only the two children with the most descendants.
	 
	\item \textit{long cascades}: delete every node with no more than one children and no uncle. 
\end{itemize}
Figure \ref{fig:pruning} shows an example of pruning. Besides, pruning can drastically reduce the size of trees and therefore speed up the computations. For our dataset, pruning reduces the number of vertex by a scale of ten. (see Figure~\ref{fig:prunning_effects})

\begin{figure}
	\center
	\subfloat[long cascade]{
	\makebox[3cm][c]{
	\begin{tikzpicture}
	[<-,>=stealth, scale=1,auto=left,every node/.style={circle, fill=black}, level/.style={sibling distance = 1cm,level distance = 1cm}]
	\node (n1) at (10,20) {}
	child{ node(n2) {}
      child{ node(n3) {}
		child{ node (n4) {} 
			child{ node (n5) {}
				child{ node (n6) {}}}}}};
	\end{tikzpicture}
	\label{fig_long_cascade}
	}
	}
	\qquad
	\subfloat[short cascade]{
	\makebox[3cm][c]{
	\begin{tikzpicture}
	[<-,>=stealth, scale=1,auto=left,every node/.style={circle, fill=black}, level/.style={sibling distance = 1cm,level distance = 1cm}]
	\node (n1) at (10,20) {}
	child{ node(n3) {}
		child{ node (n4) {}}};
	\end{tikzpicture}
	\label{fig_short_cascade}
    }
    }
	\qquad
	\subfloat[short star]{
	\begin{tikzpicture}
	[<-,>=stealth, scale=1,auto=left,every node/.style={circle, fill=black}, level/.style={sibling distance = 1cm,level distance = 1cm}]
	\node (n1) at (10,20) {}
	child{ node(n2) {}}
	child{ node(n3) {}};
	\end{tikzpicture}
	\label{fig_short_star}
    }
	\caption{Illustration of how size can affect the edit distance. The edit distance between (a) and (b) is 3 (three insertions) and the edit distance between (b) and (c) is 2 (one deletion and one insertion)}
	\label{fig:trees}
\end{figure}

\begin{figure}
	\center
	\subfloat[original tree]{
	\makebox[3cm][c]{
	\begin{tikzpicture}
	[<-,>=stealth, scale=1,auto=left,every node/.style={circle, fill=black}, level/.style={sibling distance = 1cm,level distance = 1cm}]
	\node (n1) at (10,20) {}
	child{ node(n2) {}
	child{ node(n3) {}
	child{ node(n4) {}
	child{ node(n5) {}
	child{ node(n6) {}
	child{ node(n7) {}
	child{ node(n8) {}
	child{ node(n9) {}
	child{ node(n10) {}
	child{ node(n11) {}
	child{ node(n12) {}}}}}}}
	child{ node(n13) {}}
	child{ node(n14) {}}
	child{ node(n17) {}}}}}}
	child{ node(n18) {}}}
	child{ node(n20) {}}
	child{ node(n21) {}}
	child{ node(n22) {}
	child{ node(n23) {}
	child{ node(n24) {}
	child{ node(n25) {}}}}};
	\end{tikzpicture}
	\label{fig:short_cascade}
    }
    }
	\qquad
	\subfloat[pruned tree]{
\begin{tikzpicture}
	[<-,>=stealth, scale=1,auto=left,every node/.style={circle, fill=black}, level/.style={sibling distance = 1cm,level distance = 1cm}]
	\node (n1) at (10,20) {}
	child{ node(n2) {}
	child{ node(n3) {}
	child{ node(n4) {}
	child{ node(n6) {}
	child{ node(n7) {}
	child{ node(n12) {}}}
	child{ node(n17) {}}}}}
	child{ node(n18) {}}}
	child{ node(n22) {}
	child{ node(n23) {}}};
	\end{tikzpicture}
	\label{fig:short_star}
    }
	\caption{Exemple of pruning}
	\label{fig:pruning}
\end{figure}

\begin{figure}
\includegraphics[width=1\textwidth]{prunning_effects}
\caption{Effect of prunning in size of trees}
\label{fig:prunning_effects}
\end{figure}

\section{Stochastic k-medoids}
Once trees are pruned we compute the tree edit distance between every pair of trees. Even after pruning, edit distance algorithms are too slow for large datasets. Since computing a distance represents an atomic operation for a clustering algorithm, we propose a relaxed clustering algorithm that reduces the number of computed distances.



\begin{algorithm}
	\KwData{set of tree graps $\mathcal{T}$}
	\KwResult{vector of cluster assignments $z$}
	\While{not converged}{
		\For{$i \in \{1,...,|\mathcal{T}|\}$}{
			$closest[i] \leftarrow \infty$\;
			\For{$c \in$ centroids}{
				\If{$dist(i,c) \leq closest[i]$}{
					$closest[i] \leftarrow dist(i,c)$\;
					$z[i] \leftarrow z[c]$\;
				}
			}
		}
		\For{$i \in \{1,...,|k|\}$}{
			$\mathcal{C}_i \leftarrow$ random subpopulation of cluster $i$\;
			$mincost \leftarrow \infty$\;
			\For{$c \in \mathcal{C}_i$}{
				$cost \leftarrow 0$\;
				$\mathcal{P}_i \leftarrow$ random subpopulation of cluster $i$\;
				\For{$p \in \mathcal{P}_i$}{
				$cost \leftarrow cost + dist(p,c)$\;
				}
				\If{$cost < mincost$}{
					$mincost \leftarrow cost$\;
					$centroids[i] \leftarrow c$\;
				}
			}
		}
	}
\caption{Stochastic $k$-medoids}
\end{algorithm}


% Triangulations
%%%% http://www.frahling.de/Gereon_Frahling/Publications_files/A%20fast%20k-means%20implementation%20using%20Coresets%20(Frahling,%20Sohler).pdf

% Coresets
%%%% http://cseweb.ucsd.edu/~elkan/kmeansicml03.pdf

Clustering methods that require to measure the distance between every pair of items are unfeasible for large datasets. We propose a modification of k-medoids where centroids are re-assigned by considering only subpopulations of each cluster:

\begin{itemize}
 \item new centroids are chosen from a random subset of candidates.
 \item cost of a centroid is the sum of distances between the centroid and a random subset of candidates
\end{itemize}
 at each iteration, the new centroid of a cluster is searched among a set of candidates picked at random among the current members of that cluster.

In $k-$means, in order to re-assign a centroid at every iteration we have to compute $n^{2}$ distances where $n$ is the number of members in that cluster. Within our algorithm, we only compute $|\mathcal{C}||\mathcal{P}|$ where $\mathcal{C}$ is the subset of centroid candidates and  $\mathcal{P}$ is the subset of cluster members used to estimate the cost of each candidate.

This introduction of randomness breaks the monotonicity of the algorithm. There is not guarantee of convergence to a local minimum. A small size of subsamples makes the algorithm faster but less accurate. Convergence can be brought back to the algorithm by gradually incrementing the size of the population until all population is covered; the last iterations would perform a traditional $k$-means. If the algorithm does not converge, we may select the best solution so far.


\section{Experiments}
\subsection{Dataset}
We selected 100 discussion threads randomly picked from \textit{www.boards.ie}.

(experimentations en train d'éxécution)
%\section{Conclusions}
\end{document}

