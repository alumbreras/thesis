\documentclass[../main/main.tex]{subfiles}
\begin{document}


\chapter{A new role analysis}
In this chapter, I provide a new role definition which main goal is to be used for predictions. The sections go from more intuitive to more mathematical. Finally, I briefly discuss some of the predictive tasks that can be performed with our definition.

\section{Formalization}
In this section I formalize the concepts I will deal with later. 

The online \textbf{community} is represented by a graph $G$. The representation passes by different states $G(t_0)... G(t_N)$ or  $V(t_0)... V(t_N)$  whose transitions are made by by individuals behaviors. An individual behavior is an individual creating a new link into $G$.

A \textbf{context} is a local area of the graph. The context of a behavior is what the individual sees before acting; The context is important because it conditions the behavior: an individual can behave differently when in different contexts. Contexts can be represented as subgraphs or as vectors of attributes $V$. When individuals perform their actions, they actually modify contexts.

What nodes and edges represent in the graph $G$, or what the different dimensions of the vector $V$ are, the depends on what aspects of the community and its dynamics we want to emphasize.



\section{Role definition}

% Motivation and intuition
For structuralists, individual roles are mostly given by the social structure. For interactionists, individual roles are mostly emerged from the individuals and their interactions. Whatever the best model is, and without contradicting any of the sociological schools, we can say that, wherever it comes from, a role is a archetype of behavior: individuals behave according to their role.

Now we are ready for our first definition:

DEFINITION

\marginnote{basic definition}Let $G$ be a graph representing an online community. We say that two users hold the same \textbf{active role} if they tend to behave similarly.


Under this definition we could perform, for instance, a triad census and say that two users hold the same role if they tend to participate in the same kind of triads.

Mathematically we can say that the behaviors of users within the same role are described by a same $b_r$.
\begin{equation}
b_i = b_r + \epsilon_i
\end{equation}
\todo{this definition is useful to cluster users by life evolution}
where $b_r$ is the behavior expected as a individual that holds a role $r$, $\epsilon$ represents all the individual factors, and $b_i$ is the observed behavior of user $i$.

However, this definition is useless in terms of making prediction, since we do not model when a user is going to participate in a triad. In other words, $b_r$ is not a function of anything, it is just a description a posteriori.

To incorporate predictive power, we introduce the concept of context in a new definition:

definition

\marginnote{context-aware\\ definition}Let $G$ be a graph representing an online community. Two users hold the same \textbf{active role} if they tend to behave similarly in similar contexts.


Mathematically, we turned $b_r$ into a function of the context,  $f_r(c)$:
\begin{equation}
y(u, c) = f_{r_u}(c) + \epsilon
\end{equation}

Under this definition, however, we would treat two users in the same role as they they were exactly the same person. However, even in the same role, the behavior might depend on personal attributes.

To consider personal attributes (e.g.: characteristics or history) we introduce a third definition:

definition

\marginnote{context-attributes-aware \\definition}
Let $G$ be a graph representing an online community. Two users hold the same \textbf{active role} if, having similar attributes, they tend to behave similarly in similar contexts.

 
And mathematically:
\begin{equation}
y(u,c) = f_{r_u}(c,a_u) + \epsilon
\end{equation}


Sometimes it is useful to look at the reactions after a user's contribution. A lamer, for instance, though structurally shows a regular behavior, will tend to generate long cascades of discussions. Thus, we propose a last definition:

DEFINITION

\marginnote{reaction-based definition}Let $G$ be a graph representing an online community. We say that two users hold the same \textbf{reactive role} if, after acting in s similar way on similar contexts, they tend to unchain similar reactions from other actors.



The mathematical expression is also a bit different:
\begin{equation}
g_{t+1} = f_r(\mathbf{c},\mathbf{b_u}) + \epsilon
\end{equation}

where $g_{t+1}$ is the state of the context after all the reactions happened.

\marginnote{biological analogy} We can think of a role as the biological function of the individual in the $G$ organism. Individuals shape and reshape the community like genes, or proteins, shape living organisms. And different individuals shape and re-shape in different ways, like different proteins shape and re-shape in different ways too. The hypothesis underlying this thesis is that social roles, like protein functions, allow us to better understand, and predict, the dynamics of a online community.


\section{Role-based predictions}

The strength of these definitions is that (a) it allow to group users by behavioral similarity and (b) they are prediction-centered. Though (a) is common in every role analysis (b) is seldom included.

In this section I propose a set of tasks that can be done based on the given definitions of role.

In the next chapters we study the following behaviors:
\begin{itemize}
\item Given a thread (context) a user will reply to the root or not?
\item Given a thread and a user post (context) what kind of cascade will this post generate?
\item Given a set of users with given roles, what will be the length of the thread?
\end{itemize}

\subsection{Predicting successful threads}

%The task is to predict the success of a discussion. Success can be measured, for instance, by number of posts or by number of different users.

%To predict the length of a thread some authors propose to look at the initial dynamics of the thread. For instance, the initial speed at which comments are made. Other authors propose looking at users history; for instance, the ability to start threads that become a success.

Team Role Theory \citep{Meredith} states that there are a set of roles that are necessary for a team to succeed in a project. Rather than a team, for instance, of leaders, Belbin showed that the key for success was a good balance of roles. Discussion forums can be seen as team projects and therefore examined through the lens of Team Role Theory. The members of the team are the active participants in the discussion; the common goal is to create a successful discussion. A successful discussion is, for instance, one with a lot of comments or one where many different users participated.

Let us formalize the problem. Let $\mathcal{T}$ be the set of all discussion threads $\mathcal{T}_1$,...,$\mathcal{T}_T$ in the corpus. Let $L_i$ be the number of posts in $\mathcal{T}_i$. Let $u_i$ be the set of participants in $\mathcal{T}_i$. Let $z$ be a vector containing the role of every user in the corpus. \marginnote{problem statement} Let $z^{(i)}$ be the roles of participants $\mathcal{T}_i$. Let $c^{(i)}$ be a vector containing the proportion of roles in the $z^{(i)}$ (role census). Imagine we assume there are only three possible roles $a, b, c$. Assuming that the number of threads is a linear function of the role census, that is:

\begin{equation}
\marginnote{model}
y^{(i)} = w_0 + w_1 c_a^{(i)} + w_2 c_b^{(i)} + w_3 c_c^{(i)} + \epsilon
\label{basic_belbin}
\end{equation}
find the weights $w$ and the role assignments $z$. Note that in a simple linear regression we only have to find $w$ while $c$ would be known.

\subsubsection{Non-linear model}
Equation \ref{basic_belbin} considers independence between roles. According to this, given the role with a bigger coefficient, the more users within this role, the more successful the discussion will be. To allow interdependences between roles we introduce a set of $N$ functions $\phi_i$. Every function $\phi$ performs a different combination of the original vector $c^{(i)}$: 
\begin{equation}
y^{(i)} = w_0 + w_1 \phi_1(c^{(i)}) + w_2 \phi_2(c^{(i)}) + \gamma \phi_3(c^{(i)}) + ...+ \epsilon
\label{interdependence_belbin}
\end{equation}
or more succinctly:
\begin{equation}
y^{(i)} = \phi(c^{(i)})^T w + \epsilon
\label{interdependence_belbin}
\end{equation}
where $\phi$ maps the original input vector $n^(i)$ into an $N$ dimensional vector which represents a new  feature space.

Let $\Phi$ be a matrix with the outputs of $\phi(c^{(1)})...\phi(c^{(M)})$ put along the columns. We can now treat all the instances together:
\begin{equation}
y = \Phi^T w + \epsilon
\end{equation}

Similarly to the linear model, we would like to find $w$ and $z$, as well as a good $\Phi$. 

\textit{Hint:} This model could be solved through a Gaussian Process framework.

%Now let us solve this system of equations in probabilistic terms. Let us consider that $\epsilon$ is a gaussian noise. Then the equation is equivalent to:
%\begin{equation}
%y = \Phi^T w
%\end{equation}
%where:
%\begin{equation}
%w \sim \mathcal{N}(0, \Sigma)
%\end{equation}

%Given a new role census $\mathbf{n_*}$ we want to know the probability distribution over the number of expected posts:
%\begin{equation}
%p(y_* | n_*, N, y )
%\end{equation}

%which is:
%\begin{equation}
%p(y_* | n_*, N, y )
%=
%\mathcal{N}(\frac{1}{\sigma_n^2}\phi(x_*)^T A^{-1} \Phi y, \phi(x_*)^T %A^{-1}\phi(x_*)) 
%\end{equation}

%The equation can be rewritten to:
%\begin{align}
%p(y_* | n_*, N, y )
%=
%\mathcal{N}(
%&\ \phi(x_*)^T \Sigma_p \Phi(K + \sigma_n^2 I)y,\\
%&\ \phi(x_*)^T\Sigma_p \phi(x_*) - \phi(x_*) \Sigma_p \Phi(K + \sigma_n^2 %I)^{-1} \Phi^T \Sigma_p \phi_*) 
%\end{align}

%What we just described is, in fact, a Gaussian Process model. In the next %section we will reformulate it to the standard view of Gaussian Processes

\subsection{Predicting users' replies}
It is in interesting to know how a given user reacts to a context. For instance, we could try to predict to what comment a user is going to reply, given a set of comments to chose from.


Let $\mathcal{T}$ be the set of all discussion threads $\mathcal{T}_1$,...,$\mathcal{T}_T$ in the corpus. We are interested in the first post $p_0$. Assuming that a user enters into a thread, we model the probability of answering to $p_0$ (and not to any other post in the thread) as:
\begin{equation}
p(y=1) = Bernouilli(\phi(x^T w_c))
\end{equation}
where $x$ is a vector of attributes that contains information on the user and the context (e.g.: number of posts in the thread), $w_c$ are the weights for users within the role $c$ and  $\phi$ is a function (e.g.: probit or a logit) that squashes the input into a [0,1] range so that it can be interpreted as a probability.

The question is: what attributes should we chose to make good predictions, and what are the values of $z$ (role assignments) and $w_c$ (parameters for every role)?

\subsection{Predicting reactions to a post}
Sometimes it is useful to look at the reactions after a user's contribution. A lamer, for instance, though structurally shows a regular behavior, will tend to generate long cascades of discussions. 


Let $\mathcal{R}$ be the set of reaction types to a comment. Types can be manual definition such as "long cascade" (more than N comments with no branches), "short cascade", "no response" or "tree". They can also be a set of trees where any real tree can be hashed to. Let $g_{t}$ be the local tree in which a user made a comment. Let $g_{t+1}$ be local tree that has that comment as root. Assuming that:

\begin{equation}
g_{t+1} = f_r(\mathbf{c}) + \epsilon
\end{equation}

The question is: what is a good function for $f_r$, what are its parameters for every role, and what is the role assignment $z$ for every user?

\bibliographystyle{spbasic} 
\bibliography{../bibliography}
\end{document}