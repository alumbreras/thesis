\documentclass[]{article}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{geometry}
\usepackage[export]{adjustbox}

\usepackage{tikz} % graphics,
\usetikzlibrary{fit,positioning} % tikz elements positioning
\usepackage{soul} % annotations

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

%opening
\title{Variational Inferences}
\author{Alberto Lumbreras}

\begin{document}

\maketitle

\begin{abstract}

\end{abstract}

\section{Clustering based on users behaviors shifts}
\subsection{Model specification}

\begin{align}
\pi &\sim \text{Dirichlet}(\alpha)\\
\boldsymbol{\theta} &\sim \text{Dirichlet}(\gamma)\\
\mathbf{r}_u | \boldsymbol{\theta} &\sim \text{Multinomial}(\boldsymbol{\theta})\\
\mathbf{z}_{up} | \mathbf{r}_u & \sim \text{Multinomial}(\boldsymbol{\pi}_{r_u})\\
\boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s &\sim \mathcal{NW}(\mathbf{m}_0, \beta_0, \upsilon_0, \boldmath{W}_0)\\
\mathbf{y}_{up} | \mathbf{z}_{up}, \boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s &\sim \mathcal{N}(\boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s)
\end{align}

The mixing coefficients of the behaviors style $\pi$ are:
\begin{align}
	p(\boldsymbol{\Pi}) 
	=
	\prod_{k=1}^K p(\boldsymbol{\pi}_k) = \prod_{k=1}^K \text{Dir}(\boldsymbol{\pi}_k | \alpha) = \prod_{k=1}^K C(\alpha)  \prod_{s=1}^S \pi_{ks}^{\alpha_0-1}
\end{align} 
The mixing coefficients of the roles are:
\begin{align}
p(\boldsymbol{\theta}) = \text{Dir}(\boldsymbol{\theta} | \gamma) = C(\gamma) \prod_{k=1}^K \theta_k^{\gamma_0-1}
\end{align} 
The conditional probability of the roles assignments given the roles mixing coefficients are:
\begin{align}
	p(\mathbf{R} | \boldsymbol{\theta}) = \prod_{u=1}^U \prod_{k=1}^K \theta_k^{r_{uk}}
\end{align}	
The style chosen for a given participation of a user has the conditional probability:
\begin{align}
p(\mathbf{Z} | \mathbf{R}, \boldsymbol{\Pi}) = 
\prod_{u=1}^{U}\prod_{p=1}^{P_u} 
\prod_{k=1}^{K} \left( C(\alpha) \prod_{s=1}^S \pi_{ks}^{z_{ups}} \right)^{r_{uk}}
\end{align}	
The conditional probability of the means and precisions of every style component are:
\begin{align}
p(\boldsymbol{\mu}, \boldsymbol{\Lambda}) 
= 
p(\boldsymbol{\mu} | \boldsymbol{\Lambda})p(\boldsymbol{\Lambda}) 
= 
\prod_{s=1}^S \mathcal{N}(\mathbf{m}_0, (\beta_0\boldmath{\Lambda}_s)^{-1} )\mathcal{W}(\boldmath{W}_0, \upsilon_0)
\end{align}	
The observed participations have a conditional probability:
\begin{align}
p(\mathbf{Y} | \mathbf{Z}, \boldsymbol{\mu}, \boldsymbol{\Lambda}) = \prod_{u=1}^U \prod_{p=1}^{P_u} \prod_{s=1}^S \mathcal{N}(y_{up} | \boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s))^{z_{ups}}
\end{align}	

	
\begin{figure}	
\center
 \scalebox{1}{
\begin{tikzpicture}
\tikzstyle{main}=[circle, minimum size = 12mm, thick, draw =black!80, node distance = 8mm]
\tikzstyle{connect}=[-latex, thick]
\tikzstyle{box}=[rectangle, draw=black!100]

\node (alpha) {$\alpha$};


%% Styles selection
  \node [main](pi) [right=of alpha] {$\boldsymbol{\pi}$}; 

  %% Roles selection
   \node [main](zup) [right=1.2cm of pi] {$\mathbf{z}_{up}$};
   \node[main] (ru) [above=of zup] {$\mathbf{r}_u$}; 
   \node[main] (theta) [above=1cm of ru] {$\theta$};
   \node (gamma)  [above=0.5cm of theta] {$\gamma$};

%% Participations
  \node[main, fill = black!10] (y) [right=0.6cm of zup] {$\boldsymbol{y}_u$};   

%% Distribution of every style   
  \node [main](beta) [right=1.2cm of y] {$\beta$};   


  \path 
	   %% DP
           (alpha) edge [connect] (pi)
           (pi) edge [connect] (zup)
           (zup) edge [connect] (y)
           (ru) edge [connect] (zup)
          
			(gamma) edge [connect] (theta)
			(theta) edge [connect] (ru)

			(beta) edge [connect] (y);

  % Participations plate  
  \node[rectangle, inner sep=0mm, fit= (zup) (y), label=below right:$P_u$, yshift=1mm, xshift=7mm] {};
  \node[rectangle, inner sep=4mm,draw=black!100, fit= (zup) (y)] {};  

  % Users plate   
  \node[rectangle, inner sep=1mm, fit= (ru) (y), label=above left:U, yshift=-43mm, xshift=41mm] {};
  \node[rectangle, inner sep=9mm, draw=black!100, yshift=-2mm, fit= (ru) (y)] {};  
\end{tikzpicture}
}
\caption{Graphical model}
\label{fig:general}
\end{figure}

\subsection{Variational distribution}
In order to derive the variational approximation of the posterior, we write down the joint distribution of the variables, which is given by
\begin{align}
p(\mathbf{Y}, \mathbf{Z}, \mathbf{R}, \boldsymbol{\Pi}, \boldsymbol{\theta}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
=
p(\mathbf{Y} | \mathbf{Z}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
p(\mathbf{Z} | \mathbf{R}, \boldsymbol{\Pi})
p(\mathbf{R} | \boldsymbol{\theta})
p(\boldsymbol{\Pi})
p(\boldsymbol{\theta})
p(\boldsymbol{\mu} | \boldsymbol{\Lambda})p(\boldsymbol{\Lambda})
\end{align}
We now consider a variational distribution which factorizes as:
\begin{align}
q(\mathbf{Z}, \mathbf{R}, \boldsymbol{\Pi}, \boldsymbol{\theta}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
=
q(\mathbf{R}) q(\mathbf{Z}, \boldsymbol{\Pi}, \boldsymbol{\theta}, \boldsymbol{\mu}, \boldsymbol{\Lambda}) 
\end{align}

\subsection*{$q(\mathbf{R})$}
We take the expectancy of the joint distribution with respect to all the other variables and push into the constant factor anything that does not depend on $\mathbf{R}$:

\begin{align}
	\ln q(\mathbf{R}) &= \mathbb{E}_{\mathbf{Z}, \boldsymbol{\Pi}, \boldsymbol{\theta}, \boldsymbol{\mu}, \boldsymbol{\Lambda}}
	[\ln p(\mathbf{Y} | \mathbf{Z}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
	p(\mathbf{Z} | \mathbf{R}, \boldsymbol{\Pi})
	p(\mathbf{R} | \boldsymbol{\theta})
	p(\boldsymbol{\Pi})
	p(\boldsymbol{\theta})
	p(\boldsymbol{\mu} | \boldsymbol{\Lambda})p(\boldsymbol{\Lambda})]\\
	&=
\mathbb{E}_{\mathbf{Z}, \boldsymbol{\Pi}}
[\ln p(\mathbf{Z} | \mathbf{R}, \boldsymbol{\Pi})] 
+
\mathbb{E}_{\boldsymbol{\theta}}
[\ln p(\mathbf{R} | \boldsymbol{\theta})]\\
&=
\sum_{u=1}^{U}\sum_{p=1}^{P_u} 
\sum_{k=1}^{K} {r_{uk}} \left( \ln C(\alpha) + \sum_{s=1}^S \mathbb{E}[{z_{ups}}] \mathbb{E}[\ln \pi_{ks}]  \right)
+
\sum_{u=1}^U \sum_{k=1}^K r_{uk} \mathbb{E}[\ln \theta_k]\\
&=
\sum_{u=1}^{U}
\sum_{k=1}^{K} 
{r_{uk}} 
\sum_{p=1}^{P_u} 
\left( \ln C(\alpha) + \sum_{s=1}^S \mathbb{E}[{z_{ups}}] \mathbb{E}[\ln \pi_{ks}] \right)
+
\sum_{u=1}^U \sum_{k=1}^K r_{uk} \mathbb{E}[\ln \theta_k]\\
&=
\sum_{u=1}^{U}
\sum_{k=1}^{K} 
{r_{uk}} 
\ln \rho_{uk}
\end{align}
Taking the exponent, we get a Multinomial distribution
\begin{align}
q(\mathbf{R}) = \prod_{u=1}^{U}\text{Multinomial}(r_u | \boldsymbol{\rho}_u)
\end{align}
where $\boldsymbol{\rho}_u$ has components  $\rho_{uk}$ given by
\begin{align}
	\ln \rho_{uk} = 
	\sum_{p=1}^{P_u} 
	\sum_{s=1}^S \mathbb{E}[{z_{ups}}] \mathbb{E}[\ln \pi_{ks}]
	+
	\mathbb{E}[\ln \theta_k] + \text{const}
\end{align}
We assume $\boldsymbol{\rho}_u$ is properly normalized. Then the expectation is:
\begin{align}
\mathbb{E}[r_{uk}] = \rho_{uk}
\end{align}

\subsection*{$q(\mathbf{Z}, \boldsymbol{\Pi}, \boldsymbol{\theta}, \boldsymbol{\mu}, \boldsymbol{\Lambda}) $ (not feasible)}
We repeat the process:
\begin{align}
	\ln q(\mathbf{Z}, \boldsymbol{\Pi}, \boldsymbol{\theta}, \boldsymbol{\mu}, \boldsymbol{\Lambda}) 
	=
	\mathbb{E}_{\mathbf{R}}
		[\ln p(\mathbf{Y} | \mathbf{Z}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
		p(\mathbf{Z} | \mathbf{R}, \boldsymbol{\Pi})
		p(\mathbf{R} | \boldsymbol{\theta})
		p(\boldsymbol{\Pi})
		p(\boldsymbol{\theta})
		p(\boldsymbol{\mu} | \boldsymbol{\Lambda})p(\boldsymbol{\Lambda})]	
\end{align}
This let us with the full joint distribution, which is analytically intractable since it does not correspond to any familiar distribution. We will try another factorization.

\subsection*{$q(\mathbf{Z})$}
\begin{align}
\ln q(\mathbf{Z}) 
&=
\mathbb{E}_{\mathbf{R},\boldsymbol{\Pi}, \boldsymbol{\theta}, \boldsymbol{\mu}, \boldsymbol{\Lambda}}
[\ln p(\mathbf{Y} | \mathbf{Z}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
p(\mathbf{Z} | \mathbf{R}, \boldsymbol{\Pi})
p(\mathbf{R} | \boldsymbol{\theta})
p(\boldsymbol{\Pi})
p(\boldsymbol{\theta})
p(\boldsymbol{\mu} | \boldsymbol{\Lambda})p(\boldsymbol{\Lambda})]\\
&=
\mathbb{E}_{\boldsymbol{\mu}, \boldsymbol{\Lambda}}
[\ln p(\mathbf{Y} | \mathbf{Z}, \boldsymbol{\mu}, \boldsymbol{\Lambda})]
+
\mathbb{E}_{\mathbf{R}, \boldsymbol{\Pi}}
[\ln p(\mathbf{Z} | \mathbf{R}, \boldsymbol{\Pi})]\\
&=
\sum_{u=1}^U \sum_{p=1}^{P_u} \sum_{s=1}^S 
z_{ups} \ln \mathcal{N}(y_{up} | \boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s))^
+
\sum_{u=1}^{U}\sum_{p=1}^{P_u} 
\sum_{k=1}^{K} r_{uk} \ln \left( C(\alpha) \prod_{s=1}^S \pi_{ks}^{z_{ups}} \right)\\
&=
\sum_{u=1}^U \sum_{p=1}^{P_u} \sum_{s=1}^S 
z_{ups} \ln \mathcal{N}(y_{up} | \boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s))^
+
\sum_{u=1}^{U}\sum_{p=1}^{P_u} 
\sum_{k=1}^{K} r_{uk} \sum_{s=1}^S z_{ups} \ln \pi_{ks}\\
&=
\sum_{u=1}^U \sum_{p=1}^{P_u} \sum_{s=1}^S 
z_{ups} \ln \mathcal{N}(y_{up} | \boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s))^
+
\sum_{u=1}^{U}\sum_{p=1}^{P_u} 
\sum_{k=1}^{K} \sum_{s=1}^S r_{uk} z_{ups} \ln \pi_{ks}\\
&=
\sum_{u=1}^U \sum_{p=1}^{P_u}\sum_{s=1}^S
\left( 
z_{ups} \ln \mathcal{N}(y_{up} | \boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s))^
+
 \sum_{k=1}^{K} z_{ups} r_{uk} \ln \pi_{ks} \right)\\
 &=
 \sum_{u=1}^U \sum_{p=1}^{P_u}\sum_{s=1}^S
 z_{ups} \ln \rho_{ups}
\end{align}
where:
\begin{align}
\ln \rho_{ups} =  
 \frac{1}{2}  \mathbb{E}[\ln \boldsymbol{\Lambda}_s]-\frac{D}{2} \ln 2\pi - \frac{1}{2}
 \mathbb{E}_{\boldsymbol{\mu}, \boldsymbol{\Lambda}}\left[(y_{up} - \boldsymbol{\mu}_s)^T \boldsymbol{\Lambda}_s (y_{up} - \boldsymbol{\mu}_s)
 \ln (y_{up} | \boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s))\right]
 +
 \sum_{k=1}^{K} \mathbb{E}[r_{uk}] \mathbb{E}[\ln \pi_{ks}]
\end{align}

Taking the exponential as usual, we get a Multinomial distribution:
\begin{align}
	q(z_{up}) = \prod_{s=1}^{S} \rho_{ups}^{z_{ups}}
\end{align}
And therefore:
\begin{align}
	\mathbb{E}[z_{ups}] = \rho_{ups}
\end{align}
\subsection*{$q(\boldsymbol{\theta}, \boldsymbol{\Pi}, \boldsymbol{\mu}, \boldsymbol{\Lambda})$}
\begin{align}
\ln q(\boldsymbol{\theta}, \boldsymbol{\Pi}, \boldsymbol{\mu}, \boldsymbol{\Lambda}) &=
\mathbb{E}_{\mathbf{R},\mathbf{Z}}
[\ln p(\mathbf{Y} | \mathbf{Z}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
p(\mathbf{Z} | \mathbf{R}, \boldsymbol{\Pi})
p(\mathbf{R} | \boldsymbol{\theta})
p(\boldsymbol{\Pi})
p(\boldsymbol{\theta})
p(\boldsymbol{\mu} | \boldsymbol{\Lambda})p(\boldsymbol{\Lambda})]
\end{align}
We find the following induced factors:
\begin{align}
\ln q(\boldsymbol{\Pi}) = \mathbb{E}_{\mathbf{R},\mathbf{Z}} 
[\ln p(\mathbf{Z} | \mathbf{R}, \boldsymbol{\Pi})
p(\boldsymbol{\Pi})]
\end{align}
\begin{align}
\ln q(\boldsymbol{\theta}) = \mathbb{E}_{\mathbf{R}} 
[\ln p(\mathbf{R} | \boldsymbol{\theta})
p(\boldsymbol{\theta}))]
\end{align}
\begin{align}
\ln q(\boldsymbol{\mu}, \boldsymbol{\Lambda}) 
= 
\mathbb{E}_{\mathbf{Z}} 
[\ln p(\mathbf{Y} | \mathbf{Z}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
p(\boldsymbol{\mu} | \boldsymbol{\Lambda})p(\boldsymbol{\Lambda})]
\end{align}

\subsection*{$q(\boldsymbol{\Pi})$}
\begin{align}
\ln q(\boldsymbol{\Pi}) &= \mathbb{E}_{\mathbf{R},\mathbf{Z}} 
[\ln p(\mathbf{Z} | \mathbf{R}, \boldsymbol{\Pi})
p(\boldsymbol{\Pi})]\\
&=
\sum_{u=1}^{U}\sum_{p=1}^{P_u} 
\sum_{k=1}^{K} \mathbb{E}[r_{uk}] \left( \ln  C(\alpha) +\sum_{s=1}^S \mathbb{E}[z_{ups}] \ln \pi_{ks} \right)
+
\sum_{k=1}^K 
\left(
\ln C(\alpha)  + (\alpha_0-1) \sum_{s=1}^S \ln \pi_{ks}\right)
+ \text{const}\\
&=
\sum_{u=1}^{U}\sum_{p=1}^{P_u} 
\sum_{k=1}^{K} \mathbb{E}[r_{uk}] \left(\sum_{s=1}^S \mathbb{E}[z_{ups}] \ln \pi_{ks} \right)
+
\sum_{k=1}^K \left((\alpha_0-1) \sum_{s=1}^S \ln \pi_{ks}\right)
+ \text{const}\\
&=
\sum_{k=1}^{K} 
\left(\sum_{s=1}^S \sum_{u=1}^{U}\sum_{p=1}^{P_u} \mathbb{E}[r_{uk}]  \mathbb{E}[z_{ups}] \ln \pi_{ks} \right)
+
\sum_{k=1}^K \left((\alpha_0-1) \sum_{s=1}^S \ln \pi_{ks}\right)
+ \text{const}\\
&=
\sum_{k=1}^{K} 
\sum_{s=1}^S 
\left(
\sum_{u=1}^{U}\sum_{p=1}^{P_u} \mathbb{E}[r_{uk}]  \mathbb{E}[z_{ups}] \ln \pi_{ks}
+
(\alpha_0-1) \ln \pi_{ks}\right)
+ \text{const}
\end{align}
Taking the exponent, we get $k$ Dirichlet distributions:
\begin{align}
q(\boldsymbol{\Pi}) = 
\prod_{k=1}^{K} \text{Dir}(\pi_{k} | \boldsymbol{\alpha}_k) 
\end{align}
where $\boldsymbol{\alpha}_k$ has components $\alpha_{ks}$ given by:
\begin{align}
\alpha_{ks} = \alpha_0 + \sum_{u=1}^{U}\sum_{p=1}^{P_u} \mathbb{E}[r_{uk}]  \mathbb{E}[z_{ups}]
\end{align}

From the standard properties of the Dirichlet distribution, we have:
\begin{align}
	\ln \tilde{\pi}_{ks} = \mathbb{E}[\ln \pi_{ks}] = \psi(\alpha_{ks}) - \psi(\hat{\alpha}_k)
\end{align}

\subsection*{$q(\boldsymbol{\theta})$}

\begin{align}
\ln q(\boldsymbol{\theta}) &= \mathbb{E}_{\mathbf{R}} 
[\ln p(\mathbf{R} | \boldsymbol{\theta})
p(\boldsymbol{\theta}))]\\
&=
\sum_{u=1}^U \sum_{k=1}^K \mathbb{E}[r_{uk}] \ln \theta_k
+
\ln C(\gamma) + (\gamma_0-1) \sum_{k=1}^K \ln \theta_k
+ \text{const}\\
&=
\sum_{k=1}^K 
\left(
\sum_{u=1}^U \mathbb{E}[r_{uk}] 
+
(\gamma_0-1) \right)\ln \theta_k + \text{const}
\end{align}
Taking the exponent we get a Dirichlet distribution:
\begin{align}
q(\boldsymbol{\theta}) = \text{Dir}(\boldsymbol{\theta} | \boldsymbol{\gamma})
\end{align}
where $\boldsymbol{\gamma}$ has components $\gamma_k$ given by:
\begin{align}
\gamma_k = \gamma_0 + \sum_{u=1}^U \mathbb{E}[r_{uk}] 
\end{align}
From the standard properties of the Dirichlet distribution, we have:
\begin{align}
\mathbb{E}[\ln \theta_k] = \psi(\gamma_k) - \psi(\hat{\gamma})
\end{align}

\subsection*{$q(\boldsymbol{\mu}, \boldsymbol{\Lambda})$}
\begin{align}
\ln q(\boldsymbol{\mu}, \boldsymbol{\Lambda}) 
&= 
\mathbb{E}_{\mathbf{Z}} 
[\ln p(\mathbf{Y} | \mathbf{Z}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
p(\boldsymbol{\mu} | \boldsymbol{\Lambda})p(\boldsymbol{\Lambda})]\\
&=
\sum_{u=1}^U \sum_{p=1}^{P_u} \sum_{s=1}^S \mathbb{E}[z_{ups}] \ln \mathcal{N}(y_{up} | \boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s))
+
\sum_{s=1}^S \mathcal{N}(\mathbf{m}_0, (\beta_0\boldmath{\Lambda}_s)^{-1} )\mathcal{W}(\boldmath{W}_0, \upsilon_0)\\
&=
\sum_{s=1}^S \left( \sum_{u=1}^U \sum_{p=1}^{P_u}  \mathbb{E}[z_{ups}] \ln \mathcal{N}(y_{up} | \boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s))
+
\mathcal{N}(\mathbf{m}_0, (\beta_0\boldmath{\Lambda}_s)^{-1} )\mathcal{W}(\boldmath{W}_0, \upsilon_0) \right)
\end{align}

We derivate each individual $q(\boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s)$:
\begin{align}
\ln q(\boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s) 
&=
\sum_{u=1}^U \sum_{p=1}^{P_u}  \mathbb{E}[z_{ups}] \ln \mathcal{N}(y_{up} | \boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s))
+
\mathcal{N}(\mathbf{m}_0, (\beta_0\boldmath{\Lambda}_s)^{-1} )\mathcal{W}(\boldmath{W}_0, \upsilon_0) 
\end{align}
we get to anorther Normal-Wishart:
\begin{align}
q(\boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s) = \mathcal{N}(\boldsymbol{\mu}_s | \mathbf{m}_s, (\beta_s\boldsymbol{\Lambda}_s)^{-1})\mathcal{W}(\boldsymbol{\Lambda}_s | \mathbf{W}_s, \upsilon_s) 
\end{align}
where:
\begin{align}
\beta_s &= \beta_0 + N_s\\
\mathbf{m}_k &= \frac{1}{\beta}_s(\beta_0\mathbf{m}_0 + N_s \overline{y}_s)\\
\mathbf{W}_k^{-1} &= \mathbf{W}_0^{-1} + N_s\mathbf{S}_s + \frac{\mathbf{\beta}_0 N_s}{\beta_0 + N_k}(\overline{y}_s - \mathbf{m}_0)(\overline{y}_s - \mathbf{m}_0)^T\\
\upsilon_s &= \upsilon_0 + N_s
\end{align}

\section{Dual-view clustering}
\begin{figure}
	\center
	\scalebox{1}{
		\begin{tikzpicture}
		\tikzstyle{main}=[circle, minimum size = 12mm, thick, draw =black!80, node distance = 8mm]
		\tikzstyle{connect}=[-latex, thick]
		\tikzstyle{box}=[rectangle, draw=black!100]
		
		%% Dirichlet
		\node (alpha) {$\alpha$}; 
		\node [main](pi) [above=0.5cm of alpha] {$\boldsymbol{\pi}$}; 
		\node [main](z) [above=of pi] {$\mathbf{z_u}$};
		
		%% Clustering
		\node[main, fill = black!10] (a_u) [left=0.5cm of z] {$\mathbf{a}_u$}; 
		\node[main] (theta_ar) [above=1cm of a_u] {$\boldsymbol{\theta_k^{\text{(a)}}}$};   
		
		%% Prediction
		\node[main] (f_u) [right=0.5cm of z] {$\mathbf{f_u}$}; 
		\node[main] (theta_fr) [above=1cm of f_u] {$\boldsymbol{\theta}_k^{\text{(f)}}$};   
		\node[main, fill = black!10] (y) [right=0.6cm of f_u] {$\boldsymbol{y}_u$};   
		
		\node [main](G_a0) [above=1cm of theta_ar] {$G_0^{\text{(a)}}$};   
		\node [main](G_f0) [above=1cm of theta_fr] {$G_0^{\text{(f)}}$};   
		
		\path 
		%% DP
		(alpha) edge [connect] (pi)
		(pi) edge [connect] (z)
		(z) edge [connect] (f_u)
		(z) edge [connect] (a_u)
		
		%% Clustering
		(G_a0) edge [connect] (theta_ar)
		(theta_ar) edge [connect] (a_u)
		
		%% Prediction
		(G_f0) edge [connect] (theta_fr)
		(theta_fr) edge [connect] (f_u)
		(f_u) edge [connect] (y);
		
		
		% User draws  
		\node[rectangle, inner sep=0mm, fit= (a_u) (z) (f_u), label=below right:U, yshift=1mm, xshift=34mm] {};
		\node[rectangle, inner sep=3mm,draw=black!100, fit= (z) (f_u) (a_u) (y)] {};  
		
		% Mixture specific parameters   
		\node[rectangle, inner sep=1mm, fit= (theta_ar) (theta_fr), label=above left:K, yshift=-17mm, xshift=34mm] {};
		\node[rectangle, inner sep=3mm, draw=black!100, fit= (theta_fr) (theta_ar)] {};  
		\end{tikzpicture}
	}
	\caption{Graphical model of the generative process. Views are connected through the latent assignments $\mathbf{z}$.}
	\label{fig:general}
\end{figure}

\subsection{Model}

\begin{align}
\pi &\sim \text{Dirichlet}(\alpha)\\
\boldsymbol{\theta} &\sim \text{Dirichlet}(\gamma)\\
\mathbf{r}_u | \boldsymbol{\theta} &\sim \text{Multinomial}(\boldsymbol{\theta})\\
\mathbf{z}_{up} | \mathbf{r}_u & \sim \text{Multinomial}(\boldsymbol{\pi}_{r_u})\\
\boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s &\sim \mathcal{NW}(\mathbf{m}_0, \beta_0, \upsilon_0, \boldmath{W}_0)\\
\mathbf{y}_{up} | \mathbf{z}_{up}, \boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s &\sim \mathcal{N}(\boldsymbol{\mu}_s, \boldsymbol{\Lambda}_s)
\end{align}

\subsection{Variational distribution}
In order to derive the variational approximation of the posterior, we write down the joint distribution of the variables, which is given by
\begin{align}
p(\mathbf{Y}, \mathbf{Z}, \boldsymbol{\Pi}, \boldsymbol{\theta}^{(a)}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
=
p(\mathbf{Y} | \mathbf{Z}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
p(\mathbf{Z} | \mathbf{R}, \boldsymbol{\Pi})
p(\mathbf{R} | \boldsymbol{\theta})
p(\boldsymbol{\Pi})
p(\boldsymbol{\theta})
p(\boldsymbol{\mu} | \boldsymbol{\Lambda})p(\boldsymbol{\Lambda})
\end{align}
We now consider a variational distribution which factorizes as:
\begin{align}
q(\mathbf{Z}, \mathbf{R}, \boldsymbol{\Pi}, \boldsymbol{\theta}, \boldsymbol{\mu}, \boldsymbol{\Lambda})
=
q(\mathbf{R}) q(\mathbf{Z}, \boldsymbol{\Pi}, \boldsymbol{\theta}, \boldsymbol{\mu}, \boldsymbol{\Lambda}) 
\end{align}

%%%%%%%%%%%%%%%
\end{document}
