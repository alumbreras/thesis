\documentclass[11pt]{article}
\usepackage{amssymb}
\usepackage[utf8]{inputenc}
\usepackage[round, colon, authoryear]{natbib} % for harvard citation style
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\DeclareGraphicsExtensions{{.pdf},{.png},{.jpg}}
\graphicspath{ {../../img/report_mai_14/} }

\begin{document}

\title{Technical Report}
\author{Alberto Lumbreras}
\date{\today}
\maketitle

%%% INTRO
\section{Introduction}
Individual behavior is an observable cue resulting from many factors that we may summarize in two types, psychological and social. Amongst the psychological factors there are education, experience, temper, personality or ambitions. Amongst the sociological factors there are the role and position of the individual in a given community, or the situation and context in which the actions of the individuals are performed. All these factors form a network of interdependent factors which is, we could say, impossible to disentangle.

There are many approaches to the study of individual behavior, the choice depending on the questions to be answered. An obvious question is what makes an individual behave in a certain way. Such question looks at the \textbf{causes} of behavior, which makes it maybe the hardest one, and falls into the field of psychologists, neuroscientists, sociologists and the like. A second question, and one often posed in the study of online communities, is what is the \textbf{set of behaviors} that coexist in a community, what these behaviors are like, and what is the \textbf{interrelation between individual and group behavior}. 

Though the later might look simpler than the former, we would like to point out two common problems. First, is is not trivial to define the set of behaviors since deciding what constitutes a part of a behavior is an arbitrary decision made by the researcher. Imagine, for instance, that we want to study the group dynamics of students in a class. Certainly the fact that a student scratches his head more than usual is not relevant to the study and therefore it will not be annotated by the researcher. However, the same scratching should not be neglected in a study of obsessive-compulsive disorders. Online communities face a similar problem; the candidate phenomena can be measures of centrality, activity, text and so on and so forth, and the choices must depend on the question which we want to answer.

Another problem arises when studying individual-group relationships. Left aside emerging collective patterns, some group dynamics are an aggregation of individual dynamics. For instance, the number of posts in a forum will grow if users in this forum write more posts. Thus, if we do not chose carefully the attributes, we can obtain conclusions such as `a community with high activity is composed of highly active individuals". Angeletou et al (ref) fall in such fallacy. They characterize user behaviors in terms of structural attributes such as number of responses per post or average posts per thread. Then, they observe that successful forums (those with high number of posts) are made by individuals with high scores in such attributes. Such a circular reasoning should be avoided by carefully selecting what to observe and inspecting the results.  \footnote{It is not clear in their paper if they characterize individuals by their behavior in that forum or outside other forums; the circular reasoning fallacy would be only in the former case.} 

To avoid these problems, we will pose the problem as a \textbf{prediction task}. If behaviors are meaningful it is because they are regular; that is, if the individual is exposed to the same situation in two different moments, it is likely that her behavior will be similar. We could then model the behavior of every individual hoping that it will have not only descriptive but also predictive power\footnote{Predictive power does not imply descriptive power. This constitutes a major line of criticism to the so-called \textit{black box} models such as Neural Networks.}. Yet, modeling every single individual would lead us as many models as individuals, with no information shared between the models. If we are interested not in specific individuals but in general archetypes, we need to build models that reflect typical behavioral archetypes. Such archetypes is what we will call \textbf{roles}.

\section{Problem formalization}
Operational to this goal, a vague definition of role would be `a set of \textbf{statistically-consistent} behaviors showed by one or many individuals".  We can express this as the following function $f$:

$$
f:A \times C \rightarrow B
$$

where $A$ is the set of attributes of an individual over the past threads, $C$ is the current context and $B$ is the behavior we want to predict, e.g whether a user with such $A$ will react to context $C$.

\subsection{User-based models vs  role-based models: $f_r$ or $f_u$?}

Should we model users or roles? The more natural approach seems to infer a model $f_r$ for every role, and apply this function to predict behaviors of every user within that role. A second alternative is to infer a model $f_u$ for every user, ``personalizing''  the predictions; however,  we would lose the connection to the role level. To get this connection back, one can  group users ``a posteriori"  by similarity of their functions $f_u$, interpreting the resulting clusters as roles.

Since the concept of role is central to our study, I propose to model $f_r$. In the following sections, we discuss how learn the models $f_r$ and how to codify the contexts $C$. The incorporation of users attributes ($A$) is  left for future work. 

\section{$f_r$: classic prediction schemes}

\subsection{Discriminative models}
The question now turns into how to learn $f_r$. A first approach is that of discriminative models. In such models, we first decide the function $f$ with which we will fit our data (e.g.: a polynomial of grade $n$); second, we define a loss function to measure how well $f$ predicts the outputs (e.g.: Mean Squared Errors). If we overfit $f$ to our data, $f$ will have no generalization power, that is, it will memorize the data (and the noise!), but will not learn the real pattern, being unable to predict out of the training dataset. To avoid this, a regularization factor is often introduced such that complex models are discouraged. Then, we find the parameters that minimize the following equation:

$$
J(\theta) = \frac{1}{N}\sum_{i=1}^N L(f(x_i; \theta), y_i) + \lambda C(\theta)
$$

where $L$ is our loss  function and  $C(\theta)$ is a regularization factor (e.g, the $L_2$ norm of the parameters). Optimization methods are often used to find $\hat{\theta}$ when it is hard of impossible to derive such equation analytically. Neural Networks, Support Vector Machines, or Decision Trees are based on this schema of maximizing a loss function.

\subsection{Generative models}
Either if we have some knowledge on the data or if we want a more descriptive model, we can build a generative model of our data and use the likelihood function as the loss function. In this case, the model is called generative and the function to minimize is:

$$
J(\theta) = -\frac{1}{N}\sum \mathcal{L}(y | x)p(x)
$$

Generative models make stronger assumptions than discriminative ones since they account for a more complete explanation on how the data was generated. On the other hand, they allow us to use the full potential of a bayesian framework, that is:

\begin{itemize} 
 \item we can estimate the MLE parameters and the quantify the uncertainty of the estimations.
 \item we can make use of hierarchical and non-parametrical models that allow us to define more expressive models
 \item and we can use prior knowledge as a starting point of our inference.
\end{itemize}

\subsection{Discriminative vs Generative}

Since we are looking for both descriptive and predictive power, it seems natural to chose the generative model framework. Once the choice is made, our next task is to specify the generative process.

Our hypothesis is that users behave according to a latent role, but we do not know neither the role to which every user belongs or the behavior associated to every role.  This can be formalized as a \textbf{mixture models} problem. In mixture models, we consider that samples (behaviors) are drawn from a set of $K$ latent components or clusters (roles), and that every component is associated to a probability distribution with a set of unknown parameters $\theta_{1},...\theta_{K}$. Typically we assume a family of the probability distribution of the components (e.g: three components that draw samples from $\mathcal{N}(\mu_1, \sigma)$, $\mathcal{N}(\mu_2, \sigma)$ and $\mathcal{N}(\mu_3, \sigma)$), and then estimate the parameters, the most likely component to which every individual belongs to and even the number of components.

We can extend this and model the evolution of users assignments to roles and the evolution of roles parameters. The natural framework for this are the Gaussian Processes, which are well known in the literature and a very active topic of research.

\subsection{Some issues on mixture models}

Bayesian inference methods sample the posterior distribution of this parameters. By looking at the sampled posterior we know the Maximum Likelihood Estimator and the variance associated to it (the p-values are here substituted by ``credible intervals", those that include for instance the 90\% of the posterior density.

Though highly flexible, mixture models might pose some issues when when inferring their latent parameters. Unfortunately, some posterior distributions are \textbf{multimodal}, that is, their density function has several regions separated by regions with low density. Though inference methods such as Markov Chain Monte Carlo guarantee that they will eventually explore all the modes of the posterior, traveling from one region to the other takes an impracticable number of steps. Some variations of MCMC have been proposed to tackle this problem, such as split-merge MCMC or tempered algorithms.

%Yet, even if the the sampler explores all the posterior, the problem of \textbf{unidentifiablity} arises. That is, the algorithm can not distinguish between the different modes, giving the same likelihood to different partitions.

\section{$C$: codification of contexts}

What should we consider as a relevant context? Certainly the whole tree provides too much information both relevant and irrelevant. Moreover, considering the whole tree will make every context unique, not allowing the prediction algorithm to generalize from one context to another.

We start considering that in order to predict whether a user will respond to a post the context that matters is the local one. Specifically, we consider a maximum of 6 posts from the post to the root or 3 different users (recall that 6 is the maximum number of edges between three users in a traditional triad census).

How to codify this context?  In the graph at the left side of figure \ref{context_hash}, nodes are posts and edges indicate replies between posts; authors are just an attribute of every node. This is not enough, since all chains would look similar even if they express different patterns (e.g. a chain of a two actors discussing with each others  $A \leftarrow B \leftarrow A \leftarrow B$ will look the same than   $A \leftarrow B \leftarrow C \leftarrow D$). We could express the graph as an authors graph, representing authors as nodes and edges as posts between actors; nonetheless, this type of graph looses the information about the structure of conversations. 

Alternatively we can represent the graph as shown at the right of figure \ref{context_hash}. This representation keeps the information about the structure and the users so that in a conversation between three users, we can distinguish between $A\leftrightarrow B$ and $A \leftarrow B \leftarrow A \leftarrow B \leftarrow A \leftarrow B \leftarrow A$.

\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{context_hash}
\caption{Hash signature of a toy context. The hashing scheme collapses the information about authors so that two conversations involving different authors but that share a same schema are considered equivalent.  Besides, contexts have an artificial limit of three authors or six posts. (a full triad in a classic triad census).}
\label{context_hash}
\end{center}
\end{figure}

%%%%%%%%%%%%%%
%%%%%%%%%%%%%%
\section{Experiments}

%%%
\subsection{Users reactions to contexts}
Consider the first participation of a user in a thread. When we examined the thread, there were already $N$ posts. Which post will she chose? Can we decide, based on the choices of a user, whether she feels some ``attraction" for some specific contexts? We might expect, for instance, that few users will start their participation in a context where two users have been long replying each other, since it might denote a heated discussion. 

Imagine a user that participates in for threads. We write the population of contexts in every thread and the one chosen by the user (his `entry context"):
\begin{align}
T_1: & c_1, \boldsymbol{c_1}, c_2, c_3, c_3, c_3, c_3, c_3, c_3\\
T_2: & c_1, \boldsymbol{c_2}, c_3\\
T_3: & c_5, c_5, \boldsymbol{c_6}, c_7, c_8, c_{15}\\
T_4: & \boldsymbol{c_1}
\end{align}

If the user had no special preferences she would tend to use the most frequent context.
However, in $T_1$, $T_2$ and $T_3$  she did not chose the most frequent context, and in $T_4$ she had no choice. To test the significance of this observations we need to design a significance test.

If we take context $c_1$, the probability of being chosen by the user is:
\begin{align}
T_1:  & Bernoulli(2/9)\\
T_2:  & Bernoulli(1/3)\\
T_3:  & Bernoulli(0)\\
T_4:  & Bernoulli(1)
\end{align}
To consider the whole series as a single experiment, we aggregate all these independent Bernouillis and obtain a non trivial distribution called Poisson Binomial Distribution (PDB). A PDB gives us the probability of $k$ successes over $K$ trials where each trial has a different probability of success.

The PDB is therefore our null hypothesis:
$$
H_0: k \sim PDB(p_1, p_2,..,p_K)
$$

To test the null hypothesis, we count the number of times a user chose a context ($k$) and measure its p-value assuming the null hypothesis. (we use the R package \textit{poibin} that provides a numerical approximation).

Figure \ref{pvalues} show how far is every user from the null hypothesis for every context. The x-axis show the number of trials $K$, and the y-axis show the negative log of the p-value.  The red dotted line indicates a p-value of 0.95. Points below the line are not significant. Over the like, those at the right side are users that chose the context a number of times significantly higher than expected. Those at the left chose the context a number of times significantly less. 

For instance, \emph{context 0} corresponds to the first post in the thread. The figure show that, even in the presence of other contexts, this an attractive context for most of users. In plain words: users tend to respond to the seed. \emph{Context 01} corresponds to the two first posts in the thread. Users chose this context significantly less than expected by the null hypothesis, meaning that they prefer other contexts if possible (for instance, context 0) There is some users place at the right, but most of them have a low p-value.

Though the analysis has been made with a small sample of users, it seems that only a small set of contexts show a clear similar effect in all users. Thus, in the next experiment we study this behaviors user by user.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=1\textwidth]{pvalues}
\caption{Selection of plots of the p-values corresponding to a user behaving randomly, that is,  according to a Poisson Binomial Distribution (Null hypothesis)}
\label{pvalues}
\end{center}
\end{figure}

%%%
\subsection{Clustering users with similar reactions}
From the former analysis we create a vector of likes, dislikes, neutral for every user. Those users over the p-value threshold and at the right of the plot are considered to like the context (+1). Those to the left dislike the context (-1). Those place under the p-value line are codified as 0.

We define the following distance metric:
$$
d(u,v) = \sum_{i} abs(u_{c_i} - v_{c_i})
$$
and perform a hierarchical clustering. The result is shown in figure \ref{clustering}. The algorithm detected four big clusters. A further analysis need to be conducted on what users are in every cluster. 

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{clustering}
\caption{Clustering}
\label{clustering}
\end{center}
\end{figure}

%%%
\subsection{Evolution of contexts}
Every context can evolve to a different or a more complex context depending on who responses to the context. For instance, does 01 evolve into 012 (new user joining the conversation) or rather to 010 (the same two users)?. The matrix in \ref{transitions} shows the percentage of transitions from every pair of contexts. Note that most transitions are `forbidden", that is, it is not possible to evolve from 0 to 10101. These forbidden transitions are colored in green. We observe that the most likely, for every context, is to grow keeping the same users. \footnote{For computational reasons, we did not perform the complete matrix, but it can be easily done.}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=1\textwidth]{transitions}
\caption{Transitions between contexts}
\label{transitions}
\end{center}
\end{figure}

%%%%%%%%%%%
%%%%%%%%%%%
\section{Towards context-based predictions}
In this report we have shown a first analysis using a basic concept of context. Yet, the central task, that of predicting, remains to be done.

Remember toy the example used before:
\begin{align}
T_1: & c_1, \boldsymbol{c_1}, c_2, c_3, c_3, c_3, c_3, c_3, c_3\\
T_2: & c_1, \boldsymbol{c_2}, c_3\\
T_3: & c_5, c_5, \boldsymbol{c_6}, c_7, c_8, c_{15}\\
T_4: & \boldsymbol{c_1}
\end{align}

Given a set of items, the task of predicting which one the user will chose is known in machine learning as  ``Preference Learning" or ``Learning to Rank". The goal is to find a  function that ranks items minimizing the discrepancies with the observed choices. Ranking can be done point-wise (a score is estimated for every item, and the item selected is the one with higher score), pair-wise (we estimate the ranking pair by pair) or list-wise (a whole ranking is estimated). The pair-wise approach is the most common, though a point-wise approach is even more simple since it can be treated as a linear regression problem.

However, we have an extra problem. Our items are repeated, and therefore it is not enough to know that $a\succ b$, since if there is one instance of $a$  and 100 instance of $b$, we might think that users will pick up $b$ unless they prefer $a$ \emph{much more} than $b$.

As far as I know nobody has dealt with this problem (ca m'etonne...). 
\end{document}
