\chapter{Conditional distributions for the dual-view mixture model}

%\noindent \textbf{\Large Appendix}
%\vspace{-0.1cm}
%\adjustmtc

\minitoc% 

\section{Chinese Restaurant Process}
In this section we recall the derivation of a Chinese Restaurant Process. Such a process will be used as the prior over cluster assignments in the model. This prior will then be updated through the likelihoods of the observations through the different views. 

Imagine that every user $u$ belongs to one of $K$ clusters. $z_u$ is the cluster of user $u$ and $\mathbf{z}$ is a vector that indicates the cluster of every user. Let us assume that $z_u$ is a random variable drawn from a multimomial distribution with probabilities $\boldsymbol{\pi}= (\pi_1,...,\pi_K)$. Let us also assume that the vector $\boldsymbol{\pi}$ is a random variable drawn from a Dirichlet distribution with a symmetric concentration parameter $\boldsymbol{\alpha} = (\alpha/K,...,\alpha/K)$. We have: 
\begin{align*}
z_u | \boldsymbol{\pi} &\sim \text{Multinomial}(\boldsymbol{\pi})\notag\\
\boldsymbol{\pi} &\sim \text{Dirichlet}(\boldsymbol{\alpha})
\end{align*}
The marginal probability of the set of cluster assignments $\mathbf{z}$ is:
\begin{align*}
p(\mathbf{z}) =& 
\int \prod_{u=1}^U p(z_u | \boldsymbol{\pi})p(\boldsymbol{\pi} | \boldsymbol{\alpha})
\text{d}\boldsymbol{\pi}\\
=&\int 
   \prod_{i=1}^K \pi_i^{n_i} 
   \frac{1}{B(\boldsymbol{\alpha})}
   \prod_{j=1}^K \pi_j^{\alpha/K-1}
   \text{d}\boldsymbol{\pi}\\
=&
\frac{1}{B(\boldsymbol{\alpha})}
\int 
   \prod_{i=1}^K \pi_i^{\alpha/K + n_i - 1}
   \text{d}\boldsymbol{\pi}
\end{align*}
where $n_i$ is the number of users in cluster $i$ and $B$ denotes the Beta function. Noticing that the integrated factor is a Dirichlet distribution with concentration parameter $\boldsymbol{\alpha} + \mathbf{n}$ but without its normalizing factor:
\begin{align*}
p(\mathbf{z})=&
\frac{
B(\boldsymbol{\alpha} + \mathbf{n})
}{
B(\boldsymbol{\alpha})
}%\\&\times #uncomment to break the line in two column mode
\int 
   \frac{1}{
   B(\boldsymbol{\alpha + \mathbf{n}})
   }
   \prod_{i=1}^K \pi_i^{\alpha/K + n_i - 1}
   \text{d}\boldsymbol{\pi}\\
=& 
\frac{
B(\boldsymbol{\alpha} + \mathbf{n})
}{
B(\boldsymbol{\alpha})
}
\end{align*}
which expanding the definition of the Beta function becomes:
\begin{align}
p(\mathbf{z})=
\frac{
\prod_{i=1}^K \Gamma(\alpha/K + n_i)
}
{
\Gamma \left(\sum_{i=1}^K \alpha/K + n_i \right)
}
\frac{
\Gamma \left(\sum_{i=1}^K \alpha/K \right)
}
{
\prod_{i=1}^K \Gamma(\alpha/K)
}
= 
\frac{
	\prod_{i=1}^K \Gamma(\alpha/K + n_i)
}
{
	\Gamma \left(\alpha + U \right)
}
\frac{
	\Gamma \left(\alpha \right)
}
{
	\prod_{i=1}^K \Gamma(\alpha/K)
}
\label{eq:p_z}
\end{align}
where $U=\sum_{i=1}^{K}n_i$. Note that marginalizing out $\boldsymbol{\pi}$ we introduce dependencies between the individual clusters assignments under the form of the counts  $n_i$. The conditional distribution of an individual assignment given the others is:
\begin{align}
\label{eq:z_cond}
p(z_u  = j| \mathbf{z_{-u}}) 
=
\frac{p(\mathbf{z})}
{p(\mathbf{z}_{-u})}
\end{align}
To compute the denominator we assume cluster assignments are exchangeable, that is, the joint distribution $p(\mathbf{z})$ is the same regardless the order in which clusters are assigned. This allows us to assume that $z_u$ is the last assignment, therefore obtaining $p(\mathbf{z_{-u}})$ by considering how Equation $\ref{eq:p_z}$ before $z_u$ was assigned to cluster $j$. 
\begin{align}
\label{eq:p_z_minus}
p(\mathbf{z}_{-u}) =& 
\frac
{
\Gamma(\alpha/K + n_j-1)
\prod_{i\neq j} \Gamma(\alpha/K + n_i)
}
{\Gamma
\left(
\alpha + U -1 
\right)
}
%\\ &\times
\frac{
\Gamma \left(\alpha \right)
}
{
\prod_{i=1} \Gamma(\alpha/K)
}
\end{align}
And finally plugging Equations \ref{eq:p_z_minus} and \ref{eq:p_z} into Equation \ref{eq:z_cond}, and cancelling out the factors that do not depend on  the cluster assignment $z_u$, and finally using the identity $a \Gamma(a) = \Gamma(a+1)$ we get:

\begin{align*}
p(z_u = j| \mathbf{z}_{-u}) 
&=
\frac
{\alpha/K + n_j-1}
{\alpha + U -1}
=
\frac
{\alpha/K + n_{-j}}
{\alpha + U -1} 
\end{align*}

where $n_{-j}$ is the number of users in cluster $j$ before the assignment of $z_u$.

The Chinese Restaurant Process is the consequence of considering $K \rightarrow \infty$. For clusters where $n_{-j}>0$, we have:
\begin{align*}
p(z_u = j \text{ s.t } n_{-j}>0 | \mathbf{z}_{-u}) 
&=
\frac
{n_{-j}}
{\alpha + U -1}
\end{align*}
and the probability of assigning $z_u$ to any of the (infinite) empty clusters is: 
\begin{align*}
p(z_u = j \text{ s.t } n_{-j}=0 | \mathbf{z_{-u}}) 
=\;& \lim_{K\rightarrow \infty}
(K - p)\frac
{\alpha/K}
{\alpha + U -1} 
= 
\frac
{\alpha}
{\alpha + U -1} 
\end{align*}
where $p$ is the number of non-empty components.
It can be shown that the generative process composed of a Chinese Restaurant Process were every component $j$ is associated to a probability distribution with parameters $\boldsymbol{\theta}_j$ is equivalent to a Dirichlet Process.

\section{Conditionals for the feature view}
In this appendix we provide the conditional distributions for the feature view to be plugged into the Gibbs sampler. Note that, except for $\betaoa$, conjugacy can be exploited in every case and therefore their derivations are straightforward and well known. The derivation for $\betaoa$ is left for another section:
\subsection{Component parameters}
\subsubsection*{Components means $p(\Muk | \cdot )$:}
\begin{align*}
p(\Muk | \cdot ) 
&\propto
p\left(\Muk | \Muo, \invRo\right) 
\prod_{u \in k} p\left(\mathbf{a}_u | \Muk, \Sk, \mathbf{z}\right)\\
&\propto
\mathcal{N}\left(\Muk | \Muo, \invRo\right) 
\prod_{u \in k} \mathcal{N}\left(\mathbf{a}_u | \Muk, \Sk\right)\\
&=
\mathcal{N}(\boldsymbol{\mu', \Lambda'})
\end{align*}
where:
\begin{align*}
\boldsymbol{\Lambda'} &= \Ro + n_k \Sk\\ 
\boldsymbol{\mu'} &= \boldsymbol{\Lambda'^{-1}} \left(\Ro \Muo + \Sk \sum_{u\in k} \mathbf{a}_u\right)
\end{align*}

\subsubsection*{Components precisions $p(\Sk | \cdot )$:}
\begin{align*}
p(\Sk | \cdot ) 
\propto\;& 
p\left(\Sk |\betaoa, \Wo\right)
\prod_{u \in k} p\left(\mathbf{a}_u | \Muk, \Sk, \mathbf{z}\right)\\
\propto\;&
\mathcal{W}\left(\Sk |\betaoa, (\betaoa
\Wo)^{-1}\right)%\\&\times 
\prod_{u \in k} \mathcal{N}\left(\mathbf{a}_u | \Muk, \Sk\right)\\
=\;& \mathcal{W}(\beta', \mathbf{W}')
\end{align*}
where:
\begin{align*}
\beta' &= \betaoa + n_k\\
\mathbf{W}' &= 
\left[ \betaoa\Wo + \sum_{u \in k} (\mathbf{a}_u - \Muk)(\mathbf{a}_u- \Muk)^T  \right]^{-1}
\end{align*}

\subsection{Shared hyper-parameters}
\subsubsection*{Shared base means $p(\Muo | \cdot)$:}
\begin{align*}
p(\Muo | \cdot) 
&\propto
p\left(\Muo | \boldsymbol{\mu_a}, \boldsymbol{\Sigma_a}\right)
\prod_{k = 1}^K p\left(\Muk | \Muo, \Ro \right)  \\
&\propto
\mathcal{N}\left(\Muo | \boldsymbol{\mu_a, \Sigma_a}\right)
\prod_{k = 1}^K\mathcal{N}\left(\Muk | \Muo, \invRo\right)  \\
&=\mathcal{N}\left(\boldsymbol{\mu'}, \boldsymbol{\Lambda'}^{-1}\right)
\end{align*}
where:
\begin{align*}
\boldsymbol{\Lambda'} &= \boldsymbol{\Lambda_{a}} + K \Ro\\ 
\boldsymbol{\mu'} &= \boldsymbol{\Lambda'}^{-1} \left(\boldsymbol{\Lambda_{a}} \boldsymbol{\mu_{a}} + K \Ro \overline{\Muk}\right)
\end{align*}


\subsubsection*{Shared base precisions $p(\Ro | \cdot)$:}
\begin{align*}
p(\Ro | \cdot) 
\propto\;&
p\left(\Ro | D, \boldsymbol{\Sigma_a^{-1}}\right) 
\prod_{k = 1}^K p\left(\Muk | \Muo, \Ro\right) \\
\propto\;&
\mathcal{W}\left(\Ro | D, (D\boldsymbol{\Sigma_a})^{-1}\right)
%\\&\times 
\prod_{k = 1}^K \mathcal{N}\left(\Muk | \Muo,  \invRo \right) \\
=\;&
\mathcal{W}(\upsilon', \boldsymbol{\Psi}')
\end{align*}
where:
\begin{align*}
\upsilon' &= D+K\\
\boldsymbol{\Psi'} &=
\left[D\boldsymbol{\Sigma_a} + \sum_k (\Muk- \Muo)(\Muk- \Muo)^T \right]^{-1}
\end{align*}

\subsubsection*{Shared base covariances $p(\Wo | \cdot)$:}
\begin{align*}
p(\Wo | \cdot) 
\propto\;&
p\left(\Wo  | D, \frac{1}{D} \boldsymbol{\Sigma_a}\right) 
\prod_{k=1}^K p\left(\Sk | \betaoa, \invWo\right)\\
\propto\;&
\mathcal{W}\left(\Wo | D, \frac{1}{D} \boldsymbol{\Sigma_a}\right)
%\\&\times 
\prod_{k=1}^K \mathcal{W}\left(\Sk | \betaoa, \left(\betaoa\Wo\right)^{-1}\right)\\
=\;&
\mathcal{W}(\upsilon', \boldsymbol{\Psi}')
\end{align*}
where:
\begin{align*} 
\upsilon' &=D + K\betaoa\\
\boldsymbol{\Psi}' &=
\left[D\boldsymbol{\Sigma_a}^{-1} +  \betaoa\sum_{k=1}^K\Sk\right]^{-1}
\end{align*}

\subsubsection*{Shared base degrees of freedom $p(\betaoa | \cdot)$:}
\begin{align*}
p(\betaoa | \cdot) 
&\propto 
p(\betaoa) \prod_{k=1}^K p\left(\Sk | \Wo ,  \betaoa\right)\\
&=p(\betaoa | 1, \frac{1}{D})\prod_{k=1}^K  \mathcal{W} \left(\Sk | \Wo ,  \betaoa\right)
\end{align*}
where there is no conjugacy we can exploit. We may sample from this distribution with Adaptive Rejection Sampling.

%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conditionals for the behavior view}
In this appendix we provide the conditional distributions for the behavior view to be plugged into the Gibbs sampler. Except for $\beta_{b_0}$, conjugacy can be exploited in every case and therefore their derivations straightforward and well known. The derivation for $\beta_{b_0}$ is left for another section:
\subsection{Users parameters}
\subsubsection*{Users latent coefficient $p(b_u | \cdot )$:}\label{sec:appendix_coefficients}

%%%%%%%%%%%%%%
% User by user (this derivation is wrong. To avoid messing up with interdependencies between users participations use the matricial derivation computing all coefficients at once)
%%%%%%%%%%%%%%
%\begin{align*}
%p(b_u | \cdot )
%=& 
%p(b_u | \mu_{z_u}, s_{z_u}, \mathbf{z_u}) \prod_{t=1}^T p(y_t | \mathbf{p, b})\\
%&= \mathcal{N}(b_u | \mu_{z_u}, s_{z_u}) \prod_{t=1}^T \mathcal{N}(y_t|\mathbf{p^T b, \sigma_y})\\
%=&
%\mathcal{N}(\mu', \sigma'^2)
%\end{align*}
%\begin{align}
%\sigma'^{-2} &= s_{z_u} + T_u \sigma_y^{-2} \\
%\mu' &= \sigma'^{2}(s_{z_u}\mu_{z_u}+ T_u \sigma_y^{-2} \overline{y_t})
%\end{align}
Let $\mathbf{Z}$ be a $K\times U$ a binary matrix where $\mathbf{Z}_{k,u}=1$ denotes whether user $u$ is assigned to cluster $k$. Let $\mathbf{I_{[T]}}$ and $\mathbf{I_{[U]}}$ identity matrices of sizes $T$ and $U$, respectively. Let $\boldsymbol{\mu}^\text{(f)} = (\mu_1^{\text{(f)}},...,\mu_K^{\text{(f)}})$ and  $\mathbf{s}^\text{(f)} = (s_1^{\text{(f)}},...,s_K^{\text{(f)}})$  Then:
\begin{align*}
p(\mathbf{b} | \cdot ) 
\propto&\;
p(\mathbf{b} | \boldsymbol{\mu}^\text{(f)}, \mathbf{s}^\text{(f)}, \mathbf{Z}) p(\mathbf{y} | \mathbf{P, b})\\
\propto&\; 
\mathcal{N}(\mathbf{b} | \mathbf{Z}^T\boldsymbol{\mu}^\text{(f)}, \mathbf{Z}^T \mathbf{s}^\text{(f)} \mathbf{I_{[U]}}) 
\mathcal{N}(\mathbf{y}|\mathbf{P}^T \mathbf{b, \sigma_y I_{[T]}})\\
=&\; 
\mathcal{N}(\mathbf{\boldsymbol{\mu'}, \boldsymbol{\Lambda'}^{-1}})
\end{align*}
where:
\begin{align*}
\mathbf{\Lambda'} &= \mathbf{Z}^T \mathbf{s}^\text{(f)} \mathbf{I_{[U]}} +  \mathbf{P}\sigma_\textbf{y}^{-2} \mathbf{I}_{[T]} \mathbf{P}^T  \\
\boldsymbol{\mu'} &= \mathbf{\Lambda'}^{-1}(\mathbf{Z}^T \mathbf{s}^\text{(f)} \mathbf{Z}^T\boldsymbol{\mu}^\text{(f)}+ \mathbf{P} \sigma_\text{y}^{-2} \mathbf{I_{[T]} y})
\end{align*}
\subsection{Component parameters}
\subsubsection*{Components means $p(\muk| \cdot )$:}

\begin{align*}
p(\muk | \cdot ) 
&\propto
p\left(\muk | \muo, \invro\right) 
\prod_{u \in k} p(b_u | \muk, \sk, \mathbf{z})\\
&\propto
\mathcal{N}\left(\muk | \muo, \invro\right) 
\prod_{u \in k} \mathcal{N}(b_u | \muk, \sk)\\
&=\mathcal{N}(\boldsymbol{\mu'}, \boldsymbol{\Lambda'}^{-1})
\end{align*}
where:
\begin{align*}
\boldsymbol{\Lambda'} &= \ro + n_k \sk\\ 
\boldsymbol{\mu'} &= \boldsymbol{\Lambda'^{-1}} \left(\ro \muo + \sk \sum_{u\in k} b_u\right)
\end{align*}


\subsubsection*{Components precisions $p(\sk | \cdot )$:}
\begin{align*}
p(\sk | \cdot ) 
&\propto 
p(\sk |\betaof, \wo)
\prod_{u \in k} p(b_u | \muk, \sk, \mathbf{z})\\
&\propto
\mathcal{G}\left(\sk |\betaof, \left(\betaof\wo\right)^{-1}\right)
\prod_{u \in k} \mathcal{N}(b_u | \muk, \sk)\\
&=
\mathcal{G}(\upsilon', \psi')
\end{align*}
where:
\begin{align*}
\upsilon' &= \betaof + n_k\\
\psi' &=
\left[ \betaof \wo + \sum_{u \in k} \left(b_u - \muk\right)^2  \right]^{-1}
\end{align*}

\subsection{Shared hyper-parameters}
\subsubsection*{Shared base mean $p(\muo | \cdot)$:}
\begin{align*}
p(\muo | \cdot) 
&\propto  
p(\muo | \mu_{\hat{b}}, \sigma_{\hat{b}})
\prod_{k = 1}^K p(\muk | \muo, \ro )  \\
&\propto
\mathcal{N}(\muo | \mu_{\hat{b}}, \sigma_{\hat{b}})
\prod_{k = 1}^K\mathcal{N}\left(\muk | \muo, \invro\right)  \\
&= \mathcal{N}(\mu', \sigma'^{-2})
\end{align*}
where:
\begin{align*}
\sigma'^{-2} &= \sigma_{\hat{b}}^{-2} + K \ro\\ 
\mu' &= \sigma_{\hat{b}}^{2'} (\sigma_{\hat{b}}^{-2} 
\mu_{\hat{b}} + K \ro \overline{\muk})
\end{align*}


\subsubsection*{Shared base precision $p(\ro | \cdot)$}
\begin{align*}
p(\ro | \cdot) 
&\propto 
p(\ro | 1, \sigma_{\hat{b}}^{-2}) 
\prod_{k = 1}^K p(\muk | \muo, \ro) \\
&\propto
\mathcal{G}(\ro | 1, \sigma_{\hat{b}}^{-2}) 
\prod_{k = 1}^K \mathcal{N}\left(\muk | \muo,  \invro\right) \\
&= \mathcal{G}(\upsilon', \psi')
\end{align*}
where:
\begin{align*}
\upsilon' =& 1+K\\
\psi' =& \left[\sigma_{\hat{b}}^{-2} + \sum_{k=1}^K \left(\muk- \muo\right)^2\right]^{-1}
\end{align*}

\subsubsection*{Shared base variance $p(\wo | \cdot)$:}
\begin{align*}
p(\wo | \cdot) 
&\propto 
p(\wo | 1, \sigma_{\hat{b}})
\prod_{r=1}^K p\left(\sk | \betaof, \wo\right)\\
&\propto
\mathcal{G}(\wo | 1, \sigma_{\hat{b}}) 
\prod_{k=1}^K \mathcal{G}\left(\sk | \betaof, \left(\beta\wo\right)^{-1}\right)\\
&= \mathcal{G}(\upsilon', \psi')   
\end{align*}
\begin{align*}
\upsilon' =& 1 + K\betaof\\
\psi' =& 
\left[\sigma_{\hat{b}}^{-2} +  \betaof \sum_{k=1}^K \sk\right]^{-1}
\end{align*}

\subsubsection*{Shared base degrees of freedom $p(\betaof | \cdot)$:}
\begin{align*}
p(\betaof | \cdot) 
&\propto
p(\betaof) \prod_{r=1}^K p(\sk | \wo ,  \betaof)\\
&=p(\betaof | 1, 1)\prod_{r=1}^K  \mathcal{G} \left(\sk |\betaof, \left(\betaof  \wo \right)^{-1}\right)
\end{align*}
where there is no conjugacy we can exploit. We will sample from this distribution with Adaptive Rejection Sampling.

\subsection{Regression noise}
Let the precision $s_{\text{y}}$ be the inverse of the variance $\sigma_{\text{y}}^{2}$. Then:
\begin{align*}
p(s_{\text{y}} | \cdot) &\propto
p(s_{\text{y}} | 1,\sigma_{0}^{-2}) \prod_{t=1}^T p(y_t | \mathbf{p^T b}, s_{\text{y}})\\
&\propto \mathcal{G}(s_{\text{y}} | 1,\sigma_{\text{0}}^{-2}) \prod_{t=1}^T \mathcal{N}( y_t | \mathbf{p^T b}, s_{\text{y}})\\
&= \mathcal{G}(\upsilon', \psi')  
\end{align*}
\begin{align*}
\upsilon' &= 1+T\\ 
\psi' &= \left[\sigma_{\text{0}}^{2} + \sum_{t=1}^{T}\left(y_t-\mathbf{p^Tb}\right)^2\right]^{-1}
\end{align*}


%%%%%%%%%%%%
\section{Sampling $\betaoa$}
For the feature view, if:
\begin{align*}
\frac{1}{\beta - D + 1} \sim \mathcal{G}(1, \frac{1}{D})
\end{align*}
we can get the prior distribution of $\beta$ by variable transformation:
\begin{align*}
p(\beta) =\;& \mathcal{G}(\frac{1}{\beta-D+1})|\frac{\partial}{\partial \beta}\frac{1}{\beta-D+1}|\\
&\propto \left(\frac{1}{\beta-D+1}\right)^{-1/2} \exp\left(-\frac{D}{2(\beta-D+1)}\right)
%\\&\times
\frac{1}{(\beta-D+1)^2}\\
&\propto \left(\frac{1}{\beta-D+1}\right)^{3/2} \exp\left(-\frac{D}{2(\beta-D+1)}\right)
\end{align*}
Then:
\begin{align*}
p(\beta) &\propto  (\beta - D + 1)^{-3/2} \exp\left( -\frac{D}{2(\beta - D +1)}\right)
\end{align*}
The Wishart likelihood is:
\begin{align*}
\mathcal{W}(\mathbf{S}_k | \beta, (\beta\mathbf{W})^{-1})
=&
\frac{(|\mathbf{W}| (\beta/2)^D)^{\beta/2}}{\Gamma_D(\beta/2)}
|\mathbf{S}_k|^{(\beta-D-1)/2} 
%\\&\times
\exp\left(- \frac{\beta}{2}\text{Tr}(\mathbf{S}_k\mathbf{W})\right)\\
=&
\frac{(|\mathbf{W}| (\beta/2)^D)^{\beta/2}}{\prod_{d=1}^{D} \Gamma(\frac{\beta+d-D}{2})}
|\mathbf{S}_k|^{(\beta-D-1)/2}
%\\&\times 
\exp\left(- \frac{\beta}{2}\text{Tr}(\mathbf{S}_k\mathbf{W})\right)\\
\end{align*}
We multiply both equations, the Wishart likelihood (its $K$ factors) and the prior, to get the posterior:
\begin{align*}
p(\beta | \cdot) =& 
 \left(\prod_{d=0}^D \Gamma (\frac{\beta}{2} + \frac{d-D}{2}) \right)^{-K}
%\\&\times
\exp\left(-\frac{D}{2(\beta-D+1)}  \right)
(\beta-D+1)^{-3/2}
\\&\times
(\frac{\beta}{2})^{\frac{KD\beta}{2}}
%\\&\times 
\prod_{k=1}^K (|\mathbf{S}_k||\mathbf{W}|)^{\beta/2} \exp\left(-\frac{\beta}{2} \text{Tr}(\mathbf{S}_k\mathbf{W})\right)
\end{align*}
Then if $y = \ln\beta$:
\begin{align*}
p(y | \cdot) =\;& 
e^y
\left(\prod_{d=0}^D \Gamma (\frac{e^y}{2} + \frac{d-D}{2}) \right)^{-K}
%\\&\times 
\exp\left(-\frac{D}{2(e^y-D+1)}  \right)
(e^y-D+1)^{-3/2}
\\&\times
(\frac{e^y}{2})^{\frac{KDe^y}{2}}
%\\&\times 
\prod_{k=1}^K (|\mathbf{S}_k||\mathbf{W}|)^{e^y/2} \exp\left(-\frac{e^y}{2} \text{Tr}(\mathbf{S}_k\mathbf{W})\right)
\end{align*}
and its logarithm is:
\begin{align*}
\ln p(y | \cdot) 
=\;& 
y 
-K \sum_{d=0}^D \ln\Gamma (\frac{e^y}{2} + \frac{d-D}{2})
%\\&\times 
-\frac{D}{2(e^y-D+1)}
-\frac{3}{2}\ln(e^y-D+1)
\\&
+\frac{KDe^y}{2}(y - \ln2)
%\\&
+\frac{e^y}{2} \sum_{k=1}^K \left( \ln (|\mathbf{S}_k||\mathbf{W}|) - \text{Tr}(\mathbf{S}_k\mathbf{W})\right)
\end{align*}
which is a concave function and therefore we can use Adaptive Rejection Sampling (ARS). ARS sampling works with the derivative of the log function:
\begin{align*}
\frac{\partial}{\partial y} \ln p(y | \cdot) 
=& 
1-K \frac{e^y}{2} \sum_{d=1}^D \Psi (\frac{e^y}{2} + \frac{d-D}{2})
%\\&
+\frac{De^y}{2(e^y-D+1)^2}
-\frac{3}{2}\frac{e^y}{e^y-D+1}
\\&
+\frac{KDe^y}{2}(y - \ln2) + \frac{KDe^y}{2}
%\\&
+\frac{e^y}{2} \sum_{k=1}^K \left(\ln (|\mathbf{S}_k||\mathbf{W}|) - \text{Tr}(\mathbf{S}_k\mathbf{W})\right)
\end{align*}
where $\Psi(x)$ is the digamma function.

%%% Wolfram Alpha
%% d/dx(((D*x*K-3)/2)*ln(x/2))
%% d/dx(-\frac{D}{2(x-D+1)})
%% - K*(d/dx(\sum_{d=1}^{D}(log(\Gamma ((x + d - D)/2)))))

%%%%%%%%%%%%%%%%%%%%%%%%
\section{Sampling $\betaof$}
For the behavior view, if 
\begin{align*}
	\frac{1}{\beta} \sim \mathcal{G}(1,1)
\end{align*}
the posterior of $\beta$ is:
\begin{align*}
	p(\beta | \cdot) 
	=&  
	\Gamma(\frac{\beta}{2})^{-K}\exp\left(\frac{-1}{2\beta}\right) \left(\frac{\beta}{2}\right)
	^{(K \beta -3)/2}
	%\\&\times
	\prod_{k=1}^{K} (s_k w)^{\beta/2} \exp\left(-\frac{\beta s_k w}{2}\right)
\end{align*}
Then if $y=\ln\beta$:
\begin{align*}
	p(y | \cdot) =&  e^y \Gamma(\frac{e^y}{2})^{-K}\exp\left(\frac{-1}{2e^y}\right) \left(\frac{e^y}{2}\right)
	^{(K e^y -3)/2}
	%\\&\times 
	\prod_{k=1}^{K} (s_k w)^{e^y/2} \exp\left(-\frac{e^y s_k w}{2}\right)
\end{align*}
and its logarithm:
\begin{align*}
	\ln p(y | \cdot) =& y -K\ln\Gamma \left(\frac{e^y}{2}\right) + \left(\frac{-1}{2e^y}\right)%\\&
	+\frac{Ke^y-3}{2}\left(y - \ln2\right)%\\&
	+ \frac{e^y}{2}\sum_{k=1}^{K} \left(\ln (s_k w) - s_k w \right)
\end{align*}
which is a concave function and therefore we can use Adaptive Rejection Sampling. The derivative is:
\begin{align*}
	\frac{\partial}{\partial y} \ln p(y | \cdot) =& 
	1 
	-K \Psi \left(\frac{e^y}{2}\right) \frac{e^y}{2}
	+ \left(\frac{1}{2e^y}\right)
	%\\&
	+\frac{Ke^y}{2} \left(y - \ln2\right) + \frac{Ke^y-3}{2}
	%\\&
	+ \frac{e^y}{2}\sum_{k=1}^{K}\left(\ln (s_k w) - s_k w\right)
\end{align*}
where $\Psi(x)$ is the digamma function.

%%%%%%%%%%%%%%%%%%%%%%%%
\section{Sampling $\alpha$}
Since the inverse of the concentration parameter $\alpha$ is given a Gamma prior
\begin{align*}
	\frac{1}{\alpha} \sim \mathcal{G}(1,1)
\end{align*}
we can get the prior over $\alpha$ by variable transformation:
\begin{align*}
p(\alpha) 
\propto~& 
\alpha^{-3/2} \exp \left(-1/(2\alpha)\right)
\end{align*}
Multiplying the prior of $\alpha$ by its likelihood we get the posterior:
\begin{align*}
	p(\alpha | \cdot) 
	\propto~& 
	\alpha^{-3/2} \exp \left(-1/(2\alpha)\right)
	\times
	\frac{\Gamma(\alpha)}{\Gamma(\alpha+U)}
	\prod_{j=1}^{K}
	\frac{\Gamma(n_j + \alpha/K)}{\alpha/K}\\
	\propto~& 
	\alpha^{-3/2} \exp \left(-1/(2\alpha)\right)
	\frac{\Gamma(\alpha)}{\Gamma(\alpha+U)}\alpha^K \\
	\propto~&\alpha^{K-3/2} \exp \left(-1/(2\alpha)\right)
	\frac{\Gamma(\alpha)}{\Gamma(\alpha+U)}
\end{align*}
Then if $y=\ln \alpha$:
\begin{align*}
p(y | \cdot) = e^{y(K-3/2)}
\exp(-1/(2e^y))
\frac{\Gamma(e^y)}{\Gamma(e^y +U)}
\end{align*}
and its logarithm is:
\begin{align*}
\ln p(y | \cdot) = 
y(K-3/2)
-1/(2e^y)+
\ln\Gamma(e^y) - \ln\Gamma(e^y+U)
\end{align*}
which is a concave function and therefore we can use Adaptive Rejection Sampling. The derivative is:
\begin{align*}
\frac{\partial}{\partial y} \ln p(y | \cdot) = 
(K-3/2)
+1/(2e^y)+
e^y\Psi(e^y) - e^y\Psi(e^y+U)
\end{align*}

