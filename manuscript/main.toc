\contentsline {chapter}{Contents}{ix}{section*.8}
\contentsline {chapter}{\chapternumberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Role Theory}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Online forums}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Vocabulary}{4}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Forums as graphs}{5}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Role detection in online communities}{5}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Social sciences}{6}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Blockmodeling}{7}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Feature-based}{9}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}Triads and motifs}{10}{subsection.1.3.4}
\contentsline {subsection}{\numberline {1.3.5}Other methods}{12}{subsection.1.3.5}
\contentsline {section}{\numberline {1.4}Industrial context of the thesis}{13}{section.1.4}
\contentsline {section}{\numberline {1.5}Outline of the thesis}{14}{section.1.5}
\contentsline {chapter}{\chapternumberline {2}Analysis of datasets}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Forum dynamics}{19}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Posting activity}{19}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Newbies versus oldies}{20}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}User dynamics}{23}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Posting}{23}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Lifespans}{23}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Communities}{24}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Conversation dynamics}{26}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Thread size}{26}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Thread duration}{28}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Motifs of interaction in threads}{28}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Summary}{32}{section.2.4}
\contentsline {chapter}{\chapternumberline {3}Role detection based on conversation structures}{33}{chapter.3}
\contentsline {section}{\numberline {3.1}Discussion trees}{34}{section.3.1}
\contentsline {section}{\numberline {3.2}Methodology}{35}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Neighborhood extraction}{35}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Clustering}{37}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Radius-based neighborhoods}{37}{section.3.3}
\contentsline {subsubsection}{Coloring}{38}{section*.9}
\contentsline {subsubsection}{Pruning}{39}{section*.10}
\contentsline {subsubsection}{Choice of radius}{39}{section*.11}
\contentsline {subsection}{\numberline {3.3.1}Results for radius-based neighborhood}{39}{subsection.3.3.1}
\contentsline {subsubsection}{Results for $\mathbf {r=1}$}{39}{section*.12}
\contentsline {subsubsection}{Results for $\mathbf {r=2}$}{43}{section*.13}
\contentsline {section}{\numberline {3.4}Order-based neighborhoods}{47}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Pruning and coloring}{47}{subsection.3.4.1}
\contentsline {subsubsection}{Choice of radius}{48}{section*.14}
\contentsline {subsection}{\numberline {3.4.2}Results for order-based neighborhood}{48}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Time-based neighborhoods}{51}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Choice of radius}{54}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Results for time-based neighborhood}{54}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Comparative analysis}{57}{section.3.6}
\contentsline {section}{\numberline {3.7}Summary}{58}{section.3.7}
\contentsline {chapter}{\chapternumberline {4}Role detection based on thread growth models}{61}{chapter.4}
\contentsline {section}{\numberline {4.1}Network Growth models}{63}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Barabasi-Albert (1999)}{64}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Kumar et al. (2010)}{65}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}G\IeC {\'o}mez et al. (2010)}{65}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}G\IeC {\'o}mez et al. (2012)}{66}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2}A new role-based network growth model}{67}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Formalization}{68}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Expectation-Maximization for the role-based growth model}{68}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Experiments}{71}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Dataset}{71}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Inference}{71}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Structural properties}{74}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Link prediction}{74}{subsection.4.3.4}
\contentsline {section}{\numberline {4.4}Summary}{79}{section.4.4}
\contentsline {chapter}{\chapternumberline {5}Role detection based on features and latent behavioral functions}{81}{chapter.5}
\contentsline {section}{\numberline {5.1}Introduction. Why a dual-view model?}{82}{section.5.1}
\contentsline {section}{\numberline {5.2}Related models}{82}{section.5.2}
\contentsline {section}{\numberline {5.3}Model description}{84}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Mixture models}{84}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Dual-view mixture model}{85}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Infinite number of clusters}{87}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}Application to role detection in online forums}{88}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Feature view}{89}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Behavior view}{90}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Shared parameters}{91}{subsection.5.4.3}
\contentsline {section}{\numberline {5.5}Inference}{91}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Predictive distribution}{92}{subsection.5.5.1}
\contentsline {section}{\numberline {5.6}Experiments}{92}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Compared models}{93}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}Metrics}{94}{subsection.5.6.2}
\contentsline {paragraph}{Metric for clustering:}{94}{section*.15}
\contentsline {paragraph}{Metric for predictions:}{95}{section*.16}
\contentsline {subsection}{\numberline {5.6.3}Agreement between views}{95}{subsection.5.6.3}
\contentsline {subsection}{\numberline {5.6.4}Disagreement between views}{97}{subsection.5.6.4}
\contentsline {paragraph}{(a) Lack of information in feature view:}{99}{section*.17}
\contentsline {paragraph}{(b) Lack of information in both views:}{99}{section*.18}
\contentsline {paragraph}{(c) Lack of information in behavior view:}{99}{section*.19}
\contentsline {subsection}{\numberline {5.6.5}Iris dataset}{99}{subsection.5.6.5}
\contentsline {subsection}{\numberline {5.6.6}Computational cost}{101}{subsection.5.6.6}
\contentsline {subsection}{\numberline {5.6.7}Summary of the experiments}{102}{subsection.5.6.7}
\contentsline {section}{\numberline {5.7}Summary}{103}{section.5.7}
\contentsline {chapter}{\chapternumberline {6}Contributions and perspectives}{105}{chapter.6}
\contentsline {section}{\numberline {6.1}Contributions}{105}{section.6.1}
\contentsline {section}{\numberline {6.2}Roles or not roles?}{105}{section.6.2}
\contentsline {section}{\numberline {6.3}Perspectives}{106}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Technical}{106}{subsection.6.3.1}
\contentsline {subsubsection}{Clustering based on count data}{106}{section*.20}
\contentsline {subsubsection}{A scalable dual-view model}{106}{section*.21}
\contentsline {subsection}{\numberline {6.3.2}Methodological}{107}{subsection.6.3.2}
\contentsline {subsubsection}{A dictionary of conversations}{107}{section*.22}
\contentsline {subsubsection}{A better tree growth model}{107}{section*.23}
\contentsline {subsubsection}{Combining structure and text}{108}{section*.24}
\contentsline {chapter}{Bibliography}{109}{section*.26}
\contentsline {appendix}{\chapternumberline {A}Conditional distributions for the dual-view mixture model}{119}{appendix.A}
\contentsline {section}{\numberline {A.1}Chinese Restaurant Process}{119}{section.A.1}
\contentsline {section}{\numberline {A.2}Conditionals for the feature view}{120}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}Component parameters}{121}{subsection.A.2.1}
\contentsline {subsection}{\numberline {A.2.2}Shared hyper-parameters}{121}{subsection.A.2.2}
\contentsline {section}{\numberline {A.3}Conditionals for the behavior view}{123}{section.A.3}
\contentsline {subsection}{\numberline {A.3.1}Users parameters}{123}{subsection.A.3.1}
\contentsline {subsection}{\numberline {A.3.2}Component parameters}{124}{subsection.A.3.2}
\contentsline {subsection}{\numberline {A.3.3}Shared hyper-parameters}{124}{subsection.A.3.3}
\contentsline {subsection}{\numberline {A.3.4}Regression noise}{126}{subsection.A.3.4}
\contentsline {section}{\numberline {A.4}Sampling $\beta _{0}^\text {(a)}$}{126}{section.A.4}
\contentsline {section}{\numberline {A.5}Sampling $\beta _{0}^\text {(f)}$}{127}{section.A.5}
\contentsline {section}{\numberline {A.6}Sampling $\alpha $}{128}{section.A.6}
