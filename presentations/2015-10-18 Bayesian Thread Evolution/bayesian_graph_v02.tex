\documentclass[11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{graphicx} % adjustbox loads it
\usepackage{epstopdf} % loads eps
\usepackage[normalem]{ulem}

% insert here the call for the packages your document requires
\usepackage{geometry}
\usepackage[export]{adjustbox}
\usepackage[labelfont=bf, labelsep=space]{caption}
%\usepackage{subcaption}
\usepackage{subfig}
\usepackage{amssymb}
\usepackage{amsmath} 

\usepackage{natbib} % enables author year and other citation styles
\usepackage[bookmarks,bookmarksopen,bookmarksdepth=2]{hyperref} % active links
\hypersetup{backref,
	colorlinks=true,
	citecolor=blue,
	linkcolor=blue}

\usepackage{tikz} % graphics,
\usetikzlibrary{fit,positioning} % tikz elements positioning
\usepackage{soul} % annotations


\DeclareGraphicsExtensions{{.pdf},{.png},{.jpg}}
\graphicspath{ {./img/} }
\DeclareMathOperator*{\argmax}{arg\,max}

\title{Time-sensitive graph growth model for online discussions}
\author{Alberto Lumbreras}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
\section{Introduction}

% Forums 
Online forums have gained popularity during the last years and are of the predominant forms of dialog between Internet users. Most forums sites host many subforums where every subforum covers a topic. In every subforum, users can start new conversations, or threads, by posting an initial message. Every thread then composed by one initial post and a set of comments (or posts) where every comment is a reply either to some other comment or to the initial post. 

% Threads as graphs
A discussion thread can be represented by a tree graph $G$ where vertices represent users posts and an edges $(u,v)$ means the post $u$ is a reply to post $v$. Some authors have posed the question of what determines the growth of a discussion thread. To this aim, they have proposed several stochastic \textit{growth models} able to generate synthetic trees that reproduce some of the properties of the real conversations. 

Nevertheless, there is an important element that remains to be included to these growth models: \textbf{time}. Current models consider only the sequence of posts arrivals, and are unable to model the speed changes that can be observed in real online discussions. These speed changes might be due to factors such as circadian cycles or the controversy of a debate. Another problem that has barely received attention is that of \textbf{prediction}. Given the evolution of a discussion $G_1,..., G_t$, what is the most likely state of the graph at $G_{t+1}?$ 

In this paper we try to find a growth model that is time-sensitive and that is able to make predictions by learning from the recent evolution of a discussion.




\begin{figure}
	\subfloat[$p=0.1$]{\includegraphics[width=0.33\textwidth]{erdos_01}}
	\subfloat[$p=0.5$]{\includegraphics[width=0.33\textwidth]{erdos_05}}
	\subfloat[$p=1$]{\includegraphics[width=0.33\textwidth]{erdos_1}}
	\caption{Erdös-Rényi graphs}
	\label{fig:erdos_rendy}
\end{figure}


\section{Related work}\label{sec:related-work}
Random graph models are stochastic generators of graphs that try to reproduce the properties of a some real-world graphs. Ideally, these models should reproduce a large set of properties using a minimum number of assumptions and parameters.
%Random graphs  
One of the simplest random graph models is that of Erdös-Rényi, where the number of nodes is given from the beginning and an edge between any pair of nodes is created with a probability $p$  (Figure~\ref{fig:erdos_rendy}). After this foundational work, other graph models have attempted to mimic real-world networks. The Watts-Strogatz model, for instance, generates graphs with some \textit{small-network} properties such as low average distance between vertices and high transivity (friends of my friends are also my friends).

An interesting family of random graphs is formed by the \textit{growth models}. Growth models try to reproduce not only the final properties of the network but also how the network is built. The \textit{preferential attachment} model proposed by \cite{Barabasi1999} is the best well-known member of this family.
The Barabasi-Albert model builds a graph by sequentially adding its vertices; once a new vertex $x$ is added to the graph it decides whether to create an edge to an existing vertex $i$ with probability

\begin{align}
	p(x \sim i | G) =\frac{d_{i}^\alpha}{Z}; &\qquad
	Z = \sum_{j=1}^{|V(G)|} d_j^\alpha
\end{align}
where $d_i$ is the degree of the vertex $i$ before vertex $x$ is added\footnote{To avoid loaded notations we avoid writing $G_{t-1}$ $d_{i,t-1}$ and $Z_{t-1}$ when it is clear by the context that they correspond to the last state of the graph before adding the new node $x$.}. No This model reproduces a rich-get-richer phenomena controlled by the parameter $\alpha$. The particular case of $\alpha=1$ is called \textit{linear preferential-attachment} since the probabilities increase linearly with the number of degrees. Figure~\ref{fig:Barabasi-Albert} shows examples of Barabasi-Albert graphs generated with different $\alpha$. The Barabasi-Albert explains very well the power-law degree distributions observed in many real graphs.

\begin{figure}
	\centering
	\subfloat[$\alpha=0$]{\includegraphics[width=0.33\textwidth]{barabasi_0}}
	\subfloat[$\alpha=1$]{\includegraphics[width=0.33\textwidth]{barabasi_1}}
	\subfloat[$\alpha=1.8$]{\includegraphics[width=0.33\textwidth]{barabasi_1_8}}
	\caption{Barabasi-Albert graphs with one edge created at every step.}
	\label{fig:Barabasi-Albert}
\end{figure}



During the recent years, some authors have proposed models to explain the growth of online conversations. \citep{Kumar2010, Gomez2010, Wang2012e, Gomez2012a}. We describe this models in the following sections.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Preferential attachment and recency}
\cite{Kumar2010} proposed a model that combines both \textit{preferential-attachment} and \textit{recency}. The higher the degree of a post and the later it was published, the easier for this post to attract the incoming replies. At every time step, a decision is made to stop the thread or to add a new post. Every new post choses its parent according to:
\begin{align}
p(x\sim i | G) = \frac{h(d_u, r_u)}{Z}; \qquad h(d_u, r_u) =\alpha d_u + \tau^{r_u} ; \qquad Z = \sum_{n=1}^{|V(G)|} h(d_n, r_n) + \delta
\end{align}
where $r_u$ is the number of time steps since $u$ was added to the thread. The authors report that when the alternative function $h(d_u, r_u) = d_u \tau^{r_u}$ is used, the recency factor prevents the preferential attachment factor from generating heavy-tailed degree distributions. The choice of placing $\alpha$ as a coefficient instead of an exponent is made for mathematical convenience so that $Z$ does not depend on the graph structure at that particular moment.

The authors also propose an improvement of the model to account for the identity of posts authors. For a new post $v$ replying to a post $u$, its author $a(v)$ can be either $a(u)$ (a self-reply), another author $a(w)$ that has already participated in the chain from $u$ to the root, or some other new author belonging to the set of authors $A$ that have not participated in the chain:
\begin{align}
a(v) = 
\begin{cases}
a(w) & \text{ with probability } \gamma\\
a(u) & \text{ with probability } \epsilon\\
a \in A & \text{ with probability } 1 -\gamma - \epsilon 
\end{cases}
\end{align} 

\subsubsection*{Parameter estimation}
The parameters are estimated by a grid search computing the maximum likelihood estimation of different $\alpha, \tau, \gamma, \epsilon$.

\subsection{Preferential attachment and root bias}
In \cite{Gomez2010} the authors combine \textit{preferential-attachment} with a \textit{bias towards the root}. The probability of choosing an existing parent $k$ is:
\[
%p(\pi_t = k | \boldmath{\pi}_{(1:t-1)}) 
%\propto 
%(\beta_k d_{k,(t-1)})^{\alpha_k}
p(x \sim k | G) 
\propto 
(\beta_k d_{k})^{\alpha_k}
\]
where 
\begin{align}
\alpha_k = 
\begin{cases}
	\alpha_1 & \text{ for } k=1 \\	
	\alpha_c & \text{for } k \in \{2,...,t\}
\end{cases}\notag\\
\beta_k = 
\begin{cases}
\beta & \text{ for } k=1 \\	
1 & \text{for } k \in \{2,...,t\}
\end{cases}
\end{align}
Note that $\alpha_k$ is the preferential attachment exponent and that if $\alpha_1=\alpha_c$ and $\beta=1$ we recover the Barabasi-Albert model of preferential attachment.
\subsubsection*{Parameter estimation}
Maximum Likelihood Estimation of $\alpha_1, \alpha_c$ and $\beta$ is done by minimizing the negative log-likelihood:

%\begin{equation}
%	\log \mathcal{L}(\boldsymbol{\Pi} | \alpha_k, \beta_k)
%	=
%	\sum_{i=1}^{N}
%	\sum_{t=2}^{|\pi_i|}
%	\alpha_k (\log \beta_k+ \log d_{k,1:(t-1)}) 
%	-
%	\log \sum_{l=1}^{t} (\beta_l d_{l,1:(t-1)})
%\end{equation}
\begin{equation}
\log \mathcal{L}(\mathbf{G} | \alpha_k, \beta_k)
=
\sum_{i=1}^{N}
\sum_{t=2}^{|V(G_i|}
\alpha_k (\log \beta_k+ \log d_{k,(t-1)}) 
-
\log \sum_{l=1}^{t} (\beta_l d_{l,(t-1)})
\end{equation}
Since this is a convex function, the minimization is done with the Nelder-Mead algorithm (\texttt{fminsearch} in Matlab).The authors fitted the parameters to several datasets and then generate graphs that resemble the original conversations (see Figure~\ref{fig:Gomez}).

	\begin{figure}
		\centering
		\subfloat[real]{\includegraphics[width=0.45\textwidth]{gomez_real}}\hfill
		\subfloat[synthetic]{\includegraphics[width=0.45\textwidth]{gomez_synthetic}}
		\caption{Random grahs for discussion threads. Gómez-Kappen-Kaltenbrunner}
		\label{fig:Gomez}
	\end{figure}


\subsection{Preferential attachment, root bias and recency}
In \cite{Gomez2012a} the authors combine \textit{preferential-attachment}, a \textit{bias towards the root} and \textit{novelty}. Unlike in their former model in \cite{Gomez2010}, here they sum these factors instead of multiplying them:

\begin{equation}
%p(\pi_t = k | \boldmath{\pi}_{(1:t-1)}) 
%\propto 
%(\beta_k d_{k,(t-1)})^{\alpha_k}
p(x \sim k | G) 
\propto 
\beta_k + \alpha d_{k, (t-1)} + \tau^{t-k}
\end{equation}

\subsubsection*{Parameter estimation}
The negative log-likelihood to be minimized is: 
%\begin{equation}
%	\log \mathcal{L}(\boldsymbol{\Pi} | \alpha_k, \beta_k, \tau)
%	=
%	\sum_{i=1}^{N}
%	\sum_{t=2}^{|\pi_i|}
%	\beta_k+
%	\alpha_k d_{k,1:(t-1)}
%	+ \tau^{t-k+1}	
%\end{equation}
\begin{equation}
\log \mathcal{L}(\mathbf{G} | \alpha, \beta_k, \tau)
=
\sum_{i=1}^{N}
\sum_{t=2}^{|V(G_i)|}
\log
\left(
\beta_k+
\alpha d_{k,(t-1)}
+ \tau^{t-k}
\right)
- \log
\sum_{l=1}^{t}
\left(\beta_l+
\alpha d_{l, (t-1)}
+ \tau^{t-l}
\right)
\end{equation}
It is not clear to me how they optimize the parameters, but I think they use a similar method as in \cite{Gomez2010}.



\subsection{Time-sensitive preferential attachment}
In \cite{Wang2012e} authors make two observations. On the one hand, that distance between its posts follows an upper-truncated Pareto distribution. On the other hand, that threads grow faster when they are featured in the front page (or some sections showing the top discussions at that moment) and they slow down once they disappear from the front page. From this, they propose a model that models the growth of a thread in a given forum. As for the structure, they use preferential-attachment. It is the only existing model that includes real time.

\subsubsection*{Parameter estimation}
Their model has two parts: a upper-truncated distribution to model the time of response and the preferential attachment to model the structure.
Authors use their Maximum Likelihood estimators, which are both known in the literature.

\subsection{Limitations of current models}
There are two aspects that might me improved:

\begin{itemize}
	\item Time is only poorly combined with the structure in \cite{Wang2012e}. Actually in this model time is independent of the structure and vicecersa. As suggested in \cite{Gomez2012a} (Conclusions) combining both time and structure is an interesting line of research. Besides, there are probably other ways of consider time.
	
	\item These models estimate their parameters once and therefore very different threads are summarized with common parameters. However, imagine that we learned our parameters from a set of threads $\mathbf{G}$ and now we want to make predictions on a particular new thread $G^*$, that is, we want to compute $p(x \sim i | G_{1:t-1}^*, \mathbf{G})$. There is a lot to be learned from the particular ongoing dynamics of the conversation until time $t$, and making predictions based on the globally estimated parameters will not be flexible enough to adapt the prediction to the last observations. Bayesian inference is a natural way to do this since we will be constantly updating our believes every time a new observation (post) arrives. The challenge here is its computational cost, so we should  
	probably work with fast approximations to the posterior
\end{itemize}
% second goal: predictions
Growth graph models are stochastic processes governed by a set of parameters. Once these parameters are estimated, the model does not change anymore. Yet, threads can vary a lot and therefore the parameters that explain the global dataset do not usually explain the specific dynamics of a particular discussion. 

\section{Preferential attachment, recency and time}
Our final goal is to introduce time in a model similar to that of \cite{Gomez2012a}. 
To begin with, we propose a basic model with preferential attachment and recency (no root bias). We assume we know the total number of posts and that they are added sequentially. The probability of replying to post $k$ is:

\begin{equation}
p(x \sim k | G) 
=
\frac{
	\overbrace{
		d_{k}^{\alpha}
	}^{Popularity}
	\times
	\overbrace{
		h_{k}^{\beta}
	}^{Depth} 
	\times
	\overbrace{
		\tau_{k}^{-\lambda}
	}^{Recency}
}
{
	\sum_{k=1}^{N}
	d_{k}^{\alpha}
	\times
	h_{k}^{\beta} 
	\times
	\tau_{k}^{-\lambda}
}
\end{equation}

Figure~\ref{fig:lumbreras_samples} shows some examples of graphs generated with this model (see the Appendix for more examples).
\begin{figure}
	\centering
	\subfloat[$\alpha=0.75\quad\beta,\lambda =0.10$]{\includegraphics[width=0.33\textwidth]{lumbreras_alpha}}
	\subfloat[$\beta=0.75\quad\alpha,\lambda =0.10$]{\includegraphics[width=0.33\textwidth]{lumbreras_beta}}
	\subfloat[$\lambda=0.75\quad\alpha,\beta =0.10$]{\includegraphics[width=0.33\textwidth]{lumbreras_lambda}}
	\caption{Samples of graphs generated by our base model.}
	\label{fig:lumbreras_samples_3}
\end{figure}
The total likelihood of the graph $G_t$ at a given snapshot $t$ is the product of likelihoods of the individual observations across all the snapshots:
\begin{align}
\mathcal{L}(\alpha, \beta, \lambda | G_t) 
&= 
\prod_{i=1}^N
\prod_{t=2}^{|\pi_i|}
\frac{
	d_{k,t}^{\alpha}
	\times
	h_{k}^{\beta} 
	\times
	\tau_{k,t}^{-\lambda}
}
{
	\sum_{k=1}^{N}
	d_{k,t}^{\alpha}
	\times
	h_{k}^{\beta} 
	\times
	\tau_{k,t}^{-\lambda}
}
\end{align} 

We generated one hundred of artificial graphs with some fixed parameters $\alpha, \beta$ and $\lambda$. To recover the maximum likelihood estimators of the parameters, we performed a grid search computing the likelihood for a different combinations of $\alpha, \beta$ and $\lambda$. Then we generate another set of artificial graphs and visually check whether they look like the original ones. We note that the grid search often recovers a different $\beta$. Surprisingly for us, the graphs generated with this $\beta$ do look like the original ones.

\subsection{Adding time to growth models}
Time has already been considered in \cite{Wang2012e}. However, Wand et al. consider that time is independent on the structure. Yet, our observations suggest that structure can affect speed of conversations. For instance, a chain where only two users participate seems to be much faster than many users replying to a main post.

\text{\textbf{Initial idea:}} the idea is to keep the structural level as it is and to adding a separated temporal level. The structural level will be in charge of generating the structure, while the temporal level will be in charge of telling the time delay between a reply and its parent. This delay should depend on structural information such as the depth of the parent or the average delay in the chain. I think a model like this should be doable, even if parameter estimation has to be done by grid search.


\section{Towards an online predictive model}
\footnote{Interesting reading: Flam, F.D. (2014, September 29) \href{http://www.nytimes.com/2014/09/30/science/the-odds-continually-updated.html?_r=1}{The Odds, Continually Updated.} \textit{The New York Times.}. Actually the system they use to track people use Monte Carlo simulations.}
So far we have presented models that, given some parameters, generate graphs that are similar to real graphs. These models reproduce some global measures of the real threads such as their degree distribution, the percentage of posts that reply to the root, the average depth of a thread and so on. Unfortunately, this models say little about a particular discussion. Imagine, for instance, that we estimate the parameters of a forum where all threads are perfect stars (every post is a reply to the root); given a new ongoing thread, we can use the estimated parameters to predict the post which is most likely to get the next reply. However, if the thread is not like the others because, for instance, two users started debating creating a chain in the tree, then the predictions with the estimated parameters will miserably fail. The question is: \textbf{can we predict the evolution of a graph given the observation on the recent evolution of this graph?}

\text{\textbf{Initial idea:}} Intuitively, I think it is possible. When visualizing the evolving graphs of our threads we see some clear patterns. For instance, a chain where to users as replying each other $a-b-a-b$ is likely to evolve to  $a-b-a-b-a$. The challenge then is to formalize this is a model. Bayesian statistics seems the ideal candidate for this kind of reasoning. However, we will find computational challenges.  

\subsection{Posterior distribution of paremeters}
Take our base model with \textit{preferential attachment} and \textit{recency}. Our bayesian idea is to get a first maximum likelihood estimation of the parameters by grid search (if an analytical solution is not possible). We will center our \textit{prior distribution} on this estimations $\alpha_0, \beta_0, \lambda_0$. Then, for every new graph $G_t$, we can get the posterior distribution of the parameters for \textit{this} conversation:

  \begin{align}
  p(\alpha, \beta, &\lambda | G_t) \propto
  \mathcal{L}(\alpha, \beta, \lambda | G_t) 
  \times
  \mathcal{N}(\alpha | \alpha_0)
  \mathcal{N}(\beta | \beta_0)
  \mathcal{N}(\lambda | \lambda_0)
  \end{align}
  
where we chose normal priors. The posterior distribution is obtained by multiplying the likelihood by the priors. We would like to choose conjugate priors so that the posterior is analytically tractable. However there is no such prior for our likelihood. Thus, we have two ways to get the posterior: (a) Sampling techniques (e.g.: Metropolis, Gibbs) or (b) Approximations (e.g.: Laplace, Variational Inference).\footnote{I have already implemented a Metropolis-Hastings estimation since it is very simple. The drawback is that it is very slow since, for every sample, the likelihood over all the trees must be recomputed. However, if I take the MLE as priors but then execute M-H for a single tree (since the want to infer the parameters only for that tree!), then it should be fast enough. (je crois que ce raisonnement-ci c'est correcte, mais c'est une idée qui une idée qui m'est venue à l'esprit)}


\section{Parameters estimation}

In this section we consider several techniques to estimate the parameters, be it for a bayesian or a frequentist framework. In the bayesian case, since the posterior is not analytically tractable, we need an alternative method to get its functional form. We can either use sample methods (e.g.: Metropolis-Hastings), or approximations such as Variational Inference Laplace's method.

\subsection{Grid-search of maximum likelihood}
Grid search is simple and easy to implement, and it can even be parallelized if the dataset is very big. The drawback is that we will not be able to re-estimate the parameters on-line for a given thread so that we can adapt make predictions on $G_t$ based on $G_{1:(t-1)}$.

\subsection{Grid-search of maximum a posteriori}
If we want to work with the posterior so that we can adapt the parameters to a thread, we can similarly perform a grid search on the product of the likelihood and the prior to find the optimum parameters in the posterior.

\subsection{Metropolis-Hastings}
Metropolis-Hastings is a Monte Carlo sampling technique that get samples from the posterior distribution. Metropolis-Hastings is always possible, but three or four variables is probably too much to be practical since the sampler will take too long to explore the space properly. Neither it is practical for making predictions on $G_t$ based on $G_{1:(t-1)}$ (Really? see footnote)

\subsection{Gibbs Sampling}
Gibbs Sampling requires that the conditional distribution of one parameter given the others is a known distribution, which is not the case here. Besides, as the other sampling methods it will prevent us from  making predictions on $G_t$ based on $G_{1:(t-1)}$.

\subsection{Variational Inference}
Variational Inference is a much faster inference method that extends the idea of Expectation Maximization. Unfortunately, standard Variational Inference requires conjugate priors, which we do not have. Nonetheless, a flavor  of Variational Inference that does not require conjugate priors has been recently published.

\subsection{Laplace}
Laplace approximations approximate a density function by a Gaussian by using a Taylor expansion. Unfortunately, it requires knowing the maximum of the function (the Maximum a Posteriori), which we obviously do not know.


\newpage
\appendix
\section*{Appendices}
(Only for internal discussion)

\section{Growth steps: snapshot-based or node-based}
If we want to include time in our model, an important modeling decision that we have to take is when do we want our graph to grow. We can either (a) consider adding new nodes at snapshots that represent real time or (b) add nodes sequentially assuming that the is unit of time between any consecutive nodes.

\begin{itemize}
	\item \textbf{Snapshot-based growth}: at every snapshot the graph can only grow at the snapshots. Since between two snapshots it is likely than two nodes have got a new link, we need to model probabilities individually for every node.
    Our probability function will decide whether node $i$ has had a new link or not between the former and the new snapshot. This model can represent many nodes getting links simultaneously between two snapshots, thus giving a sense of speed to the graph. However, we can only assign one link per node and snapshot. 
	
	\item \textbf{Node-based growth}: nodes are added sequentially to the graph considering that the time between nodes is a temporal unit. To decide to which node the new one is attached we model the probability not individually but as a categorical distribution that shares the probability among the current nodes. In other words, we know a new node is being attached to the graph and we just have to decide where. Note that time here is homogeneous and there are no changes of speed. To make time heterogeneous we may extend the model by assigning a time to every node. We may even want this time be dependent on the structure and viceversa.	
\end{itemize}

In this paper I will be working with this later (and more classic) model.
\section{Synthetic threads}
Figure~\ref{fig:lumbreras_samples} shows some examples of graphs generated with our base model.
\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{lumbreras_samples}
	\caption{Samples of graphs generated by our base model.}
	\label{fig:lumbreras_samples}
	\end{figure}
\bibliographystyle{chicago} % or plain
\bibliography{/local/home/alumbreras/Documents/PhD/bibtex/library}
\end{document}


