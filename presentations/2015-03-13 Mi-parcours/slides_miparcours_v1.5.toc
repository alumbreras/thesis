\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Work done}{8}{0}{2}
\beamer@subsectionintoc {2}{1}{Blockmodeling}{9}{0}{2}
\beamer@subsectionintoc {2}{2}{Motifs}{11}{0}{2}
\beamer@subsectionintoc {2}{3}{Contexts}{13}{0}{2}
\beamer@sectionintoc {3}{Dual-view learning}{15}{0}{3}
\beamer@sectionintoc {4}{Experiments}{22}{0}{4}
\beamer@sectionintoc {5}{Conclusions}{29}{0}{5}
