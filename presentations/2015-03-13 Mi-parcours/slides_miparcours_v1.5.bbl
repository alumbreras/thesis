\begin{thebibliography}{}

\bibitem[\protect\citeauthoryear{Adamic, Zhang, Bakshy, and Ackerman}{Adamic
  et~al.}{2008}]{Adamic2008}
Adamic, L.~A., J.~Zhang, E.~Bakshy, and M.~S. Ackerman (2008).
\newblock {Knowledge sharing and yahoo answers}.
\newblock In {\em Proceeding of the 17th international conference on World Wide
  Web - WWW '08}, New York, New York, USA, pp.\  665. ACM Press.

\bibitem[\protect\citeauthoryear{Angeletou, Rowe, and Alani}{Angeletou
  et~al.}{2011}]{Angeletou2011a}
Angeletou, S., M.~Rowe, and H.~Alani (2011).
\newblock {Modelling and analysis of user behaviour in online communities}.
\newblock {\em The Semantic Web–ISWC 2011\/}.

\bibitem[\protect\citeauthoryear{Chan, Hayes, and Daly}{Chan
  et~al.}{2010}]{Chan2010}
Chan, J., C.~Hayes, and E.~Daly (2010).
\newblock {Decomposing discussion forums using common user roles}.
\newblock In {\em Proceedings of the WebSci10: Extending the Frontiers of
  Society On-Line}.

\bibitem[\protect\citeauthoryear{Cheng, Agrawal, Choudhary, Liu, and
  Zhang}{Cheng et~al.}{2014}]{Cheng2014}
Cheng, Y., A.~Agrawal, A.~Choudhary, H.~Liu, and T.~Zhang (2014, December).
\newblock {Social Role Identification via Dual Uncertainty Minimization
  Regularization}.
\newblock In {\em 2014 IEEE International Conference on Data Mining}, pp.\
  767--772. IEEE.

\bibitem[\protect\citeauthoryear{Faust and Skvoretz}{Faust and
  Skvoretz}{2002}]{KatherineFaust}
Faust, K. and J.~Skvoretz (2002).
\newblock {Comparing networks across space and time, size and species}.
\newblock {\em Sociological methodology\/}~{\em 32}, 267--299.

\bibitem[\protect\citeauthoryear{Furtado, Andrade, Oliveira, and
  Brasileiro}{Furtado et~al.}{2013}]{Furtado2013}
Furtado, A., N.~Andrade, N.~Oliveira, and F.~Brasileiro (2013).
\newblock {Contributor profiles, their dynamics, and their importance in five
  q\&a sites}.
\newblock {\em Proceedings of the 2013 conference on Computer supported
  cooperative work - CSCW '13\/}, 1237.

\bibitem[\protect\citeauthoryear{Kemp, Griffiths, and Tenenbaum}{Kemp
  et~al.}{2004}]{CharlesKemp}
Kemp, C., T.~L. Griffiths, and J.~B. Tenenbaum (2004).
\newblock {Discovering latent classes in relational data}.
\newblock {\em dspace.mit.edu\/}.

\bibitem[\protect\citeauthoryear{Maia, Almeida, and Almeida}{Maia
  et~al.}{2008}]{Maia2008}
Maia, M., J.~Almeida, and V.~Almeida (2008).
\newblock {Identifying user behavior in online social networks}.
\newblock {\em Proceedings of the 1st workshop on Social network systems -
  SocialNets '08\/}, 1--6.

\bibitem[\protect\citeauthoryear{Nowicki and Snijders}{Nowicki and
  Snijders}{2001}]{Nowicki2001}
Nowicki, K. and T.~A.~B. Snijders (2001).
\newblock {Estimation and prediction for stochastic blockstructures}.
\newblock {\em Journal of the American Statistical Association\/}~{\em
  96\/}(455), 1077--1087.

\end{thebibliography}
