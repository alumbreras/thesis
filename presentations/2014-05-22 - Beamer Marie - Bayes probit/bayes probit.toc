\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{Intro}{3}{0}{1}
\beamer@sectionintoc {2}{Regression, Binary regression and Bayesian binary regression}{8}{0}{2}
\beamer@sectionintoc {3}{Bayesian probit regression}{12}{0}{3}
\beamer@sectionintoc {4}{Experiment with synthetic data}{17}{0}{4}
