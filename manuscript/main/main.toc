\contentsline {chapter}{Contents}{1}{section*.1}
\contentsline {chapter}{\chapternumberline {1}Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Context}{4}{section.1.1}
\contentsline {subsection}{(Offline) roles}{4}{section*.2}
\contentsline {subsection}{Roles in online communities}{5}{section*.3}
\contentsline {subsubsection}{Bottom-up and top-down: detecting roles vs search roles}{5}{section*.4}
\contentsline {subsubsection}{(Positional analysis)}{5}{section*.5}
\contentsline {section}{\numberline {1.2}Graph representations}{6}{section.1.2}
\contentsline {subsection}{Interaction graphs}{6}{section*.6}
\contentsline {subsection}{Coparticipation graphs}{6}{section*.8}
\contentsline {subsection}{Discussion tree graphs}{6}{section*.10}
\contentsline {section}{\numberline {1.3}Online role detection}{7}{section.1.3}
\contentsline {subsection}{Ethnology}{7}{section*.12}
\contentsline {subsection}{Clustering}{8}{section*.13}
\contentsline {subsection}{Blockmodeling}{8}{section*.14}
\contentsline {subsection}{Triads ans motifs}{9}{section*.16}
\contentsline {subsection}{Matrix Factorization}{11}{section*.18}
\contentsline {subsubsection}{Language-based roles}{11}{section*.19}
\contentsline {subsection}{Others}{11}{section*.20}
\contentsline {section}{\numberline {1.4}Drawbacks of current methods}{11}{section.1.4}
\contentsline {section}{\numberline {1.5}Summary and contributions}{12}{section.1.5}
\contentsline {chapter}{\chapternumberline {2}Role analysis (deprecated)}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Roles in graphs}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}Structural-based roles}{14}{section.2.2}
\contentsline {section}{\numberline {2.3}Roles in online forums}{14}{section.2.3}
\contentsline {section}{\numberline {2.4}Forum analysis}{14}{section.2.4}
\contentsline {section}{\numberline {2.5}Forum dynamics}{15}{section.2.5}
\contentsline {chapter}{\chapternumberline {3}Datasets}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Datasets overview}{18}{section.3.1}
\contentsline {section}{\numberline {3.2}Forum dynamics}{19}{section.3.2}
\contentsline {subsection}{Posting activity}{19}{section*.22}
\contentsline {subsection}{Births, lives and deaths}{22}{section*.25}
\contentsline {subsection}{New users and old users}{22}{section*.26}
\contentsline {section}{\numberline {3.3}Conversation dynamics}{23}{section.3.3}
\contentsline {subsection}{Length of conversations}{23}{section*.30}
\contentsline {subsection}{Speed of conversations}{23}{section*.32}
\contentsline {subsection}{(forum analysis)Structure of conversations (triad census)}{23}{section*.34}
\contentsline {section}{\numberline {3.4}User dynamics}{28}{section.3.4}
\contentsline {subsection}{Posting}{28}{section*.37}
\contentsline {subsection}{Lifespans}{28}{section*.39}
\contentsline {subsection}{Communities}{28}{section*.41}
\contentsline {section}{\numberline {3.5}Summary}{30}{section.3.5}
\contentsline {chapter}{\chapternumberline {4}Structural-temporal neighbourhoods to characterise users}{35}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{35}{section.4.1}
\contentsline {section}{\numberline {4.2}Discussion trees}{36}{section.4.2}
\contentsline {section}{\numberline {4.3}Structural neighbourhoods}{36}{section.4.3}
\contentsline {section}{\numberline {4.4}Structural-temporal neighbourhoods}{37}{section.4.4}
\contentsline {subsection}{Order-based}{37}{section*.45}
\contentsline {subsection}{Time-based}{38}{section*.46}
\contentsline {section}{\numberline {4.5}Neighbourhood colouring and pruning}{40}{section.4.5}
\contentsline {subsection}{Colouring}{40}{section*.47}
\contentsline {subsection}{Pruning}{41}{section*.48}
\contentsline {section}{\numberline {4.6}Experiments}{41}{section.4.6}
\contentsline {subsection}{Neighbourhood extraction}{42}{section*.49}
\contentsline {subsection}{Comparing neighbourhood methods}{42}{section*.50}
\contentsline {subsubsection}{Size and frequency distribution}{42}{section*.51}
\contentsline {subsubsection}{Discrepancies}{43}{section*.54}
\contentsline {subsection}{Conversation-based clustering of users}{45}{section*.56}
\contentsline {section}{\numberline {4.7}Conclusions}{47}{section.4.7}
\contentsline {chapter}{\chapternumberline {5}Detection of user roles with thread growth models}{49}{chapter.5}
\contentsline {section}{\numberline {5.1}Introduction}{49}{section.5.1}
\contentsline {section}{\numberline {5.2}Network Growth models}{50}{section.5.2}
\contentsline {subsection}{Barabasi-Albert (1999)}{50}{section*.59}
\contentsline {subsection}{Kumar (2010)}{51}{section*.62}
\contentsline {subsection}{Gomez (2010)}{52}{section*.63}
\contentsline {subsection}{Gomez (2012)}{53}{section*.65}
\contentsline {subsection}{Wang model}{53}{section*.66}
\contentsline {subsection}{Parameter estimation}{53}{section*.67}
\contentsline {section}{\numberline {5.3}Patters of reply}{54}{section.5.3}
\contentsline {subsection}{Patterns of reply}{54}{section*.68}
\contentsline {section}{\numberline {5.4}A role-based network growth model}{56}{section.5.4}
\contentsline {subsection}{Formalization}{56}{section*.71}
\contentsline {subsection}{Expectation-Maximization}{56}{section*.72}
\contentsline {subsection}{EM for the random threads model}{57}{section*.73}
\contentsline {subsection}{Number of clusters}{58}{section*.74}
\contentsline {section}{\numberline {5.5}Experiments}{58}{section.5.5}
\contentsline {subsection}{Reproduction of structural properties}{58}{section*.75}
\contentsline {subsection}{Recommendation of posts}{60}{section*.78}
\contentsline {section}{\numberline {5.6}Open questions}{60}{section.5.6}
\contentsline {chapter}{\chapternumberline {6}Non-parametric clustering over user features and latent behavioral functions with dual-view mixture models}{63}{chapter.6}
\contentsline {section}{\numberline {6.1}Introduction}{63}{section.6.1}
\contentsline {section}{\numberline {6.2}Model description}{65}{section.6.2}
\contentsline {subsection}{Mixture models}{65}{section*.82}
\contentsline {subsection}{Dual-view mixture model}{66}{section*.83}
\contentsline {subsection}{Infinite number of clusters}{68}{section*.85}
\contentsline {section}{\numberline {6.3}Application to role detection in online forums}{69}{section.6.3}
\contentsline {subsection}{Feature view}{70}{section*.86}
\contentsline {subsection}{Behavior view}{71}{section*.87}
\contentsline {subsection}{Shared parameters}{72}{section*.88}
\contentsline {section}{\numberline {6.4}Inference}{72}{section.6.4}
\contentsline {subsection}{Predictive distribution}{73}{section*.89}
\contentsline {section}{\numberline {6.5}Experiments}{73}{section.6.5}
\contentsline {subsection}{Compared models}{74}{section*.90}
\contentsline {subsection}{Metrics}{75}{section*.92}
\contentsline {paragraph}{Metric for clustering:}{75}{section*.93}
\contentsline {paragraph}{Metric for predictions:}{76}{section*.94}
\contentsline {subsection}{Agreement between views}{76}{section*.95}
\contentsline {subsection}{Disagreement between views}{78}{section*.98}
\contentsline {paragraph}{(a) Lack of information in feature view:}{80}{section*.102}
\contentsline {paragraph}{(b) Lack of information in both views:}{80}{section*.103}
\contentsline {paragraph}{(c) Lack of information in behavior view:}{80}{section*.104}
\contentsline {subsection}{Iris dataset}{80}{section*.105}
\contentsline {subsection}{Computational cost}{82}{section*.110}
\contentsline {subsection}{Summary of the experiments}{84}{section*.111}
\contentsline {section}{\numberline {6.6}Conclusions}{84}{section.6.6}
\contentsline {section}{\numberline {.1}Chinese Restaurant Process}{86}{section.Alph0.1}
\contentsline {section}{\numberline {.2}Conditionals for the feature view}{87}{section.Alph0.2}
\contentsline {subsection}{Component parameters}{88}{section*.112}
\contentsline {subsection}{Shared hyper-parameters}{88}{section*.115}
\contentsline {section}{\numberline {.3}Conditionals for the behavior view}{90}{section.Alph0.3}
\contentsline {subsection}{Users parameters}{90}{section*.120}
\contentsline {subsection}{Component parameters}{91}{section*.122}
\contentsline {subsection}{Shared hyper-parameters}{91}{section*.125}
\contentsline {subsection}{Regression noise}{93}{section*.130}
\contentsline {section}{\numberline {.4}Sampling $\beta _{0}^\text {(a)}$}{93}{section.Alph0.4}
\contentsline {section}{\numberline {.5}Sampling $\beta _{0}^\text {(f)}$}{94}{section.Alph0.5}
\contentsline {section}{\numberline {.6}Sampling $\alpha $}{95}{section.Alph0.6}
\contentsline {chapter}{Bibliography}{97}{section*.132}
\contentsline {chapter}{\chapternumberline {A}Conclusions}{105}{appendix.A}
\contentsline {chapter}{Bibliography}{107}{section*.134}
