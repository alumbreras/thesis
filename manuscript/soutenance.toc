\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Analysis of datasets}{7}{0}{2}
\beamer@sectionintoc {3}{Role detection: three approaches}{8}{0}{3}
\beamer@sectionintoc {4}{Role detection based on structure-based}{8}{0}{4}
\beamer@sectionintoc {5}{Role detection based on conversational motifs}{9}{0}{5}
\beamer@sectionintoc {6}{Role detection based on features and behavioral functions}{11}{0}{6}
