# Probability distribution of users LIFESPANS in the final date of the dataset

# Plot the average age of the users that participated every month

library(dplyr)
library(ggplot2)
library(zoo)
library(data.table)
library(foreach)
library(doParallel)

subforums <- c(
  "france",
  "gameofthrones",
  "MachineLearning",
  "podemos",
  "TwoXChromosomes"
)



cl<-makeCluster(7)
registerDoParallel(cl)  

df <- foreach(subforum=subforums, .packages=c('dplyr', 'zoo'), .combine=rbind) %dopar% {
  load(paste0('data/df.posts.', subforum, '.rda'))
  
  # Convert dates
  df.posts <- df.posts %>% 
    mutate(date=as.numeric(date)) %>% 
    mutate(rdate = as.Date(as.POSIXct(date, origin="1970-01-01")))  %>%
    mutate(yearmon = as.yearmon(rdate))
  
  # Summarize by user
  df.users <- df.posts %>% 
    group_by(user) %>% 
    summarise(birth=min(rdate), 
              death=max(rdate), 
              nposts=n(), 
              nthreads=length(unique(thread))) %>%
    mutate(life=death-birth)
  
  n.freq <- tabulate(as.numeric(df.users$life))
  n.prob <- n.freq / sum(n.freq)
  
  data.frame(ndays = 1:length(n.freq),
             n.freq = n.freq,
             n.prob = n.freq/sum(n.freq),
             subforum = subforum)
}
stopCluster(cl)


# PDF
# saved in file with 930x300 dimensions
g <- ggplot(df, aes(x=ndays, y=n.prob)) + 
  geom_point(alpha=1, size=0.5) +
  #scale_x_log10() + 
  scale_y_log10() +
  theme_bw() +
  facet_grid(. ~subforum) +
  theme(strip.background = element_rect(fill = 'white'),
        aspect.ratio = 1) +
  xlab("days") + ylab("probability") +
  ggtitle("Lifespan distribution")
print(g)
ggsave(file='users_distribution_life.png', width=200, height=70, units='mm')


# CDF
df <- df %>% group_by(subforum) %>% arrange(desc(ndays)) %>% mutate(cum.freq=cumsum(n.prob)) %>% ungroup
#df <- df %>% group_by(subforum) %>% arrange(desc(ndays)) %>% mutate(cum.freq=cumsum(n.freq)) %>% ungroup
#df <- df %>% group_by(subforum) %>% arrange(ndays) %>% mutate(cum.freq=cumsum(n.prob)) %>% ungroup

g <- ggplot(df, aes(x=ndays, y=cum.freq)) + 
  geom_point(alpha=1, size=0.5) +
  #scale_x_log10() + 
  #scale_y_log10() +
  theme_bw() +
  facet_grid(. ~subforum, scales='fixed') +
  theme(strip.background = element_rect(fill = 'white'),
        aspect.ratio = 1) +
  xlab("days") + ylab("probability") +
  ggtitle("Probability of living more than n days")
print(g)
ggsave(file='users_distribution_life_cdf.png', width=200, height=70, units='mm')