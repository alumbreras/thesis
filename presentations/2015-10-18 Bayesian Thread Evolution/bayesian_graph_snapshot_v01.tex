\documentclass[11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{graphicx} % adjustbox loads it
\usepackage{epstopdf} % loads eps
\usepackage[normalem]{ulem}

% insert here the call for the packages your document requires
\usepackage{geometry}
\usepackage[export]{adjustbox}
\usepackage[labelfont=bf, labelsep=space]{caption}
%\usepackage{subcaption}
\usepackage{subfig}
\usepackage{amssymb}
\usepackage{amsmath} 

\usepackage{natbib} % enables author year and other citation styles
\usepackage[bookmarks,bookmarksopen,bookmarksdepth=2]{hyperref} % active links
\hypersetup{backref,
	colorlinks=true,
	citecolor=blue,
	linkcolor=blue}

\usepackage{tikz} % graphics,
\usetikzlibrary{fit,positioning} % tikz elements positioning
\usepackage{soul} % annotations


\DeclareGraphicsExtensions{{.pdf},{.png},{.jpg}}
\graphicspath{ {./img/} }
\DeclareMathOperator*{\argmax}{arg\,max}

\title{Bayesian thread evolution}
\author{Alberto Lumbreras}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
\section{Introduction}
A random graph is a graph generated by a stochastic process. The goal of a  random graph is to reproduce some interesting properties of a real graph. Ideally, the stochastic process (the model), is parametrized by a small set of parameters and is able to generate graphs that imitate a large set of properties of the real graphs under study.

The simplest model of random graph is the Erdös-Reny model which, given a set of nodes $n$, creates a link between any pair of nodes with probability 
$p$ (Figure~\ref{fig:random-graphs}). The Barabasi-Albert model reproduces the power-law distribution of node degrees that is common in many social networks. In the context of trees, the Galton-Watson process is a branching stochastic process that models phenomena such as the extinction of a family name.

Some of this models, such as Barabasi-Albert or the Galton-Watson, construct their graphs sequentially by adding one node at a time. Not only they model the final stage of a graph, but also the intermediate steps. However, they:

\begin{itemize}
	\item depend on a set of parameters that are fixed.
	\item Cannot make predictions on the graph at time $t+1$ based on the graph evolution until $t$.
\end{itemize} 

Our goal is to create a model that is not only generative but also predictive (it gives a probability to any possible event at $t+1$), and that makes these predictions based on the evolution of the graph until $t$.

\begin{figure}
	\subfloat[Erdös-Renyi]{\includegraphics[width=0.5\textwidth]{erdos}}
	\subfloat[Barabasi-Albert]{\includegraphics[width=0.5\textwidth]{barabasi}}
	\caption{Random graphs}
	\label{fig:random-graphs}
\end{figure}


\section{Generative model for discussion threads}
The most related work is that of \cite{Gomez2010}. Gomez et al model the evolution of discussion threads and reproduce a good number of properties observed in real forums. Their model is based on Barabasi-Albert. The main difference is that they assign different parameters to the root of the conversation, allowing the node to me more atractive than the other posts. The probability that a new post will reply to post $k$ is modeled as:
\[
p(\pi_t = k | \boldmath{\pi}_{(1:t-1)}) 
\propto 
(\beta_k d_{k,t})^{\alpha_k}
\]
where $\beta_k$ is the root bias and $\alpha_k$ is the exponent of the preferential attachment factor, and is different for the root. 
\begin{align}
\alpha_k = 
\begin{cases}
	\alpha_1 & \text{ for } k=1 \\	
	\alpha_c & \text{for } k \in \{2,...,t\}
\end{cases}\notag\\
\beta_k = 
\begin{cases}
\beta & \text{ for } k=1 \\	
1 & \text{for } k \in \{2,...,t\}
\end{cases}
\end{align}

for every forum under consideration, the authors find the Maximum Likelihood Estimation of $\alpha_k$ and$\beta_k$ and then generate thousands of simulated conversation graphs. Figure~\ref{fig:Gomez} shows some examples.

	\begin{figure}
		\centering
		\subfloat[real]{\includegraphics[width=0.45\textwidth]{gomez_real}}\hfill
		\subfloat[synthetic]{\includegraphics[width=0.45\textwidth]{gomez_synthetic}}
		\caption{Random grahs for discussion threads. Gómez-Kappen-Kaltenbrunner}
		\label{fig:Gomez}
	\end{figure}

\section{A predictive model for discussion threads}
So far we have presented models that, given some parameters, generate graphs that are similar to real graphs. However, examination of how conversations evolve make us think that we could even predict the evolution of a graph in the short term. Think, for instance, on a cascade. It looks like a conversation of like a-b-a-b is likely to evolve to "a-b-a-b-a" since it indicates an ongoing debate. It seems also that posts are likelier to get a response when they are more recent.

In this section, we propose a model that predicts the evolution of a graph given the observation on how the graph has evolved until now.

We consider three strategies:

\begin{itemize}
	\item (a) probability that \textbf{post $i$ will have a response before the next snapshot}: computing this probability will allow to make post recommendations and even to generate a graph step by step. However, as a generator it can only append one reply per post and snapshot, therefore limiting the speed of the graph.
	\item (b) probability that \textbf{post $i$ will have exactly $k$ new descendants before the next snapshot}. While this will allow to recommend the hottest post, it cannot be used as a generative model at all since it says nothing about the structure of the descendants: do they form a chain, a star...?
	\item (c) probability that \textbf{next post $j$ will be a response to $i$}. Here, real time disappears and snapshots are those moments where a new post is attached to the thread. It says nothing about whether a post is going to get a response. This model would rather give a distribution of probability over all the possible posts assuming that another post will arrive.
\end{itemize}


In the following the propose a model for the case (a). 

\subsection{Likelihood}
We model the probability of a given post to get a reply before the next snapshot $T$. Let $l_i$ denote the depth of post $i$. Let $d$ denote the number of current replies. Let $t$ denote the elapsed time between the creation of the post and the next snapshot $T$. 

We propose the likelihood of post $i$ of getting at least a reply before the next snapshot as:

\begin{equation}
p(x_{i(t+T)} =1| \theta, \lambda_l, \lambda_d, G_t) = 
\underbrace{
\overbrace{\frac{1}{\Gamma(k)\theta^{k}} \tau_{it}^{k-1} e^{-\frac{\tau_{it}}{\theta}}}^{Gamma(\tau_{it} | k,\theta)} 
}_{time}
\underbrace{
\underbrace{
\overbrace{\frac{\lambda}{l_i!} e^{-\lambda_l}}^{Poisson(l_i| \lambda_l)}
}_{chain}
\underbrace{
\overbrace{\frac{\lambda}{d_{it}!} e^{-\lambda_d}}^{Poisson(d_{it}| \lambda_d)}
}_{star}
}_{PA} %Poisson
\end{equation}

The total likelihood of the graph $G_t$ at a given snapshot $t$ is the product of likelihoods of the individual observations across all the snapshots:
\begin{align}
	\mathcal{L}(\theta, \lambda_l, \lambda_d | G_t) 
	= 
	\prod_T
	&
	\prod_{x_{i(t+T)} \in R} p(x_{i(t+T)}=1 |\theta, \lambda_l, \lambda_d, G_t)\notag\\
	&\prod_{x_{i(t+T)} \notin R} (1-p(x_{i(t+T)}=1 |\theta, \lambda_l, \lambda_d, G_t))
\end{align} 

or:
\begin{align}
\mathcal{L}(\theta, \lambda_l, \lambda_d | G_t) 
= 
\prod_T
&
\prod_{x_{i(t+T)} \in R} 
\mathcal{G}(\tau_{it} | k,\theta)
\mathcal{P}(l_i| \lambda_l)
\mathcal{P}(d_{it}| \lambda_d)
\notag\\
&\prod_{x_{i(t+T)} \notin R} 
(1-
\mathcal{G}(\tau_{it} | k,\theta)
\mathcal{P}(l_i| \lambda_l)
\mathcal{P}(d_{it}| \lambda_d)
)
\end{align}

Note that we cannot factorize the likelihood in three likelihoods, one per parameter. This will make the posterior unfactorisable as well.

\subsection{Posterior}
The posterior distribution is obtained by multiplying the likelihood by the priors. We would like to choose conjugate priors so that the posterior is analytically tractable. However there is no such prior for this likelihood. Assuming Gamma priors over each parameter we get:
\begin{align}
p(\theta, \lambda_l, &\lambda_d | G_t) \propto
\mathcal{L}(\theta, \lambda_l, \lambda_d | G_t) 
	\times
	\mathcal{G}(\theta | \theta_0)
	\mathcal{G}(\lambda_l | \lambda_0)
	\mathcal{G}(\lambda_d | \lambda_0)
\end{align}


\section{Inference}

Since the posterior is not analytically tractable, we need an alternative method to get its functional form. We can either use sample methods (e.g.: Metropolis-Hastings), or approximations such as Variational Inference Laplace's method.

In the following sections we analyze some of these alternatives.

\subsection{Metropolis-Hastings}

\subsection{Laplace}

\subsection{Variational Inference}

\newpage
\appendix
\section*{Appendices}
These appendices illustrate why our likelihood has no conjugate prior\footnote{Related question: http://stats.stackexchange.com/questions/177957/bayesian-link-prediction-problem-advice-to-deal-with-unfamiliar-likelihood}
\section{Equiprobable links}
If all links are equiprobable, the likelihood follows a Bernoulli distribution:
\begin{equation}
\mathcal{L}(\theta; G) = \theta^k (1-\theta)^{n-k}
\end{equation}
and then its conjugate prior is a Beta and therefore when can easily access to the posterior:
\begin{align}
	p(\theta | G) \propto~& \theta^k (1-\theta)^{n-k}
	\times 
	\text{Beta}(\alpha, \beta)\notag\\
	=~& \text{Beta}(\alpha+\sum x_i, \beta + n - \sum x_i)
\end{align}

\section{Non-equiprobable links}
If the probability of a link depends on some properties of the node (e.g.: its degree) then the likelihood becomes:
\begin{align}
	\mathcal{L}(\theta; G) = \prod_{i \in L} \theta_i \prod_{j \notin L} (1-\theta_j)
\end{align}
but this model makes no sense since we would be making $n$ inferences based on one observation for each.

\section{Feature dependent probability of links}
Imagine that the probability of a node to get a new link depends on its features (e.g.: its degree). Then we would have:

\begin{align}
		\mathcal{L}(\alpha; G) = \prod_{i \in L} d_i^\alpha \prod_{j \notin L} (1-d_j^\alpha)
\end{align}
Unfortunately we do not have a prior for $\alpha$ such that the posterior gives a known probability distribution. Even if we used a distribution from the exponential family such as the Poisson, we would had: 
\begin{equation}
\mathcal{L}(\lambda; G) =
\prod_{i \in L}
\frac{\lambda}{d_i!} e^{-\lambda}
\prod_{j \notin L}
(1-\frac{\lambda}{d_j!} e^{-\lambda})
\end{equation}
which has another strange from for which we do not have a conjugate prior.

Thus, we have no choice but to use a basic Metropolis-Hastings sampling algorithm to get the posterior distribution over the parameters.
%TODO example with R


\bibliographystyle{chicago} % or plain
\bibliography{/local/home/alumbreras/Documents/PhD/bibtex/library}
\end{document}


