# Plots participation and co-participation matrices
# Plots graph of users co-participations in threads
#
# author: Alberto Lumbreras
#
#https://www.reddit.com/r/CasualConversation
#https://www.reddit.com/r/europe/
#https://www.reddit.com/r/datascience
#https://www.reddit.com/r/science/
#https://www.reddit.com/r/france
#https://www.reddit.com/r/catalunya
#https://www.reddit.com/r/es
####################################################

library(RSQLite)
library(dplyr)
library(igraph)
library(reshape2)
library(rgexf)

source('./R/utils/extract_from_db.r')

# Load data
dataset <- "reddit"
forum <- 'podemos'
con <- dbConnect(dbDriver("SQLite"), 
                 dbname = paste0("./data/", dataset, ".db"))

# Threads in forum
thread.ids <- dbGetQuery(con,  paste0("select threadid from threads where forum='", forum, "'"))

# Users in these threads
df <- data.frame()
for(i in 1:nrow(thread.ids)){ 
  thread.users <- dbGetQuery(con,  paste0("select user from posts where thread='", thread.ids[i,], "'"))
  thread.users$thread <- thread.ids[i,]
  df <- rbind(df, thread.users)
}

# It does not work, I don't know why
#by_user_thread <- count(df, user, thread)
by_user_thread <- group_by(df, user, thread)  %>% tally()

# participation matrix
par(mfrow=c(1,1))
user_thread_matrix <- acast(by_user_thread, user ~ thread, fun.aggregate = length)
user_thread_matrix <- ifelse(user_thread_matrix>0, 1,0)
den <- mean(user_thread_matrix)*100
cat("Density (%): ", den)
image(user_thread_matrix)
title(paste0("Participation matrix (density: ",  round(den, digits=2), "%)"))

# Cooccurrences matrix
cooccur <- user_thread_matrix%*%t(user_thread_matrix)
cooccur <- ifelse(cooccur>0, 1,0)
den <- mean(cooccur)*100
cat("Density (%): ", den)
image(cooccur)
title(paste0("Co-occurrence matrix (density: ",  round(den, digits=2), "%)"))


# How many people (%) knows every user?
reach <- rowMeans(cooccur)*100
hist(as.vector(reach), breaks=1000, probability = TRUE)

# Graph
#############################################################
# remove root
cooccur <- cooccur[colnames(cooccur)!="root", colnames(cooccur)!="root"]

par(mfrow=c(1,1))
g <- graph.adjacency(cooccur, mode = "undirected", diag = FALSE)

# Remove micro-components (probably threads that have just started at the end of January)
memberships <- components(g)$membership
g <- induced_subgraph(g, memberships==1)

# Save to file for Gephi
fname <- paste0(forum, '.edges')
write_graph(g, fname, format = c("edgelist"))

# Plot
degrees <- degree(g)
q_degree <- quantile(degrees, 0.5)
V(g)$label <- V(g)$name
V(g)$label[degree(g)<10000] = ""
la <- layout_with_fr(g)
memberships.eigen <- cluster_leading_eigen(g)$membership  
table(memberships.eigen)
plot(g,
     layout = la, 
     vertex.color = memberships.eigen,
     vertex.size = ifelse(degree(g)>100, 1+0.5*tanh(degrees-q_degree), 0),
     vertex.label.cex = ifelse(degree(g)>100, 1+0.5*tanh(degrees-q_degree), 0),
     edge.width = 0.2, 
     edge.arrow.size=0.02,
     asp=9/16,
     margin=-0.15)


