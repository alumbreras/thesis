\begin{thebibliography}{}

\bibitem[\protect\citeauthoryear{Bickel and Scheffer}{Bickel and
  Scheffer}{2004}]{SteffenandTobiasScheffer}
Bickel, S. and T.~Scheffer (2004).
\newblock {Multi-view clustering}.
\newblock In {\em Proc. - Fourth IEEE Int. Conf. Data Mining, ICDM 2004}, pp.\
  19--26.

\bibitem[\protect\citeauthoryear{Caruana}{Caruana}{1997}]{Caruana1997}
Caruana, R. (1997).
\newblock {Multitask Learning}.
\newblock {\em Mach. Learn.\/}~{\em 75}, 41--75.

\bibitem[\protect\citeauthoryear{Chan, Hayes, and Daly}{Chan
  et~al.}{2010}]{Chan2010}
Chan, J., C.~Hayes, and E.~Daly (2010).
\newblock {Decomposing discussion forums using common user roles}.
\newblock In {\em Proc. WebSci10 Extending Front. Soc. On-Line}.

\bibitem[\protect\citeauthoryear{Cheng, Agrawal, Choudhary, Liu, and
  Zhang}{Cheng et~al.}{2014}]{Cheng2014}
Cheng, Y., A.~Agrawal, A.~Choudhary, H.~Liu, and T.~Zhang (2014, December).
\newblock {Social Role Identification via Dual Uncertainty Minimization
  Regularization}.
\newblock In {\em 2014 IEEE Int. Conf. Data Min.}, pp.\  767--772. IEEE.

\bibitem[\protect\citeauthoryear{Golder}{Golder}{2003}]{Golder2003}
Golder, S.~A. (2003).
\newblock {\em {A Typology of Social Roles in Usenet}}.
\newblock Ph.\ D. thesis, Harvard University.

\bibitem[\protect\citeauthoryear{G\"{o}r\"{u}r and Rasmussen}{G\"{o}r\"{u}r and
  Rasmussen}{2010}]{Gorur2010}
G\"{o}r\"{u}r, D. and C.~E. Rasmussen (2010).
\newblock {Dirichlet process gaussian mixture models: Choice of the base
  distribution}.
\newblock {\em J. Comput. Sci. Technol.\/}~{\em 25\/}(July), 653--664.

\bibitem[\protect\citeauthoryear{He and Lawrence}{He and
  Lawrence}{2011}]{He2011}
He, J. and R.~Lawrence (2011).
\newblock {A graph-based framework for multi-task multi-view learning}.
\newblock In {\em Proc. 28th Int. Conf. Mach. Learn. ICML 2011}, pp.\  25--32.

\bibitem[\protect\citeauthoryear{Labatut, Dugu\'{e}, and Perez}{Labatut
  et~al.}{2014}]{Labatut2014}
Labatut, V., N.~Dugu\'{e}, and A.~Perez (2014).
\newblock {Identifying the Community Roles of Social Capitalists in the Twitter
  Network}.
\newblock In {\em Int. Conf. Adv. Soc. Netw. Anal. Min.}

\bibitem[\protect\citeauthoryear{Neal}{Neal}{1998}]{Neal1998}
Neal, R.~M. (1998).
\newblock {Markov Chain Sampling Methods for Dirichlet Process Mixture Models}.
\newblock Technical report, University of Toronto.

\bibitem[\protect\citeauthoryear{Rasmussen}{Rasmussen}{2000}]{Rasmussen2000a}
Rasmussen, C.~E. (2000).
\newblock {The Infinite Gaussian Mixture Model}.
\newblock In {\em Adv. Neural Inf. Process. Syst.}, pp.\  554--560.

\bibitem[\protect\citeauthoryear{Sun}{Sun}{2013}]{Sun2013}
Sun, S. (2013).
\newblock {A survey of multi-view machine learning}.
\newblock {\em Neural Comput. Appl.\/}~{\em 23}, 2031--2038.

\bibitem[\protect\citeauthoryear{Welser, Cosley, Kossinets, Lin, Dokshin, Gay,
  and Smith}{Welser et~al.}{2011}]{Welser2011}
Welser, H.~T., D.~Cosley, G.~Kossinets, A.~Lin, F.~Dokshin, G.~Gay, and
  M.~Smith (2011, February).
\newblock {Finding social roles in Wikipedia}.
\newblock In {\em Proc. 2011 iConference - iConference '11}, New York, New
  York, USA, pp.\  122--129. ACM Press.

\bibitem[\protect\citeauthoryear{Welser, Gleave, Fisher, and Smith}{Welser
  et~al.}{2007}]{Welser2007}
Welser, H.~T., E.~Gleave, D.~Fisher, and M.~A. Smith (2007).
\newblock {Visualizing the Signatures of Social Roles in Online Discussion
  Groups Finding Social Roles in Online Discussion}.
\newblock {\em J. Soc. Struct.\/}~{\em 8\/}(2), 1--32.

\bibitem[\protect\citeauthoryear{Xu, Tao, and Xu}{Xu et~al.}{2013}]{Xu2013}
Xu, C., D.~Tao, and C.~Xu (2013).
\newblock {A Survey on Multi-view Learning}.

\bibitem[\protect\citeauthoryear{Yang and He}{Yang and He}{2013}]{Yang2013}
Yang, H. and J.~He (2013).
\newblock {NOTAM 2 : Nonparametric Bayes Multi-Task Multi-View Learning}.
\newblock In {\em World Stat. Congr.}

\bibitem[\protect\citeauthoryear{Zhang, Ghahramani, and Yang}{Zhang
  et~al.}{2005}]{Zhang2005}
Zhang, J., Z.~Ghahramani, and Y.~Yang (2005).
\newblock {Learning multiple related tasks using latent independent component
  analysis}.
\newblock In {\em Adv. Neural Inf. Process. Syst.}, pp.\  1585--1592.

\bibitem[\protect\citeauthoryear{Zhang, Ghahramani, and Yang}{Zhang
  et~al.}{2008}]{Zhang2008}
Zhang, J., Z.~Ghahramani, and Y.~Yang (2008).
\newblock {Flexible latent variable models for multi-task learning}.
\newblock {\em Mach. Learn.\/}~{\em 73\/}(December 2007), 221--242.

\end{thebibliography}
