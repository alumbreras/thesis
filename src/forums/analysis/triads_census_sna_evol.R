# Gets the SNA in R and DO NOT plot it, but generates some basic stats
# like facets with community sizes, densities, and so on
# Creates the social network of interactions

# http://www.math-evry.cnrs.fr/publications/logiciels

triads.names <- c('003', '012', '102', '021D',
                  '021U','021C','111D','111U','030T','030C','201','120D','120U','120C','210','300') 

triads.isos <- c(1, 3, 6, 2, 4, 5, 9, 7, 11, 10, 8, 13, 12, 14, 15) # ignores the first, 003

if(FALSE){
  sapply(0:15, function(i) which(triad.census(graph_from_isomorphism_class(3, i))==1))
  
  gg <- lapply(triads.isos, function(i) graph_from_isomorphism_class(3, i))
  
  par(mfrow=c(4,4))
  sapply(gg, function(i) {plot(i, edge.arrow.size=0.1, vertex.color='black') 
    title(as.character(which.max(triad.census(i))))})
  
}

# iso to igraph
# 0   003
# 1   012
# 2   021U
# 3   102
# 4   021C
# 5   111D
# 6   021D
# 7   030T
# 8   120D
# 9   111U
# 10  201
# 11  030C
# 12  120C
# 13  120U
# 14  210
# 15  300

# graphs to iso
# 003 NA
# 012 1
# 102 3
# 021D 6
# 021U 2
# 021C 4
# 111D 5
# 111U 9
# 030T 7
# 030C 11
# 201 10
# 120D 8
# 120U 13
# 120C 12
# 210 14
# 300 15

# 003
# A,B,C, the empty graph.
#
# 012 / id iso:  1
# A->B, C, the graph with a single directed edge.
# 
# 102 / id: 3
# A<->B, C, the graph with a mutual connection between two vertices.
# 
# 021D / id:6
# A<-B->C, the out-star.
# 
# 021U / id: 2
# A->B<-C, the in-star.
# 
# 021C / id:4
# A->B->C, directed line.
# 
# 111D / id: 5
# A<->B<-C.
# 
# 111U / id:9
# A<->B->C.
# 
# 030T / id: 7
# A->B<-C, A->C.
# 
# 030C/ id: 11
# A<-B<-C, A->C.
# 
# 201 / id: 10
# A<->B<->C.
# 
# 120D / id: 8 
# A<-B->C, A<->C.
# 
# 120U/ id: 13
# A->B<-C, A<->C.
# 
# 120C / id: 12
# A->B->C, A<->C.
# 
# 210 / id:14
# A->B<->C, A<->C.
# 
# 300 / id: 15
# A<->B<->C, A<->C, the complete graph.

library(igraph)
library(data.table)
library(lubridate)
library(dplyr)
library(ggplot2)
library(zoo)
library(parallel)
library(doParallel)
library(foreach)
#library(paloma)
#library(NeMo)
#library(sna)

data.list <- list()
data.list.motifs <- list()

subforums <- c(
  "france",
  "gameofthrones",
  "MachineLearning",
  "podemos",
  "TwoXChromosomes"
)


months <- c('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12')
years <- c('2013', '2014', '2015', '2016')

deleted <- c('AutoModerator', '[deleted]')
deleted <- c('[deleted]')



options(digits=7)
if(detectCores() > 20)
{
  ncores <- 20 # mediamining server
} else {
  ncores <- 6 # local computer
}

counter <- 1
for (i in 1:length(subforums)){
  subforum <- subforums[i]
  cat("\n ",  subforum)
  
  load(paste0('data/df.posts.', subforum, '.rda'))
  
  df.posts <- df.posts %>% 
    mutate(date=as.numeric(date)) %>% 
    mutate(rdate = as.Date(as.POSIXct(date, origin="1970-01-01")))  %>%
    mutate(date=date - min(date)) %>%
    arrange(desc(date))
  
  
  parent.users <- df.posts[match(df.posts$parent, df.posts$postid),]$user
  df.posts <- df.posts %>% 
    mutate(parent.user=parent.users)
  
  dates <- seq(min(df.posts$rdate), max(df.posts$rdate), by = '1 month')
  
  cl = makeCluster(ncores, outfile="",  port=11439)
  registerDoParallel(cl)
  res <- foreach(j=2:length(dates), 
          .combine=rbind, 
          .packages = c("igraph", "dplyr")) %dopar%{
  
  #for (j in 2:length(dates)){
    cat("\n ",  subforum)
    print(dates[j])
    
    # Posts written this month
    df.posts.m <- filter(df.posts,  rdate >= dates[j-1] & rdate < dates[j])
    if(nrow(df.posts.m)==0) {
      next
    }
    
    df.edges <- df.posts.m %>% select(user, parent.user, date) %>%
      group_by(user,parent.user) %>%
      summarise(n = n()) %>%
      filter(!is.na(parent.user)) %>%
      select(from=user, to=parent.user, weight=n) %>%
      filter(!(from %in% deleted) & !(to %in% deleted))
    
    greal <- graph.data.frame(df.edges, directed=TRUE)
    
    # z-values with respect to null model
    grandomized <- lapply(1:100, function(i) rewire(greal, keeping_degseq()))
    census.real <- igraph::triad_census(greal)
    census.rand <- lapply(grandomized, function(g) igraph::triad_census(g)) %>% do.call(rbind, .)
    means <- apply(census.rand, 2, mean)
    devs <- apply(census.rand, 2, sd)
    zscores <- (census.real - means) / devs
    greater <- t(apply(census.rand, 1, function(row) census.real > row))
    pvalues <-colSums(greater)/100
    # census.triads.sna <- sna::triad.census(get.adjacency(g, sparse=FALSE))
    
    # motifs
    # generalization of triads to N vertices. Not real motifs
    #census.motifs <- motifs(g, size=3)
    
    
    #demo(paloma)
    #res <- getExceptional(get.edgelist(g, names=FALSE), kmin=2, kmax=3, directed=TRUE)
    #res <- getExceptMotif(as.adjacency(g), motif=3, directed = TRUE, model = 'EDD')
    #res <- getExceptMotif(as.adjacency(g, sparse=FALSE), motif=3, directed = TRUE, model = 'EDD')
    #print.paloma.exceptional(res)
    
    #graph_from_isomorphism_class(3, 1)
    #triads.isos <- c(0, 1, 3, 6, 2, 4, 5, 9, 7, 11, 10, 8, 13, 12, 14, 15)
    
    data.frame(id = triads.names,
               n = census.real,
               p = pvalues,
               z = zscores,
               date = dates[j],
               subforum = subforum)

    # data.list[[counter]] <- data.frame(id = triads.names,
    #                                    n = census.real,
    #                                    p = pvalues,
    #                                    z = zscores,
    #                                    date = dates[j],
    #                                    subforum = subforum)
    # #data.list.motifs[[counter]] <- data.frame(id = 0:15,
    #                                          n = census.motifs,
    #                                          date = dates[j],
    #                                          subforum = subforum)
    
    counter <- counter + 1
  }
  data.list[[length(data.list)+1]] <- res
  
}

# Plot group sizes
df <- rbindlist(data.list)
df$id <- as.factor(df$id)

if(FALSE){
  save(df, file='data/df.triads_census_evol.rda')
  load('data/df.triads_census_evol.rda')
}

#df <- df %>% filter(! id %in% c(0,1,3))
df <- df %>% filter(! id %in% c('003', '012')) %>% # often negative because of integer overflow
  filter(! id %in% c('102')) # mutual dyad too dominant
 filter(! id %in% c('021U')) # mutual dyad too dominant

g <- ggplot(df, aes(x=date, y=n, group=id, colour=id, shape=id)) + 
  scale_x_date(date_breaks = "1 month") + 
  scale_color_discrete(name='triad')+
  scale_shape_discrete(name='triad')+
  geom_point(alpha=1, size=1) + geom_line()+
  theme_bw() +
  facet_grid(subforum ~ ., scale='free') +
  theme(axis.text.x = element_text(angle = 90, hjust=1),
        strip.background = element_rect(fill = 'white'))
print(g)
ggsave(file='forum_interactions_triad_census_evol.png', width=210, height=275*6/9, units='mm')

# Plot normalized
####################
df <- df %>% group_by(date, subforum) %>% mutate(n.norm = n/max(n)) %>% ungroup()
g <- ggplot(df, aes(x=date, y=n.norm, group=id, colour=id, shape=id)) + 
  scale_x_date(date_breaks = "1 month") + 
  geom_point(alpha=1, size=1) + geom_line()+
  theme_bw() +
  facet_grid(subforum ~ ., scale='free') +
  theme(axis.text.x = element_text(angle = 90, hjust=1),
        strip.background = element_rect(fill = 'white'))
print(g)


# z-scores
###########################
g <- ggplot(df, aes(x=date, y=z, group=id, colour=id, shape=id)) + 
  scale_x_date(date_breaks = "1 month") + 
  geom_point(alpha=1, size=1) + geom_line()+
  theme_bw() +
  facet_grid(subforum ~ ., scale='free') +
  theme(axis.text.x = element_text(angle = 90, hjust=1),
        strip.background = element_rect(fill = 'white'))
print(g)
