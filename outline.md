# Planning

##1. Introduction
(status: 5%) 

(encore ouvert, je le ferai à la fin)

- Context: Social Media Analysis (SMA) (Julien)
- Overview of role analysis and detection
- What is a forum
- Aim and scope: 
    - role detection w.r.t conversation (local structure) (towards a mathematical verion of Golder?)
    - structure-based: study the impact of roles in the structure of conversations (structure is dynamic)


##2. Analyzing discussion forums
(status: 30% prèmieres figures de la prémière moitié. Pour la détection des rôles, je ferai des choses basiques avec des librairies existantes)

- Intro  
- Datasets (présentation du dataset et quelques statistiques de taille, #users...)
-  General statistics
     - Probability distributions (time to parent, parent degree,...)
     - Users dynamics (births, deaths, sleeping)
     - Communities (taille, evolution de taille...)
     - Triads (evolution du census sur l'ensemble, census par rapport à taille du thread)

- Methods for role detection     
    - Ethnology
    - Blockmodeling (example sur reddit)
    - Clustering (example sur reddit)
    - Matrix Factorization (example sur reddit?)
    - Triads  (example sur reddit?) (pont avec chapître suivant)

> Julien: 
> Finalement, n'est-ce pas une partie où tu peux indiquer :
> a) les principales techniques utilisées pour analyser les forums de     discussion (et/ou conversations, ce qui n'est pas tout à fait pareil
> b) l'application de plusieurs de ces techniques de l'état de l'art à ton propre jeu de données

> Alberto:
> Voire structure actuelle

## 3. Roles based on structural-temporal neighbourhoods
(status: 50% papier soumis)

* Clustering with triads (not triads (sur des DCA), mais structural neighbourhood with radius=1)
* Clustering with structural-neighbourhood
* Clustering with structural-temporal neighbourhoods
* Experiments
* Discussion

## 4. Roles based on structural generative models 
(status: 50%. XP's pour la recommandation assez avancés)

* Intro to generative models
* Gomez 2013
* Lumbreras 2016 (EM init with clustering from previous chapter?)
* Experiments
    - Likelihood
    - Recommendations 
   (estimation des parametres en training set,  selection du nombre de clsuters en validation set, test en test set. Pour les utilisateurs plus actives)

## 5. Dual-view clustering 
(status: 90%)

Melange de features (Ch. 2, 3) et des modeles generatives (Ch. 4) 
 Paper avec intro et conclusion adaptés

## 6. Conclusions


## Questions
 - Quelles jeux des donnés? Les plus facile pour moi c'est rester sur Reddit (N forums differents: serie télé, parti politique, actualité...). Faut-il ajouter p.ex.: Slashdot? Le seul intéret de Slashdot par rapport à reddit c'est que l'interface et differente.
 
 J > à mon avis, il est un peu tard pour ça : rester sur Reddit me paraît la solution la plus raisonnable (même si, évidemment, cela aurait été mieux de tester sur un autre jeu de données)
