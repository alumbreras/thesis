\iffalse

\RequirePackage{fix-cm}
%\documentclass[twocolumn]{svjour3}          % twocolumn
\documentclass[smallextended]{svjour3}   


\smartqed  % flush right qed marks, e.g. at end of proof

\usepackage{graphicx} % adjustbox loads it
\usepackage{epstopdf} % loads eps
\usepackage[normalem]{ulem}

% insert here the call for the packages your document requires
\usepackage{geometry}
\usepackage[export]{adjustbox}
\usepackage[labelfont=bf, labelsep=space]{caption}
%\usepackage{subcaption}
\usepackage{subfig}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath} 

\usepackage{natbib} % enables author year and other citation styles
\usepackage[bookmarks,bookmarksopen,bookmarksdepth=2]{hyperref} % active links
\hypersetup{backref,
	colorlinks=true,
	citecolor=blue,
	linkcolor=blue}

\usepackage{tikz} % graphics,
\usetikzlibrary{fit,positioning} % tikz elements positioning
\usepackage{soul} % annotations


\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\fi


\documentclass[../main/main.tex]{subfiles}

\begin{document}
\chapter{Detection of user roles with thread growth models}

\section{Introduction}
Random graph models are stochastic generators of graphs that try to reproduce the properties of a some real-world graphs. Ideally, these models should reproduce a large set of properties using a minimum number of assumptions and parameters. If the generated graphs and the real-world graphs share some relevant properties, then the proposed growth mechanism might be a reasonable approximation of the growth laws under which the real-world graphs evolve \citep{Kolaczyk2009}. Formally, a growth model is a probability distribution that quantifies the probability of an existing vertex $i$ of being chosen as the parent for a new vertex $x_t$:
\begin{align*}
p(x_t \sim i | G_{t-1}; \boldsymbol{\theta})
\label{eq:growth_model}
\end{align*} 
where $G_{t-1}$ is the state of the graph before $x_t$ is attached and $\boldsymbol{\theta}$ is the vector of model parameters.

Online discussions can be regarded as evolving tree graphs where vertices represent messages and a directed edge indicates that a message is a reply to another message. The tree starts with the root message that starts the conversation, and then evolves towards some form of tree. Different models have been proposed to account for both the way how a tree evolves and the final properties of the tree. The parameters of these models are fixed and every new vertex is assumed to chose its parent according to the current state of the graph $G_{t-1}$ and a set of fixed parameters that govern the whole process. These parameters regulate, for instance, the tendency to reply to the root, or how fast a vertex with more replies attracts more replies.  Since the parameters are fixed, these models implicitly assume that the choice of a parent is independent of the user who writes the new post.

In this chapter, we explicitly assume that posts written by different users may have different parameters. Some users, for instance, might tend to reply to the root and avoid conversations deeper in the tree. Others might tend to ignore old posts. Others might be specially attracted by popular posts. Formally, we assume that there are $K$ latent types of users and that users of type $k$ behave according to their own group parameters $\boldsymbol{\theta}_k$.  Equation~\ref{eq:growth_model} depends now on the author of post $x_t$:
\begin{align*}
p(x_t \sim i | G_{t-1}; \boldsymbol{\theta}_{z_u})
\end{align*}
where $z_u$ is the group of user $u$ and  $\boldsymbol{\theta}_{z_u}$ are the parameters of that group.

The remaining of this chapters is structure as follows: first, we recall the Preferential Attachment model and we present the thread models. Then we present our model, which finds $k$ sets of parameters for $k$ types of user. Finally, we compare both models and show that, while there is no a remarkable difference on how they reproduce structural properties of real threads, our model can be used for recommendation of posts. 

%In the following sections we describe the growth models that have been proposed to explain the growth of online conversations. A summary is shown in Table \ref{tab:growth_models}  \citep{Kumar2010, Gomez2010, Wang2012e, Gomez2012}.

\section{Network Growth models}\label{sec:related-work}
Growth models try to reproduce not only the final properties of the network but also how the network is built. 

We represent a discussion tree at time step $t$ as a vector of parents $\boldsymbol{\pi}_{(1:(t-1))} = (\pi_1,...,\pi_{t-1})$ where $\pi_i$ is the parent of the post written at the time-step $i$. Note that this representation does not lose any structural information since, for every post, this vector indicates its parent. 
At any given time $t$, we will need to extract some properties of the posts. The degree of post $i$ at time $t$, for instance, will be denoted as $d_{i,t}$, meaning the degree of post $i$ the moment \textit{before} post $t$ is added to the thread. 

In the following sections we will discuss about how to model the probability
\begin{align*}
p(\pi_t = i| \boldsymbol{\pi}_{1:t-1}) 
\end{align*}

Different probability functions give different hypothesis on the mechanisms of growth of a graph. This is why these are called \textit{growth models}.  

\subsection{Barabasi-Albert (1999)}

The \textit{preferential attachment} model proposed by \cite{Barabasi1999} is the best known growth model. The Barabasi-Albert model builds a graph by sequentially adding its vertices; once a new vertex $t$ is added to the graph it decides whether to create an edge to an existing vertex $i$ with probability

\begin{align}
	p(\pi_t = i | \boldsymbol{\pi}_{1:(t-1)}) =\frac{d_{i,t}^\alpha}{Z_t}; &\qquad
	Z_t = \sum_{j=1}^{t} d_{j,t}^\alpha
\end{align}
where $d_{i,t}$ is the degree of the vertex $i$ before vertex $t$ is added. This model reproduces a rich-get-richer phenomena controlled by the parameter $\alpha$. The particular case of $\alpha=1$ is called \textit{linear preferential-attachment} since the probabilities increase linearly with the number of degrees. Figure~\ref{fig:Barabasi-Albert} shows examples of Barabasi-Albert graphs generated with different $\alpha$. Graphs generated by the Barabasi-Albert model reproduce some interesting properties of the real networks such as a power-law degree distribution.

\begin{figure}
	\centering
	\subfloat[$\alpha=0$]{\includegraphics[width=0.33\textwidth]{barabasi_0}}
	\subfloat[$\alpha=1$]{\includegraphics[width=0.33\textwidth]{barabasi_1}}
	\subfloat[$\alpha=1.8$]{\includegraphics[width=0.33\textwidth]{barabasi_1_8}}
	\caption{Barabasi-Albert graphs with one edge created at every step.}
	\label{fig:Barabasi-Albert}
\end{figure}





\begin{table}
 \begin{center}
   \tabcolsep = 0.8\tabcolsep
   \begin{tabular}{|l|c|l|}
      \hline
     	 Authors				& $p(\pi_t = k | \boldsymbol{\pi}_{1:t-1}) \propto$ &Parameters \\
   \hline 
\cite{Barabasi1999} & $d_{k,t}^\alpha$  & degree \\
\cite{Gomez2010} & $(\beta_k d_{k,t})^{\alpha_k}$ & degree, root\\
\cite{Kumar2010} & $\alpha d_{k,t} + \tau^{t-k}$ & degree, recency \\      
\cite{Gomez2012} & $\beta_k + \alpha d_{k,t} + \tau^{t-k}$ & degree, recency, root\\
   \hline
   \end{tabular}
 \end{center}
\label{tab:growth_models}
 \caption{Growth models for online discussions}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Kumar (2010)}
\cite{Kumar2010} proposed a model that combines both \textit{preferential-attachment} and \textit{recency}. The higher the degree of a post and the later it was published, the easier for this post to attract the incoming replies. Besides, at every time step, a decision is made to stop the thread or to add a new post. Every new post choses its parent according to:
\begin{align}
p(\pi_t = i | \boldsymbol{\pi}_{1:t-1}) = \frac{\alpha d_{i,t} + \tau^{t-i}}{Z_t}
\end{align}
and the probability of stopping the thread is
\begin{align}
p(\pi_t = \emptyset | \boldsymbol{\pi}_{1:t-1}) = \frac{\delta}{Z_t} 
\end{align}



The authors report that when the alternative function $d_{i,t} \tau^{t-i}$ is used, the recency factor prevents the preferential attachment factor from generating heavy-tailed degree distributions. The choice of placing $\alpha$ as a coefficient instead of an exponent is made for mathematical convenience so that $Z_t$ does not depend on the graph structure at that particular moment:
\begin{align}
Z_t 
= 
\delta + \sum_{j=1}^{t} \alpha d_{j,t} + \tau^{t-j}
=
\delta + 2(t-1) + \beta + \frac{\tau(\tau^t+1)}{\tau-1}
\end{align}


The authors also propose an improvement of the model to account for the identity of posts authors. For a new post $v$ replying to a post $u$, its author $a(v)$ can be either $a(u)$ (a self-reply), another author $a(w)$ that has already participated in the chain from $u$ to the root, or some other new author belonging to the set of authors $A$ that have not participated in the chain:
\begin{align}
a(v) = 
\begin{cases}
a(w) & \text{ with probability } \gamma\\
a(u) & \text{ with probability } \epsilon\\
a \in A & \text{ with probability } 1 -\gamma - \epsilon 
\end{cases}
\end{align} 

The Maximum Likelihood Estimators of the parameters $\alpha, \tau, \gamma, \epsilon$ are found by a grid search.

\subsection{Gomez (2010)}
In \cite{Gomez2010} the authors combine \textit{preferential-attachment} with a \textit{bias towards the root}. The probability of choosing an existing parent $k$ is:
\begin{align}
p(\pi_t = k | \boldsymbol{\pi}_{1:t-1}) 
=
(\beta_k d_{k,t})^{\alpha_k}
\end{align}
where 
\begin{align}
\alpha_k = 
\begin{cases}
	\alpha_1 & \text{ for } k=1 \\	
	\alpha_c & \text{for } k \in \{2,...,t\}
\end{cases}\notag\\
\beta_k = 
\begin{cases}
\beta & \text{ for } k=1 \\	
1 & \text{for } k \in \{2,...,t\}
\end{cases}
\end{align}

Note that $\alpha_k$ is the preferential attachment exponent and that if $\alpha_1=\alpha_c$ and $\beta=1$ we recover the Barabasi-Albert model of preferential attachment.

The Maximum Likelihood Estimators of the parameters $\alpha, \tau, \gamma, \epsilon$ are found using the loglikelihood. The minimization is done with the Nelder-Mead algorithm (\texttt{fminsearch} in Matlab). Though Nelder-Mead can be used in non-convex functions, this loglikelihood is convex. Parameters are fitted to the dataset under study and then we generate graphs that resemble the original conversations (see Figure~\ref{fig:Gomez}).

	\begin{figure}
		\centering
		\subfloat[real]{\includegraphics[width=0.45\textwidth]{gomez_real}}\hfill
		\subfloat[synthetic]{\includegraphics[width=0.45\textwidth]{gomez_synthetic}}
		\caption{Random grahs for discussion threads. Gómez-Kappen-Kaltenbrunner}
		\label{fig:Gomez}
	\end{figure}

\subsection{Gomez (2012)}
In \cite{Gomez2012} the authors combine \textit{preferential-attachment}, a \textit{bias towards the root} and \textit{novelty}. Unlike in their former model in \cite{Gomez2010}, here they sum these factors instead of multiplying them:

\begin{align}
p(\pi_t = i | \boldsymbol{\pi}_{1:t-1}) 
\propto 
\frac{\beta_i + \alpha d_{i,t} + \tau^{t-i}}{Z_t}
\end{align}

As in \cite{Gomez2010}, Maximum Likelihood Estimators are found by Nelder-Mead optimization. Unlike their previous model, this loglikelihood is non-convex. Authors reported that, for large enough data, the optimization algorithm tends to give the same optimum for different initialization.



\subsection{Wang model}

%\subsection{Time-sensitive preferential attachment}
In \cite{Wang2012e} authors make two observations. On the one hand, that distance between its posts follows an upper-truncated Pareto distribution. On the other hand, that threads grow faster when they are featured in the front page (or some sections showing the top discussions at that moment) and they slow down once they disappear from the front page. From this, they propose a model that models the growth of a thread in a given forum. Unlike the other models, Wang's is not a generative model, but a statistical model of some properties of the discussions such as the size of a discussion or the indegree of a post.

\subsection{Parameter estimation}
A good model is a plausible hypothesis on how the network grows. Nonetheless, in order for the model to be useful, inference of its parameters should be practical. If analytical inference is not possible, at least numerical optimization methods should be relatively fast and scalable. Among the models described above, none of them is analytically tractable. 

\cite{Barabasi1999}...

\cite{Kumar2010} reports they use a simple grid search in the parameter space. A grid search consists on measuring the likelihood of the model for different values of the parameters $\alpha$ and $\tau$ and choosing the parameters that give the maximum likelihood. While the implementation of this straightforward, there is a trade-off between resolution (distance between two vectors of parameters) and computational cost.

\cite{Gomez2010} maximize the negative loglikelihood by numerical optimization. First, they note that their model is convex, that is, that any optimization method will reach the same optimum. Then they apply Nelder-Mead optimization.

\cite{Gomez2012} have a non-convex loglikelihood, and therefore different optimization runs may reach different local maximums. However, they report that, in practice, when the number of thread is large enough, a Nelder-Mead optimization always finds the same parameters.

%loglikelihood

\section{Patters of reply}
\subsection{Patterns of reply}
A tree is, in its most elementary level, an ordered set of posts that reply to some precedent post. Thus, reply patterns are the most basic pattern that we can analyse.  We analyse three basic patterns that have been useful to explain some important characteristic of the growth dynamics of discussion trees. 


\begin{figure}
\centering
\includegraphics[width=1\textwidth]{distribution_dparent}
\includegraphics[width=1\textwidth]{distribution_lag}
\includegraphics[width=1\textwidth]{distribution_tparent}
\caption{Probability distributions}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{cumdist_lag}
\includegraphics[width=1\textwidth]{cumdist_dparent}
\includegraphics[width=1\textwidth]{cumdist_tparent}
\caption{Cumulative distributions}
\end{figure}




\section{A role-based network growth model}
%Motivation
Current models consider that the likelihood of choosing a parent is independent on the user who writes the post. In other words, that the model parameters are shared by all the users. However, it seems reasonable to think that different users may behave according to different parameters (e.g.: different attraction towards the root or towards popular posts). In this section, we present a modification to the model of \citep{Gomez2012} that finds different parameters for different groups of users. We also analyse whether this feature can improve the model when used for a task of post recommendation. 

\subsection{Formalization}
For any given post $n$, let $d_n$ denote the degree of its parent; let $r_n$ be 1 if the parent of $n$ is the root, and 0 otherwise. Let $l_n$ be the number of time steps elapsed between the parent of $n$ and $n$ ($l_n \geq 1$). Let $\mathbf{X} = \{\mathbf{x}_1,...,\mathbf{x}_N\}$ be the set of posts and let $\mathbf{x}_i = \{t_i, d_i, r_i, l_i\}$  be the set of features associated to a post. Let us assume that there are there are $K$ different types, or roles, of users that behave following different parameters $\boldsymbol{\theta}_1$,...,$\boldsymbol{\theta}_K$ where  $\boldsymbol{\theta}_k = \{\alpha_k, \beta_k, \tau_k$\}. Let $z_u$ be the role of user $u$. Let $N_u$ be de set of posts written by $u$. The likelihood can be expressed as:
\begin{align}
\ln p(\mathbf{X} | \boldsymbol{\theta}) = 
\sum_{u=1}^U \sum_{n \in N_u} 
\frac{1}{Z_n}\left(\alpha_{z_u} d_{n} + \beta_{z_u}r_{n} + \tau_{z_u}^{l_n}\right)
\end{align}
where $Z_n$ is a normalization factor that guarantees that the probabilities of all possible choices sum up to one. Let $t$ be the number of time steps between the root and the post $n$. The normalization factor is computed for every new post as:

\begin{align*}
Z_n &=  
\sum_{m | t_m < t_n} \alpha_{z_m} d_m + \beta_{z_m} r_m + \tau_{z_m}^{l_m} \qquad\text{for $m$ in same thread than $n$}
\\&=
\alpha_{z_m}(2t -1) + \beta_{z_m} + \frac{\tau_{z_m}(\tau_{z_m}^t-1)}{\tau_{z_m}-1}
\end{align*}
where $z_m$ denote the cluster of the user who wrote the post $m$. Note that this sum does not depend on the structure of the discussion.

\subsection{Expectation-Maximization} 
We want to estimate the parameters of each role $\boldsymbol{\theta}_1,...,\boldsymbol{\theta}_K$ and the latent role of every user $z_1,...,z_U$. If there was one one group of $\boldsymbol{\theta}$ there would be no $\mathbf{Z}$ and we could use Nelder-Mead to find the parameters. However, if there are different groups then the optimization of the parameter will depend on the group since the parameters will be optimized considering who belongs to that group. This is a classic scenario that can be solved by Expectation Maximization (EM). In EM, we do an iterative optimization over the parameters $\boldsymbol{\theta}_1,...,\boldsymbol{\theta}_K$ and the class assignments $\boldsymbol{z}_1,...,\boldsymbol{z}_U$ until the likelihood converges. In particular, we maximize a lower bound of the loglikelihood:
\begin{align}
\ln p(\mathbf{X} | \boldsymbol{\theta}) 
\leq
\overbrace{
\underbrace{
\sum_{\mathbf{Z}} q(\mathbf{Z})\ln p(\mathbf{X, \mathbf{Z} | \boldsymbol{\theta}})
}_{\mathbb{E}_\mathbf{Z}\left[\ln p(\mathbf{X, \mathbf{Z} | \boldsymbol{\theta}})\right]} 
-
\underbrace{
\sum_{\mathbf{Z}}
q(\mathbf{Z}) 
\ln q(\mathbf{Z})
}_{entropy~H(\mathbf{Z})}
}^{\mathcal{L}(q, \boldsymbol{\theta})}
\end{align}
where $q$ is some arbitrary probability distribution. The EM algorithm starts by some initial values of $\boldsymbol{\theta}$. In the E-step, we adjust the function $q(\mathbf{Z})$ so that it maximizes the lower bound assuming that the parameters are fixed. This happens when $q(\mathbf{Z})$ is the posterior:
\begin{align}
q(\mathbf{Z}) = p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta}) 
\end{align}
Moreover, in that case the lower bound $\mathcal{L}(q,\boldsymbol{\theta})$ reaches the true loglikelihood $\ln p(\boldsymbol{X} | \boldsymbol{\theta})$. In the M-step we optimize the lower bound with respect to the parameters. We will now make the $\boldsymbol{\pi}$ parameters of the prior distribution $p(\mathbf{Z} | \boldsymbol{\pi})$ explicit. Since we fixed $q(\mathbf{Z})$, the entropy term is constant, and thus we just have to maximize the first term. After we plug-in the recently computed $q(\mathbf{Z})$ and using the chain rule, we obtain:
\begin{align*}
\argmax_{\boldsymbol{\theta}, \boldsymbol{\pi}} 
\underbrace{
\sum_{\mathbf{Z}} p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})
\overbrace{
\left(\ln p(\mathbf{Z} | \boldsymbol{\pi})  + \ln p(\mathbf{X  | \mathbf{Z}, \boldsymbol{\theta}})\right)
}^{\ln p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})}
}_{\mathbb{E}_\mathbf{Z}\left[\ln p(\mathbf{X, \mathbf{Z} | \boldsymbol{\theta}})\right]} 
\end{align*}

We repeat the E and the M step until the improvement in the likelihood is lower than some threshold $\epsilon$.

\subsection{EM for the random threads model}
In this section we provide the exact equations to maximize in the E and M steps for our model.
Let $\mathbf{X}_u$ the submatrix of $\mathbf{X}$ composed of all posts written by user $u$. Let $\mathbf{Z} = \{\mathbf{z}_1,...,\mathbf{z}_U\}$ be the indicators matrix where $\mathbf{z}_i=\{z_{i1},...,z_{iK}\}$ and where $z_{ik}$ is one if user $i$ belongs to group $k$ and zero otherwise. For, the M-step, the expectation of the complete loglikelhood is:
\begin{align}
\mathbb{E}\left[
\ln p(\mathbf{X, Z} | \boldsymbol{\theta}) 
\right]
&= 
\mathbb{E}\left[
\sum_{u=1}^U 
\sum_{k=1}^K 
z_{uk}
\left\lbrace\ln \pi_k + 
\ln p(\mathbf{X}_u | \boldsymbol{\theta}_k)\right\rbrace
\right]
=
\sum_{k=1}^K 
\sum_{u=1}^U 
\mathbb{E}\left[z_{uk}\right]
\left\lbrace
\ln \pi_k + 
\ln p(\mathbf{X}_u | \boldsymbol{\theta}_k)
\right\rbrace
\label{eq:m-step}
\end{align}
where, for a given cluster $k$, each $\mathbf{X}_u$ contributions proportionally to $\mathtt{E}[z_{uk}]$ being considered are those of users that belong to that cluster. In the E-step we update the posterior
\begin{align}
p(\mathbf{Z} | \mathbf{X},\boldsymbol{\theta})
=
\frac{
p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})
}
{
\sum_{\mathbf{Z}} p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})
}
=
\frac
{\prod_{u=1}^U\prod_{k=1}^K  p(\mathbf{X}_u | \boldsymbol{\theta}_k)^{z_{uk}}}
{\sum_{\mathbf{Z}} \prod_{u=1}^U\prod_{k=1}^K  p(\mathbf{X}_u | \boldsymbol{\theta}_k)^{z_{uk}}}
\end{align}
which can be easily factorized by users, and then we can obtain the expected value for each $z_{uk}$:
\begin{align}
\mathbb{E}[z_{uk}] 
=
\sum_{z_{uk}} z_{uk} 
\frac
{\pi_k p(\mathbf{X}_u | \boldsymbol{\theta}_k)}
{\sum_{k=1}^K \pi_k p(\mathbf{X}_u | \boldsymbol{\theta}_k)}
=
\frac
{\pi_k p(\mathbf{X}_u | \boldsymbol{\theta}_k)}
{\sum_{k=1}^K \pi_k p(\mathbf{X}_u | \boldsymbol{\theta}_k)}
\label{eq:e-step}
\end{align}
where the likelihood $p(\mathbf{X}_u | \boldsymbol{\theta}_k)$ can be also factorised:
\begin{align}
   p(\mathbf{X}_u | \boldsymbol{\theta}_k) = \prod_{n \in N_u} p(\mathbf{x}_n | \boldsymbol{\theta}_k)
\end{align}

The E-step is done with Equation~\ref{eq:e-step} and the M-step is done with Equation~\ref{eq:m-step} Since due to the form of our likelihood the equation cannot be analytically maximized, we use Nelder-Mead optimization.

\subsection{Number of clusters}
In order to chose a number of cluster we validate the likelihoods of the model for different $k$ in a validation set and chose the model with more loglikelihood in this data (log predictive density). For a discussion on other possible methods see \citep{BDA} (Chapter 7).

\section{Experiments}
In this section, we validate our model for two different tasks. First, we analyse whether our model explain some structural properties of discussion threads better than \cite{Gomez2012}. Second, we compare both models for the task of recommending a post to a user than joins the discussion. We do all our experiments in two Reddit forums: \texttt{podemos} and \texttt{gameofthrones}.

\subsection{Reproduction of structural properties} 
For each dataset, we estimated the parameters of \citep{Gomez2012} and our model. Then, we generated artificial threads and measured the following properties:

\begin{itemize}
\item degree distribution: number of replies to a post.
\item subtree size: number of descendants of a post.
\item size versus depth: number of posts versus length of longest chain.
\item width versus depth: maximum number of posts in a level versus depth.
\end{itemize} 

\begin{figure}
\center
\includegraphics[width=1\textwidth]{benchmark_structures_gof}
\caption{Synthetic trees vs real trees in Game of Thrones}
\end{figure}
\begin{figure}
\center
\includegraphics[width=1\textwidth]{benchmark_structures_podemos}
\caption{Synthetic trees vs real trees in Podemos}
\end{figure}

\subsection{Recommendation of posts}
Assuming that we know that a new user is going to participate in the discussion, the recommendation task is to predict to which posts he will reply, taking the one with more probability according to our likelihood function and the estimated parameters:
\begin{align*}
\argmax_k p(x \sim k)
\end{align*}
Surprisingly, the post with the maximum likelihood is always the root.
We therefore propose an order-based recommendation where we rank the posts by their likelihood. The higher the real parent is in the rank, the better the recommendation.  

Figure \ref{fig:post_rankings} the accuracy of the models for different thread sizes. Interestingly, we observe three different segments. In short threads, all models perform equally well since the parent is chosen among a small number of candidates. When we increase in thread length the parent candidates also increase and the recommendations become harder, specially for the Barabasi model which only considers the degree of the posts. Finally, in extremely long threads, recommendation accuracy resembles that of short threads becase late posts are more likely to reply to the root, which is often the post with biggest degree. 

\begin{figure}
\center
\includegraphics[width=0.5\textwidth]{benchmark_ranking_by_threadsize}%
\includegraphics[width=0.5\textwidth]{benchmark_ranking_by_chosen}
\caption{Evluation of models for post recommendation (Podemos)}
\label{fig:post_rankings}
\end{figure}

%%TODO
To guarantee that we have a minimum amount of information for learning we select those users than have at least 50 samples in training and 50 samples in test.

\section{Open questions}

\begin{itemize}
	\item Time is only poorly combined with the structure in \cite{Wang2012e}. Actually in this model time is independent of the structure and vicecersa. As suggested in \cite{Gomez2012} (Conclusions) combining both time and structure is an interesting line of research.
	
	\item These models estimate their parameters once and therefore very different threads are summarized with common parameters. However, imagine that we learned our parameters from a set of threads $\mathbf{G}$ and now we want to make predictions on a particular new thread $G^*$, that is, we want to compute $p(x \sim i | G_{1:t-1}^*, \mathbf{G})$. There is a lot to be learned from the particular ongoing dynamics of the conversation until time $t$, and making predictions based on the globally estimated parameters will not be flexible enough to adapt the prediction to the last observations. Bayesian inference is a natural way to do this since we will be constantly updating our believes every time a new observation (post) arrives. The challenge here is its computational cost, so we should probably work with fast approximations to the posterior
	
\end{itemize}


\newpage
%\appendix
\section*{Appendices}
\section*{Expectation-Maximization}

\begin{align}
\ln p(\mathbf{X} | \boldsymbol{\theta}) &=  \ln \sum_{\mathbf{Z}}  p(\mathbf{X}, \mathbf{Z}  | \boldsymbol{\theta}) \\
&=\ln \sum_{\mathbf{Z}} q(\mathbf{Z}) \frac{p(\mathbf{X}, \mathbf{Z}  | \boldsymbol{\theta})}{ q(\mathbf{Z})}\\
&\geq \sum_{\mathbf{Z}} q(\mathbf{Z}) \ln \frac{p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta)}}{q(\mathbf{Z})}
\end{align}


The equality holds when $q(\mathbf{Z})$ is the posterior $p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})$:
\begin{align}
\ln p(\mathbf{X} | \boldsymbol{\theta}) &=  \sum_{\mathbf{Z}} p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta}) \ln \frac{p(\mathbf{X, Z} | \boldsymbol{\theta})}{ p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})} \qquad \blacksquare
\label{eq:em_general}
\end{align}

%\bibliographystyle{chicago} % or plain
%\bibliography{../bibliography}
\end{document}