%\documentclass[11pt]{amsart}
\documentclass[11pt]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{Cluster-based regression}
\author{Alberto Lumbreras}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
\section{Introduction}
In the context of team management , Belbin's Team Roles theory claims that there are nine roles that workers tend to play in a project. For the project to succeed, rather than building teams with only one role Belbin theory recommends seeking and equilibria of these complementary roles.  The roles Belbin identified are Plant, Resource Investigator, Co-ordinator, Shaper, Monitor Evaluator, Teamworker, Implementer, Finisher and Specialist.

Following Belbin's work, we assume that forum users play a limit set of roles modeled as latent variables and that the composition of roles in a thread is an indicator of  its success.

In this paper, we propose a basic formalization of Belbin's model using a linear regression of the role census and apply it to predict the success of discussion threads. 

\section{Linear regression}
A linear regression model follows the equation
\begin{equation}
\mathbf{y} = \mathbf{X^T}\mathbf{w} + \mathbf{e}
\end{equation}
where $\mathbf{y}$ is the vector of outputs of dimensions $n \times D$, $\mathbf{X}$ is the matrix of input instances\footnote{Following the machine learning convention, the inputs are represented in a matrix $\mathbf{X}$ where instances are columns.} and $\mathbf{e}$ is a vector with the noise of every instance ${\epsilon_1,...\epsilon_n}$. $\mathbf{w}$ is the vectors of the coefficients to be found.

Given the inputs $\mathbf{X}$ and the outputs $\mathbf{y}$, the task is to find the vector $\mathbf{w}$ that better fits the data. Different fitness metrics and different methods to find the parameters that maximize the fitness give place to different regression algorithms
\subsection{Ordinary Least Squares}
One possible fitness measure is the Sum of Squared Errors (SSE), given by
\begin{equation*}
SSE = e^Te = (\mathbf{y}-\mathbf{X^T}\mathbf{w})^T (\mathbf{y}-\mathbf{X^T}\mathbf{w}) 
=
\mathbf{y^T}\mathbf{y} + \mathbf{w^T}\mathbf{X}\mathbf{X^T}\mathbf{w} - 2\mathbf{w^T}\mathbf{X}y
\end{equation*}
The most basic way to compute the weights of a linear regression model is the Ordinary Least Squares (OLS). OLS minimize the sum of the squared errors.

The first factor is contanst, the third factor is a first order derivative and the second factor derivative has the following form:
\begin{align*}
\frac{\partial \mathbf{w^T} \mathbf{B}\mathbf{w}}{\partial \mathbf{w}}
= (\mathbf{B} + \mathbf{B}^T) \mathbf{w}
\end{align*}

Therefore the expression of the derivative is:
\begin{equation*}
\frac{\partial SSE}{ \partial \mathbf{w}} =  2\mathbf{X}\mathbf{X^T}\mathbf{w} - 2\mathbf{X}\mathbf{y}
\end{equation*}

And the weights $w$ that minimize the SSE are:
\begin{align*}
2\mathbf{X}\mathbf{X^T}\mathbf{w} - 2\mathbf{X}\mathbf{y} &= 0 \\
\mathbf{X}\mathbf{X^T}\mathbf{w} &= \mathbf{X}\mathbf{y} \\
(\mathbf{X}\mathbf{X^T})^{-1}\mathbf{X}\mathbf{X^T}\mathbf{w} &= (\mathbf{X}\mathbf{X^T})^{-1}\mathbf{X}\mathbf{y}\\
\mathbf{w} &= (\mathbf{X}\mathbf{X^T})^{-1}\mathbf{X}\mathbf{y}
\end{align*}

\subsection{Maximum Likelihood Estimation}
The problem can be expressed in probability terms. If the noise $\mathbf{e}$
follows a normal distribution of variance $\sigma_\epsilon^2$, the equation of the linear regression can be expressed as:

\begin{equation}
\mathbf{y} \sim \mathcal{N}(\mathbf{X^Tw}, \sigma_\epsilon^2)
\end{equation}

The maximum likelihood estimation (MLE) of  $\mathbf{w}$ searches the coefficients that maximize:
\begin{align*}
\operatorname*{arg\!\max}_w p (y | w ) = \mathcal{N}(\mathbf{y} | \mathbf{X^Tw}, \sigma_\epsilon^2) = K \exp(-\frac{1}{2\sigma_\epsilon^2}(\mathbf{y-X^Tw})^T(\mathbf{y-X^Tw})
\end{align*}
which is equivalent to minimize the exponent:
\begin{align}
(\mathbf{y}-\mathbf{X^T}\mathbf{w})^T (\mathbf{y}-\mathbf{X^T}\mathbf{w}) 
\end{align}
and therefore OLS is equivalent to MLE when the noise is normally distributed.
\subsection{Bayesian estimation}
Bayesian estimation considers that $\mathbf{w}$ is a random variable with some \textit{prior} distribution.
\begin{align*}
\mathbf{y} &\sim \mathcal{N}(\mathbf{X^Tw}, \sigma_\epsilon^2) \\
\mathbf{w} &\sim \mathcal{N}(0, \Sigma_0)
\end{align*}
Since $\mathbf{w}$ is now a random variable and not a fixed parameters, bayesian estimation finds its posterior distribution, that is, its updated distribution after seeing the data:
\begin{align}
p(\mathbf{w | y}) \propto p(\mathbf{y |w}) p(\mathbf{w})
\end{align}
Since both $p(\mathbf{y |w})$ (the likelihood) and $p(\mathbf{w})$ (the prior) are normal distributions, their product is another normal distribution with a mean $\mathbf{m_p}$ and variance $\Sigma_p$.

\section{Role-based regression}

Imagine an discussion forum with $T$ threads and a population of $U$ users that participate along the different threads. Previous research has suggested that users can be classified by roles, that is, they can be grouped according to some behavioral or structural similarity in the network. Our hypothesis is that the success of a thread can be modeled as a linear combination of the number of users in every role. Moreover, we define roles merely in terms of contribution to success. Thus, users with similar contribution levels are considered to play the same role.

\subsection{Notation}

Let $\mathbf{P}$ (observed participations matrix) be an $U \times T$ matrix where every column represents a thread. $p_{ij} = 1$ if user $i$ participates in thread $j$ and $0$ otherwise. 
\[
\mathbf{P}_{U \times T} =
\begin{bmatrix}
p_{11} & p_{12} &\cdots& p_{1T} \\
p_{21} & p_{22} &\cdots& p_{2T} \\
\vdots &  & & \vdots \\
p_{U1} & p_{U2} &\cdots& p_{UT} \\
\end{bmatrix}
\]

Let $\mathbf{C}$ (latent census matrix) be an $R \times T$ matrix where every column represents a thread and $c_{ij}$ indicates the percentage of users that hold role $i$ in thread $j$.
\[
\mathbf{C}_{R \times T} =
\begin{bmatrix}
c_{11} & c_{12} &\cdots& c_{1T} \\
c_{21} & c_{22} &\cdots& c_{2T} \\
\vdots &  & & \vdots \\
c_{R1} & c_{R2} &\cdots& c_{RT} \\
\end{bmatrix}
\]

Let $\mathbf{Z}$ (latent roles matrix) be an $R \times U$ matrix where every column represents a users and $p_{ij} = 1$ if user $i$ holds role $j$ and $0$ otherwise. A user only holds one role.
\[
\mathbf{Z}_{R \times U} =
\begin{bmatrix}
z_{11} & z_{12} &\cdots& z_{1U} \\
z_{21} & z_{22} &\cdots& z_{2U} \\
\vdots &  & & \vdots \\
z_{R1} & z_{R2} &\cdots& z_{RU} \\
\end{bmatrix}
\]

\subsection{Model}
For a given thread $t$, we model its success as a linear combination of its census:
\begin{align*}
y^{(t)} = \sum_{i=1}^R c_{it} w_i+ e_t
\end{align*}
and we can express all threads together:
\begin{align}
\mathbf{y} = \mathbf{C^T} \mathbf{w}+\mathbf{e}
\end{align}
In probabilistic terms:
\begin{equation}
\mathbf{y} \sim \mathcal{N}(\mathbf{C^T} \mathbf{w}, \mathbf{Ie})
\end{equation}

\subsection{MLE estimation}
Unlike a regular linear regression where we have to find the optima $\mathbf{w}$, in our model we have find $\mathbf{C}$ as well. $\mathbf{C}$ can be decomposed as the product $\mathbf{ZP}$. Then, replacing in the former equation, 
\begin{align*}
\mathbf{y} = \mathbf{(ZP)^T} \mathbf{w}+\mathbf{e}
\end{align*}
that can be rearranged as:
\begin{align}
\mathbf{y} = \mathbf{P^T}(\mathbf{Z^T} \mathbf{w})+\mathbf{e}
\end{align}
and by naming $\mathbf{Z^Tw} = \mathbf{a}$ we have:
\begin{equation}
\mathbf{y} = \mathbf{P^T}\mathbf{a}+\mathbf{e}
\end{equation}
which is a classic linear regression and therefore its maximum likelihood $\mathbf{a}$ is:
\begin{equation}
\mathbf{a} = \mathbf{(P P^T)}^{-1}\mathbf{P}\mathbf{y}
\end{equation}
The meaning of $\mathbf{a}$ is the following. If we were performing a regression over all the participation vectors $p$, $a_i$ would be the coefficient associated to user $i$. In other words, $a_i$ indicates the estimated strength with which a user contributes to the success $y$.

Finally, we have the equation:
\begin{equation*}
\mathbf{Z^Tw} = \mathbf{a}
\end{equation*}
which looks like
\[
\begin{bmatrix}
z_{11} & z_{21} &\cdots& z_{R1} \\
z_{12} & z_{22} &\cdots& z_{R2} \\
\vdots &  & & \vdots \\
z_{1U} & z &\cdots& z_{RU} \\
\end{bmatrix}
\begin{bmatrix}
w_1\\
w_2\\
\vdots\\
w_R
\end{bmatrix}
= 
\begin{bmatrix}
a_1\\
a_2\\
\vdots\\
a_U
\end{bmatrix}
\]
Note that every row in $\mathbf{Z^T}$ represents a single variable; let $z_i$ denote the position of the only 1 in the row, that is, the identifier of the role hold by user $i$. It is easy to see that the left side of the equation is a vector of size $U$ where row $i$ contains the  weight associated to the role $z_i$:
\[
\begin{bmatrix}
w_{z_1}\\
w_{z_2}\\
\vdots\\
w_{z_U}
\end{bmatrix}
=
\begin{bmatrix}
a_1\\
a_2\\
\vdots\\
a_U
\end{bmatrix}
\]
The system has $R$ variables. Therefore, for the equation to have a unique solution, the number of roles will be chosen as $rank(\mathbf{a})$ which in this case is just the number of different $a_i$. Every $a_i$ is the weight $w$ associated to the role $z_i$; thus, vector $\mathbf{a}$ gives us the role partition that uses the minimum number of possible roles. Two users $i$ and $j$ have the same role if and only if $a_i=a_j$.

Unfortunately, the most likely is that $rank(\mathbf{a})$ be almost $U$, since it is unlikely that two users participate in identical threads, with same census $\mathbf{c}$ and same success $y$. This issue is a consequence of our implicit definition of role: two users have the same role if they contribute exactly the same to the success of threads. We will soften this constraining definition in the following section.

The only solution to this equation is to give a different role to every user with a different $a$, which in practice means assigning U roles.

\subsection{Bayesian estimation}

In the last section, we ended up with a vector $\mathbf{a}$ that indicates how much users contribute to the success of the threads they participate in.
Two users, we implicitly said, hold the same role if they both contribute the same to their respective threads. Nonetheless, this definition is limiting and tends to assign a different role to every user, making the analysis useless.

\subsubsection{Personalities drawn from roles}
To alleviate this consider every coefficient $w_{z_i}$ to be drawn from a gaussian distribution associated to its role $\mathcal{N}(m_r, \sigma)$. Intuitively, we will seek coefficients that seems to be drawn from the same distribution and which therefore belong to the same latent role. This is a well known model called Gaussian Mixture Model.

To formalize it, we consider the following generative process:
\begin{itemize}
\item Draw a $R$ dimensional vector with the probabilities of every role,  $
\pi \sim \mbox{Dirichlet}(\alpha)$.
\item For every role $r \in \mathcal{R}$:
\begin{itemize}
\item Draw the means of its normal distribution, $m_r \sim \mathcal{N}(0, \sigma_0)$.
\end{itemize}
\item For every user $ u \in \mathcal{U}$:
\begin{itemize}
\item Draw a role, $z_i \sim \mbox{Categorical}(\pi)$.
\item Draw a coefficient $a_u$, $a_u \sim \mathcal{N}(m_{z_i}, \sigma)$.
\end{itemize}
\item For every thread $ t \in \mathcal{T}$:
\begin{itemize}
\item Draw a length, $y_t \sim  \mathcal{N}(\mathbf{Pa}, \sigma_t)$.
\end{itemize}

\end{itemize}

The random variables of this process are:
\begin{align*}
y^{(t)} | \mathbf{z}, \mathbf{m} &\sim \mathcal{N}(\mathbf{Pa}, \sigma)\\
a_u | \mathbf{z}, \mathbf{m} &\sim \mathcal{N}(m_{z_u}, \sigma)\\
m_r &\sim \mathcal{N}(0, \sigma_0)\\
z_u &\sim Multinomial(\pi)\\
\pi &\sim Dir(\alpha)
\end{align*}

The joint probability distribution of the weights is:
\begin{align}
p(\mathbf{y, a, z, m, \pi}) = 
\prod_{r=1}^R p(\pi | \alpha )
\prod_{u=1}^U p(z_u | \pi ) 
\prod_{u=1}^U p(a_u | \mathbf{z, m})
\prod_{t=1}^T p(y_t | \mathbf{z, a})
\prod_{r=1}^R p(m_r)
\end{align}

\subsubsection{No personality. Everyone plays a role}
An alternative generative model is this one:

\begin{align*}
y_u | \mathbf{z}, \mathbf{m} &\sim \mathcal{N}(\mathbf{C^T m}, \sigma)\\
m_r &\sim \mathcal{N}(0, \sigma_0)\\
z_u &\sim Multinomial(\pi)\\
\pi &\sim Dir(\alpha)
\end{align*}


The joint probability distribution of the weights is:
\begin{align}
p(\mathbf{y, z, m, \pi}) = 
\prod_{r=1}^R p(\pi | \alpha )
\prod_{u=1}^U p(z_u | \pi ) 
\prod_{t=1}^T p(y_t | \mathbf{z, m})
\prod_{r=1}^R p(m_r)
\end{align}

The difference with the former model is that no personal coefficients are drawn from a role archetype. Users in the same role are assumed to behave exactly like the same. Although  this model is less flexible, it it easier to compute.

\subsubsection{Expectation Maximization}
(EM for the second case)

We want to maximize the joint distribution w.r.t $\mathbf{z}$ and $\mathbf{m}$. We use expectation-maximization algorithm, with the following two steps:

\begin{itemize}
\item \textbf{Expectation}: 
\\
\begin{align*}
Q_i(z_i) = p(z_u = x | \mathbf{y, m}, \pi)
&=
\frac
{p(\mathbf{z, y, m}, \pi)}
{\sum_{z_u=1}^R p(\mathbf{z, y, m}, \pi)}
\\
&=
\frac{
p(z_u | \pi ) 
\prod_{t=1}^T p(y_t | \mathbf{z, m})
}
{
\sum_{z_u = 1}^R
p(z_u | \pi ) 
\prod_{t=1}^T p(y_t | \mathbf{z, m})
}\\
&=
\frac{
\pi_x
\prod_{t=1}^T p(y_t | \mathbf{z, m})
}
{
\sum_{z_u = 1}^R
\pi_{z_u} 
\prod_{t=1}^T p(y_t | \mathbf{z, m})
}
\end{align*}
The likelihood  $p(y_t | \mathbf{z, m})$ only depends on $z_u$ for those threads $T_u$ where user $u$ participates. Therefore we can cancel out all the rest of the threads:
\begin{align*}
Q_i(z_i) = p(z_u = x | \mathbf{y, m}, \pi)
&=
\frac{
\pi_x
\prod_{t \in T_u} p(y_t | \mathbf{z, m})
}
{
\sum_{z_u = 1}^R
\pi_{z_u} 
\prod_{t \in T_u} p(y_t | \mathbf{z, m})
}
\end{align*}
\item \textbf{Maximization}
To maximize w.r.t the means $w$ we need to find:
\begin{align}
\operatorname*{arg\!\max}_{w_r} &  
\sum_{t}^T \sum_{x=1}^R 
p(z_u = x | \mathbf{y, m}, \pi) 
\log\bigg( \frac{ p( \mathbf{y, z, m}, \pi)}
{p(z_u = x | \mathbf{y, m}, \pi)}\bigg)\\
\operatorname*{arg\!\max}_{w_r} &
\sum_{t}^T
p(z_u = r | \mathbf{y, m}, \pi) 
\log\bigg( 
\frac{ 
\mathcal{N}(y_t | \mathbf{c^T w}, \sigma)
}
{
p(z_u = r | \mathbf{y, m}, \pi)
}\bigg)\\
\operatorname*{arg\!\max}_{w_r} &
\frac{1}{2\sigma}
\sum_{t}^T
p(z_u = r | \mathbf{y, m}, \pi) 
\frac{ 
(y_t - \mathbf{c^T w})^2
}
{
p(z_u = r | \mathbf{y, m}, \pi)
}\\
\end{align}

\end{itemize}


\subsubsection{Experiments}

\newpage
\appendix
\section{Appendix}
Given the joint probability:
\begin{align}
p(\mathbf{y, a, z, m, \pi}) = 
\prod_{r=1}^R p(\pi | \alpha )
\prod_{u=1}^U p(z_u | \pi ) 
\prod_{u=1}^U p(a_u | \mathbf{z, m})
\prod_{t=1}^T p(y_t | \mathbf{z, a})
\prod_{r=1}^R p(m_r)
\end{align}
we want to compute its integral with respect to $\pi$ to marginalize  $\pi$.

First, we integrate out $\pi$ since we are not interested on it (equation \ref{int_pi}):
\begin{equation*}
\int_\pi p(\pi | \alpha) \prod_u^U p(z_u | \pi) 
= \int_\pi \frac{\Gamma(\sum_{r=1}^R \alpha_r)}{\prod_{r=1}^R \Gamma(\alpha_r)}\prod_{r=1}^R \pi_r^{\alpha_r -1} \prod_{u=1}^U \pi_r^{[z_u=r]} \\
\end{equation*}
Let $q_r$ be the number of users $u \in \mathcal{U}$ such that $z_u=r$. The we can reduce the equation to: 
\begin{equation*}
\int_\pi \frac{\Gamma(\sum_{r=1}^R \alpha_r)}{\prod_{r=1}^R \Gamma(\alpha_r)}\prod_{r=1}^R \pi_r^{\alpha_r + q_r -1} 
\end{equation*}
which is almost the Dirichlet distribution. If we multiply and divide by the normalization factor of Dirichlet$(\alpha + q)$ we get:
\begin{equation*}
\int_\pi 
\frac
{\Gamma(\sum_{r=1}^R \alpha_r)}
{\prod_{r=1}^R \Gamma(\alpha_r)}
\prod_{r=1}^R \pi_r^{\alpha_r + q_r -1} 
\frac
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
\end{equation*}
which is:
\begin{equation*}
\frac
{\Gamma(\sum_{r=1}^R \alpha_r)}
{\prod_{r=1}^R \Gamma(\alpha_r)}
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
\int_\pi 
\mbox{Dirichlet}(\alpha + q)
\end{equation*}
and finally, since the integral of a density function is one:
\begin{equation*}
\frac
{\Gamma(\sum_{r=1}^R \alpha_r)}
{\prod_{r=1}^R \Gamma(\alpha_r)}
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
\end{equation*}
Since $\alpha_r$ is a fixed parameter, the left factor does not change the shape of the final distribution, and therefore we can drop it into a proportionality constant. The final expression is then:

\begin{equation}
\int_\pi p(\pi | \alpha) \prod_u^U p(z_u | \pi) =  
p(\mathbf{z} | \alpha) 
\propto
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
\end{equation}
Note that when intergrating out $\pi$ we introduce dependencies between the different $z_u$.

\section{Old stuff}
\subsubsection{Conditional $p(z_u | \mathbf{z_{-u}, m, a})$}
For the Gibbs sampler, we need to find out the conditional distribution of a role assignment given all the other latent variables being sampled (the rest of the role assignments and the vector $\mathbf{w}$)
\begin{equation*}
p(z_u = x| \mathbf{z_{-u}, m, a}) 
=
\frac
{ p(\mathbf{z} | \alpha) \prod_{j=1}^U p(a_j | z_j, m_{z_j})\prod_{r=1}^R p(m_r)}
{\sum_{z_u=1}^R \bigg[ p(\mathbf{z} | \alpha) \prod_{j=1}^U p(a_j | z_j, m_{z_j})\prod_{r=1}^R p(m_r)\bigg]_{(z_{-u})}}\\
\end{equation*}
Note that the denominator is constant with respect to $z_u$, acting just as a normalization factor.  The posterior distribution of $z_u$ can be found by computing the numerator at every $z_u$ and then normalizing so that it sums up to one. However, if the denominator has common factors with the numerator we can simplify the numerator and speed up the computations. For instance, we can simplify to:
\begin{equation*}
\frac
{p(\mathbf{z} | \alpha) p(a_u | z_u, m_{z_u})}
{\sum_{z_u=1}^R \bigg[p(\mathbf{z} | \alpha) p(a_u | z_u, m_{z_u})\bigg]_{(z_{-u})}}\\
\end{equation*}

and by expressing $p(\mathbf{z})$ as $p(\mathbf{z_u} | \mathbf{z_{-u}})p(\mathbf{z_{-u}})$ we will be able to cancel out $p(\mathbf{z_{-u}})$:

\begin{align*}
\frac
{p(z_u | \mathbf{z_{-u}}) p(\mathbf{z_{-u}}) p(a_u | z_u, m_{z_u})}
{\sum_{z_u=1}^R \bigg[p(z_u | \mathbf{z_{-u}}) p(\mathbf{z_{-u}}) p(a_u | z_u, m_{z_u})\bigg]_{(z_{-u})}}\\
\frac
{p(z_u | \mathbf{z_{-u}})) \prod_{j=1}^U p(a_j | z_j, m_{z_j})}
{\sum_{z_u=1}^R \bigg[p(z_u | \mathbf{z_{-u}})\prod_{j=1}^U p(a_j | z_j, m_{z_j})\bigg]_{(z_{-u})}}
\end{align*}
and since the denominator is constant:
\begin{equation}
p(z_u = x| \mathbf{z_{-u}, m, a}) 
\propto
p(z_u | \mathbf{z_{-u}}) p(a_u | z_u, m_{z_u})
\end{equation}

Now we develop the factor $p(z_u | \mathbf{z_{-u}})$:
\begin{align}
p(z_u | \mathbf{z_{-u}}) &= \frac{p(\mathbf{z})}{\sum_{z_u=1}^R \bigg[p(\mathbf{z})\bigg]_{(z_{-u})}}\\
&=
\frac
{
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
}
{
\sum_{z_u=1}^R
\bigg[
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}\bigg]_{(z_{-u})}
}\\
&=
\frac
{
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}
}
{
\sum_{z_u=1}^R
\bigg[
\frac
{\prod_{r=1}^R \Gamma(\alpha_r + q_r)}
{\Gamma(\sum_{r=1}^R \alpha_r + q_r)}\bigg]_{(z_{-u})}
}
\end{align}

Using the identity $\Gamma(n+1) = n\Gamma(n)$:
\begin{align}
\propto 
   \frac
      {\alpha_x + q_x}
      {R\alpha + U}
{p(a_u | x, m_{x})}
\label{posterior_z}
\end{align}

TODO: If we re-define $\alpha = \alpha/R$, the final result is:
\begin{equation}
p(z_u = x| \mathbf{z_{-u}, m, a}) 
= 
   \frac
      {\alpha/R + q_x - 1}
      {\alpha + U-1}
{p(a_u | x, m_{x})}
\label{posterior_z}
\end{equation}



%% OTRA opci�n es integrar m.

\subsubsection{Conditional $p(m_r |\mathbf{m_{-r}, z})$}
The conditional distribution of a role mean is: 
\begin{equation*}
p(m_r | \mathbf{a, z}) =
\frac
{\prod_{u=1}^U p(z_u | \alpha) \prod_{j=1}^U p(a_j | z_j, m_{z_j})\prod_{r=1}^R p(m_r)}
{\int_{m_r} \prod_{u=1}^U p(z_u | \alpha) \prod_{j=1}^U p(a_j | z_j, m_{z_j})\prod_{r=1}^R p(m_r)}\\
\end{equation*}
The left factors cancel out, as well as all the factors that do not depend on that specific mean $m_r$:
\begin{equation*}
\frac
{\prod_{j \mid z_j = r} p(a_j | z_j, m_{z_j}) p(m_r)}
{\int_{m_r} \prod_{j \mid z_j = r} p(a_j | z_j, m_{z_j}) p(m_r)}\\
\end{equation*}

As usual, the denominator is constant, so we can ignore it. The numerator is the product of two gaussians, which gives another gaussian:
\begin{align*}
\prod_{j \mid z_j \neq r} p(a_j | z_j, m_r)p(m_r) &\propto 
\prod_{j \mid z_j \neq r} \bigg\{\frac{(a_j-m_r)^2}{2\sigma^2}\bigg\} \exp\bigg\{\frac{m_r^2}{\sigma_0^2}\bigg\}\\
&=\exp\bigg\{\sum_{j \mid z_j \neq r} \frac{(a_j-m_r)^2}{2\sigma^2}\bigg\}\exp \bigg\{\frac{m_r^2}{2\sigma_0^2}\bigg\}\\
&=\exp\bigg\{\sum_{j \mid z_j \neq r} \frac{(a_j-m_r)^2}{2\sigma^2} + \frac{m_r^2}{2\sigma_0^2}\bigg\}\\
\end{align*}
we are looking for a gaussian with an exponent like:
\begin{equation}
\frac{(m_r - m_{r_p})^2}{2\sigma_{r_p}^2}
\end{equation}
where $m_{r_p}$ and $\sigma_{r_p}$ are the posterior mean and the posterior variance of $m_r$.
To get an expression like that, we re-arrange the terms by \textit{completing the square}:
\begin{align*}
\sum_{j \mid z_j = r} \frac{(a_j-m_r)^2}{2\sigma^2} + \frac{m_r^2}{2\sigma_0^2}
&=
\sum_{j \mid z_j = r}  \frac{(a_j^2 +m_r^2 -2a_j m_r)}{2\sigma^2} +
\frac{m_r^2}{2\sigma_0^2}\\
&=
\sum_{j \mid z_j = r}  \bigg[\frac{a_j^2}{2\sigma^2} + \frac{m_r^2}{2\sigma^2} -\frac{2a_j m_r}{2\sigma^2}\bigg] +
\frac{m_r^2}{2\sigma_0^2}\\
&\propto
m_r^2\bigg(\frac{c_r}{\sigma^2}+\frac{1}{\sigma_0^2}\bigg) - m_r \frac{2c_r\overline{a}}{\sigma^2}
\end{align*}
where $\overline{a}$ is the mean of the $a$ that belong to role $r$ and $c_r$ is the number of users assigned to that role.

For the sake of notation, let $\tau$ denote the precision so that $\tau=1/\sigma$ and $\tau=1/\sigma_0$. By identification:
\begin{align}
\tau_{r_p}^2 &= c_r \tau^2 + \tau_0^2\\
m_{r_p} &= \tau_{r_p}^{2} c_r \overline{a} \tau^{2}
\end{align}

And these are the parameters of the posterior distribution of $\mathbf{w_r}$:
\begin{equation}
p(m_r | \mathbf{a, z}) = \mathcal{N}(m_{r_p}, \tau_{r_p}^{-1})
\label{posterior_w}
\end{equation}
\subsubsection{Gibbs sampler}
We use equations \ref{posterior_z} and \ref{posterior_w} to build our Gibbs sampler. The algorithm is as follows:
\begin{itemize}
\item Initialize at some state $(\mathbf{z, w})$
\item Repeat $N$ times:
   \begin{itemize}
   \item For every $u \in \mathcal{U}$:
      \begin{itemize}
      \item sample $z_u | \mathbf{z_{-u}, m, a} $ from equation \ref{posterior_z}
      \item update current state $(\mathbf{z, w})$
      \end{itemize}
   \item For every $r \in \mathcal{R}$:
      \begin{itemize}
      \item sample $w_r | \mathbf{z, a}$ from equation \ref{posterior_w}
      \item update current state $(\mathbf{z, w})$
      \end{itemize}
   \end{itemize}
\end{itemize}
\end{document} 