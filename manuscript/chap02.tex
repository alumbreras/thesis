\chapter{Datasets}
\minitoc%
%\begin{abstract}
%\textit{Online forums represent a specific case of social networks with some particular properties. In this chapter, we present our forum dataset and analyse its structural and dynamic properties at three different levels: forum, discussion threads, and users.}
%\end{abstract}

\lettrine{O}{ne} of the major issues in this tesis was obtaining a complete and large dataset. While there are plenty of available datasets for social networks, forum datasets are mostly conceived for natural language processing and most of them lack the metadata indicating which posts replied to which posts. We have explored several datasets: 

\begin{itemize}
\item \texttt{IMDb} (\url{http://imdb.com}): the IMDb dataset had been internally crawled by Technicolor. It contains the discussion threads about the top 100 rated and the bottom 100 rated films in 2013. Unfortunately, lots of posts on the site have been deleted under unknown criteria. In the dataset, these posts have neither text nor parent, leaving most trees disconnected and thus difficult to work with. We reported some initial research and a discussion around it in \cite{Lumbreras2013}.

\item \texttt{Boards.ie} (\url{http://www.boards.ie}): \textit{boards.ie} is an Irish forum organised around different subjects such as sports or politics. It was released by the owner company at the ICWSM 2012 conference and contains ten years of data. There is no metadata indicating which post replied to which posts, and replies must be inferred from text quotations and user mentions in the posts. This is a challenge in itself because, among other reasons, the quotation and naming conventions in the forum are too open and changing through the years. Research with this dataset includes \cite{Kan2012,Angeletou2011,Chan2010}.

\item \texttt{Slashdot} (\url{http://www.slashdot.com}): Slashdot is a technology-news forum where moderators publish news posts for users to comment on them. In this dataset the tree structure is explicit, but the first post is missing. This forum has been widely analysed in \cite{Gomez2008,Kaltenbrunner2007,Gomez2010,Gomez2012}. 

\item \texttt{Reddit} (\url{http://www.reddit.com}): Reddit is a giant forum of forums, called \textit{subreddits}. Subreddits cover all kinds of topics and new subreddits are continuously created. Since July 2015, a dataset with all Reddit content from 2007 is available for download and updated in a monthly basis. This is, by far, the best publicly available dataset regarding quality and quantity. Some Reddit data has been analysed in \cite{Wang2012e}.
\end{itemize}

\begin{table}
 \begin{center}
   \tabcolsep = 0.8\tabcolsep
   \begin{tabular}{|l|c|c|c|c|}
      \hline
     Forum 			& Threads 	& Posts 		& Users 		& Posts/user \\
   \hline 
science         		& 122,041    & 3,647,857 & 479,862 	& 7.6\\
TwoXChromosomes     	& 120,231    & 3,690,732 & 335,740 	& 10.9 \\
gameofthrones      	& 156,937    & 3,326,169 & 278,748 	& 11.9\\
4chan             	& 70,396    	& 2,428,638 	& 226,412 	& 10.72\\
philosophy       	& 29,175    	& 650,732   	& 86,411  	& 7.53 \\
podemos        		& 88,815    	& 1,368,457 	& 30,032  	& 45.56 \\
france         		& 40,967    	& 1,083,957 	& 23,323  	& 46.47 \\
MachineLearning    	& 11,509    	& 114,407   	& 14,572  	& 7.8  \\
   \hline
   \end{tabular}
 \end{center}
 \caption{Datasets}
\label{tab:forums}
\end{table}

%Forum: 4chan
%Threads: 68,549
%Posts: 2,361,721
%Users: 223,569
%
%Forum: MachineLearning
%After cleaning:
%Threads: 10,797
%Posts: 105,147
%Users: 13,883
%
%Forum: TwoXChromosomes
%Threads: 117,233
%Posts: 3,606,658
%Users: 330,021
%
%After cleaning:
%Threads: 39050
%Posts: 1028591
%Users: 22944
%
%Forum: podemos
%Threads: 85,727
%Posts: 1,336,728
%Users: 30,355
%
%Forum: gameofthrones
%Threads: 154,461
%Posts: 3,283,700
%Users: 277,988
%
%Forum: philosophy
%Threads: 28,302
%Posts: 633,344
%Users: 85,121
%
%Forum: science
%threads: 118,446
%Posts: 3,562,177
%Users: 473,965
%
%Forum: Feminism
%Threads: 18400
%Posts: 203432
%Users: 27907

We finally chose the Reddit dataset because it is the only one with complete trees and years of data. We chose a set of subforums from which we downloaded all comments between 2013 and 2016 (Table~\ref{tab:forums}). In the following sections, we analyse these subforums at three different levels: forum level, conversation level and user level.

\section{Forum dynamics}
The analysis at forum level will give us an overview of the amount of activity in every forum, their periodicity (if any) or peaks that might happen due to internal or external factors. We will also analyse at which paces new users join the forum and how the activity is distributed between older and newer users.

\subsection{Posting activity}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_posts_by_day}
\caption{Posts by day}
\label{fig:posts_by_day}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_lengths_by_day}
\caption{Thread lengths by day}
\label{fig:lengths_by_day}
\end{figure}

\textit{How do the activity levels evolve over time?} 
We first look at the number of comments posted every day in each forum (Figure ~\ref{fig:posts_by_day}). Interestingly, they show different patterns on their activity growth, their periodicities and their peaks:

\begin{enumerate}
\item \textit{Growth}:  some forums like \textit{france} and \textit{MachineLearning} show a smooth increase in the average number of posts per day. \textit{gameofthrones} is stable during the inter-season hiatus, but increases its activity levels every time a new season begins, probably thanks to an increasing popularity of the TV series. \textit{Podemos} is the forum with the most drastic initial transition, certainly coupled with its popularity and the expectations that the party raised in Spain. Its current decrease of the mean activity levels might signal either a disaffection or just that the former levels of activity were not sustainable in the long term. 

\item \textit{Periodicity}: while most forums only have weekly periods with activity decreasing during the weekends, \textit{gameofthrones} has two different periods: the inter-season period, where no new episodes are broadcasted and the levels of activity do not change significantly, and the intra-season periods, where the broadcasting of every episode generates a new peak. As a curiosity, we noted that even if one might think the height of a peak is proportional to the positive reception of the episode, it might not always be the case: the most popular episode of the third season is, according to the ratings in IMDb, the ninth, often known as the \textit{Red Wedding}, which has a rating of 9.9 over 10; however, this rating does not correspond to the highest activity peak of our graphic. 

\item \textit{Spontaneous peaks}: forums can have spontaneous peaks often related to an external event. In \textit{france}, the two main peaks correspond to the two terrorist attacks in 2015. In \textit{podemos}, the highest peak corresponds to the foundational congress of the party. 
\end{enumerate}

Moreover, a visual analysis of forum thread lengths from Figure \ref{fig:lengths_by_day} shows that peaks tend to be generated by longer threads. This is not surprising since, when there occurs some external event that is relevant to the forum community, the discussion of the event is centralised in a single thread.

\subsection{Births, lives and deaths}
\textit{How many users join the community every month?}
We can think of a forum as a sort of biological community where individuals are born, live and die. From the point of view of the researcher that wants to extract types of behaviours, the longer individuals live, the easiest for any algorithm it will be to detect some patterns. Put the other way round, if all individuals posted only once, it would be harder to learn from them and especially to make a significant classification of individuals in roles. Besides, even if we ignore the minimal quantitative conditions for a community to exist, it seems obvious that it needs more than one participation per individual. In order to analyse how stable the communities are in terms of population, we define \textit{birth}, \textit{death}, \textit{life expectancy} and \textit{age}:

\begin{itemize}
\item Birth (or join): a birth is the event where a user participates for the very first time in a forum. A high rate of births is an indicator of the interest of the forum towards non-users.

\item Death (or abandon): a death is the event where a user participates in the forum for the last time. Note that deaths might be misleading because we cannot know whether the user will continue contributing to the forum in the future. 

\item Life expectancy: the life of an individual (measured in time or posts) for the time period (or the number of posts) between its first and last post. The life expectancy at month $t$ is calculated by averaging the lifespans of all the individuals born at $t$. We consider that all individuals die at the end of our observation period. In the context of forums, life expectancy is an indicator of the level of engagement of the cohort that joined the forum that month. We define as \textit{survivors} those users with a life expectancy higher than 10 posts.
%before: retention as a user that will make more than ten posts 
\item Age: the \textit{age} of a user at time $t$ is the time between $t$ and its date of birth.
\end{itemize}

Figure \ref{fig:births_retentions} shows the number of users that are born every month and the number of survivors. Both factors are very stable, and peaks in births are translated to peaks in survivors, meaning that the ratio of survivors, although rather low, is approximately constant.

Also, comparing Figures \ref{fig:lengths_by_day} and \ref{fig:births_retentions} we can strongly detect rises in the activity, thread lengths and number of births. % but are the new users the ones that create the peak? NO!


\subsection{New users and old users}
\textit{Who is responsible for most of the content?} Despite being a minority, old users are responsible, at least, for 75\% of the posts every month (Figure \ref{fig:forum_posts_new_vs_old_members}). \textit{podemos} is the most extreme case, where old users account for almost the totality of posts. 

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_births_retentions}
\caption{Number of births and number of survivors}
\label{fig:births_retentions}
\end{figure}

The age distribution of posters shows a different pattern from forum to forum. Figure \ref{fig:forum_users_age_by_month} shows the age distribution of each month posters. In \texttt{podemos}, the mean age increases linearly within time, meaning that the population is getting older without many new users joining the community. The population of \texttt{gameofthrones} gets older during the season break, but every time a new season begins, with the activity peaks, they enjoy a \textit{baby-boom} that decreases the mean age drastically. \texttt{france} behaves very stable concerning age; after the two terrorist attacks, a lot of new users participated in the discussion and then abandoned the forum, decreasing the mean age during that month only. \texttt{machine learning} and \texttt{TwoXChromosomes} enjoy the participations of older members while new participants keep joining the forum, keeping the mean age almost constant.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_forum_posts_new_vs_old_members}
\caption{Posts written by new and old members}
\label{fig:forum_posts_new_vs_old_members}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_forum_users_age_by_month}
\caption{Distribution of user age by month}
\label{fig:forum_users_age_by_month}
\end{figure}

\subsection{Triad census}
\textit{What are the dominant micro structures of the interactions graph?}
A triad census tells us which are the dominant micro-structures in a graph. We built an interaction graph for every month and computed its triad census (Figure~\ref{fig:triad_census_evol}). We see a strong dominance of the \textit{in-star} in forums \textit{TwoXChromosomes} and \textit{gameofthrones}, and at one particular moment of \textit{MachineLearning}, probably related to a long thread. This motif is especially frequent in \textit{TwoXChromosomes}, meaning that there are more celebrities or that users get more replies per post. Also, chains (120C, 021C) and out-stars (021D) are more present in \textit{france}, \textit{podemos}, and \textit{MachineLearning}.

The interpretation of triads in the interactions graph is ambiguous. We do not know, for instance, if a vertex is in a lot of \textit{in-star} because the user tends to get replies from different people or because he got replies from different people in \textit{one} post. Later in this chapter, we will compute the triad census over the conversation trees in order to analyse the conversations without distortions.

% todo: measure transitivity, % of closed triads
 
%For every thread, we built its interaction graph and then counted the frequence of every triad. The aggregation of all the triad census over all the threads starting at month $m$ is reported in Figure~\ref{fig:triad_census_evol}. The triad that dominates all the time is the \textit{in-star} (021U). This triad increases its presence in months with long threads. The triad 111D seems to be inversely correlated to in the in-star, decreasing when the former increases, and vice-versa. Our speculation is that, in the original tree, this triad represents a cascade $u_1, u_2, u_1, u3$. However, it is not possible to confirm that from the interaction tree. In the next Chapter, we propose new motifs to analyse the tree graph directly. 

%The \texttt{MachineLearning} forum is the one with richer structures, probably meaning that threads are less centred in the original post and that users have richer conversations along the tree. It is interesting to see that in Podemos, even if has been conceived as a place of open debate between party supporters, the in-star dominates considerably more than in \textit{MachineLearning} or \textit{France}.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_forum_interactions_triad_census_evol}
\caption{Evolution of triad census over time}
\label{fig:triad_census_evol}
\end{figure}

\section{Conversation dynamics}

\subsection{Size}
\textit{How large are threads?}
The distribution of thread sizes may be affected by factors such as the type of forum and the user interface. Nevertheless, most forums share  common traits regarding thread sizes such as a high probability of small threads and a long tail of long threads. In \cite{Gomez2012}, the authors report different distributions between Wikipedia and some news-oriented forums such as Slashdot. While Slashdot showed a distribution with some scale, Wikipedia has a scale-free distribution. Even if Reddit is more similar to a news-oriented forum like Slashdot, the length distributions of our Reddit forums are almost scale-free, like Wikipedia (Figure~\ref{fig:distribution_threadlengths}). 

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_distribution_threadlengths}
\includegraphics[width=1\textwidth]{ch2_threads_with_at_least_n_posts}
\caption{Threads length}
\label{fig:distribution_threadlengths}
\end{figure}

\subsection{Speed}
\textit{How fast are conversations?}
We define the \textit{speed of a conversation} as the number of posts per time unit. Looking at conversations with more than 10 posts, we observe that the speed of conversations often goes through two phases: posts arrive faster during the first day and then the speed decreases drastically; during the second phase, new posts may arrive months after the previous one and they will usually be replies to the root post (Figure~\ref{fig:thread_speed}). 

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_thread_speed}
\caption{Examples of speed in conversations with more than ten posts. The inflexion points between the two phases (coloured blue and red) are detected by a changepoint detection algorithm \citep{Killick2012}}.
\label{fig:thread_speed}
\end{figure}

\subsection{Shape}
\textit{What are the micro-structures of conversations?}
Unlike triads in the interactions graph, triads over the tree graphs are unambiguous. There are four possible triads in a tree: the empty triad (003), the \textit{directed edge} (012) the \textit{in-star} (021U), and the \textit{chain} (021C). The empty triad is not interesting and the \textit{directed edge} is trivial in a tree. Thus, we focus on the \textit{in-star} and the \textit{chain}. To know how significant the frequency of a triad is, we need to compare it to its frequency in a null model. Our null model is a random tree that grows by new vertices choosing a random parent among the existing vertices; this is actually the most entropic way of building a tree (assuming that the tree has a fixed number of vertices that are added one by one). 
%https://fisher.osu.edu/~schroeder.9/AMIS900/ech4.pdf %uniform distribution as the one with maximum entropy

We compute the $z$-score of a triad in a tree graph $g$ of size $|V(g)|$ as follows. With our null model, we generate 100 random trees of size $|V(g)|$.  Let $f_g$ be the frequency of the triad in $g$. Let $\mu_r$ and $\sigma_r$ be the mean and the standard deviation of that triad in the set of random trees. The $z$-score is
\begin{equation}
z = \frac{f_g-\mu_r}{\sigma_r}
\end{equation}

Triads with z-score near zero are not relevant because they have the same frequency as in random graphs. On the other hand, triads with higher (or lower) z-scores are more (or less) frequent than in random graphs, which means that the null model cannot justify their high (or low) frequency. Figure~\ref{fig:tree_motifs_by_date} shows the evolution of the triad census in our forums. The \textit{in-star} is usually higher than in the random graphs, especially in the activity peaks with long threads. That means that long threads are made of posts that attract many replies rather than long chains of posts. In other words, long threads are wide rather than deep. \textit{MachineLearning} and \textit{france} have the slowest frequency of \textit{in-stars}.

When compared to the original set of 16 triads, a set of two triads looks like a very limited tool to analyse conversational structures. We will come back to this point in Chapter 3, where will attempt to classify users by the discussions they participate in.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_tree_2motif_census_by_date}
\caption{3-vertices motifs in trees}
\label{fig:tree_motifs_by_date}
\end{figure}

\section{User dynamics}
 
\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_distribution_timetoroot}
\includegraphics[width=1\textwidth]{ch2_posts_at_n_seconds_max_from_root}
\caption{Time from posts to root}
\end{figure}

\subsection{Posting}
\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_distribution_activity}
\includegraphics[width=1\textwidth]{ch2_users_with_at_least_n_posts}
\caption{Users activity}
\label{fig:distribution_activity}
\end{figure}
\textit{How big are inequalities in posting activity?}
As commonly observed in social networks, a minority of users creates most of the content. The number of posts per user follows a power law (Figure~\ref{fig:distribution_activity}) and most forums have only around 1,000 users that have written more than 100 posts. Though this is not surprising, this will make it very difficult to learn behavioural patterns from most users, including role detection.

%Besides, in a user activity may follow periods of high activity and periods of low activity or even lurk.
%\includegraphics[scale=•]{•}=1\textwidth]{timetoroot_by_rank}

\subsection{Lifespans}
\textit{How long do users stay in the forums?}
We define as \textit{lifespan} the number of days between the first and the last post of the user. Figure~\ref{fig:users_distribution_life} shows that most users are casual participants, posting once and then abandoning the forum forever (they may also become silent readers, which is known as  \textit{lurking}). Only around 25\% of users live more than a year. Lifespans have no important differences between forums, except for \textit{gameofthrones}, where we can observe the fact that every year a new cohort joins the forum.
\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_users_distribution_life}
\includegraphics[width=1\textwidth]{ch2_users_distribution_life_cdf}
\caption{Users lifespan}
\label{fig:users_distribution_life}
\end{figure}

\subsection{Communities} % This should probably go to thread level? 
% Motivation
\textit{How big are user communities?}
Given a social graph, a community is a set of users with a high density of links. In forums, communities correspond to groups of users that, for some reason, tend to interact (or co-participate) between them. The existence of communities may mean that behaviours are transmitted easier between member individuals; this has recently been proven for the case of memes \citep{Weng2014}. 

Depending on how we build the graph we may have two kinds of communities: interaction communities and co-participation communities. In the interaction graph, we assign an (undirected) edge between two users $u$ and $v$ if the sum of replies in either way is at least 10. In the coparticipations graph, we assign an (undirected) edge between two users $u$ and $v$ if they co-participated at least in 10 different threads. We build one graph for each month and then detect the communities in each graph with the Louvain algorithm \citep{Blondel2008}. Figure~\ref{fig:communities_interaction} and Figure~\ref{fig:communities_coparticipation} show, for each month, the relative sizes of the top $n$ communities as well as the size distribution over the aggregated three-years graph.
Since community members evolve on a month-to-month basis, tracking the evolution of a community from one month to another is not trivial. For the sake of simplicity, we did not try to detect the correspondence between communities in two consecutive months. Instead, we plot the relative size of communities each month. 

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_distribution_communitysize_interaction_strong}
\includegraphics[width=1\textwidth]{ch2_distribution_communitysize_interaction_strong_evol}
\caption{Communities in the interaction graph}
\label{fig:communities_interaction}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_distribution_communitysize_coparticipation_strong}
\includegraphics[width=1\textwidth]{ch2_distribution_communitysize_coparticipation_strong_evol}
\caption{Communities in the co-participation graph}
\label{fig:communities_coparticipation}
\end{figure}

Size proportions are quite variable, and we observed by visual inspection of the dynamic graph (not shown), that sometimes the ranking changes and smaller communities become bigger and vice-versa. Figures also show the size of the community per individual. Interestingly, most forums have a few communities with hundreds of members, even though most users belong to micro communities.

\subsection{Cohesiveness}
%
\textit{How cohesive are the forums?}
We finally look at the cohesion of the user network to measure to which degree users know each other. Among all the cohesion metrics, density is arguably the simplest. \textit{Density} measures the proportion of existing links with respect to the maximum possible number. The main limitation of density is that it computes the network average, while some parts of a network are usually denser than others.\textit{ k-cores} is a more sophisticated metric that is able to better capture the different densities in a graph. A k-cores decomposition is a way to peel a graph. Formally, we say that given a graph $G$, the k-core of $G$ is defined as the maximal connected (induced) subgraph of $G$ where all the vertices have at least degree $k$. \citep{Seidman1983}.

Figure~\ref{fig:kcores} shows the density and the size of each core. Unsurprisingly, the higher the $k$, the higher its density. In particular, \textit{podemos} and \textit{france} have the most dense cores with more than 150 users and densities above 0.5.

We also measure the temporal diameter of a core as the number of seconds between the birth of the younger user and the death of the older. The diameters of all forums, except for \textit{podemos}, is three years. The shorter diameter of \textit{podemos} is due to the fact that the forum was created later.   

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{ch2_kcores_properties}
\caption{Properties of users in each k-core}
\label{fig:kcores}
\end{figure}

Figure~\ref{fig:cliques} shows the temporal diameters of the maximal cliques in \textit{MachineLearning}. While we would expect that there is an upper bound in the temporal diameter of cliques, we find that even the diameters of the largest cliques are higher than three years.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch2_community_cliques_temporal_diameter}
\caption{Temporal diameter of cliques \textit{MachineLearning}}
\label{fig:cliques}
\end{figure}


\section{Forums are not teams}

%\section{Conditions for existence of roles}
One of the first hypothesis of this thesis was that online forums formed communities where roles emerge. According to this, the content of these roles---the functions attached to them---would be established by the community rather than by the individual and, in a given community, we could observe its members taking different roles and working as a social team that wanted to achieve a task. This hypothesis was inspired by the work of \cite{belbin2012team}, who identified a set of nine roles whose combination made a team successful. If Belbin's theory were to be applicable, or adaptable, to forums, we could try to develop the analytical tools to detect the roles operating in a given forum and, from that set, the specific role that every user plays. That would help us understand the contribution of each individual to the community and improve our understanding of the dynamics at the community level since we knew how to go from role composition to forum dynamic.

I seems, according to the analysis in this chapter, that Belbin's theory cannot be applied to our forums, and it might be also true for all large forums that are not task-oriented. I cannot be applied because large forums are not like to teams. The main differences between a team and a forum are their size, the density of their relationship networks, and the dynamic changes in the structure. Teams are relatively small groups of people that work together from the beginning to the end of the project; mathematically, they can be thought of as a clique with vertices that do not change. An online forum can have hundreds, thousands, or millions of users where most of them are strangers to each other and where members constantly join and abandon the community; mathematically, they are big random graphs where vertices appear and disappear.

% For Belbin, we would need that most active users (>100 posts) belong to a knitted community.

\section{Summary}
%Limitations of current models
In this chapter, we have introduced our dataset and a descriptive analysis to understand some general characteristics of the forums under study. We have followed analysis in three levels:

% Forums
\textit{Forums}: We have found that forums are quite regular in terms of the number of posts, though they can show growing or decreasing tendencies, indicating the health of the forum. Some forums associated to periodical events (like TV series) show a tendency strongly coupled with the real event. External events seem to trigger the activity in the forums through old users, and this increase in the activity is concentrated around a few long threads that attract lots of new users.

\textit{Conversations}: Conversation lengths follow a quasi-power law distribution with almost no scale. Conversations have two phases, a first one where posts arrive at a regular rate during one day, and then a second one where the speed goes almost to zero. Conversations have a strong tendency towards the in-star, meaning that conversations are more dominated by starts than by cascades.

% Users
\textit{Users}: All forums have a core of users that have an ongoing presence and make the most of the content, and a much bigger periphery of users who only participate once and then just \textit{lurk} or abandon the forum. Only around 25\% of users live more than a year. The social network of user interactions is very sparse, meaning that most users do not know each other. The size of the biggest communities and cliques is on the scale of tens.

In the beginning, we considered roles as community-based. A role would be defined by the community and not by the individual, and therefore a new individual would just fill in a role after some initial integration. We think this may happen for closer communities or task-oriented communities such as Wikipedia. However, the dimensions of the forums make them more similar to cities than task-oriented communities, and the role study needs to take another approach. The fact that there is only a small elite of active users that do not even interact with each other rejects the notion of Team Roles. The approach that we take in the following chapters is to look at a role as a behavioural function that does not depend on the community. 