# Blockmodeling over the SNA of each forum
###########################################
# Check this
# http://sna.stanford.edu/lab.php?l=6

#and this (node-level triad mining)
# https://arxiv.org/pdf/1410.1594.pdf

# package to count motifs vs randomized graph
# Etienne et al
# paloma-package {paloma}


#BM packages:
############################################

# R Packages
# https://cran.r-project.org/web/packages/lda/lda.pdf

# Blockmodeling (non-stochastic)
# https://cran.r-project.org/web/packages/blockmodeling/blockmodeling.pdf

# Blockmodels (stochastic, VB-EM, selection of numebr of clusters by ICL)
# http://arxiv.org/pdf/1602.07587v1.pdf

# mixnet 
# https://cran.r-project.org/web/packages/mixer/mixer.pdf

# Recent packages and research 
# https://graph-tool.skewed.de/static/doc/inference.html

library(igraph)
library(data.table)
library(dplyr)
library(reshape2)
library(ggplot2)

library(blockmodels)
library(mixer)
source('R/posts_to_graphs.R')

data.list <- list()
data.list.originals <- list()
data.limits <- list()

subforums <- c("MachineLearning",
               "TwoXChromosomes",
               "france",  
               "podemos",
               "gameofthrones")

for (i in 1:length(subforums)){
  subforum <- subforums[i]
  load(paste0('data/df.posts.', subforum, '.rda'))
  
  # Create the sociomatrix
  
  df.posts <- df.posts %>% 
    mutate(date=as.numeric(date)) %>% 
    mutate(rdate = as.Date(as.POSIXct(date, origin="1970-01-01")))  %>%
    mutate(date=date - min(date))
  
  parent.users <- df.posts[match(df.posts$parent, df.posts$postid),]$user
  df.posts <- df.posts %>% 
    mutate(parent.user=parent.users) %>%
    filter(user!='[deleted]' & parent.user!='[deleted]')
  
  
  # Edges and weights (edge if more than 10 replies)
  #matrix.coparticipations <- matrix.coparticipations.from.posts(df.posts, maxusers=1000)
  matrix.coparticipations <- matrix.interactions.from.posts(df.posts, maxusers=1000, mode='undirected')
  cat('subforum:', subforum)
  print(dim(matrix.coparticipations))
  
  rownames(matrix.coparticipations) <- NULL
  colnames(matrix.coparticipations) <- NULL
  df.coparticipations <- melt(matrix.coparticipations)
  names(df.coparticipations) <- c('from', 'to', 'weight')
  df.coparticipations.bin <- df.coparticipations
  df.coparticipations.bin$weight <- as.numeric(df.coparticipations.bin$weight>0)
  g <- graph.data.frame(filter(df.coparticipations.bin,weight>0))
  sociomatrix.bin <- get.adjacency(g, sparse=FALSE, names=FALSE) # square
  
  df.coparticipations.bin$subforum <- subforum
  data.list.originals[[i]] <- df.coparticipations.bin
  
  # Apply another blockmodeling (Poisson, for counting data)
  if(FALSE){
    BM <- BM_poisson(
      membership_type = "SBM",
      adj = sociomatrix.bin,
      explore_min=2,
      explore_max=5,
      ncores=5)
    BM$estimate()
    k <- which.max(BM$ICL)
    z.posterior <- BM$memberships[[k]]$Z
    cluster <- apply(z.posterior, 1, which.max)
    neworder<-order(cluster)
  }
  
  # Apply another blockmodeling (Bernouilli or Poisson)
  if(FALSE){
    BM <- BM_bernoulli(
      membership_type = "SBM",
      adj = sociomatrix.bin,
      explore_min=2,
      explore_max=5,
      ncores=5)
    BM$estimate()
    k <- which.max(BM$ICL)
    z.posterior <- BM$memberships[[k]]$Z
    cluster <- apply(z.posterior, 1, which.max)
    neworder<-order(cluster)
  }
  
  # Daudin BM (Bernouilli 0/1)
  ########################################################
  # Assume edges come from Bernouilli distributions (0 or 1)
  bm.bernouilli <- mixer(sociomatrix.bin, qmin = 3, qmax=20, method = 'classification')
  plot(bm.bernouilli) # chose option 2 to print final matrix
  z.posterior <- getModel(bm.bernouilli)$Taus
  
  
  # Re-organize following source code of mixer
  cluster <- apply(z.posterior, 2, which.max)
  edges <- bm.bernouilli$edges
  neworder<-order(cluster)
  n <- max(edges) # users
  m <- t(matrix(order(neworder)[as.numeric(edges)],2)) # re-label nodes
  m <- as.data.frame(m)
  names(m) <- c('from', 'to')
  # get the symmetrix adjacency matrix with igraph
  m.undirected <- get.adjacency(as.undirected(graph.edgelist(as.matrix(m))), sparse = FALSE)
  
  #mm <- as.data.frame(xtabs( ~ from+to, data=m))
  
  #m.directed <- acast(m, from ~ to, fun.aggregate = sum(Freq)) # square
  #m.undirected <- (m.directed | t(m.directed))*1
  
  df.m <- m.undirected %>% melt
  names(df.m) <- c('from', 'to', 'weight')
  df.m$cluster <- cluster[neworder]
  df.m$subforum <- subforum
  
  data.list[[i]] <- df.m
  
  # store limits
  limits <- table(cluster) # find the class limits
  limits <- cumsum(limits)[1:(length(limits)-1)] + 0.5 
  data.limits[[i]] <- data.frame(limit=limits, subforum=subforum)
  
}
df <- rbindlist(data.list)
df.limits <-  rbindlist(data.limits)
df.originals <- rbindlist(data.list.originals)

g <- ggplot(df.originals, aes(x = from, y = to, fill = as.factor(weight))) +
  geom_raster() + coord_fixed() + scale_y_discrete() + scale_x_discrete() +
  scale_fill_manual(values=c("white", "black"), name="")+
  facet_grid(. ~subforum, scales='free') +
  theme_bw()+ xlab('user') + ylab('user')+
  theme(axis.line        = element_blank(),
        axis.text        = element_blank(),
        axis.ticks       = element_blank(),
        strip.background = element_rect(fill = 'white'), 
        aspect.ratio = 1) +
  ggtitle("Original coparticipation matrices")
print(g)
#ggsave(file='blockmodels_pre_1000.png', width=200, height=70, units='mm')
ggsave(file='blockmodels_pre_inters_1000.png', width=200, height=70, units='mm')



g <- ggplot(df, aes(x = from, y = to, fill = as.factor(weight))) +
  geom_raster() + coord_fixed() + scale_y_discrete() + scale_x_discrete() +
  scale_fill_manual(values=c("white", "black"), name="")+
  facet_grid(. ~subforum, scales='free') +
  geom_vline(aes(xintercept = limit), df.limits, color='red') +
  geom_hline(aes(yintercept = limit), df.limits, color='red') +
  theme_bw()+ xlab('user') + ylab('user') +
  theme(axis.line        = element_blank(),
        axis.text        = element_blank(),
        axis.ticks       = element_blank(),
        strip.background = element_rect(fill = 'white'), 
        aspect.ratio = 1) +
  ggtitle("Re-arranged coparticipation matrices")
print(g)

#ggsave(file='blockmodels_1000.png', width=200, height=70, units='mm')
ggsave(file='blockmodels_inters_1000.png', width=200, height=70, units='mm')
