\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Mixture models}{7}{0}{2}
\beamer@sectionintoc {3}{Dual-view mixture models}{9}{0}{3}
\beamer@sectionintoc {4}{Experiments}{13}{0}{4}
\beamer@sectionintoc {5}{Conclusions}{20}{0}{5}
