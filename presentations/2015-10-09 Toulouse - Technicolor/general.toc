\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Part I: Role detection in social networks}{7}{0}{1}
\beamer@subsectionintoc {1}{1}{Position-based (blockmodeling)}{8}{0}{1}
\beamer@subsectionintoc {1}{2}{Feature-based}{11}{0}{1}
\beamer@subsectionintoc {1}{3}{Discussion}{13}{0}{1}
\beamer@sectionintoc {2}{Part II: Role detection in discussion trees}{15}{0}{2}
\beamer@subsectionintoc {2}{1}{Discussion trees}{16}{0}{2}
\beamer@subsectionintoc {2}{2}{Feature-based analysis of roles}{21}{0}{2}
\beamer@subsectionintoc {2}{3}{Prediction-based}{24}{0}{2}
\beamer@subsectionintoc {2}{4}{Generative model for threads}{37}{0}{2}
\beamer@subsectionintoc {2}{5}{Discussion}{39}{0}{2}
\beamer@sectionintoc {3}{Future work}{40}{0}{3}
