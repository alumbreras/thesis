# Degrees distributions
library(dplyr)
library(data.table)
library(ggplot2)

data.list <- list()
subforums <- c("4chan",
               "MachineLearning",
               "TwoXChromosomes",
               "france",  
               "podemos",
               "gameofthrones",  
               "philosophy",
               "science")


subforums <- c("MachineLearning",
               "TwoXChromosomes",
               "france",  
               "podemos",
               "gameofthrones")

for (i in 1:length(subforums)){
  subforum <- subforums[i]
  load(paste0('data/df.posts.', subforum, '.rda'))
  
  cat('\n', subforum)
  df.posts <- df.posts %>% 
    mutate(date=as.numeric(date)) %>% 
    mutate(rdate = as.Date(as.POSIXct(date, origin="1970-01-01")))  %>%
    mutate(date=date - min(date)) %>%
    arrange(desc(date))

  by_user <-  df.posts %>%  group_by(user) %>% arrange(date) %>% 
                            mutate(lurking = c(-1, diff(date)))
  by_user[by_user$lurking == -1,]$lurking <- NA
  
  df.users <- by_user %>% group_by(user) %>%
                          summarise(nposts = n(), 
                                   mean = mean(lurking, na.rm=TRUE), 
                                   sd =   sd(lurking, na.rm=TRUE)) %>%
                          as.data.frame

  df.users <- df.users[complete.cases(df.users),]
  df.users <- mutate(df.users, subforum=subforum)
  data.list[[i]] <- df.users
}

df <- rbindlist(data.list)
df <- filter(df, nposts>10)
par(mar=c(0,0,0,0)) # Remove unnecessary margins

# saved in file with 930x300 dimensions
g <- ggplot(df, aes(x=mean, y=sd)) + 
  geom_point(alpha=1, size=0.1) +
  geom_smooth() +
  #scale_x_log10() + scale_y_log10() +
  theme_bw() +
  facet_grid(. ~subforum) +
  theme(strip.background = element_rect(fill = 'white'),
        aspect.ratio = 1) +
  xlab("mean") + ylab("sd") +
  ggtitle("Interpost (pdf)")
print(g)
ggsave(file='distribution_interpost_by_user.png', width=200, height=70, units='mm')

print(g)
