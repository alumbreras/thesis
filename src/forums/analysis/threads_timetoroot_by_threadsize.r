# Number of posts in a thread vs time.
# The goal is to demonstrate that there is a point where the speed decreases,
# and then the thread dies
# This was initially found in Wang 2012
#
# author: Alberto Lumbreras

library(dplyr)
library(data.table)
library(ggplot2)
data.list <- list()
subforums <- c("gameofthrones", "podemos", 'sex', '4chan')

for (i in 1:length(subforums)){
  subforum <- subforums[i]
  load(paste0('data/df.posts.', subforum, '.rda'))

  df.posts <- mutate(df.posts, date=as.numeric(date)) %>% 
              mutate(rdate = as.Date(as.POSIXct(date, origin="1970-01-01"))) %>% 
              arrange(date) %>% 
              group_by(thread) %>% 
              mutate(time.to.root = date-min(date),
                     rank=rank(date, ties.method='first')) %>%
              as.data.frame %>%
              mutate(subforum = subforum)
  
  data.list[[i]] <- df.posts
}

df <- rbindlist(data.list)

g <- ggplot(filter(df, rank<1000), aes(x=rank, y=time.to.root)) +
     geom_point(alpha=1, size=0.2) +
     theme_bw() +
     facet_grid(. ~subforum) +
     theme(strip.background = element_rect(fill = 'white'),
           aspect.ratio = 1)

print(g)
