Online forums have gained popularity during the last years and are of the predominant forms of dialog between Internet users. Most forums sites host many subforums where every subforum covers a topic. In every subforum, users can start new conversations, or threads, by posting an initial message. Every thread then composed by one initial post and a set of comments (or posts) where every comment is a reply either to some other comment or to the initial post. 

% Threads as graphs
A discussion thread can be represented by a tree graph $G$ where vertices represent users posts and an edges $(u,v)$ means the post $u$ is a reply to post $v$. Some authors have posed the question of what determines the growth of a discussion thread. To this aim, they have proposed several stochastic \textit{growth models} able to generate synthetic trees that reproduce some of the properties of the real conversations. 



%Random graphs  
One of the simplest random graph models is that of Erdös-Rényi, where the number of nodes is given from the beginning and an edge between any pair of nodes is created with a probability $p$  (Figure~\ref{fig:erdos_rendy}). After this foundational work, other graph models have attempted to mimic real-world networks. The Watts-Strogatz model, for instance, generates graphs with some \textit{small-network} properties such as low average distance between vertices and high transivity (friends of my friends are also my friends).

