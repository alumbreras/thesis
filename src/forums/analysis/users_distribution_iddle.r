# Degrees distributions
library(dplyr)
library(data.table)
library(ggplot2)

data.list <- list()
subforums <- c("MachineLearning",
               "TwoXChromosomes",
               "france",  
               "podemos",
               "gameofthrones")

for (i in 1:length(subforums)){
  subforum <- subforums[i]
  load(paste0('data/df.posts.', subforum, '.rda'))
  
  cat('\n', subforum)
  df.posts <- df.posts %>% 
              mutate(date=as.numeric(date)) %>% 
              mutate(rdate = as.Date(as.POSIXct(date, origin="1970-01-01")))  %>%
              mutate(date=date - min(date)) %>%
              arrange(desc(date))
            
  df.posts <- df.posts %>%  
              group_by(user) %>% 
              arrange(date) %>% 
              mutate(mins.iddle = c(-1, floor(diff(date)/60))) %>%
              mutate(hours.iddle = floor(mins.iddle/60)) %>%
              ungroup
  
  df.posts[df.posts$mins.iddle == -1,]$mins.iddle <- NA
  df.posts <- df.posts[complete.cases(df.posts),]

  # Iddle distribution in seconds  
  mins.iddle <- df.posts %>% count(mins.iddle) %>% arrange(desc(n)) %>% select(n)
  mins.iddle <- c(as.matrix(mins.iddle))
  n.freq <- tabulate(mins.iddle)
  n.prob <- n.freq / sum(n.freq)
  
  # Iddle distribution in hours 
  hours.iddle <- df.posts %>% count(hours.iddle) %>% arrange(desc(n)) %>% select(n)
  hours.iddle <- c(as.matrix(hours.iddle))
  n.freq <- tabulate(hours.iddle)
  n.prob <- n.freq / sum(n.freq)
  
  data.list[[i]] <- data.frame(units = 1:length(n.freq),
                               n.freq = n.freq,
                               n.prob = n.prob,
                               subforum = subforum)
}

df <- rbindlist(data.list)
df <- df %>% group_by(subforum) %>% arrange(desc(units)) %>% mutate(cum.prob=cumsum(n.prob)) %>% ungroup

par(mar=c(0,0,0,0)) # Remove unnecessary margins

# saved in file with 930x300 dimensions
g <- ggplot(df, aes(x=units, y=n.prob)) + 
  geom_point(alpha=1, size=0.1) +
  scale_x_log10() + scale_y_log10() +
  theme_bw() +
  facet_grid(. ~subforum) +
  theme(strip.background = element_rect(fill = 'white'),
        aspect.ratio = 1) +
  xlab("hours") + ylab("probability") +
  ggtitle("Iddle time (pdf)")
print(g)
ggsave(file='distribution_iddletime_hours.png', width=200, height=70, units='mm')

g <- ggplot(df, aes(x=units, y=cum.prob)) + 
  geom_point(alpha=1, size=0.1) +
  scale_x_log10() + scale_y_log10() +
  theme_bw() +
  facet_grid(. ~subforum, scales='fixed') +
  theme(strip.background = element_rect(fill = 'white'),
        aspect.ratio = 1) +
  xlab("hours") + ylab("probability") +
  ggtitle("Iddle time (cdf)")
print(g)
ggsave(file='cumdist_iddletime_hours.png', width=200, height=70, units='mm')

