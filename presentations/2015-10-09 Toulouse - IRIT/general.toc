\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Part I: Role detection in social networks}{7}{0}{1}
\beamer@subsectionintoc {1}{1}{Position-based (blockmodeling)}{8}{0}{1}
\beamer@subsectionintoc {1}{2}{Feature-based}{11}{0}{1}
\beamer@subsectionintoc {1}{3}{Discussion}{14}{0}{1}
\beamer@sectionintoc {2}{Part II: Role detection in discussion trees}{16}{0}{2}
\beamer@subsectionintoc {2}{1}{Discussion trees}{17}{0}{2}
\beamer@subsectionintoc {2}{2}{Feature-based analysis of roles}{22}{0}{2}
\beamer@subsectionintoc {2}{3}{Prediction-based}{25}{0}{2}
\beamer@subsectionintoc {2}{4}{Generative model for threads}{38}{0}{2}
\beamer@subsectionintoc {2}{5}{Discussion}{43}{0}{2}
\beamer@sectionintoc {3}{Future work}{44}{0}{3}
