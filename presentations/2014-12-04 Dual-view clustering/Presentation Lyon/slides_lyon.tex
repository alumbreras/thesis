\documentclass[10pt,handout]{beamer} %beamer contiene muchos temas

\usepackage[applemac]{inputenc} %spanish accents
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{amssymb}
\usepackage[T1]{fontenc}
\usepackage{cmbright} %fonts for use in mathematics
\usepackage{fancybox}
\usepackage{multirow}
\usepackage{textpos}
\usepackage[ruled]{algorithm2e}
\usepackage{pdfpages}
\usepackage{tikz}
\usetikzlibrary{fit,positioning}
\usepackage{soul}

\DeclareGraphicsExtensions{{.pdf},{.png},{.jpg}}
\graphicspath{ {./img/} }

% set  theme options
\setbeamertemplate{frametitle}[default][center]
\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{itemize items}[square]
\setbeamertemplate{itemize subitem}[circle]
\setbeamercolor{title}{fg=black!80!black}
\setbeamercolor{frametitle}{fg=black!80!black}
\setbeamercolor{itemize item}{fg=red}
\setbeamercolor{itemize subitem}{fg=red}

\setbeamerfont{frametitle}{series=\bfseries, size=\LARGE}
\setbeamerfont{framesubtitle}{series=\bfseries, size=\normalsize}

%%%% Technicolor footer
%gets rid of bottom navigation bars and symbols
\setbeamertemplate{footline}[page number]{}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{
  \parbox{\linewidth}{\vspace*{-8pt}\insertpagenumber\hfill}
}

% logo technicolor
\usepackage{tikz}
\usebackgroundtemplate{
  \tikz[overlay,remember picture] 
  \node[opacity=1, at=(current page.south east),anchor=south east,inner sep=2pt] {
    \includegraphics[width=0.2\textwidth]{logo_technicolor}};
}

\newcommand{\explain}[2]{\underset{\mathclap{\overset{\uparrow}{#2}}}{#1}}
\newcommand{\explainup}[2]{\overset{\mathclap{\underset{\downarrow}{#2}}}{#1}}

\usetikzlibrary{shapes,arrows}
\setbeamerfont{author}{size=\Large}
\setbeamerfont{title}{size=\fontsize{15}{20}\bfseries}
\setbeamerfont{subtitle}{size=\Large\normalfont\slshape}

\setbeamertemplate{title page}{%
\begin{tikzpicture}[remember picture,overlay]
% Title
\node[anchor=east] at ([yshift=-30pt, xshift=-5pt]current page.north east) (title)
  {\parbox[t]{.8\paperwidth}{\raggedleft%
    \usebeamerfont{title}\textcolor{black}{{\inserttitle}}}};

% Subtitle
\node[anchor=east] at ([yshift=-50pt, xshift=-5pt]current page.north east) (title)
  {\parbox[t]{.8\paperwidth}{\raggedleft%
    \usebeamerfont{subtitle}\textcolor{black}{{\insertsubtitle}}}};

% Authors
\node[anchor=east] at ([yshift=25pt, xshift=-65pt]current page.south east) (author)
  {\parbox[t]{.6\paperwidth}{\raggedright
   \usebeamerfont{author}\textcolor{black}{\insertauthor}}};
   
% ERIC logo
\node[anchor=east] at ([yshift=25pt, xshift=-115pt]current page.south east) (ERIC)
{\includegraphics[width=1.2cm]{logo_ERIC}};
\end{tikzpicture}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \tableofcontents[currentsection]
  \end{frame}
}
\AtBeginSubsection[]
{
\begin{frame}<beamer>
\tableofcontents[
  currentsection,
  sectionstyle=show/shaded,
  subsectionstyle=show/shaded/hide
]
\end{frame}
}

%%%%%%%%%%%%%%%%%%%%% TITLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Multi-view learning of roles in online forums}
\subtitle{Lyon - \insertdate}
\author{Alberto Lumbreras, \\Sup:  Jouve B.,  Velcin J.}

%%%%%%%%%%%%%%%%%%%%%%% END OF PREAMBLE %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%% TITLE PAGE
{

\usebackgroundtemplate{
  \tikz[overlay,remember picture] 
  \node[opacity=1, at=(current page.south east),anchor=south east, inner ysep=5pt, inner xsep=-1pt] {
    \includegraphics[width=1.2\textwidth]{front_background}};
}


%\usebackgroundtemplate{}
\setbeamertemplate{footline}{}
\frame{\titlepage}
\addtocounter{framenumber}{-1}

}

%%%%%%%%%%%%%%%%%%%%% TABLE OF CONTENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{frame}
%\tableofcontents
%\end{frame}
\section{Intro}
\begin{frame}\frametitle{Problem}
Detection of \textbf{roles} in \textbf{online forums} usually based on feature clustering (centrality, number of posts,...)
\begin{itemize}
\item Can we use add \textbf{behavioral functions} so that we can use roles to make \textbf{predictions}?
\end{itemize}
\vfill
Examples:
\begin{itemize}
\item When role $r$ participates in thread, flame wars are more likely.
\item When role $r$ participates in thread, thread is length $N$.
\item When role $r$ talks to role $s$, $s$ never answers back.
\item Role $r$ prefers threads of type $t$.
\end{itemize}
\end{frame}

\begin{frame}
\begin{center}
\Huge{Behaviors as mixtures of roles}
\end{center}
\begin{center}
\begin{align}
f_i &\sim F(f_{z_i})\\
z_i &\sim \text{Multinomial}(\boldsymbol{\pi})
\end{align}
\large{Find roles $\mathbf{z}$ and make predictions from $\mathbf{f}$}

\end{center}
\end{frame}


 \begin{frame} \frametitle{Bayesian framework}

\begin{equation}
\underbrace{p(\theta | y)}_{posterior} 
= 
\frac{\overbrace{p(y, \theta)}^{\text{joint probability}}}{\int_\theta p(y, \theta)}
=
\frac{\overbrace{p(y | \theta)}^{likelihood} \overbrace{p(\theta)}^{prior}}{\int_\theta p(y | \theta) p(\theta)}
\propto 
\overbrace{p(y | \theta)}^{likelihood} \overbrace{p(\theta)}^{prior}
\end{equation}
\pause
If we can solve the integral:
\begin{itemize}
\item get $p(\theta|y)$ analytically.
\end{itemize}
else:
\begin{itemize}
\item sample from $p(\theta|y)$ with MCMC or approximate $p(\theta|y)$ with Variational Inference (\textit{active research}).
\end{itemize}
\pause
\vfill
\textbf{Bayesian predictions}: the \textit{predictive posterior} makes honest predictions:
 \begin{equation}
p(y^{(test)} | y^{(train)}) = \int_\theta p(y^{(test)} | \theta) p(\theta | y^{(train)})\text{d}\theta
\end{equation}
\end{frame}
 

 \begin{frame} \frametitle{Dirichlet Process} 
\framesubtitle{with Chinese Restaurant}

Finite Mixture Model:
\begin{align}
y_i | z_i, \theta &\sim F(\theta_{z_i})\notag\\
z_i &\sim \text{Discrete}(\pi_1,...,\pi_k)\notag\\
\boldsymbol{\pi} &\sim \text{Dirichlet}(\alpha/K,...,\alpha/K)\notag\\
\theta_z &\sim G_0
\end{align}
\pause
If we integrate out $\boldsymbol{\pi}$ and then $K\rightarrow \infty$, a \textbf{Chinese Restaurant} distribution appears:
\begin{align}
y_i | z_i, \theta &\sim F(\theta_{c_i})\notag\\
z_i | \mathbf{z}_{-i} &\sim \text{CRP}(\alpha)
\propto \begin{cases}
n_{-u,j}& \text{if } n_{-u,j}>0 \\
\alpha & \text{if } n_{-u,j}=0
\end{cases}  \notag\\
\theta_c &\sim G_0\notag
\end{align}

\pause 
\textbf{Non-parametric clustering}: We can use this for clustering when we do not know neither number of clusters nor the shape of every cluster (unlike K-means) 
 \end{frame}

\begin{frame} \frametitle{Inference in Dirichlet Process}
\begin{align}
p(\mathbf{z}, \theta | y) = \frac{p(y|\mathbf{z}, \theta) p(\mathbf{z} | \alpha)p(\theta | G_0)}
{\int p(y|\mathbf{z}, \theta) p(\mathbf{z} | \alpha)p(\theta | G_0) \text{d}\mathbf{z}\text{d}\theta}
\end{align}
\begin{itemize}
\item No analytic solution. We cannot do that integral.
\end{itemize}
\pause
\vfill
Markov Chain Monte Carlo methods:
\begin{itemize}
\item Metropolis-Hastings. Take samples from the joint posterior:
   \begin{align}
    p(\mathbf{z}, \theta | y)\propto p( y|\mathbf{z}, \theta) p(\mathbf{z} | \alpha)p(\theta | G_0)
    \end{align} 
\pause
\item Gibbs sampling. Take iterative samples from every conditional:
\begin{align}
 p(z_i = j | z_{-i}, \theta, y) 
 &\propto 
 p(z_i = j| z_{-i}) p(y|\theta, \mathbf{z}) p(\theta | G_0)
 =
\begin{cases}
 n_j^{(-i)} p(y|\theta, \mathbf{z}) p(\theta | G_0)\\
\alpha \int p(y|\theta, \mathbf{z}) p(\theta | G_0)\text{d}\theta
\end{cases}\\
p(\theta | \mathbf{z}, y) &\propto p(\theta | G_0) p(y | \theta, \mathbf{z})
\end{align}
Gibbs is specially useful when we have access to the analytic form of the conditional distributions. (e.g.: we used conjugate priors)
\end{itemize}
\end{frame} 

\section{Model}

\begin{frame} \frametitle{Dual-view learning with Chinese Restaurant prior} 
\framesubtitle{Generative process}
\begin{itemize}
\item $\mathbf{a_u}$: feature vector of user $u$ (observed) (descriptive view, non-supervised view)
\item $f_u$ behavior function of user $u$ (latent). (predictive view, supervised view)
\item $y_u$ be the (observed) outcome of $f_u$. (targets) 
\end{itemize}
\vfill
Generative process:
\begin{itemize}
\item For every cluster $k$:
   \begin{itemize}
   \item Draw a stereotypical feature and a stereotypical behavior from their respective base distributions $\boldsymbol{\theta_k^{(a)}} \sim \mathbf{G_0^{(a)}}$ and $ \boldsymbol{\theta_k^{(f)}} \sim \mathbf{G_0^{(f)}}$ 
   \end{itemize}

\item For every user $u$:
   \begin{itemize}
   \item Draw a cluster assignment $z_u \sim \text{CRP}(\alpha)$.
   \item Draw user features  $\mathbf{a_u} \sim F_a(\theta_{z_u}^{(a)})$. 
   \item Draw a user behavior function $f_u \sim F_b(\theta_{z_u}^{(f)})$.
   \end{itemize}

\item For every $f_u$:
   \begin{itemize}
   \item Draw a behavior $y_u$.
   \end{itemize}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Dual-view learning with Chinese Restaurant prior} 
\framesubtitle{Graphical model}

\begin{figure}
\center
 \scalebox{.8}{
\begin{tikzpicture}
\tikzstyle{main}=[circle, minimum size = 12mm, thick, draw =black!80, node distance = 10mm]
\tikzstyle{connect}=[-latex, thick]
\tikzstyle{box}=[rectangle, draw=black!100]

%% Dirichlet
  \node (alpha) {$\alpha$}; 
  \node [main](pi) [above=1cm of alpha] {$\pi$}; 
  \node [main](z) [above=of pi] {$\mathbf{z_u}$};

%% Clustering
  \node[main, fill = black!10] (a_u) [left=1cm of z] {$\mathbf{a_u}$}; 
  \node[main] (theta_ar) [above=1.5cm of a_u] {$\boldsymbol{\theta_k^{(a)}}$};   

%% Prediction
  \node[main] (f_u) [right=1cm of z] {$\mathbf{f_u}$}; 
  \node[main] (theta_fr) [above=1.5cm of f_u] {$\boldsymbol{\theta_k^{(f)}}$};   
   \node[main, fill = black!10] (y) [right=1.5cm of f_u] {$\boldsymbol{y}$};   
   
  \node [main](G_a0) [above=1cm of theta_ar] {$\mathbf{G_0^{(a)}}$};   
  \node [main](G_f0) [above=1cm of theta_fr] {$\mathbf{G_0^{(f)}}$};   

  \path 
	   %% DP
           (alpha) edge [connect] (pi)
           (pi) edge [connect] (z)
           (z) edge [connect] (f_u)
           (z) edge [connect] (a_u)
           
           %% Clustering
           (G_a0) edge [connect] (theta_ar)
           (theta_ar) edge [connect] (a_u)

           %% Prediction
           (G_f0) edge [connect] (theta_fr)
           (theta_fr) edge [connect] (f_u)
           (f_u) edge [connect] (y);


  % User draws  
  \node[rectangle, inner sep=0mm, fit= (a_u) (z) (f_u), label=above left:U, yshift=-17mm, xshift=39mm] {};
  \node[rectangle, inner sep=4.4mm,draw=black!100, fit= (z) (f_u) (a_u)] {};  

  % Mixture specific parameters   
  \node[rectangle, inner sep=0mm, fit= (theta_ar) (theta_fr), label=above left:$\infty$, yshift=-17mm, xshift=39mm] {};
  \node[rectangle, inner sep=4.4mm, draw=black!100, fit= (theta_fr) (theta_ar)] {};  

\end{tikzpicture}
}
\label{fig:general}
\end{figure}

 \end{frame}
 
\begin{frame} \frametitle{Why should it it work?}
$p(\mathbf{z} | \cdot)$ is a ``Solomonic posterior". Since there is only one clustering for both views, the posterior will listen each view in proportion to how dogmatic the view is about that specific clustering.

\begin{align}
\frac
{p(\mathbf{z}| \cdot)}
{p(\mathbf{z'}| \cdot)}
= 
\frac{
p(\mathbf{z} | \alpha)
}{
p(\mathbf{z'} | \alpha)
}
\frac{
p(\mathbf{a} | \boldsymbol{\theta^{(a)}}, \mathbf{z})
}{
{p(\mathbf{a} | \boldsymbol{\theta^{(a)}}, \mathbf{z'})}
}
\frac{
p(\mathbf{f} | \boldsymbol{\theta^{(f)}}, \mathbf{z})
}{
{p(\mathbf{f} | \boldsymbol{\theta^{(f)}}, \mathbf{z'})}
}
\end{align}
Every view has to fit its parameters to the \textbf{data} and the \textbf{shared clustering}.
\end{frame}  
 
\section{Experiments}

\begin{frame} \frametitle{Toy data}
Users features:
\begin{align}
\mathbf{a_u} \sim \mathcal{N}((2\pi \frac{z_u}{5},2\pi \frac{z_u}{5}), \Sigma_a)
\end{align}
Users coefficients:
\begin{align}
b_u \sim \mathcal{N}(\mu_{z_u}, \sigma)
\end{align}
Threads lengths:
\begin{align}
\mathbf{y} \sim \mathcal{N}(\mathbf{P^T b}, \sigma_y \mathbf{I})
\end{align}

\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]{gen_circle}
\includegraphics[width=0.4\textwidth]{gen_b_linear}
\end{figure}
\end{frame} 

\begin{frame}\frametitle{Benchmarks}
\begin{figure}
\centering
\includegraphics[width=1\textwidth]{benchmark}
\caption{Comparison of the three models for different numbers of users and threads. Bars indicate means of $L$-criterion score over 10 experiments. Error bars are standard deviations.}
\label{fig:benchmark_bars}
\end{figure}
\end{frame}
\begin{frame}\frametitle{Benchmarks}
\begin{figure}
\centering
\includegraphics[width=1\textwidth]{benchmark_ari}
\caption{Comparison of the three models for different numbers of users and threads. Bars indicate means of Adjusted Rand Index over 10 experiments. Error bars are standard deviations.}
\label{fig:benchmark_bars_ari}
\end{figure}

\end{frame} 

\begin{frame}\frametitle{Confusions}
\begin{itemize}
\item Case 1: overlap two groups of attributes.
\item Case 2: overlap two groups of coefficients.
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]
{confused_A_DP_U=100_T=1000_sigma_y=0_22_density=0_5_samples=999}
\includegraphics[width=0.4\textwidth]{confused_b_DP_U=100_T=1000_sigma_y=0_22_density=0_5_samples=999}
\caption{Results for unclear behavior (left) and unclear features (right)}
\label{fig:confused}
\end{figure}
\begin{itemize}
\item The two views help each other.
\end{itemize}
\end{frame} 

\begin{frame}\frametitle{Posteriors}


\end{frame} 
\section{Conclusions}
\begin{frame}\frametitle{Conclusions}
\begin{itemize}
\item Dual-view can enhance predictions if views provide complementary information.
\item Robustness: if not enough data in one view, the other might help.
\item Warning: do not use contradictory information!
\end{itemize}
\end{frame}

\begin{frame}
\begin{center}
{\Huge Thanks!
}\end{center}
\end{frame}



\bibliographystyle{plain}
\bibliography{/Volumes/LLAVERO/Papers/Learning_to_rank}
\end{document}
