\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\author{Alberto Lumbreras}
\begin{document}


Supose we have a dataset with $n$ observations $\mathbf{X} = \{\mathbf{x}_1,...\mathbf{x}_n\}$ and a (log) likelihood function:
\begin{align}
\ln p(\mathbf{X} | \boldsymbol{\theta})
\end{align}

I can get the Maximum Likehood Estimators of the parameter, which I call $\hat{\boldsymbol{\theta}_0}$. I denote the achieved likelihood $\mathcal{L}_0$.

Now I want to fit the same model using mixtures, assuming that there are $K$ groups of points with parameters $\boldsymbol{\theta}_1, ..., \boldsymbol{\theta}_K$. For that, we introduce the a matrix $\mathbf{Z}$ of latent variables where $z_{nk}=1$ if point $n$ belongs to cluster $k$.

The likelihood can the be re-expressed as:

\begin{align*}
\ln p(\mathbf{X} | \boldsymbol{\theta}) = \ln \sum_{\mathbf{Z}} p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})
\end{align*}

Because optimizing with that sum is usually hard, we do a couple of tricks:

\begin{align*}
\ln p(\mathbf{X} | \boldsymbol{\theta}) 
= 
\ln  
\sum_{\mathbf{Z}} q(\mathbf{Z})
\frac{ p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})}{q(\mathbf{Z})}
=
\ln
\mathbb{E}_q\left(
\frac{ p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})}{q(\mathbf{Z})}
\right)
\end{align*}
which is the logarithm of a expected value. By Jennsenn inequality, we know that:
\begin{align*}
\ln \mathbb{E}[x] \geq \mathbb{E}[\ln (x)]
\end{align*}

and then:

\begin{align*}
\ln p(\mathbf{X} | \boldsymbol{\theta}) 
= 
\ln  
\sum_{\mathbf{Z}} q(\mathbf{Z})
\frac{ p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})}{q(\mathbf{Z})}
\geq
\overbrace{
\sum_{\mathbf{Z}} q(\mathbf{Z})
\ln \frac{ p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})}{q(\mathbf{Z})}
}^{\mathcal{L}}
\end{align*}

The equality holds if the function inside the logarithm is constant. And this happens when $q(\mathbf{Z})$ is the posterior of $\mathbf{Z}$. Therefore we have the general EM expression of the lower bound:

\begin{align*}
\mathcal{L}
= 
\sum_{\mathbf{Z}} p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})
\ln 
\overbrace{
\frac{ p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})}{p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})}
}^{p(\mathbf{X} | \boldsymbol{\theta})} 
=
\overbrace{
\sum_{\mathbf{Z}}
p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta}) \ln p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})
}^{Q}
- 
\overbrace{
\sum_{\mathbf{Z}} p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})\ln p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})
}^{H(Z) \text{ (entropy)}}
\end{align*}

\textbf{k=1}

If there is only one cluster, $p(\mathbf{Z} | \cdot)$ can be thought of as a constant, and 
\begin{align*}
\mathcal{L} = \ln p(\mathbf{X}|  \boldsymbol{\theta}) = \mathcal{L}_0
\end{align*}
and if the plug the estilmators $\hat{\boldsymbol{\theta}}_0$ we obtain:
\begin{align*}
\ln p(\mathbf{X}|  \hat{\boldsymbol{\theta}}_0) = \mathcal{L}_0
\end{align*}
\textbf{K overlapped clusters}

The likelihood of $K$ clusters completely overlapped, all sharing the same parameters $\hat{\boldsymbol{\theta}}_0$ we have that $p(\mathbf{X} | \boldsymbol{\theta})$ does not depend on $\mathbf{Z}$, therefore
\begin{align*}
\mathcal{L}
=
\ln 
p(\mathbf{X} | \boldsymbol{\theta})  
\sum_{\mathbf{Z}} p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})
= 
\ln 
p(\mathbf{X} | \boldsymbol{\theta}) 
\end{align*}

Let us demonstrate it again using $Q$ and $H(\mathbf{Z})$.

\begin{align*}
\mathcal{L}
= 
\overbrace{
\sum_{\mathbf{Z}}
p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta}) 
\ln p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})
}^{Q}
- 
\overbrace{
\sum_{\mathbf{Z}} p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})\ln p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})
}^{H(Z) \text{ (entropy)}}
\end{align*}
Since the $\boldsymbol{\theta}$ are equal for every cluster, then:
\begin{align*}
\mathcal{L}
&= 
\overbrace{
\ln p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})
\underbrace{
\sum_{\mathbf{Z}}
p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta}) 
}_1
}^{Q}
- 
\overbrace{
\ln p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})
\underbrace{
\sum_{\mathbf{Z}} p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})
}_1
}^{H(Z) \text{ (entropy)}}
\\&=
\overbrace{
\ln p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})
}^{Q}
- 
\overbrace{
\ln p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})
}^{H(Z) \text{ (entropy)}} = \ln p(\mathbf{X}| \boldsymbol{\theta})
\end{align*}

%If the priors $p(\mathbf{Z})$ are symmetric, then the posterior probabilities $p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})$ are $1/K$

\end{document}