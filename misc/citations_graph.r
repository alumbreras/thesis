# The text file where the edges are codified is manually built through Mendeley Desktop interface.
# author: Alberto Lumbreras

# Also tried:
# pdf-extract extract --references myfile.pdf
#(cannot install)

# State of the Art using Conditional Random Fields:
# http://aye.comp.nus.edu.sg/parsCit/#d
# (cannot install)

# Mendeley API:
# python SDK is buggy, and anyway it cannot do local searches


# Get keys of cited papers from .aux files:
path <- './chapters'
files.name <- list.files(path=path, pattern=".aux", recursive=TRUE, full.names=TRUE)
keys <- c()
for(i in 1:length(files.name)){
  lines <- readLines(files.name[i])
  for(j in 1:length(lines)){
    line <- lines[j]
    if (grepl("bibcite", line)){
      key <- unlist(strsplit(line, "{", perl=TRUE))[2]
      key <- substr(key, 1, nchar(key)-1) # remove }
      keys <- c(keys, key)
    }
  }
}
keys <- unique(keys)

# Draw social citations graph from the .txt file
library(igraph)
lines <- readLines("citations_graph2.txt")
lines <- lines[-1]
g <- graph.empty()

df <- data.frame(matrix(NA, nrow=10000, ncol=2))
names(df) <- c("from", "to")
n <- 0
for(i in 1:length(lines)){
  splitted <- unlist(strsplit(lines[i], "{", perl=TRUE))[2]
  splitted <- substr(splitted, 1, nchar(splitted)-1) # remove last }
  line.keys <- unlist(strsplit(splitted, ","))
  dst <- line.keys[1]
  src.list <- line.keys[-1]
  for(j in 1:length(src.list)){
    src <- src.list[j] 
    df[n,] <-  c(src, dst)
    n <- n+1
  }
}

df <- df[complete.cases(df),]
g <- graph_from_data_frame(df)
  
la <- layout_with_fr(g)
memberships.eigen <- cluster_leading_eigen(g)$membership 
memberships.waltrap <- cluster_walktrap(g)$membership
memberships.betweenness <- edge.betweenness.community(g)$membership

# Size proportional to degree
plot(g,
     layout = la, 
     vertex.color = memberships.eigen,
     vertex.size = 1+log(graph.strength(g, mode="in")+2, 3), 
     edge.width = 1.5, 
     edge.arrow.size=0.2,
     asp=9/16,
     margin=-0.15)
title("Citations - all")

# Only cited papers
idx <- which(!(V(g)$name %in% keys))
g <- delete.vertices(g, idx)
la <- layout_with_fr(g)
memberships.eigen <- cluster_leading_eigen(g)$membership 
plot(g,
     layout = la, 
     vertex.color = memberships.eigen,
     vertex.size = 1+log(graph.strength(g, mode="in")+2, 3), 
     edge.width = 1.5, 
     edge.arrow.size=0.2,
     asp=9/16,
     margin=-0.15)
title("Citations - PhD")

cat("Cites to these keys not found\n")
print(keys[!(keys %in% unique(df$to))])
