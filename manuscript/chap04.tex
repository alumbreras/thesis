\chapter{Detection of user roles with thread growth models}\label{ch:four}
\begin{abstract}
We propose a method to cluster users based on behavioural models. 
\end{abstract}

\minitoc% 


\lettrine{I}{magine} that we observe the behaviours of individuals in a given population. If we count how many times each person has engaged in each behaviour, we might cluster them and find groups of people that behave in a similar way. We might find, for instance, the group of firefighters; the observed behaviours are eating, sleeping, exercising, fire fighting and rescue cats from trees. However, this clusters is purely descriptive; if we see the firefighter in a given context such as a firing and a cat on top of a tree, the cluster has nothing to say about which action will the firefighter choose: saving the cat or fighting the fire. 

In the last chapter, we proposed detecting roles based on the structure of the conversations where users participate. We claimed that this way of characterising users captures relevant behaviours better than other featured-based methods. Nonetheless, this approach is purely descriptive. There is still a missing link that connects a user's observed behaviour to some \textit{behavioural function} that models why the user has chosen that action. 
%
We conceptualise a behavioural function as a probability distribution over the space of all possible behaviours in a given context. We assume that there exists a  finite \textit{repertoire} of behavioural functions and that all the observed behaviours of a user are drawn from one of these functions. We say that users who share the same behavioural function hold the same role. 

In this chapter we set three main goals: (a) proposing a behavioural function for discussion threads (b) finding groups of users with the same behavioural function (the same parameters) and (c) testing whether these behavioural functions have predictive power ---if they can predict the behaviour of a user in a new context.  

We will use random graph models as the basis for our behavioural functions. In particular, we will focus our attention on growth models. Growth models are random generators of graphs that try to mimic the growing mechanism of a network through stochastic processes governed by a set of parameters. Formally, a growth model defines a probability distribution that quantifies the probability of an existing vertex $i$ of being chosen as the parent for a new vertex $x_t$:
\begin{equation}
p(x_t \sim i | G_{t-1}; \boldsymbol{\theta})
\label{eq:growthmodel}
\end{equation} 
where $G_{t-1}$ is the state of the graph before $x_t$ is attached and $\boldsymbol{\theta}$ are the parameters of the model ---the specification of this probability distribution depends on what we think is a reasonable assumption about the growth process. These models may be seen as behavioural functions since they model the way how users choose a post to reply. The \textit{repertoire} of possible behaviours is then a set of parameters $\boldsymbol{\theta}_1,... \boldsymbol{\theta}_K$, and the above probability will therefore depend on the $\boldsymbol{\theta}$ associated to the author of the post $x_i$ ---the author's role.

The structure of this chapter is as follows. In Section~\ref{sec:related-work} we present the different growth models that can be applied to tree graphs. In Section~\ref{sec:lumbreras} we address our goal (a); we propose an adaptation of one of the models to allow that posts written by different users have different growth parameters $\boldsymbol{\theta}$. The idea is very simple, and consists on estimating, by Expectation-Maximization, clusters of users with their own parameters. In Section~\ref{sec:experiments} we address our goals (b) and (c) by finding clusters of users and their parameters in a Reddit dataset, and by testing whether our model is a better predictor in a test set.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Network Growth models}\label{sec:related-work}

Random graph models are stochastic generators of graphs that try to reproduce the properties of some real-world network. A good random graph reproduces many relevant properties with few assumptions and a small number of parameters. If the generated graphs and the real-world graphs share some relevant properties, then the proposed growth mechanism might be a reasonable approximation of the growth laws under which the real-world graphs evolve \citep{Kolaczyk2009}. 

We represent a discussion tree at time step $t$ as a vector of parents $\boldsymbol{\pi}_{(1:(t-1))} = (\pi_1,...,\pi_{t-1})$ where $\pi_i$ is the parent of the post written at the time-step $i$. Note that this representation does not lose any structural information. Following this notation, Equation~\ref{eq:growthmodel} can be re-expressed as:
\begin{align}
p(\pi_t = i| \boldsymbol{\pi}_{1:(t-1)}, \boldsymbol{\theta}) 
\end{align}

%%%%%%%%%%%%%%%%%%%%%%%
Our growing graph is therefore a tree that starts its growing process with a first vertex (root post) written at $t=0$ that triggers a conversation. The parent of the next post, written at $t=1$, will always be the root ($\boldsymbol{\pi}_1=1$) Then, at each time-step $t$ a post is added to the tree creating a new vertex (a reply) to an older post $i$ ($\boldsymbol{\pi}_t = i$). One might hypothesise that users tend to reply to popular posts (\textit{preferential attachment}) or that they prefer recent posts, or well-written posts, or that all posts have indeed the same probability of being replied. Two growth models for discussion trees have been proposed in \cite{Kumar2010} and \cite{Gomez2012}. In \cite{Kumar2010}, the probability of replying to a post depends on the number of replies and its recency. In \cite{Gomez2012}, the probability depends on the number of replies, its recency and whether a post is the root.

The remaining of this section is as follows. First, we recall the Preferential Attachment model of \cite{Barabasi1999} and three other growth models for discussion threads \citep{Kumar2010,Gomez2010,Gomez2012}. Then we present our model, which finds $k$ sets of parameters for $k$ types of user and is based on \cite{Gomez2012}. 

%%%%%%%%%%%%%%%%%%%%%%%
In the following sections, we describe the growth models that have been proposed to explain the growth of online conversations. A summary is shown in Table~\ref{tab:models}.

%%%%%%%%%%%%%%%%%%%%%%% TABLE OF MODELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
 \begin{center}
   \tabcolsep = 0.8\tabcolsep
   \begin{tabular}{|l|c|l|}
      \hline
       Authors    & $p(\pi_t = k | \boldsymbol{\pi}_{1:t-1}) \propto$ &Parameters \\
   \hline 
\cite{Barabasi1999} & $d_{k,t}^\alpha$  & degree \\
\cite{Gomez2010} & $(\beta_k d_{k,t})^{\alpha_k}$ & degree, root\\
\cite{Kumar2010} & $\alpha d_{k,t} + \tau^{t-k}$ & degree, recency \\      
\cite{Gomez2012} & $\beta_k + \alpha d_{k,t} + \tau^{t-k}$ & degree, recency, root\\
   \hline
   \end{tabular}
 \end{center}
 \caption{Growth models for online discussions}
 \label{tab:models}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\subsection{Barabasi-Albert (1999)}
The \textit{preferential attachment} model proposed by \cite{Barabasi1999} is the best known growth model. The Barabasi-Albert model builds a graph by sequentially adding its vertices; once a new vertex $t$ is added to the graph it decides whether to create an edge to an existing vertex $i$ with probability

\begin{align}
 p(\pi_t = i | \boldsymbol{\pi}_{1:(t-1)}) =\frac{d_{i,t}^\alpha}{Z_t}; &\qquad
 Z_t = \sum_{j=1}^{t} d_{j,t}^\alpha
\end{align}
where $d_{i,t}$ is the degree of the vertex $i$ before vertex $t$ is added. This model reproduces a rich-get-richer phenomena controlled by the parameter $\alpha$. The particular case of $\alpha=1$ is called \textit{linear preferential-attachment} since the probabilities increase linearly with the number of degrees. Figure~\ref{fig:Barabasi-Albert} shows examples of Barabasi-Albert graphs generated with different $\alpha$. Graphs generated by the Barabasi-Albert model reproduce some interesting properties of the real networks such as a power-law distribution of the vertices degrees.

\begin{figure}
 \centering
 \subfloat[$\alpha=0$]{\includegraphics[width=0.33\textwidth]{ch4_barabasi_0}}
 \subfloat[$\alpha=1$]{\includegraphics[width=0.33\textwidth]{ch4_barabasi_1}}
 \subfloat[$\alpha=1.8$]{\includegraphics[width=0.33\textwidth]{ch4_barabasi_1_8}}
 \caption{Barabasi-Albert graphs with one edge created at every step.}
 \label{fig:Barabasi-Albert}
\end{figure}



\subsection{Kumar et al. (2010)}
In \cite{Kumar2010} the authors propose a model that combines both \textit{preferential-attachment} and \textit{recency}. The higher the degree of a post and the later it was published, the easier for this post to attract the incoming replies. Besides, at every time step, a decision is made to stop the thread or to add a new post. Every new post chooses its parent according to:
\begin{align}
p(\pi_t = i | \boldsymbol{\pi}_{1:t-1}) = \frac{\alpha d_{i,t} + \tau^{t-i}}{Z_t}
\end{align}
and the probability of stopping the thread is
\begin{align}
p(\pi_t = \emptyset | \boldsymbol{\pi}_{1:t-1}) = \frac{\delta}{Z_t} 
\end{align}

The authors report that when the alternative function $d_{i,t} \tau^{t-i}$ is used, the recency factor prevents the preferential attachment factor from generating heavy-tailed degree distributions. The choice of placing $\alpha$ as a coefficient instead of an exponent is made for mathematical convenience so that $Z_t$ does not depend on the graph structure at that particular moment:
\begin{align}
Z_t 
= 
\delta + \sum_{j=1}^{t} \alpha d_{j,t} + \tau^{t-j}
=
\delta + 2(t-1) + \beta + \frac{\tau(\tau^t+1)}{\tau-1}
\end{align}

The authors also propose an improvement of the model to account for the identity of posts authors. For a new post $v$ replying to a post $u$, its author $a(v)$ can be either $a(u)$ (a self-reply), another author $a(w)$ that has already participated in the chain from $u$ to the root, or some other new author belonging to the set of authors $A$ that have not participated in the chain:
\begin{align}
a(v) = 
\begin{cases}
a(w) & \text{ with probability } \gamma\\
a(u) & \text{ with probability } \epsilon\\
a \in A & \text{ with probability } 1 -\gamma - \epsilon 
\end{cases}
\end{align} 

The Maximum Likelihood Estimators of the parameters $\alpha, \tau, \gamma, \epsilon$ are found by a grid search.

\subsection{Gómez et al. (2010)}
In \cite{Gomez2010} the authors combine \textit{preferential-attachment} with a \textit{bias towards the root}. The probability of choosing an existing parent $k$ is:
\begin{align}
p(\pi_t = k | \boldsymbol{\pi}_{1:t-1}) 
=
\frac{(\beta_k d_{k,t})^{\alpha_k}}{Z_t}
\end{align}
where 
\begin{align}
\alpha_k = 
\begin{cases}
 \alpha_1 & \text{ for } k=1 \\ 
 \alpha_c & \text{for } k \in \{2,...,t\}
\end{cases}\notag\\
\beta_k = 
\begin{cases}
\beta & \text{ for } k=1 \\ 
1 & \text{for } k \in \{2,...,t\}
\end{cases}
\end{align}

Note that $\alpha_k$ is the preferential attachment exponent and that if $\alpha_1=\alpha_c$ and $\beta=1$ we recover the Barabasi-Albert model of preferential attachment.

The Maximum Likelihood Estimators of the parameters $\alpha, \tau, \gamma, \epsilon$ are found using the Nelder-Mead algorithm. Though Nelder-Mead can be used in non-convex functions, this loglikelihood is convex. Figure~\ref{fig:Gomez} shows some trees generated with their estimated parameters.
 \begin{figure}
  \centering
  \subfloat[real]{\includegraphics[width=0.45\textwidth]{ch4_gomez_real}}\hfill
  \subfloat[synthetic]{\includegraphics[width=0.45\textwidth]{ch4_gomez_synthetic}}
  \caption{Random grahs for discussion threads. Gómez-Kappen-Kaltenbrunner}
  \label{fig:Gomez}
 \end{figure}

\subsection{Gómez et al. (2012)}
In \cite{Gomez2012} the authors combine \textit{preferential-attachment}, a \textit{bias towards the root} and \textit{novelty}. Unlike in their former model in \cite{Gomez2010}, here they sum these factors instead of multiplying them:

\begin{equation}
p(\pi_t = i | \boldsymbol{\pi}_{1:t-1}) 
=
\frac{\beta_i + \alpha d_{i,t} + \tau^{t-i}}{Z_t}
\label{eq:gomez2012}
\end{equation}

As in \cite{Gomez2010}, Maximum Likelihood Estimators are found by Nelder-Mead optimisation. However, the log-likelihood is now non-convex. Authors reported that for large enough data the problem seems to approach convexity and the optimisation algorithm tends to give the same optimum for different initializations.


\section{A role-based network growth model}\label{sec:lumbreras}
%Usually, a growth model has a fixed set of parameters and their specific values allow giving more or less importance to some aspects of the model (e.g: recency, degree). In the context of forums, however, it may be convenient to assume that users can be divided into groups and that each group of users has different parameters. That means that posts written by users in different groups will be applied have different growth processes.

%Motivation
The models presented above consider that the probability of choosing a parent is independent on the user who writes the post. In other words, that the model parameters are shared by all the users. However, it seems reasonable to think that different users may behave according to different parameters. Some users, for instance, might tend to reply to the root and avoid conversations deeper in the tree. Others might tend to ignore old posts. Others might be especially attracted by popular posts. Formally, we assume that there are $K$ latent types of users and that users of type $k$ behave according to their own group parameters $\boldsymbol{\theta}_k$. We think of these parameters as the ones that control the different user roles. Thus, we will say that users in with similar parameters (similar behavioural functions) share the same role. In this section, we present a modification to the model of \citep{Gomez2012} that finds different parameters for different groups of users.

\subsection{Formalization}
For any given post $n$, let $d_n$ denote the degree of its parent; let $r_n$ be 1 if the parent of $n$ is the root, and 0 otherwise. Let $l_n$ be the number of time-steps elapsed between the parent of $n$ and $n$ ($l_n \geq 1$). Let $\mathbf{X} = \{\mathbf{x}_1,...,\mathbf{x}_N\}$ be the set of posts and let $\mathbf{x}_i = \{t_i, d_i, r_i, l_i\}$  be the set of features associated to a post. Let us assume that there are there are $K$ different types, or roles, of users that behave following different parameters $\boldsymbol{\theta}_1$,...,$\boldsymbol{\theta}_K$ where  $\boldsymbol{\theta}_k = \{\alpha_k, \beta_k, \tau_k$\}. Let $z_u$ be the cluster ---or role--- of user $u$. Let $N_u$ be the set of posts written by $u$. The log-likelihood of the whole dataset can be expressed as:
\begin{align}
\ln p(\mathbf{X} | \boldsymbol{\theta}) = 
\sum_{u=1}^U \sum_{n \in N_u} 
\frac{1}{Z_n}\left(\alpha_{z_u} d_{n} + \beta_{z_u}r_{n} + \tau_{z_u}^{l_n}\right)
\end{align}
where $Z_n$ is a normalization factor that guarantees that the probabilities of all possible choices sum up to one. Let $t$ be the time-step when the post $n$ is written and let $M$ denote the set of posts that have been added to the thread before the post $n$. The normalization factor associated to that post is:

\begin{align}
Z_n &=  
\sum_{m \in M} \alpha_{z_n} d_m + \beta_{z_n} r_m + \tau_{z_n}^{l_m}=
\alpha_{z_n} (2t -1) + \beta_{z_n} + \frac{\tau_{z_n}(\tau_{z_n}^t-1)}{\tau_{z_n}-1}
\end{align}
Note that this sum only depends on the time-step $t$ and the model parameters, but it does not depend on the particular structure of the thread \footnote{We denote as $z_n$ the cluster of the author of post $n$ in order to be consistent with the literature as well as with our following section and our following chapter. This should not be confused with $Z_t$ that refers to the normalisation factor. Nevertheless, this normalisation factor will not appear any more.}. In practice, $\mathbf{X}$ may be represented as a matrix of feature vectors $\mathbf{x}_i$ that makes the computing of the log-likelihood easy to vectorise in some programming languages.

\subsection{Expectation-Maximization for the role-based growth model}\label{sec:em}
We want to estimate the parameters of each role $\boldsymbol{\theta}_1,...,\boldsymbol{\theta}_K$ and the latent role of every user $z_1,...,z_U$. If there was one one group of $\boldsymbol{\theta}$ there would be no $\mathbf{Z}$ and we could use Nelder-Mead to find the parameters. However, if there are different groups then the optimization of the parameter will depend on the group since the parameters will be optimized considering who belongs to that group. This is a classic scenario that can be solved by Expectation-Maximization (E-M). In Expectation-Maximization, we do an iterative optimization over the parameters $\boldsymbol{\theta}_1,...,\boldsymbol{\theta}_K$ and the class assignments $\boldsymbol{z}_1,...,\boldsymbol{z}_U$ until the likelihood converges. In particular, we maximize this log-likelihood: 
%a lower bound of the loglikelihood:
%\begin{align*}
%\argmax_{\boldsymbol{\theta}, \boldsymbol{\pi}} 
%\underbrace{
%\sum_{\mathbf{Z}} p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})
%\overbrace{
%\left(\ln p(\mathbf{Z} | \boldsymbol{\pi})  + \ln p(\mathbf{X  | \mathbf{Z}, \boldsymbol{\theta}})\right)
%}^{\ln p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})}
%}_{\mathbb{E}_\mathbf{Z}\left[\ln p(\mathbf{X, \mathbf{Z} | \boldsymbol{\theta}})\right]} 
%\end{align*}
\begin{align}
\underbrace{
\sum_{\mathbf{Z}} p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})
\overbrace{
\left(\ln p(\mathbf{Z} | \boldsymbol{\pi})  + \ln p(\mathbf{X  | \mathbf{Z}, \boldsymbol{\theta}})\right)
}^{\ln p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})}
}_{\mathbb{E}_\mathbf{Z}\left[\ln p(\mathbf{X, \mathbf{Z} | \boldsymbol{\theta}})\right]} 
\end{align}
where $\boldsymbol{\pi}$ are the a priori probabilities assigned to each cluster. At each iteration, we update  the posterior $p(\mathbf{Z} | \mathbf{X}, \boldsymbol{\theta})$ using the $\mathbf{\theta}$ of the last iteration (E-step) and then we re-compute the parameters $\boldsymbol{\theta, \pi}$  that maximise the whole term using the updated posterior (M-step). We repeat the E and the M step until the improvement in the log-likelihood is lower than some threshold.

We now provide the exact equations to maximize in the E and M steps for our model. Let $\mathbf{X}_u$ the submatrix of $\mathbf{X}$ composed of all posts written by user $u$. Let $\mathbf{Z} = \{\mathbf{z}_1,...,\mathbf{z}_U\}$ be the indicators matrix where $\mathbf{z}_i=\{z_{i1},...,z_{iK}\}$ and where $z_{ik}$ is one if user $i$ belongs to group $k$ and zero otherwise. For, the M-step, the expectation of the complete log-likelhood is:
\begin{align}
\mathbb{E}\left[
\ln p(\mathbf{X, Z} | \boldsymbol{\theta}) 
\right]
&= 
\mathbb{E}\left[
\sum_{u=1}^U 
\sum_{k=1}^K 
z_{uk}
\left\lbrace\ln \pi_k + 
\ln p(\mathbf{X}_u | \boldsymbol{\theta}_k)\right\rbrace
\right]\\
&=
\sum_{k=1}^K 
\sum_{u=1}^U 
\mathbb{E}\left[z_{uk}\right]
\left\lbrace
\ln \pi_k + 
\ln p(\mathbf{X}_u | \boldsymbol{\theta}_k)
\right\rbrace
\label{eq:m-step}
\end{align}
where, for a given cluster $k$, each $\mathbf{X}_u$ contributes proportionally to $\mathtt{E}[z_{uk}]$. We note that the parameters of each cluster can be optimised separately as:
\begin{equation}
\argmax_{\boldsymbol{\theta}_k}
\sum_{u=1}^U 
\mathbb{E}\left[z_{uk}\right]
\left\lbrace
\ln \pi_k + 
\ln p(\mathbf{X}_u | \boldsymbol{\theta}_k)
\right\rbrace
\end{equation}

and for the  $\pi$ parameter:
\begin{equation}
\argmax_{\boldsymbol{\pi}_k}
\sum_{u=1}^U 
\mathbb{E}\left[z_{uk}\right]
\left\lbrace
\ln \pi_k + 
\ln p(\mathbf{X}_u | \boldsymbol{\theta}_k)
\right\rbrace
\end{equation}



 In the E-step we update the posterior:
\begin{align}
p(\mathbf{Z} | \mathbf{X},\boldsymbol{\theta})
=
\frac{
p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})
}
{
\sum_{\mathbf{Z}} p(\mathbf{X}, \mathbf{Z} | \boldsymbol{\theta})
}
=
\frac
{\prod_{u=1}^U\prod_{k=1}^K  p(\mathbf{X}_u | \boldsymbol{\theta}_k)^{z_{uk}}}
{\sum_{\mathbf{Z}} \prod_{u=1}^U\prod_{k=1}^K  p(\mathbf{X}_u | \boldsymbol{\theta}_k)^{z_{uk}}}
\end{align}
which can be easily factorized by users, and then we can obtain the expected value for each $z_{uk}$:
\begin{align}
\mathbb{E}[z_{uk}] 
=
\sum_{z_{uk}} z_{uk} 
\frac
{\pi_k p(\mathbf{X}_u | \boldsymbol{\theta}_k)}
{\sum_{k=1}^K \pi_k p(\mathbf{X}_u | \boldsymbol{\theta}_k)}
=
\frac
{\pi_k p(\mathbf{X}_u | \boldsymbol{\theta}_k)}
{\sum_{k=1}^K \pi_k p(\mathbf{X}_u | \boldsymbol{\theta}_k)}
\label{eq:e-step}
\end{align}
where the likelihood $p(\mathbf{X}_u | \boldsymbol{\theta}_k)$ can be also factorised:
\begin{align}
   p(\mathbf{X}_u | \boldsymbol{\theta}_k) = \prod_{n \in N_u} p(\mathbf{x}_n | \boldsymbol{\theta}_k)
\end{align}

The E-step is done with Equation~\ref{eq:e-step} and the M-step is done with Equation~\ref{eq:m-step} Because, due to the form of our likelihood, Equation~\ref{eq:m-step} cannot be analytically maximized, we use Nelder-Mead optimization as in \cite{Gomez2012}.

\section{Experiments}\label{sec:experiments}

%         subforum threads
%            <chr>   <int>
%1          france   31935
%2   gameofthrones   91418
%3 MachineLearning    8727
%4         podemos   65529
%5 TwoXChromosomes   93274
Ideally, we would like our model to be descriptive and predictive. That is, we would like it to give clusters of users and parameters of each cluster, but also to use these parameters to predict a user behaviour in new threads. If the model can predict, it would confirm that meaningful roles exist and that the behaviour of a group of users is consistent, not just circumstantial or mere noise.

In this section we infer the parameters for our model and find clusters of users in the \textit{podemos} dataset of Reddit. Then we benchmark our model against \cite{Gomez2012} (henceforth \textit{gomez} and \textit{lumbreras}) in two different tasks. First, we test whether our model can generate synthetic threads that are more realistic than those generated by \textit{gomez}. Lastly, we test whether our model can make better predictions of post replies in a test set.

\subsection{Dataset}
In order to test the predictive power of our model we split each user's posts in \textit{training} (50\%), \textit{validation} (25\%) and \textit{test} (25\%). We used the training set of posts to estimate the parameters of \textit{gomez} ($\alpha, \beta, \tau$) and \textit{lumbreras} ($\boldsymbol{\pi}$, $\alpha_k, \beta_k, \tau_k$ for each cluster, $p(z_u = k | \mathbf{X}_u, \boldsymbol{\theta}_k)$ for each user). We used the validation to select the final number of clusters in \textit{lumbreras}, and we used the test set to compare the results of the two models. 

Users with only one or two posts will be assigned to some of the clusters (and its parameters) even if one or two posts are clearly not enough information to infer nothing about the user. Thus, we selected the 1000 users with more posts in the forum.

%Note that to estimate the parameters we only need the vector $\mathbf{x}_i = \{t_i, d_i, r_i, l_i\}$ that describe each post reply. Thus, given the vector $\mathbf{x}$ of each post we can ignore the rest of the tree. We will only need the whole tree when testing the model since we will try to predict the parent of a post given a (real) tree.

\subsection{Inference}
To infer the cluster of each user $\mathbf{z}_1,...\mathbf{z}_U$ and the cluster parameters $\boldsymbol{\theta}_1,...\boldsymbol{\theta}_K$ we initialise the parameters $\alpha, \beta, \tau$ for each cluster and run the Expectation and Maximization steps defined in Section~\ref{sec:em}. Regarding the M-step, the non-convexity of the log-likelihood might make it necessary to try different re-starts to reduce the odds of being trapped in a local maximum. However, as in \cite{Gomez2012}, we noted that the Nelder-Mead optimisation in our Reddit dataset only gives different maxima when the number of observations is small.  

% Number of clusters
% Page 5
%http://www.ics.uci.edu/~smyth/courses/cs274/readings/fraley_raftery.pdf
%7]), the method can be shown to converge to a local maximum of the mixture
%likelihood (2).  Although the conditions under which convergence has been proven do not
%always hold in practice, the method is widely used in the mixture modeling context with
%good results. Moreover, for each observation i ,(1 − max kz∗ik) is a measure of uncertainty in the associated classification (Bensmail et al. [9]).

%For a discussion on other possible methods see \citep{BDA} (Chapter 7).

%https://www.hse.ru/pubs/share/direct/document/112026734	
%he trouble with these criteria is that they tend to be monotone over K when a cluster structure is not well manifested (Windham and  Cutler 1992
\begin{figure}
\center
\includegraphics[width=0.5\textwidth]{ch4_linkprediction_BICpodemos.eps}%
\includegraphics[width=0.5\textwidth]{ch4_linkprediction_BICgameofthrones.eps}
\caption{BIC values for model selection in \textit{podemos} and \textit{gameofthrones}}
\label{fig:BIC}
\end{figure}

\textbf{Number of clusters.---} To decide the number of clusters, we computed the Akaike Information Criteria (AIC) and the Bayesian Information Criteria (BIC) for multiple models from one cluster (which recovers \cite{Gomez2012}) to more than 50. The AIC and the BIC are measures of the likelihood penalised by the complexity of the model. In particular, the AIC is defined as:
\begin{align*}
AIC = -2 \mathcal{L} + 2n_p
\end{align*}
where $\mathcal{L}$ is the log-likelihood and $n_p$ is the number of parameters (for $k$ clusters with three parameters in each cluster, $n_p = 3k + k-1$). And the BIC is defined as:
\begin{align}
BIC = -2 \mathcal{L} + log(n)n_p
\end{align}
where $n$ is the number of observations. The best model candidates are considered those that minimise the applied metric. We computed the AIC and BIC in the training set and we found that both the AIC and BIC decrease almost monotonically. In Figure~\ref{fig:BIC} we show the BIC curves for \textit{podemos} and \textit{gameofthrones}. We obtained similar results when we computed the AIC and BIC in the validation set. This monotonicity might indicate a poor clustering structure in the data \citep{Caulkins1992}. To check this, we computed, for each user, the uncertainty of is classification as $(1 - max(z_{i1},...z_{iK}))$ \citep{Bensmail1997} and we obtained a mean of 0.12 and a median of 0.03, which means that the model is very sure about the user memberships. Thus, the flat BIC might mean that most users are not in the middle of two clusters but that adding a new cluster will usually achieve a better fit. We chose $k=5$ to keep a low complexity of the model---as a sanity check, we repeated the experiments below for $k=10$ and $k=15$, obtaining very similar results. 

%             [,1]       [,2]      [,3]
%[1,] 5.796980e-02 40.9495367 0.8462366
%[2,] 4.564061e-02  4.1186629 0.8132622
%[3,] 4.257737e-06  1.4835162 0.9041666
%[4,] 9.032735e-03  0.4611963 0.6972683
%[5,] 3.642460e-02  1.8007139 0.8987985

%  1   2   3   4   5 
% 99 436  35 240 181 
\begin{table}
 \begin{center}
  \tabcolsep = 0.8\tabcolsep
  \begin{tabular}{|r|c|c|c|c|c|}
\hline
k & $\alpha$ & $\beta$ & $\tau$ & $\pi$ & size\\
\hline 
1 & 0.0579  & 40.95  & 0.84 & 0.10 & 99\\ 
2 & 0.046  & 4.11  & 0.81 & 0.43 & 436\\ 
3 & 0   & 1.48  & 0.90 & 0.035 & 35\\
4 & 0.009   & 0.46  & 0.69 & 0.24 & 240\\
5 & 0.0364  & 1.80  & 0.89 & 0.18 & 181\\
\hline
Gomez & 0.00 & 3.58 & 0.93 & - & \\
\hline
   \end{tabular}
 \end{center}
\caption{Estimated paramaters}
\label{tab:parameters}
\end{table}

\begin{figure}
\center
\includegraphics[width=0.6\textwidth]{ch4_podemos_cluster_posts.eps}%
\caption{Number of posts per user in each cluster (means in big red circles)}
\label{fig:cluster_posts}
\end{figure}

The estimated parameters are shown in Table~\ref{tab:parameters}.  Figure~\ref{fig:cluster_posts} shows the posts per user in each cluster, assuming a final hard assignment of users to clusters where users are assigned to their most likely cluster. All clusters have different parameters that \textit{gomez}, meaning that not all users have behave similarly in our training set. Cluster 1, for instance, is a group of users with a very high $\beta$ (attraction to root posts). 

\subsection{Structural properties}
After estimating the parameters for \textit{gomez} and \textit{lumbreras} we generated 10,000 synthetic threads with each in order to see whether there is some structural difference between the two. We generated the threads as follows. We assume that we know the authors and the order in which they participate, but we do not know to whom they will reply within their posts ---we need the authorship information to know which parameters we have to apply. Thus, for a randomly chosen thread in the dataset, we keep the sequence of authors of the posts chronologically sorted, and we remove the edges. That leaves us with a sorted sequence of posts with no tree structure. Then, we rebuild a new set of synthetic trees post by post following, for each tree, the real sequence of authors but the parameters estimated by the model. Recall that, in \textit{lumbreras}, the parameters applied to a post $v$ depend on the cluster of its author $a(v)$. In other words, a post chooses its parent according to its parameters $\alpha_{z(a(v)}, \beta_{z(a(v)}, \tau_{z(a(v)}$. If the author is not in our list of analysed users, we used the parameters estimated in Gomez. Therefore, the only difference between the trees generated by \textit{lumbreras} and \textit{gomez} is in the posts written by the 1000 most active users. 

Following \citep{Gomez2012}, we measured the following properties:
\begin{itemize}
\item degree distribution: number of replies to a post.
\item subtree size: number of descendants of a post.
\item size versus depth: number of posts in the tree versus length of the longest chain.
\end{itemize} 

The results are shown in Figure~\ref{fig:xp_synthetic_trees}. We observe that the ability to reproduce real structures is very similar in both models. This is not entirely unsurprising since, as we said above, only those users in the top 1000 have different parameters than the \textit{gomez} model. 

\begin{figure}
\center
\includegraphics[width=1\textwidth]{ch4_structures_podemos_k5}
%\includegraphics[width=1\textwidth]{ch4_structures_TwoXChromosomes_k5}
\caption{Synthetic trees vs real trees in the Podemos dataset}
\label{fig:xp_synthetic_trees}
\end{figure}

\subsection{Link prediction}
We finally analysed whether our clusters ---roles--- have predictive power. If users behave, at some degree, according to role archetypes, we should be able to predict their behaviour using the parameters associated to their estimated role. Otherwise, the clusters are only a good description of what happened.

We tested the predictive power of our clusters through a task of link prediction, proceeding as follows: for all the trees in our dataset, we removed the parent of those posts that had been labelled as \textit{test} and we tried to predict their parents with \textit{lumbreras} and \textit{gomez}. We took three different metrics ---likelihood of the test observations, percentage of hits and ranking error--- and compared the two models and the different clusters. For a better understanding of the strengths and weaknesses of the models, we included two reference models: a model that always chooses the post with the highest degree (\textit{barabasi}) and a model that always chooses the most recent post (\textit{tau}).

\textbf{Likelihood of test data.---} We summed the negative log-likelihoods of each choice of parent given the model parameters and the author of the post (for the \textit{lumbreras} model).
Given a post, a set of candidate parents and the parameters of the model, we know how to compute the likelihood of each possible parent choice. For \textit{gomez}, the predicted parent  of a post $n$, denoted as $\pi_n^*$, is the post with a maximum likelihood according to Equation~\ref{eq:gomez2012}. For \textit{lumbreras}, we first get the most likely cluster of the author
\begin{equation}
z^{'}_i = \argmax_k p(z_{ik} | \mathbf{X}, \boldsymbol{\theta}_k)
\end{equation}
and then apply the parameters of the cluster $z^{'}_i$ to the same equation.

Figure~\ref{fig:likelihoods} shows the sum of negative log-likelihoods in each cluster normalised by the number of posts of the authors in that cluster. The negative log-likelihood is lower (better) for \textit{lumbreras} in every cluster. Cluster 1, the group of users with a very high $\beta$, is specially predictable. Even if for \textit{gomez} it is also the cluster with a more likely behaviour, the improvement in \textit{lumbreras} is even better.
\begin{figure}
\center
\includegraphics[width=0.75\textwidth]{ch4_linkprediction_likelihoodspodemos_k5.eps}%
\caption{Likelihoods per cluster in the test set}
\label{fig:cluster_posts}
\end{figure}

\textbf{Hits.---} We define as a \textit{hit} when the chosen parent was the most likely parent according to the model. Figure~\ref{fig:post_rankings} (left) shows the hits for \textit{gomez}, \textit{lumbreras} and the other two reference models. On the one hand, there is almost no difference between \textit{gomez} and \textit{barabasi}, which means that \textit{gomez} usually assigns more likelihood to the post with more replies. This is surprising since \textit{gomez} has $\alpha=0$ in this dataset. A possible explanation is that, since \textit{gomez} has a high $\beta$ and root posts tend also to have a higher degree, predicting the post with the highest degree has often the same result than predicting the root. Another remarkable thing is that the \textit{tau} model is always worse except for cluster 3 where it overperforms the other models. That means that for users in cluster 4 the recency of a post is more important than its degree. \textit{lumbreras} does even better predictions for this cluster. Yet this is the only cluster where \textit{lumbreras} is clearly better than \textit{barabasi} and \textit{gomez}.
\begin{figure}
\center
\includegraphics[width=1\textwidth]{ch4_linkprediction_hits_whisperspodemos_k5.eps}%
\caption{Hits and ranking errors in the test set}
\label{fig:post_rankings}
\end{figure}

\textbf{Normalised Ranking Error.---} To get a finer metric than \textit{hits}, we defined the Normalised Ranking Error (NRE) as:

\begin{equation}
NRE = \frac{r-1}{l-1}
\end{equation}
where $r$ is the position of the chosen parent in the predicted ranking and $l$ is the length of the thread, or the number of parents to choose from ($1 \leq r \leq l$).

While the \textit{hits} where low for almost every cluster, the ranking error shows a more optimistic picture: the medians for \textit{gomez} and \textit{lumbreras} are clearly better than those of the reference models. Yet, the is barely any difference between \textit{lumbreras} and \textit{gomez}.

Finally, Figure~\ref{fig:nre_size} plots the average NRE by size. It shows that, the longer the thread, the easier for \textit{tau} to make better predictions and the harder for \textit{barabasi}. In other words, the longer the thread, the less importance of the degree and the more importance of the recency ---until 75 posts where it gets stabilised. \textit{gomez} and \textit{lumbreras} are almost equivalent for all sizes.

\begin{figure}
\center
\includegraphics[width=0.75\textwidth]{ch4_linkprediction_rankerror_by_size_abspodemosk5.eps}%
\caption{Normalized Ranking Error by size of thread}
\label{fig:nre_size}
\end{figure}

To summarise, while \textit{lumbreras} gets better likelihoods than \textit{gomez} when tested in unobserved behaviours, the difference in the likelihoods are not big enough to alter the ranking of the predicted parents. Since the classification of users in roles with different parameters does not achieve better predictions than a model with no roles, we cannot confirm the existence of roles in the sense of consistent, archetypical behaviours.

%      z    ntrain nvalidation    ntest     total
%  <int>     <dbl>       <dbl>    <dbl>     <dbl>
%1     1  266.7071    133.4040 133.8485  533.9596
%2     2  264.2385    132.1055 132.5940  528.9381
%3     3 1478.9429    739.4857 739.9714 2958.4000
%4     4  438.8583    219.4167 219.9583  878.2333
%5     5  342.7017    171.3536 171.8453  685.9006
%      z  total
%  <int>  <int>
%1     1  52862
%2     2 230617
%3     3 103544
%4     4 210776
%5     5 124148

\section{Summary}

In this chapter, we have conceptualised user roles as probability distributions over behaviours. In particular, we have studied replying behaviours: tendencies to reply to one or another post, depending on the number of replies, the recency, and whether the post is the root of the thread. These tendencies are formalised as probability distributions with three parameters. Our hypothesis has been that users that users can be divided in subgroups ---clusters or roles--- whose members behave according to the same parameters.

We had set three goals: (a) proposing a behavioural function for discussion threads (b) finding groups of users with the same behavioural function (the same parameters) and (c) testing whether these behavioural functions have predictive power ---if they can predict the behaviour of a user in a new context.  

We have shown that, indeed, allowing different model parameters for different subgroups of users increases the likelihood of the model even in the test set. Given a collection of past behaviours, our model is \textit{less surprised} about new behaviours than an equivalent model that assumes that there are no different roles. Nonetheless, in terms of practical predictions of \textit{which post will be replied next}, our role-based model makes no better predictions than a model without roles.

Based on that, our conclusion is that our proposed concept of behavioral role has some descriptive power but that the predictive power is almost marginal. It might be that users do not behave consistently and that there is more \textit{noise} than \textit{signal} in their behaviours. Or it might also be that we are not looking at the right behaviour. 

%We show that, while we are able to find clusters of users based on their past behaviours, there is no evidence that these behaviours are predictive of future behaviours. This finding calls into question our concept of role as a consistent behaviour of a user, at least with respect to the type of conversational behaviour that we consider here.