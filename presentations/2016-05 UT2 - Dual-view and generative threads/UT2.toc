\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Introduction}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{Problem}{5}{0}{1}
\beamer@subsectionintoc {1}{2}{Data}{7}{0}{1}
\beamer@sectionintoc {2}{Generative models for threads}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{Graph generative models}{10}{0}{2}
\beamer@subsectionintoc {2}{2}{Generative models for discussion threads}{12}{0}{2}
\beamer@subsectionintoc {2}{3}{Role-based generative model for threads}{16}{0}{2}
\beamer@sectionintoc {3}{Clustering based in behaviors with Bayesian cheating}{20}{0}{3}
\beamer@subsectionintoc {3}{1}{Bayesian cheating}{21}{0}{3}
\beamer@subsectionintoc {3}{2}{Dual-view model}{23}{0}{3}
\beamer@subsectionintoc {3}{3}{Experiments}{28}{0}{3}
\beamer@subsectionintoc {3}{4}{Application to generative models for threads}{32}{0}{3}
\beamer@sectionintoc {4}{Conclusions}{34}{0}{4}
