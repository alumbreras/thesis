 %\documentclass[11pt]{amsart}
\documentclass[11pt]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{Expectation-Maximization for role-based regressions}
\author{Alberto Lumbreras}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
\section{Introduction}
In this document, I elaborate the equations for the Expectation Maximization algorithm applied to the model of role-based regressions to demonstrate why EM cannot be used for a model such as the role-based regressions model due to dependencies between the latent variables.

\section{Notation}

Let $\mathbf{P}$ (observed participations matrix) be an $U \times T$ matrix where every column represents a thread. $p_{ij} = 1$ if user $i$ participates in thread $j$ and $0$ otherwise. 
\[
\mathbf{P}_{U \times T} =
\begin{bmatrix}
p_{11} & p_{12} &\cdots& p_{1T} \\
p_{21} & p_{22} &\cdots& p_{2T} \\
\vdots &  & & \vdots \\
p_{U1} & p_{U2} &\cdots& p_{UT} \\
\end{bmatrix}
\]

Let $\mathbf{C}$ (latent census matrix) be an $R \times T$ matrix where every column represents a thread and $c_{ij}$ indicates the percentage of users that hold role $i$ in thread $j$.
\[
\mathbf{C}_{R \times T} =
\begin{bmatrix}
c_{11} & c_{12} &\cdots& c_{1T} \\
c_{21} & c_{22} &\cdots& c_{2T} \\
\vdots &  & & \vdots \\
c_{R1} & c_{R2} &\cdots& c_{RT} \\
\end{bmatrix}
\]

Let $\mathbf{Z}$ (latent roles matrix) be an $R \times U$ matrix where every column represents a users and $z_{ij} = 1$ if user $j$ holds role $i$ and $0$ otherwise. A user only holds one role.
\[
\mathbf{Z}_{R \times U} =
\begin{bmatrix}
z_{11} & z_{12} &\cdots& z_{1U} \\
z_{21} & z_{22} &\cdots& z_{2U} \\
\vdots &  & & \vdots \\
z_{R1} & z_{R2} &\cdots& z_{RU} \\
\end{bmatrix}
\]

\section{Generative model}

For a given thread $t$, we model its success as a linear combination of its census:
\begin{align}
y^{(t)} = \sum_{i=1}^R c_{it} w_i+ e_t
\end{align}
which for the set of all the threads is:
\begin{align}
\mathbf{y} = \mathbf{C^T} \mathbf{w}+\mathbf{e}
\end{align}
Assuming that the noise is gaussian, we can express it in probabilistic terms as:
\begin{equation}
\mathbf{y} \sim \mathcal{N}(\mathbf{C^T} \mathbf{w}, \mathbf{Ie})
\end{equation}

$\mathbf{C}$ is the census matrix computed as the product between the roles matrix $\mathbf{Z}$ (latent) and the participations matrix $\mathbf{P}$ (observed): 
\begin{equation}
\mathbf{C=ZP}
\end{equation}

To fully specify the model, we place some priors in $\mathbf{z}$, getting to the following generative process:
\begin{align}
y_u | \mathbf{z}, \mathbf{w} &\sim \mathcal{N}(\mathbf{C^T w}, \sigma)\\
z_u &\sim Multinomial(\pi)
\end{align}

The process is as follows:
\begin{itemize}
\item For every user $ u \in \mathcal{U}$:
\begin{itemize}
\item Draw a role, $z_i \sim \mbox{Categorical}(\pi)$.
\end{itemize}
\item For every thread $ t \in \mathcal{T}$:
\begin{itemize}
\item Draw a length, $y_t \sim  \mathcal{N}(\mathbf{Cw}, \sigma_t)$.
\end{itemize}

\end{itemize}



The joint probability distribution of the weights is:
\begin{align}
p(\mathbf{y, z, | w, \pi}) = 
\prod_{u=1}^U p(z_u | \pi) 
\prod_{t=1}^T p(y_t | \mathbf{z, w})
\end{align}


\section{Expectation Maximization}
We want to maximize the loglikelihood w.r.t $\mathbf{w}$: 
\begin{align}
\operatorname*{arg\!\max}_{w} \ln
p(\mathbf{y | w})
&=
\operatorname*{arg\!\max}_{w}
\sum_{t=1}^T \ln p(y_t | \mathbf{w})\\
&=
\operatorname*{arg\!\max}_{w}
\sum_{t=1}^T  \ln  \sum_{\mathbf{z}} p(y_t, \mathbf{z} | \mathbf{w})
\end{align}
We don't know to maximize this double summation. The EM strategy is to iteratively maximize $\mathbf{w}$ with respect to some tractable lower bound and then update the lower bound so that it reaches the real function again. This strategy has convergence guarantees.

The tractable lower bound is found thanks to Jensen's inequality, that states that for any convex function $f$:

\begin{equation}
E[f([X])] \geq f(E[X])
\end{equation}
In case of concave functions such as logarithms, the inequality goes the other way. With this equation in mind, if we multiply the term inside the logarithm by probability distribution over $\mathbf{z}$ we get the expression of a expectation inside a logarithm similar to the right side of Jennsen's inequality:
\begin{align}
\sum_{t=1}^T \ln \sum_{\mathbf{z}} p(y_t, \mathbf{z} | \mathbf{w})
&=
\sum_{t=1}^T \ln \sum_{\mathbf{z}} Q_t(\mathbf{z})\frac{p(y_t, \mathbf{z} | \mathbf{w})}{Q_t(\mathbf{z})}
\end{align}
Applying Jensen's inequality, we have that:
\begin{align}
\sum_{t=1}^T \ln \sum_{\mathbf{z}} Q_t(\mathbf{z})\frac{p(y_t, \mathbf{z} | \mathbf{w})}{Q_t(\mathbf{z})}
&\geq
\sum_{t=1}^T  \sum_{\mathbf{z}} Q_t(\mathbf{z}) \ln  \frac{p(y_t, \mathbf{z} | \mathbf{w})}{Q_t(\mathbf{z})}
\end{align}
Our goal now is to maximize the function w.r.t $Q_i$ and $\mathbf{z}$. This can be done by a two-step iteration known as the the E-step and the M-step.

\subsection{E-step}
Given a $\mathbf{w}$, the optimum $\mathbf{Q}$ is found by forcing Jennsen's equality, which is reached if the function inside $E[f(x)]$ is constant. That is:
\begin{equation}
\frac{p(y_t, \mathbf{z} | \mathbf{w})}
{Q_t(\mathbf{z})}
=
c
\end{equation}
We also want to respect the condition:
\begin{equation}
\sum_\mathbf{z} Q_t(\mathbf{z}) = 1
\end{equation}
since $Q_t$ must be a probability density. This is accomplished by choosing:
\begin{equation}
Q_t(\mathbf{z}) = 
\frac{
p(y_t, \mathbf{z} | \mathbf{w})
}
{
\sum_{\mathbf{z}} p(y_t, \mathbf{z} | \mathbf{w}, \pi)
}
= 
p(\mathbf{z} | y_t, \mathbf{w}, \pi)
\label{posterior}
\end{equation}

Since the different $z$ are independent given $\pi$, we can factorize the posterior:
\begin{align}
p(\mathbf{z} | y_t, \mathbf{w}, \pi) = \prod_{u=1}^U p(z_u | \pi) 
= \prod_{u=1}^U \pi_{z_u}
\end{align}

\textit{Caveat emptor!}
\begin{itemize}
\item If the conditionals of $z_j$ are independent then we can factorize:
\begin{equation}
Q_t(\mathbf{z}) = \prod_u Q_t(z_u)
\end{equation} 
and compute the posteriors separately. Otherwise, we will not be able to compute equation \ref{posterior}.
\end{itemize}

\subsection{M-step}
\subsubsection{Coefficients $\mathbf{w}$}
Given $Q_t(\mathbf{z})$, the optimum $\mathbf{w}$ is found by taking the derivative of the equation w.r.t $\mathbf{w}$ and setting it to zero.

\begin{align}
\nabla_{\mathbf{w}} 
\sum_{t=1}^T  \sum_{\mathbf{z}} Q_t(\mathbf{z}) 
\ln  
\frac
{p(y_t | \mathbf{z}, \mathbf{w}) p(\mathbf{z} | \pi)}
{Q_t(\mathbf{z})} = 0
\end{align}
\begin{align}
\nabla_{\mathbf{w}} 
\sum_{t=1}^T  \sum_{\mathbf{z}} Q_t(\mathbf{z}) 
\ln  
p(y_t | \mathbf{z}, \mathbf{w}) 
+
\nabla_{\mathbf{w}} 
\sum_{t=1}^T  \sum_{\mathbf{z}} Q_t(\mathbf{z}) 
\ln p(\mathbf{z} | \pi)
-
\nabla_{\mathbf{w}} 
\sum_{t=1}^T  \sum_{\mathbf{z}} Q_t(\mathbf{z}) 
\ln {Q_t(\mathbf{z})} = 0
\end{align}


\begin{align}
\sum_{t=1}^T  \sum_{\mathbf{z}} Q_t(\mathbf{z}) 
\nabla_{\mathbf{w}} 
\ln  
p(y_t | \mathbf{z}, \mathbf{w}) &= 0\\
-\frac{1}{2\sigma^2}
\sum_{t=1}^T  \sum_{\mathbf{z}} Q_t(\mathbf{z}) 
\nabla_{\mathbf{w}}
(y_t - \mathbf{c^T w})^T (y_t - \mathbf{c^T w})&= 0\\
-\sum_{t=1}^T  \sum_{\mathbf{z}} Q_t(\mathbf{z}) 
\nabla_{\mathbf{w}}
(y_t^2 + \mathbf{w^T c c^T w} - 2y_t\mathbf{c^T w}) &= 0\\
\sum_{t=1}^T  \sum_{\mathbf{z}} Q_t(\mathbf{z}) 
(2y_t\mathbf{c^T}  - 2\mathbf{c c^T w}) &= 0
\end{align}

\begin{align}
\sum_{t=1}^T  \sum_{\mathbf{z}} 
Q_t(\mathbf{z})
y_t\mathbf{c^T} 
&=
\mathbf{w}
\sum_{t=1}^T  \sum_{\mathbf{z}} 
Q_t(\mathbf{z})
\mathbf{c c^T}\\
\mathbf{w}
&=
\frac{
\sum_{t=1}^T  \sum_{\mathbf{z}} 
Q_t(\mathbf{z})
y_t\mathbf{c^T}}
{
\sum_{t=1}^T  \sum_{\mathbf{z}} 
Q_t(\mathbf{z})
\mathbf{c c^T}
}
\end{align}

For a given thread $t$, we only need to take into account the role assignments of the users who participate in it. Then, we can simplify the equation: 

\begin{align}
\mathbf{w}
&=
\frac{
\sum_{t=1}^T  \sum_{\mathbf{z_{(t)}}} 
Q_t(\mathbf{z})
\mathbf{c}y_t}
{
\sum_{t=1}^T  \sum_{\mathbf{z_{(t)}}} 
Q_t(\mathbf{z})
\mathbf{c c^T}
}
=
\frac{
\sum_{t=1}^T  \sum_{\mathbf{z}} 
\prod_{u_{(t)}} \pi_{z_u}
\mathbf{c}y_t}
{
\sum_{t=1}^T  \sum_{\mathbf{z}} 
\prod_{_{(t)}} \pi_{z_u}
\mathbf{c c^T}
}
\end{align}

\textit{Caveat emptor!}:
\begin{itemize}
\item If $p(y_t, \mathbf{z} | \mathbf{z})$ belongs to the exponential family, $\ln p(y_t, \mathbf{z} | \mathbf{z})$ will have a more easily tractable expression.
\end{itemize}

\subsubsection{Role probabilities $\pi$}
\begin{align}
\nabla_{\mathbf{\pi}} 
\sum_{t=1}^T  \sum_{\mathbf{z}} Q_t(\mathbf{z}) 
\ln  
\frac
{p(y_t | \mathbf{z}, \mathbf{w}) p(\mathbf{z} | \pi)}
{Q_t(\mathbf{z})} = 0
\end{align}
Dropping all the factors that do not depend on $\pi$ we get:
\begin{align}
\sum_{t=1}^T  \sum_{\mathbf{z_{(t)}}} 
Q_t(\mathbf{z}) 
\nabla_{\mathbf{\pi}} 
\ln
p(\mathbf{z} | \pi)
= 0
\end{align}

We denote $p(\mathbf{z_u} =x | \pi)$ as $ \pi_x$. Then, for every $\pi_x$:
\begin{align}
\sum_{t=1}^T  \sum_{\mathbf{z_{(t)}}} 
Q_t(\mathbf{z}) 
\nabla_{\mathbf{\pi}} 
\ln
\pi
= 0
\end{align}
\section{Why EM can't (easily) deal with this model?}
In the E-step, we have to male computations considering all possible role assignments in a thread as well as the probability of this set of assignments. For large threads, the combinational possibilities are too high and that makes EM a bad choice for this problem. Actually, Snijders found a similar problem when dealing blockmodels, a kind of problem were the we have to consider the set of roles of the individuals in every block. 

Snijders used Gibbs sampling for his problem. Other authors have proposed the use of variational inference.  
\section*{References}
\begin{itemize}
\item Andrew Ng, lecture at Standford where he introduces the EM algorithm:\\https://www.youtube.com/watch?v=ZZGTuAkF-Hw
\item Lecture notes: http://cs229.stanford.edu/notes/cs229-notes8.pdf
\item Bishop
\item Go back and forth between Andrew's notes and Bishop. Iterate (and do the maths of a simple mixture of gaussians following Andrew's notes) until "aha!". 
\end{itemize}
\end{document} 

