\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{Roles revisited}{3}{0}{1}
\beamer@sectionintoc {2}{Blockmodels revisited}{7}{0}{2}
\beamer@sectionintoc {3}{Traditional stochastic blockmodels}{11}{0}{3}
\beamer@sectionintoc {4}{Infinite Relational Model}{17}{0}{4}
\beamer@sectionintoc {5}{Discussion}{21}{0}{5}
