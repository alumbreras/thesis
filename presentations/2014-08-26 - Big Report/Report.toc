\contentsline {chapter}{\numberline {1}Introduction}{5}
\contentsline {section}{\numberline {1.1}Roles in (offline) sociology}{6}
\contentsline {section}{\numberline {1.2}Roles in online communities}{7}
\contentsline {subsection}{\numberline {1.2.1}Expert and celebrity finding}{7}
\contentsline {subsubsection}{Simple metrics}{7}
\contentsline {subsubsection}{z-score}{7}
\contentsline {subsubsection}{Centrality metrics}{7}
\contentsline {subsection}{\numberline {1.2.2}Ethnology}{8}
\contentsline {subsection}{\numberline {1.2.3}Attribute clustering}{8}
\contentsline {subsection}{\numberline {1.2.4}Positional analysis}{8}
\contentsline {section}{\numberline {1.3}Drawbacks of current methods}{9}
\contentsline {chapter}{\numberline {2}Bayesian background}{11}
\contentsline {section}{\numberline {2.1}Principles}{11}
\contentsline {subsection}{\numberline {2.1.1}Frequentist statistics}{11}
\contentsline {subsection}{\numberline {2.1.2}Bayesian statistics}{12}
\contentsline {subsection}{\numberline {2.1.3}Bayes rule}{12}
\contentsline {subsection}{\numberline {2.1.4}Bayesian inference}{12}
\contentsline {section}{\numberline {2.2}$f_r$: classic prediction schemes}{12}
\contentsline {subsection}{\numberline {2.2.1}Discriminative models}{12}
\contentsline {subsection}{\numberline {2.2.2}Generative models}{13}
\contentsline {subsection}{\numberline {2.2.3}A probabilistic comparison between discriminative and generative}{13}
\contentsline {subsection}{\numberline {2.2.4}Discriminative vs Generative}{14}
\contentsline {section}{\numberline {2.3}Techniques}{15}
\contentsline {subsection}{\numberline {2.3.1}Bayesian regression}{15}
\contentsline {subsection}{\numberline {2.3.2}Mixture models}{15}
\contentsline {subsection}{\numberline {2.3.3}Gaussian Processes}{15}
\contentsline {chapter}{\numberline {3}A new role analysis}{17}
\contentsline {section}{\numberline {3.1}Formalization}{17}
\contentsline {section}{\numberline {3.2}Role definition}{17}
\contentsline {section}{\numberline {3.3}Role-based predictions}{19}
\contentsline {subsection}{\numberline {3.3.1}Predicting successful threads}{19}
\contentsline {subsubsection}{Non-linear model}{20}
\contentsline {subsection}{\numberline {3.3.2}Predicting users' replies}{20}
\contentsline {subsection}{\numberline {3.3.3}Predicting reactions to a post}{21}
\contentsline {chapter}{\numberline {4}Predicting the success of a thread}{23}
\contentsline {section}{\numberline {4.1}Baseline predictions}{23}
\contentsline {subsection}{\numberline {4.1.1}Metrics}{23}
\contentsline {subsubsection}{Initial speed}{23}
\contentsline {subsubsection}{Replies to root}{23}
\contentsline {subsubsection}{Number of users}{24}
\contentsline {subsubsection}{Posts length}{24}
\contentsline {subsubsection}{Ramification ratio length}{24}
\contentsline {subsection}{\numberline {4.1.2}Experiments}{24}
\contentsline {section}{\numberline {4.2}Role-based predictions}{25}
\contentsline {subsection}{\numberline {4.2.1}Motivation}{25}
\contentsline {subsection}{\numberline {4.2.2}Naive model}{25}
\contentsline {subsection}{\numberline {4.2.3}Generative model}{28}
\contentsline {subsection}{\numberline {4.2.4}Joint probability}{28}
\contentsline {subsection}{\numberline {4.2.5}Inference: Gibbs sampling}{29}
\contentsline {subsubsection}{Conditional distribution of $\mathbf {w}$}{30}
\contentsline {subsubsection}{Conditional distribution of $\mathbf {z_n}$}{32}
\contentsline {chapter}{\numberline {5}Context prediction}{33}
\contentsline {section}{\numberline {5.1}$C$: codification of contexts}{33}
\contentsline {section}{\numberline {5.2}Experiments}{34}
\contentsline {subsection}{\numberline {5.2.1}Users reactions to contexts}{34}
\contentsline {subsection}{\numberline {5.2.2}Clustering users with similar reactions}{35}
\contentsline {subsection}{\numberline {5.2.3}Evolution of contexts}{35}
\contentsline {section}{\numberline {5.3}Towards context-based predictions}{36}
\contentsline {chapter}{\numberline {6}Roles to predict reactions}{37}
\contentsline {chapter}{\numberline {7}Predicting the death of a discussion}{39}
\contentsline {chapter}{\numberline {8}Threads similarity}{41}
