\documentclass[11pt]{book}
\usepackage{geometry}              
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
\usepackage[utf8]{inputenc}
%\usepackage{graphicx} % adjustbox loads it
\usepackage[export]{adjustbox}
\usepackage{caption}
\usepackage{subcaption}
%\usepackage{subfig}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{epstopdf}
\usepackage{natbib}
\usepackage{tikz}
\usepackage{soul}
\usetikzlibrary{fit,positioning}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\graphicspath{ {./img/} }
\DeclareMathOperator*{\argmax}{arg\,max}

\newcommand\todo[1]{\textcolor{red}{#1}}
\newcommand\marie{\textcolor{blue}}

\title{\LARGE Role detection in online forums\\
\normalsize Rapport à mi-parcours
}
\author{Alberto Lumbreras\\
\newline
\underline{Supervisors}: Julien Velcin, Bertrand Jouve, Marie Guegan}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

\begin{document}

\begin{titlepage}
\begin{center}

% Upper part of the page. The '~' is needed because \\
% only works if a paragraph has started.
\includegraphics[width=0.35\textwidth]{./img/logo_lyon}~\\[1cm]

\textsc{\LARGE Université Lyon 2}\\[1.5cm]

\textsc{\Large Mid-Term Report}\\[0.5cm]

% Title
\HRule \\[0.4cm]
{ \huge \bfseries Role detection in online forums \\[0.4cm] }

\HRule \\[1.5cm]

% Author and supervisor
\noindent
\begin{minipage}{0.4\textwidth}
\begin{flushleft} \large
\emph{Author:}\\
Alberto \textsc{Lumbreras}
\end{flushleft}
\end{minipage}%
\begin{minipage}{0.4\textwidth}
\begin{flushright} \large
\emph{Supervisors:} \\
Dr.~Bertrand \textsc{Jouve}\\
Dr.~Julien \textsc{Velcin}\\
~(Technicolor) Marie \textsc{Guegan}\\
\end{flushright}
\end{minipage}

\vfill

% Bottom of the page
{\large \today}

\end{center}
\end{titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Introduction}
% Sociology
Role theory is a branch of sociology that studies the social categories, implicit or explicit, adopted by members of a community. Some of the elements of interest in role theory are the rights, duties and expectations attached to a role, how individuals negotiate their roles and how they either change roles or even re-define them. Even if (or because) role theory has been around at least since the first half of the 20th century there is neither a unified theory, nor a single definition of \textit{role}. Different schools of thought have proposed different theories and frameworks, some of which proposing conflicting ideas (see \cite{Biddle1986} for a review). Probably the most influential work has been that of \cite{Goffman1959}. 

% Computer Science
Online communities have brought a new field (and much data) to sociological research, allowing computer scientist to step in. The approach of computer scientists is a practical one, defining roles \textit{ad hod} depending on the dimension of online human behavior they are interested in (or the dimension they are able to observe and quantify). The relational nature of online communities have made the framework of Social Network Analysis, started by \cite{Moreno1934} and further formalized by \cite{Wasserman1994}, a very natural one to work with. Computer Science has tried to construct a practical understanding of a role by capturing measurable variables such as structural position or observed interactions of actors within a network. The work of \cite{Golder2003} is the starting point for the analysis of roles in online forums, and much of the consequent studies are inspired by his ethnological study of the roles of Usenet users. But in spite of the useful insights it has provided to understand how users behave in online communities, there are still open issues, some of which are addressed in this thesis. 

If we are to accept, as suggested by sociologists, that a role is attached to a behavior, we should expect some \textit{predictability} from users occupying a certain role. Yet, behavior analyses have been mostly focused on descriptive analysis, based on individual positions, rather than in predictive ones. Easier said than done: the diversity of behaviors make learning algorithms to need rather large amounts of data to do their job.

This thesis is about detecting roles to predict behaviors.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% State of the Art

\section{Online roles: an overview of the state of the art}
Researchers have proposed different techniques and methodologies to analyze or identify roles between users of online communities. We distinguish three major categories: top-down, bottom-up and ethnological studies. 

%%%%%%%%%%%%%%
\textit{Ethnology} is the most effective method to analyze roles in a community since it relays not on an algorithmic machine but on careful human observation able to capture the subtlety of human interactions: communication strategies, intentions, symbols or perceptions are difficult to spot by a machine. Ethnologists live within a community for some time, talk to its members, learn the cultural codes and norms of the community, and produce a report where all these are synthesized. In online communities, the most influential ethnological study has been that of \cite{Golder2003}, who studied the roles in an online forum. Based on a double axis of communication strategies and group perception towards the individual, Golder found \textit{celebrities, lurkers, newbies, trolls, ranters and flamers}.

%%%%%%%%%%%%%%
\textit{Top-down} approaches take an a prior definition of one or several roles, sometimes taken from previous sociological studies. The goal can be either to find users that match this role or to get news insights to refine a previous notion of that role.

\textit{Bottom-up} approaches take some features and find groups of individuals that share similar features. Some common user features are centrality measures such as \textit{in-degree}, \textit{out-degree}, \textit{closeness} and \textit{betweenness}, \textit{eigenvector centrality} (e.g. PageRank), features reflecting the user activity such as number of posts or number or words per post, or features reflecting other aspects such as the ratio of threads initiated by the user or the ratio of threads initiated by the user which obtained some response. Of course, the decision of what attributes to choose depends on the nature of the community, and some forums have badges or voting systems that allow to enrich users descriptions. Clustering techniques are a natural ally for this strategy.

\textit{Blockmodeling} can be considered as a relational clustering, but its history and its central importance in social network analysis  makes it worthy of its own category.  \citep{White1976,Boorman1976} proposed the first blockmodeling technique to find individuals that occupy similar structural positions. If our community is a family and relations are "is son of", the positions of two siblings, though not exactly the same, are structurally equivalent since their relationships with the rest of the family members are exactly the same: they both are siblings of the group "parents". This is called positional analysis. Blockmodeling finds both the structure of a community (the different positions and the relations between positions) and which position every individual holds. Positional analysis may help to understand what is going on. Nonetheless, it is only an intermediate step towards a full analysis of roles. From here the researcher must continue to analyze why these blocks are formed. 

Yet, it is difficult to put some research on one of the aforementioned categories, see for instance \cite{Glea2009} or \cite{Welser2007} where authors base their analysis on visualization of several user signatures. The range of possibilities is actually very broad and new approaches will probably appear in the future.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Our approach
\section{Our approach: roles as behavior functions}
To detect the roles played by users in large communities we need bottom-up approaches that do not need prior knowledge of what roles are in the community and what kind of behavior a user in a role is supposed to show.

Clustering, however, has two drawbacks. First, it introduces an important bias by deciding upfront which features to consider. Second, this bias is not penalized by any validation method, since \textit{given a set of features} clustering algorithms will find the best solution for these features.

To avoid falling into this tautology, we propose to see the problem as a prediction task. Besides, if roles are meaningful it is because their associated behaviors show some regularity.

Modeling every single individual would produce as many models as individuals, with no information shared between the models. If we are not interested  in specific individuals but rather in general archetypes, we need to build models that reflect typical behavioral archetypes. Such archetypes are what we will call roles.

%%%%%%%%%%%%%%%%%%
Imagine a function $f$ describing every individual behavior in a discussion forum. Though behavior depends on individual factors, there is a part of behavior attached to social roles. We can think of these shared behaviors as archetypes. We propose to model these archetypes as ideal behavioral functions and to model the deviations due to individual factors as noise. Then, an individual behavior can be expressed as:

\begin{align}
f_i &\sim F(f_{z_i})\\
z_i &\sim \text{Multinomial}(\boldsymbol{\pi})
\end{align}
where $\boldsymbol{\pi}$ is the probability of choosing a role, $z_i$ is the role of user $i$ and $F$ is the probability function that models the distribution of behaviors within the same component, cluster or role.

The challenge is to find a function $F$ that represents some aspect of users behavior and from which we can make predictions based on the role assignments $\mathbf{z}$. In the following chapters we explain our different attempts and proposals for $F$. First, within blockmodels, we model $F$ as the probability of interacting with other roles. Second, within what we call contexts, $F$ is the probability of engaging into a type of conversation (context). In the third chapter we describe our current attempt to define $F$ as a combination of users features and behaviors.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Past work}
In this section I briefly describe the most relevant methods and paths I have followed. First I explain the experiments and conclusions from applying blockmodeling to discussion forums. Then, I introduce motifs, triads that happen more than expected, and some exploratory analysis on IMDb forums. While blockmodeling can uncover relational patterns, triads in forums represent types of conversation. The idea that I wanted to explore was whether it is possible to define a role as "users that tend to talk to the same other users using the same triads".
As we will see, I found two main problems: (a) inference in blockmodeling scales bad and (b) the vast majority of users use the same very limited set of triads.

From the analysis of triads and motifs I finally present an analysis about contexts. We define the context of a post as the small discussion graph preceding the post. 
I analyze whether users have some preference when choosing
a post to reply to. Here, the tentative definition of role is "users that share context preferences" or, in other words, that react to the same contexts.

The common element in all these role definitions is that they focus on behavior (preferences, type of discussions; etc) rather than features (centrality, etc). In the last chapter I propose a model that takes into account both features and behaviors.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Role definition}
%%%% 
The concept of role has been largely discussed by sociologists. Though there isn't any unified definition (see \cite{Biddle1986}), it is clear that a role is mainly associated with a person's behavior or with its social activity. In terms of social behavior, role theory helps us to understand the ``performance" \citep{Goffman1959} of a person in a given situation (e.g.: the interaction between a doctor and a patient).
In terms of social activity, roles can be used to facilitate the analysis of parts played by individuals when analyzing, for instance, the division of labor.
While structuralists put the accent on the analysis of the structure, interactionists analyze the interactions and their meanings.

According to structuralists, a role is the set of behaviors associated to a position in a social structure (e.g. : \cite{Boorman1976}). This link between role and position is created by norms and expectations associated to each position, which condition or enable individual behaviors.

The concept of structural equivalence was introduced by \cite{Lorrain1971a}. It became evident soon that role equivalence is not related to structural but to regular equivalence. A step further would be done some time later with the introduction of stochastic equivalence. Blockmodeling became the standard technique to find this type of equivalences (for some of the most remarkable, see \cite{White1976, Holland1983,Wasserman1987,Anderson1992,Nowicki2001}).

\section{Blockmodels}

\begin{figure}
\begin{center}
\includegraphics[width=0.9\textwidth]{godfather_thread1_BM}
\caption{Blockmodel in a IMDb thread}
\label{fig:blockmodel}
\end{center}
\end{figure}


% Intro to stochastic
Stochastic blockmodeling often counts on the researcher to have an idea a priori about the number of underlying clusters that exist in the sociomatrix. In this
case, the number of clusters is a fixed parameter of the algorithm. The researcher then runs the algorithm, for different numbers of clusters, and then compares the results to decide which number of clusters  seems to be the most likely.

% Kemp
Kemp et al. (2004) introduced the Infinite Relational Model, a blockmodeling based on a Chinese Restaurant Process as a prior for the class assignment of a user. This model allows an automatic inference of the number of underlying clusters. I implemented an IRM and tested it in IMDb forums. The blockmodeling is applied, for every thread, to a matrix of participants where the cell $i,j$ is 1 if user $i$ posted a reply to user $j$ somewhere in the thread, and 0 otherwise. Figure \ref{fig:blockmodel} shows the tree of posts corresponding to a  thread and its corresponding blockmodel. For the inference I followed split-merge MCMC as suggested by Kemp.

% Results
I tested the IRM on several threads. Most of them show either an unclear structure or a center periphery structure like the one shown in the figure. Center-Periphery structures correspond to a group of users that talk with each other but do not talk to users out of this group, and a periphery of users that talk to the center users but no one talks to them, neither from the center nor from the periphery.

\subsubsection{Limitations}
Blockmodeling is a sound technique with a sociological basis. Yet, these are some limitations when applied to forums and online communities:

\begin{itemize}
\item \textbf{Lack of structure}: unlike more stable structures where sociologists used to apply blockmodeling, a single online conversation cannot develop significant structures, especially if the conversation is short. At most, we see a structure of the form center-periphery. Blockmodeling is usually followed by an analysis of the individuals that fell into each group. Do they have something in common that explains their relational similarity? Unfortunately, after an exploratory analysis of the resulting blockmodels we were unable to find common attributes between the users in the same block. Even center users seemed to be there by chance. It does not mean, however, that every forum will show the same pattern. I think some forums such as those used in online courses might show some similarity between users in the center.

\item  \textbf{Lack of comparability}: once we have the blockmodels for N threads, and even if the same group of users happened to be in all the threads (which is not the case), how to merge the results of the different blockmodels? Blockmodeling does not allow to merge different structures.

\item \textbf{Lack of scalability}: given the lack of comparability, we might want to perform a single blockmodel over the whole dataset. The relationships matrix of  our  IMDb contains one row and column per user, that is, it is a matrix with 100.000 rows and columns. My Python implementation of the IRM takes about an hour for a 1000x1000 matrix. Nonetheless, there are more efficient implementations written in C, and more efficient inference methods such as Variational Inference.  

\item \textbf{Sparseness of relationships}: the matrix of relationships in IMDb his extremely sparse. Even active users barely talk with each other more than once. 

\item \textbf{Relations are not enough}: even if the previous deficiencies could be solved, relationship patterns between users seem not enough to capture the richness of behaviors, even non relational ones. However, some authors have proposed variations of blockmodeling to include text \citep{McCallum2007} to model e-mail patterns or the duration of the relationships to model phone calls \citep{DuBois2013} among others.
\end{itemize}

\section{Motifs}
Motifs are patterns of graph interconnection, usually triads, occurring at numbers that are significantly higher than those expected if the graph is randomized. \cite{R.Milo} proposed the use of motifs to define universal classes of networks. \cite{Adamic2008} analyzed the motifs in Yahoo forums and found that, for instance, forums with Q\&A oriented topics (e.g.: programming) show different motifs than opinion-based topics (e.g.weddings).

We wanted to know whether the conversations occurring in discussion threads were different for short and long threads. To this aim, we performed an analysis of motifs over all the threads on our IMDb dataset and compared it to the length of the threads. Figure \ref{fig:triads} confirms this hypothesis. Yet, a more interesting question is whether longer threads had already a different motif population when they only had a few posts. We can do this in two ways: one way is to create two groups, short threads and long threads, and truncate all threads at $N$ posts; we then compare the means and variances of the motif populations in the group of short threads with the means and variances in the group of long threads. Another way, and the one we tried, is to try and predict whether a thread will grow further than $N+i$ posts; we tried Support Vector Machines and Classification Trees, and we did not find any predictive power in the initial population of motifs. Yet, we found that other features do allow to achieve this binary classification task with an accuracy between 50\% and 60\%, which is consistent with predictions reported by \cite{Backstrom2013}. The analysis of means and variances of short and long threads remains to be done.


\begin{figure}
\begin{center}
\includegraphics[width=0.9\textwidth]{IMDB_motifsVSlength_SE}
\caption{Triad census vs total thread length (means)}
\label{fig:triads}
\end{center}
\end{figure}

For a more extended discussion about the IMDb triad census see \cite{Lumbreras2013}.

\section{Contexts}
Can we describe users in terms of which types of triads they appear in? A quick exploration on IMDb showed no difference between users. A slightly different question is: given a thread where some users already participated, and knowing the next participant $u$, can we predict to which post they will respond? And yet a slight simplification: given a thread with $N$ posts and the subgraph in the neighborhood of every post, can we predict to which kind of subgraph (some posts have equivalent subgraphs in their neighborhoods)  $u$ will respond to? In other words, we want to know whether some users prefer some subgraphs. The assumption is that, after entering a thread, the decision on which post to respond depends on the structure of the discussion preceding each candidate post. We call these discussions or subgraphs ``contexts". We might expect, for instance, that few users will start their participation in a context where two users have been long replying each other, since it might denote a heated discussion.

% Hashing contexts
We create the subgraphs as follows: for each post, we take its parent, the parent of is parent, and so on, up to a maximum of 6 posts or three different users, so that the most complex context correspond to a fully connected triad. Then we create a hash graph as shown in Figure \ref{context_hash}. In the graph at the left, nodes are posts and edges indicate replies between posts; authors are just an attribute of every node. This is not enough as hash graph, since all chains would look similar even if they express different patterns (e.g. a chain of a two actors discussing with each other  $0 \leftarrow 1 \leftarrow 0 \leftarrow 1$ will look the same as  $0 \leftarrow 1 \leftarrow 2 \leftarrow 3$). We could express the graph as an author graph, representing authors as nodes and edges as posts between actors; nonetheless, this type of graph loses the information about the structure of conversations. Alternatively, we can represent the graph as shown at the right of figure. This representation keeps the information about the structure and the users so that in a conversation between three users, we can distinguish between $0\leftrightarrow 1$ and $0 \leftarrow 1 \leftarrow 0 \leftarrow 1 \leftarrow 0 \leftarrow 1 \leftarrow 0$.

\begin{figure}
\begin{center}
\includegraphics[width=0.35\textwidth]{context_hash}
\caption{Example of hash graph. The hashing scheme collapses the information about authors so that two conversations involving different authors but that share a same schema are considered equivalent}
\label{context_hash}
\end{center}
\end{figure}

% Significance test about context preferences
To answer the question on whether users have some context preference when they enter a thread, we create a null hypothesis for every pair of context-user. The null hypothesis states that a user chooses a context according to a Bernoulli distribution with parameter $n_c/N$ where $n_c$ is the number of times context $c$ already appears in the thread and $N$ is the total number of contexts to choose from. Given the set of $T$ threads where a user participated, the joint probability of all user's choices assuming the null hypothesis is:

\begin{align}
H_0: k &\sim \prod_{i=1}^T \text{Bernoulli}(\frac{n_c^{(i)}}{N^{(i)}})\\
&=
\text{Poisson Binomial}(\frac{n_c^{(1)}}{N^{(1)}},..,\frac{n_c^{(T)}}{N^{(T)}}) 
\end{align}

A Poisson Binomial distribution is the probability of $k$ successes over $T$ trials where each trial has a different probability of success. To test the null hypothesis, we count the number of times a user chose a context ($k$) and measure its $p$-value assuming the null hypothesis\footnote{The R package \textit{poibin} provides a numerical approximation to the Poisson Binomial}.

Results are shown in Figure \ref{fig:pvalues}. For every context, the different points correspond to the different users. The y-axis show the negative log of the $p$-value; a 0.95 level is indicated by a dashed red line; users above the line reject the null hypothesis, showing a tendency to answer to that context. The $x$-axis shows the number of threads in which the user participates and where the context already existed. Those at the right side of the plots are users that chose the context a number of times significantly higher than expected. Those at the left chose the context a number of times significantly less.

For instance, \emph{context 0} corresponds to the first post in the thread. The figure shows that, even in the presence of other contexts, this an attractive context for most of users. In plain words: users tend to respond to the seed. \emph{Context 01} corresponds to the two first posts in the thread. Users chose this context significantly less than expected by the null hypothesis, meaning that they prefer other contexts if possible (for instance, context 0) There is some users place at the right, but most of them have a low $p$-value.

Though the analysis has been made with a small sample of users, it seems that only a small set of contexts show a clear similar effect on all users. 

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=1\textwidth]{pvalues}
\caption{Significance test for the choices of contexts. For every context, every point represents a user.}
\label{fig:pvalues}
\end{center}
\end{figure}


\subsection{Evolution of contexts}
Every context can evolve to a different or a more complex context depending on who replies to the context. For instance, does \texttt{01} evolve into \texttt{012} (a new user joining the conversation) or rather to \texttt{010} (it is the first user who responded)?. The matrix in \ref{fig:transitions} shows the percentage of transitions for every pair of contexts. Note that most transitions are `forbidden", that is, it is not possible to evolve from \texttt{0} to \texttt{10101}. These forbidden transitions are colored in green. We observe that the most likely, for every context, is to grow keeping the same users.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=1\textwidth]{transitions}
\caption{Transitions between contexts}
\label{fig:transitions}
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Present and Future}
In this section we briefly introduce our undergoing work and the perspectives for future work.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Multi-view Multi-task learning}

% Introduce paper

Unfortunately, most social networks are composed by a core of users that make most of the contributions and a periphery of either newcomers or users with low activity. Recent explorations on another dataset, the \texttt{boards.ie} forum, shows a similar sparsity: from a total of 75.000 users, less than 5000 wrote more than 10 posts during 10 years. For these cases, the task of learning their behavior is harsh since no machine learning algorithm will be able to generalize, that is, to make good predictions based on a few training instances. Fortunately, users with the same role or behavior often share similar characteristics. For instance, users that solve the issues in an open source project are likely to have many years of experience.

%%%% Multi-view (link to papers)
"Multi-view" learning uses heterogeneous and complementary information to enhance some prediction or clustering tasks \citep{SteffenandTobiasScheffer, Xu2013, Sun2013}.  On the other hand, "multi-task" learning learns multiple but similar tasks using the same inputs \citep{Caruana1997, Zhang2008, Zhang2005}. Finally, multi-view multi-task learning ($M^2TV$) learns multiple tasks from heterogeneous feature spaces \citep{Yang2013,He2011}. Very recently, \cite{Cheng2014} has applied multi-view techniques to predict user labels in social networks such as LinkedIn (e.g.: engineer, professor) or IMdB (e.g: directors, actors), penalizing predictions of different labels for similar users. 

%%%% Our model
In this paper, we propose a non-parametric Bayesian dual-view model to predict user behaviors. We use a Chinese Restaurant Process (CRP) as a shared prior for two views, a features view and a behaviors view, so that users that are clustered together in one view are also clustered together in the other. This model conveys our central prior assumption: users that behave similarly tend to have similar features. The difference between our model and \cite{Cheng2014} is that (a) we use a non-parametric Bayesian model instead of regularization methods, and (b) our model can be adapted to predict arbitrarily complex behaviors instead of labels.


\begin{figure}
\center
\begin{tikzpicture}
\tikzstyle{main}=[circle, minimum size = 12mm, thick, draw =black!80, node distance = 10mm]
\tikzstyle{connect}=[-latex, thick]
\tikzstyle{box}=[rectangle, draw=black!100]

%% Dirichlet
  \node (alpha) {$\alpha$}; 
  \node [main](pi) [above=1cm of alpha] {$\pi$}; 
  \node [main](z) [above=of pi] {$\mathbf{z_u}$};

%% Clustering
  \node[main, fill = black!10] (a_u) [left=1cm of z] {$\mathbf{a_u}$}; 
  \node[main] (theta_ar) [above=1.5cm of a_u] {$\boldsymbol{\theta_k^{(a)}}$};   

%% Prediction
  \node[main] (f_u) [right=1cm of z] {$\mathbf{f_u}$}; 
  \node[main] (theta_fr) [above=1.5cm of f_u] {$\boldsymbol{\theta_k^{(f)}}$};   
   \node[main, fill = black!10] (y) [right=1.5cm of f_u] {$\boldsymbol{y}$};   
   
  \node [main](G_a0) [above=1cm of theta_ar] {$\mathbf{G_0^{(a)}}$};   
  \node [main](G_f0) [above=1cm of theta_fr] {$\mathbf{G_0^{(f)}}$};   

  \path 
	   %% DP
           (alpha) edge [connect] (pi)
           (pi) edge [connect] (z)
           (z) edge [connect] (f_u)
           (z) edge [connect] (a_u)
           
           %% Clustering
           (G_a0) edge [connect] (theta_ar)
           (theta_ar) edge [connect] (a_u)

           %% Prediction
           (G_f0) edge [connect] (theta_fr)
           (theta_fr) edge [connect] (f_u)
           (f_u) edge [connect] (y);


  % User draws  
  \node[rectangle, inner sep=0mm, fit= (a_u) (z) (f_u), label=above left:U, yshift=-17mm, xshift=39mm] {};
  \node[rectangle, inner sep=4.4mm,draw=black!100, fit= (z) (f_u) (a_u)] {};  

  % Mixture specific parameters   
  \node[rectangle, inner sep=0mm, fit= (theta_ar) (theta_fr), label=above left:$\infty$, yshift=-17mm, xshift=39mm] {};
  \node[rectangle, inner sep=4.4mm, draw=black!100, fit= (theta_fr) (theta_ar)] {};  

\end{tikzpicture}
\caption{Graphical model of the generative process. All views are connected through the latent variables $\mathbf{z}$.}
\label{fig:general}
\end{figure}

The model is a generative model that starts with a Chinese Restaurant Process assigning users to different clusters. Then, for every cluster, a stereotypical feature vector and a stereotypical behavior function are drawn from some base distribution $\mathbf{G}$. Lastly, users generate their attributes and behaviors from their cluster stereotypes and according to some distribution $F$. The generative model is equivalent to two CRP mixtures that share clusters assignments. In our model, features are observed and behaviors are unobserved latent functions. A behavior function produces an observed output $y$.

Let $\mathbf{a_u}$ be the set of (observed) features of user $u$. Let $f_u$ be a (latent) behavior function of user $u$. Let $y_u$ be the (observed) outcome of $f_u$. We assume the following generative process:
\begin{itemize}
\item For every cluster $k$:
   \begin{itemize}
   \item Draw a stereotypical feature and a stereotypical behavior from their respective base distributions $\boldsymbol{\theta_k^{(a)}} \sim \mathbf{G_0^{(a)}}$ and $ \boldsymbol{\theta_k^{(f)}} \sim \mathbf{G_0^{(f)}}$ 
   \end{itemize}

\item For every user $u$:
   \begin{itemize}
   \item Draw a cluster assignment $z_u$.
   \item Draw user features  $\mathbf{a_u} \sim F_a(\theta_{z_u}^{(a)})$. 
   \item Draw a user behavior function $f_u \sim F_b(\theta_{z_u}^{(f)})$.
   \end{itemize}

\item For every $f_u$:
   \begin{itemize}
   \item Draw a behavior $y_u$.
   \end{itemize}
\end{itemize}
The corresponding graphical model is shown in Figure \ref{fig:general}. Left and right branches of the model are called \textit{descriptive view} and \textit{predictive view}, respectively. Note that every cluster contains two sets of parameters (one for features and one for behaviors) so that the two views can be generated from the same cluster. This encodes our prior assumption that users who are similar in one view should be similar in the other. Note also that, although every user has a latent function $f_u$, the outcomes $y$ may be collective outcomes such as the final score of a collaborative project.


% z decisions
The inferences of $\mathbf{\theta^{(a)}}$, $\mathbf{\theta^{(f)}}$ and the behavior functions $\mathbf{f}$ are independent given the cluster assignments, since $\mathbf{z}$ is the only variable connecting the two views. The inference of $\mathbf{z}$ does use information of both views. The posterior of $\mathbf{z}$ is proportional to the product of its prior and the likelihoods of the different views:
\begin{align}
p(\mathbf{z} | \alpha, \boldsymbol{\theta^{(a)},\theta^{(f)}}, \mathbf{a, f}) 
\propto
p(\mathbf{z} | \alpha) 
p(\mathbf{a} | \boldsymbol{\theta^{(a)}}, \mathbf{z}) 
p(\mathbf{f} | \boldsymbol{\theta^{(f)}}, \mathbf{z})
\end{align}
The information given by each of the two views of the model is conveyed through the likelihood factors $p(\mathbf{a} | \mathbf{z})$ and $p(\mathbf{b} | \mathbf{z})$. The probability of a partition $\mathbf{z}$ over a partition $\mathbf{z'}$ is:
\begin{align}
\frac
{p(\mathbf{z}| \cdot)}
{p(\mathbf{z'}| \cdot)}
= 
\frac{
p(\mathbf{z} | \alpha)
}{
p(\mathbf{z'} | \alpha)
}
\frac{
p(\mathbf{a} | \boldsymbol{\theta^{(a)}}, \mathbf{z})
}{
{p(\mathbf{a} | \boldsymbol{\theta^{(a)}}, \mathbf{z'})}
}
\frac{
p(\mathbf{f} | \boldsymbol{\theta^{(f)}}, \mathbf{z})
}{
{p(\mathbf{f} | \boldsymbol{\theta^{(f)}}, \mathbf{z'})}
}
\end{align}
where we see that the contribution of each view depends on how more likely $\mathbf{z}$ is over the other assignments in that view. An extreme case would be a uniform likelihood in one of the views, meaning that all partitions $\mathbf{z}$ are equally likely. In that case, it will be the other view the one that will lead the inference. 

Note that the two views provide reciprocal feedback to each other through $\mathbf{z}$. That means that if one branch is more confident about a given $\mathbf{z}$ or, more likely, about a given cluster assignment $z_u$, it will force the other branch to re-consider its beliefs and adapt its parameters to fit the suggested $z_u$. 




%%%%%%%%%%%
See the attached working paper for a detailed description and first experiments on synthetic data to prove the concept.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Perspectives}
% Future
In the following months, I would like to re-examine our forum datasets to design some feasible prediction tasks. 


On the other side, it might be interesting to check into Variational Inference as a replacement for the current Monte Carlo methods used for parameter inference. Variational Inference would avoid the problems of identifiability, convergence and scalability.

The current model uses a simple linear regression for predictions. More complex models should be plugged in.

\begin{itemize}
\item \textbf{Better inference algorithms}: Inference in current multi-view model is based on Markov Chain Monte Carlo. MCMC draws samples from the posterior distribution of the parameter once the chain has converged. However, not only convergence checks give no guarantee of real convergence, but also the process can be too slow even for the size of our datasets. Moreover, MCMC in mixture models suffers from label-switching, meaning that cluster labels are often permuted from sample to sample, making it difficult to summarize which users belong to which clusters. Variational inference approximates the posterior in a Expectation-Maximization way and avoiding label-switchs.

\item \textbf{Better graph representation}: It might be useful to think of new representations of the discussion threads so that relevant information (e.g.: temporal information) is given to the learning algorithm. We have already explored possible temporal representations of discussion threads, but we have not found a satisfactory one.

\item \textbf{Better predictors}: The current model is based on a linear regression predictor. I expect the the accuracy in real data to be very poor. Better predictors (compatible with the current model, that is, that can be based on a Dirichlet Process) should be plugged-in instead.

\item \textbf{Feasible and low-dimensional prediction tasks}: Apart from better predictive models we need to redefine the prediction task so that it is both meaningful in terms of individual behavior and feasible, or in other words, learnable. We have to avoid tasks with such huge dimensions and such few instances.


\end{itemize}

\appendix
\chapter{Publications}

\begin{itemize}
\item Lumbreras A., Lanagan J., Velcin J., Jouve B. (2013), Analyse des rôles dans les communautés virtuelles : définitions et premières expérimentations sur IMDb, MARAMI 2013

\item Lumbreras A., Lanagan J, Jouve B., Velcin J. (2013), An insight into the Analysis of Roles in IMDb., Complexity in social systems: from data to models, Cergy Pontoise (95), 27-28 juin 2013.
\end{itemize}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{chicago} 
%\bibliographystyle{plainnat} 
\bibliography{./bibtex/library}
\end{document} 