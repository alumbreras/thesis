\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{State of the Art}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Etnography and exploratory}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Attribute based}{8}{0}{1}
\beamer@sectionintoc {2}{Structure-based roles}{13}{0}{2}
\beamer@subsectionintoc {2}{1}{Blockmodels}{14}{0}{2}
\beamer@subsectionintoc {2}{2}{Looking for regularities}{17}{0}{2}
\beamer@sectionintoc {3}{Ideas and hypothesis}{20}{0}{3}
