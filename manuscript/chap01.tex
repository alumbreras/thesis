\chapter{Introduction}\label{ch:intro}
%http://ici.radio-canada.ca/regions/montreal/2013/12/09/004-sociologie-metro-montreal.shtml
%http://www.lapresse.ca/debats/votre-opinion/200903/07/01-834233-des-usagers-qui-savent-vivre.php
%http://www.slate.com/articles/life/transport/2009/11/underground_psychology.html

\iffalse
% Del libro de Parsons
\A role represents the dynamic aspect of a status A The individual is socially assigned to a status and occupies it with 
relation to other statuses. When he puts the rights and duties which constitute the status into effect, he is performing a role. Role and status are quite inseparable, and the distinction between them is of only academic interest. 
\fi

% Increase of Productivity
% Political participation
% e-Health
%Forums
\lettrine{F}{rom} the first newsgroups in the 1980s up to the present day, online forums have always been amongst the most popular ways of online communication. Even after the huge expansion of social networks, forums are still used by 15\% of online users in the U.S. \citep{Duggan2015}. Modern forums cover a large variety of topics such as politics, health, technology or video-games, and many applications such as questions \& answers (Q\&A), sharing and discussing news (newsboards), seeking other students help in Massive Open Online Courses (MOOC) or discussing among supporters of a political cause. The current growth of forums like Quora, Reddit, or StackExchange suggests that this form of online communication is stronger than ever and that it has an enormous potential ahead.

% much data => new challenges and opportunities
As forums become more populated and users produce more content, they open the door to new challenges and opportunities in fields like Computer Science, Complex Systems and Sociology.
% Computer Science
Computer Scientists must develop tools to help users explore the content of the forums so that the experience is satisfactory and users do not abandon the community; areas like Machine Learning, Information Retrieval and Recommender Systems are starting to play an important role here. %http://tech.fpt.com.vn/en/others/personalized-recommendation-of-stories-for-commenting-in-forum-based-social-media-nd498040.html
% Complex Networks
The framework of Complex Networks is excellent to analyse and understand how new dynamics emerge from \textit{micro} to \textit{macro} levels, or from the individual to the community. Some models have been proposed, for instance, to explain how the structure of a conversation grows over time \citep{Kumar2010, Gomez2012}, and it is astonishing to see how often mathematical models of human dynamics have the simplicity of physical laws. 
% Sociology -- playground for sociologists
Online forums are also places where communities emerge \citep{kollock1998}. The net of interactions between users in the form of posts, or comments, ends up creating shared meanings and, in general, a common culture that defines and delimits the set of possible behaviours. As an example, the popular slogan \textit{don't feed the troll} suggests users to ignore those playing the role of a \textit{troll}. 
%Interdisciplinarity
Nonetheless, the most fruitful approaches need to be interdisciplinary. Forums are big data, complex systems and human communities and, as such, an integrated view is likely to shed more fruitful results than the sum of the parts 
\citep{McFarland2016, Tinati2014}.


%% Aim of thesis
The central topic of this thesis is \textit{online roles}. Social roles have been widely studied by sociologists, anthropologists and psychologists. For them, a social role is a behaviour that a community expects from an individual that occupies some position in that community. A canonical ethnological study of roles in online forums was done in \cite{Golder2003}. Online roles have also been studied by computer scientists, who have put more emphasis on the detection of roles. In computer science, a role is usually regarded as a set of user-centred features or as the position that the individual holds in the social graph.

We think that one of the most interesting aspects of roles in sociology is that, once we know the role of an individual, we can predict, to some extent, how the individual will behave in a given situation. Roles are both descriptive and predictive categories of behaviour. We know how to attack the two problems separately: a descriptive categorisation of users is an unsupervised learning task (clustering), while learning to predict users behaviours from a log of past behaviours is a supervised learning task. However, integrating both tasks is not trivial.

The aim of this thesis is precisely to integrate both a descriptive and a predictive vision of online roles. By thinking of roles as archetypical behaviours, we will explore some ways to find clusters of users with similar behaviours.

%From the point of view of the administrators, or an organisation that hosts tens, hundreds or thousands of forums, it is critical to have tools that allow monitoring the health of their forums. 
%The results of this research will benefit users, administrators

% some attempts to conciliate the two approaches \cite{Callero1994}

%validation through prediction
%We will pose the problem as a prediction task. We could then model the behaviour of every individual hoping that it will have not only descriptive but also predictive power. Yet, modelling every single individual would lead us as many models as individuals, with no information shared between the models. If we are interested not in specific individuals but in general archetypes, we need to build models that reflect typical behavioural archetypes. Such archetypes is what we will call roles.

% Roles as predictors
%Roles are interesting because they help to understand people's behaviour. Moreover, knowing the role of the members of a society (or of the participants in a given situation) and the expectations and norms attached to that role we can foresee behaviours. In this thesis, I look at roles from this point of view. Particularly, I see the roles as functions of behaviors and I consider that two users have the same role if they behave similarly in similar contexts. If we can infer regular behavioural functions that regulate the behaviours of (at least some) users, we could foresee their behaviour. 

\section{Context}
%The concept of role has been largely debated by sociologists. In this section, we present an overview of the general approaches to the problem of role analysis for both offline and online cases.


% Metro-bus community
Imagine a metro station. If we observed the social life for several days, including the behaviours and the interactions of the passengers, what would we find? A big amount of what happens there seems ephemeral and chaotic but, after a few days, we would notice some patterns in many aspects such as the way people choose their sits, their interpersonal distances or their conversations \citep{Nash1975}. Learning these patterns helps to understand the inner mechanisms of the metro community such as its rituals and its norms. Despite being the first time most of the metro passengers see each other, and despite the absence of explicit norms for most of the observed behaviours, the emergence of these patterns is possible thanks to a mixture of biology, culture, and a network of inter-personal interactions that are able to propagate norms, beliefs, and expectations between passengers that never shared the same wagon.

% Role
A very special pattern is what sociologists call a \textit{role}. Following Ervin Goffman's metaphor of the social drama \citep{Goffman1959}, a role is a script that an individual (actor) plays in front of others (audience) because the others expect the individual to do so. In the metro, when an old lady stands beside a sitting teenager, everyone expects the teenager to offer his sit to the lady. This expectation does not depend on the particular teenager nor the particular lady, but on their social positions.  

% Utility of roles 
The idea that a role is attached to a social position has two interesting consequences: on the one hand, a role census of a community, and the description of behaviours attached to every role, represent a good summary of the community. On the other hand, knowing the role of an individual allows us to predict the way the individual is likely to behave in a given situation, even if we have no other information about the individual. Or to put it in Golder and Donath words \citep{Golder2004}:

\begin{quotation}
Roles, in turn, are useful because, when they comprise sets of expectations, they allow us to generalize across people and have some a priori knowledge about entire categories of people, how to act toward them and what to expect in their actions toward us. More importantly, we categorize people precisely in order to understand them \citep{Foucault2003}, because having expectations is so vital to our understanding of others. 
\end{quotation}

\subsection{Role theory}
Although role theory has been around at least since the first half of the 20th century, there is neither a unified theory nor a single definition of \textit{role}. In \textit{Role Theory: Expectation, Identities, and Behaviors} \citep{Biddle1979}, the author complained:

\begin{quotation}
And as a matter of fact, the plasticity of role concepts is one of the major reasons why the role orientation is popular. Since social scientists have rarely found it necessary to spell out exactly what they meant by role, social position, or expectation, these terms could be applied with impunity to almost any purpose. Role might be considered an identity, a set of characteristic behaviours, or a set of expectations; expectations might be descriptive, prescriptive, or evaluative; and so it goes. And given such a superficial level of analysis, it would be possible either to exalt or decry role notions with equal enthusiasm since each of us would be talking about somewhat different things.
\end{quotation}

% Symbolic vs Interactionism
Social scientists have confronted the issue of roles from different approaches which, in part, mirror the sociological debate. The \textit{functionalist} approach considers that a society, as the human body, is a system where every part has a function \citep{Lynton1936, Parsons1951}; functionalism sees a role as the behaviours that the community expects from an individual \textit{because} he occupies a given position.  The \textit{symbolic interactionism} approach thinks of a society as the result of the interactions between individuals \citep{Mead1934}; interactionism sees that roles are continually negotiated and individuals take, define or modify roles as a result of their interactions. The \textit{structuralist} approach to roles models a society as a set of positions, filled by persons and relationships between positions \cite{Nadel1957}; individuals that fill the same positions are said to have the same role. Though it imposes a very restrained view of roles, it is  easy to formalize mathematically; later we will see a popular formalization called \textit{blockmodeling} \cite{White1976}. See \cite{Biddle1986} for a complete review and further discussion about the different approaches. 


Biddle proposes an soft definition that capture the elements that are shared by most approaches \cite{Biddle1979}:

\begin{quotation}
In current social science the term role has come to mean a behavioural repertoire characteristic of a person or a position; a set of standards, descriptions, norms, or concepts held for the behaviours of a person or social position; or (less often) a position itself.
\end{quotation}

% Durkheim: Functionalism. Social facts: norms, values, structures of a society. Society as a system of interrelated parts where no part can function without the other.
% Spencer: Structural functionalism. Society as human body
% Interactionism and structuralism

\section{Forums as graphs}

%In parallel to the debate between structuralists and interactiorism that debate, Moreno \citep{Moreno1934} started to look at group relationships in an original way. Moreno handed up a questionnaire to the patients of a menthol hospital. In that questionnaire, patients would write whether they were friends with the other patients. From this, Moreno filled a square matrix where a 1 would indicate that patients in that row and columns were friends, and a 0 if otherwise. That matrix was to be called a \emph{sociomatrix}, and would be the beginning of fruitful research in what would be later known as Social Network Analysis.

In this thesis, we study roles from a structural point of view. To this aim, we represent user activity in the form of graphs. Deciding what vertices and edges represent must be subjected to the type of analysis we want to make. In the context of online forums, there are at least three possible representation:
  
\subsection{Interactions graph}
An interactions graph (or replies graph) is a graph where vertices represent users and an edge between $u$ and $v$, denoted as $(u,v)$, indicates that $u$ has replied to a post of user $v$ in some of the threads. Many variations of this graph are also possible. For instance, edges can also be undirected, meaning that $(u,v)=(v,u)$, as well as weighted, where their weight represents the number of replies. Also, we can set stronger conditions for an edge to exist, such as the existence of replies in both directions or a minimum number of replies.
Figure \ref{fig:interactions_matrix} shows some examples of interaction graphs.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch1_forum_graph_interactions_top_1000}%
\caption{Matrix of interactions between the top 1000 users with more posts. For a better visualisation, values are log-normalized: $m_{ij} = \log( m_{ij}/m_{max})$}
\label{fig:interactions_matrix}
\end{figure}

%Speculation: forums with bigger density have more culture


\subsection{Coparticipations graph}
A coparticipation graph is a graph where vertices represent users, and where edges between $u$ and $v$, denoted as $(u,v)$, indicate that $u$ and $v$ have participated in the same discussion. Just as the interaction graphs, variations of this representation are also possible. Nevertheless, this graph is always undirected.
Figure \ref{fig:coparticipations_matrix} shows some examples of coparticipation graphs.
    
\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch1_forum_graph_coparticipations_top_1000}%
\caption{Matrix of coparticipations between the top 1,000 users with more posts. For a better visualisation, values are log-normalized: $m_{ij} = \log( m_{ij}/m_{max})$}.
\label{fig:coparticipations_matrix}
\end{figure}

\subsection{Discussion tree graphs}
A discussion tree graph is a graph that represents a single thread. Vertices correspond to posts and an edge between post $u$ and $v$, denoted as $u,v$, indicates that $u$ is a reply to $v$. Figure \ref{fig:trees} shows some examples of tree discussion graphs.

\begin{figure}
\centering
\includegraphics[scale=0.3]{ch1_tree1}%
\includegraphics[scale=0.3]{ch1_tree2}%
\includegraphics[scale=0.3]{ch1_tree3}
\caption{Tree representation of three conversation threads in Reddit.}
\label{fig:trees}
\end{figure}

Unlike the interactions and the coparticipation graphs, tree graphs represent the local dynamic of the conversations, which makes them very appropriate to analyse the way how different users converse.

\section{Role detection in online communities}

% Forums 
In some sense, an online forum is similar to a metro. Imagine now an online forum. Just as it happens in the metro, forums are places with none or few explicit norms and where most of the participants have never seen each other. The collective behaviour of the forum users also follows several patterns \citep{Whittaker1998} and individuals also play several social roles. 

%Bottom-up and top-down. detecting vs finding
In order to find these roles, we can follow either a top-down or a bottom-up approach. Top-down approaches take an \emph{a priori} definition of one or several roles and examine the community to find persons that match these patterns; an example of this are the methods to find trolls \citep{Kumar2014}, anti-social users \citep{Cheng2015}, influencers \citep{Agarwal2008}, celebrities \citep{Forestier2012} or leaders \citep{Goyal2008}. On the other hand, bottom-up approaches look for behavioural patterns among users to obtain a descriptive definition of roles; as a canonical example, see for instance \cite{Chan2010}. In summary, top-down approaches are techniques to find known roles, while bottom-up approaches are techniques to detect unknown roles.  

In this section, we present the main approaches to the problem of detecting users roles in social networks and especially in forums. 
%For a recent and general review see \cite{Zighed2012}.

\subsection{Social sciences}
Ethnological and sociological methodologies are arguably the most complete ones when it comes to identifying and describing the roles played by members of a community. 

Ethnologists live within a community for some time, talk to its members, learn the cultural codes and norms of the community, and produce a report where all these are synthesised. In online communities, the most popular ethnological study has been that of \cite{Golder2004}, who studied the roles in an online forum. These roles are the celebrity, the lurker, the newbie, the troll, the ranter, and the flamer.

\cite{Glea2009} describe a sociological multi-level iterative strategy to identify roles in online communities. They suggest the combination of a general qualitative and quantitative analysis---where a first draft of the existing roles will come up---and a more specific analysis of the structural and behavioural patterns (e.g.: egonets) of each role in order to refine the findings at the higher levels. The analyse goes on until the researcher finds a good correlation between a role category and a behavioural pattern.

\begin{quote}
It is important to choose the right starting point for social role research. Starting from simple behavioral regularities or distinctive social positions is a flawed approach because it is overly inductive: of the infinite patterns we can find, there is no reason to think that the ones that initially stand out will be of any social significance. Starting from abstract categories like 'altruist' commits the error of over-deduction: simply because we can label a set of activities as altruistic does not necessarily mean that those behaviours are motivated by altruism. The key is to connect higher levels and abstract categories to observable behavioral regularities and distinctive network positions in order to discover types of actors relevant to theoretical questions in social research. \cite{Glea2009} 
\end{quote}

When the size of the community is significant, the researcher can only identify and describe the key roles by studying a small set of individuals. However, these methods are not able to label each member of the community. 

% We would like a sort of automated sociologist.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Blockmodeling}
The family of blockmodeling algorithms searches groups of users that share similar relationships to the other groups. Blockmodeling works over the \textit{sociomatrix}, a $U \times U$ matrix where the cell $(i,j)$ contains a value that reflects the relationship from user $i$ to user $j$. The relationship may be, for instance, the level of affection from $i$ to $j$, the number of telephone calls or the number of replies from $i$ to $j$ in a discussion forum. 

%This sort of analysis was called positional analysis. Positions in a social network can help to understand what is going on. Nonetheless, it is only an intermediate step towards the analysis of roles. Once the researcher has discovered the positions in its sociomatrix, he must continue to analysis why this blocks are formed. This is a task that lays out of the algebraic domain.

The initial motivation of blockmodeling raised from sociology and the study of \textit{structural roles} or individuals that hold the same \textit{position} in their social networks. If our community is a family and relations are ``is son of", the positions of two siblings, thought not exactly the same, are structurally equivalent since their relationships with the rest of the family members are exactly the same: they both are siblings of their parents, as well as siblings of their mothers' brother, and so forth. 

Historically, different notions of similarity have motivated different algorithms. \textit{Structural equivalence} \citep{Lorrain1971} considers that two users are equivalent if they are equally related to the same other users. \textit{Regular equivalence} \cite{White1983} considers that two users are equivalent if they are equally related to the same other groups of equivalent users. Finally, \textit{stochastic equivalence} \citep{Holland1983} considers that two users are similar if they \textit{tend} to be equally related to the other groups of equivalent users.  Following this last definition, \cite{Nowicki2001} derived a general Bayesian model that has been the base of many Stochastic Blockmodeling adaptations to different kinds of data. The basic model can be expressed as:
\begin{align}
p(\mathbf{Z}, \boldsymbol{\theta} | \mathbf{G}) 
\propto 
p(\mathbf{G} | \mathbf{Z}, \theta) p(\mathbf{Z})
\end{align}
where $\mathbf{Z}$ is a matrix of cluster assignments, $\boldsymbol{\theta}$ are the model parameters, and $\mathbf{G}$ is the sociomatrix of relations. Stochastic Blockmodeling can be adapted to edges with all sorts of weight distributions such as Poisson \citep{Dubois2013} or Gaussian. Other variations are also possible, such as allowing users to belong to many clusters (Mixed Membership Stochastic Blockmodels, \cite{Airoldi2008}) or to change their interaction patterns, and thus their roles, over time \citep{Fu2009, Xing2011}. An appealing contribution is that of non-parametric blockmodeling that infers the number of clusters  by means of a Dirichlet Process prior  over the cluster assignments \cite{kemp2004,Kemp2006}.

Unfortunately, in most settings we cannot solve the integral which is necessary to normalize the probability distribution. Thus, inference is often done either by Monte Carlo sampling (usually Gibbs Sampling) or by Variational Inference. While the former gives us samples from the true posterior, the latter is much faster at the expense of an approximation to the posterior. Due to the relational nature of blockmodeling, scalability is an issue in many real world problems, which has increased the popularity of Variational Inference approaches \citep{Daudin2008, Latouche2012}.

Although blockmodeling can be applied to many types of adjacency matrices, its application to forums has some particular limitations. Figure \ref{fig:blockmodels} shows the blockmodels found by the blockmodeling proposed in \cite{Daudin2008} (\texttt{mixer} package in \texttt{R}), in the binary coparticipation matrices of our forums. Blockmodels uncover interesting structures in interaction or coparticipation matrices, such as blocks of users that avoid each other. 
First, the white blocks detected, for instance, might represent a group of users that arrived some time later and thus did not have time to coparticipate with the other users; in order to solve this, we would need a dynamic or longitudinal blockmodeling where nodes can appear and disappear. Second, even if a blockmodel is applied thread by thread, we do not know how to compare the positions of a user in two different structures, making it difficult to say anything about the role of a user beyond a particular conversation.

 
  
%VB: Daudin2008, Airoldi2008, Fu2009, Latouche2012 
%MCMC Kemp2006, McCallum2007,DuBois2013

% Implementations:  \textit{mixer} package in R implements the paper \cite{Daudin2008}. Edges Bernoilli (binary matrices)
% Bernouilli, Gaussian or Poisson:  \cite{Leger2015} for \textit{blockmodels} package

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch1_blockmodels_pre_interactions_100}
\includegraphics[width=1\textwidth]{ch1_blockmodels_interactions_100_variational_minweight2.png}
\caption{Blockmodeling over the interactions matrix of the 100 most active users}
\label{fig:blockmodels}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{ch1_blockmodels_pre}
\includegraphics[width=1\textwidth]{ch1_blockmodels}
\caption{Blockmodeling over the co-participation matrix of the 1,000 most active users}
\label{fig:blockmodels}
\end{figure}


%%%%%%%%%%%%%%%
%Application to threads? Probably not

\subsection{Feature-based}
Most role detections in discussion forums are based on describing each user through a set of features and then classifying users by clustering or by some rules based on thresholds to find groups of users with similar features. The resulting clusters are interpreted as roles. 

\cite{Himelboim2009} search \textit{catalists}, users that start discussions and attract many participants and posts, since they are the ones that set the topics that the community debates about. They classify a user as a catalyst if they have a high score in three metrics: number of posts attracted by their threads (reply share), number of users in their threads (replier share), and ratio of threads that get comments from at least two users. 

\cite{Welser2007,Glea2009} selected users with recognised roles and found that the egonet of a user is very attached to its role. Based on this idea, \cite{Buntain2014} select some features from the egonet and find clusters over them. The features are undirected network density, low degree distribution, ratio of neighbours with low degree, proportion of intense ties, clustering coefficients, and triangle density.

\cite{Chan2010, White2012} select a set of user features and perform a cluster over them; they analyse the resulting clusters and manually merge some of them if they represent the same type of user; then each cluster is defined as a role. The set of features is the indegree and outdegree exponents of their egonets, the ratio of neighbours that replied to each other, the mean and standard deviation of posts per thread, their in-degree, the number of posts with at least one reply, the ratio of initiated threads. Using these same features, \citep{Angeletou2011} build a rule-based system with adaptive thresholds instead of a clustering.

\cite{Rowe2013} extend their work on \citep{Angeletou2011} and propose five features: the proportion of users that the user has replied to, the proportion of threads created by the user, the proportion of threads started by the user, and the average points per post awarded to the user.

In the context of TV-series forums, \cite{Anokhin2012a} use some basic features and adapt some others from the citation analysis literature such as the h-index \citep{Hirsch2005} and the g-index \citep{Egghe2006}. They also use a cross-topic entropy to measure how focused is a user in a TV-show.

\cite{Forestier2012} look for celebrities based on the ethnological study of \cite{Golder2004}. They build some meta-features based on the number of posts the in-degree and out-degree of the user in the reply network, and the number of threads.

Table~\ref{table:features} summarises the main features used in the literature.

One of the advantages of these methods is that the results are easy to interpretate since each role is clearly attached to a range of values in each feature. Besides, a different choice of features will give us a set of roles based on different criteria, which may be useful if the researcher is especially interested in one specific aspect of user behaviour.


\begin{table}
\begin{tabular}{ |l|l| } 
 \hline
 Feature & References \\
 \hline 
 
 \textbf{Threads started} &  \\ 
 Threads started    & \cite{Rowe2013}\\
 Threads started with replies & \cite{Himelboim2009,Chan2010} \\
 Posts in threads     & \cite{Himelboim2009} \\ 
 Users in threads     & \cite{Himelboim2009} \\ 

 \hline
 \textbf{Posts written} & \\
 Posts with reply & \cite{Chan2010}\\
 Votes per post  & \cite{Rowe2013}\\
 
 \hline
 \textbf{Activity levels}& \\
     Posts &  \cite{Forestier2012, Rowe2013}\\
    Mean Posts/thread &  \cite{Chan2010}\\
    Std. dev. post/thread & \cite{Chan2010}\\
    Focus Dispersion & \cite{Rowe2013}\\
    Avg. post-to-thread tf-idf & \cite{Nolker2005}\\
    Avg. poster-to-poster tf-idf & \cite{Nolker2005}\\
    Threads with reciprocal replies &\cite{Chan2010, White2012}\\
    
 
 \hline
 \textbf{Egonet} & \\
    In-degree distribution exponent & \cite{Chan2010, Forestier2012}\\
    Out-degree distribution exponent & \cite{Chan2010}\\
    Reciprocal neighbours & \cite{Chan2010, White2012}\\
    Indegree & \cite{Chan2010, White2012} \\ & \cite{Forestier2012, Rowe2013}\\
    Outdegree  & \cite{White2012, Forestier2012} \\ & \cite{Rowe2013}\\
    Degree & \cite{Nolker2005, Buntain2014}\\
    Neighbours with low degree & \cite{Buntain2014}\\
    Weighted indegree & \cite{White2012}\\
    Weighted outdegre & \cite{White2012}\\
    Density & \cite{Buntain2014}\\
    Neighbours with intense ties & \cite{Buntain2014}\\
    Clustering coefficient & \cite{Buntain2014}\\
    Triangle density &  \cite{Buntain2014}\\

\hline
\textbf{Replies graph} & \\
Betweenness & \cite{Nolker2005}\\
Closeness & \cite{Nolker2005}\\

\hline
\textbf{Language} & \\
Content-based features & \cite{Lui2010}\\
\hline
\end{tabular}
\caption{Features for role detection}
\label{table:features}
\end{table}

\subsection{Triads and motifs}
A triad is a graph consisting of three vertices. There are 16 possible triads, from the empty triad with no vertices to the fully connected triad (Figure~\ref{fig:triads}).
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{ch1_triads}
\caption{List of all possible triads and their commonly given IDs}
\label{fig:triads}
\end{figure}
Triads have been largely used in social network analysis since they represent their most basic building blocks \citep{Wasserman1994}. When a triad is statistically over-represented it is called a motif \citep{Milo2002}. Motifs have been used in online forums to analyse patterns over the Q\&A graph \citep{Adamic2008}. 
 
In order to test whether a triad is over-represented, we need a reference null model. The null model can be built by randomizing the original graph while preserving some of its properties such as the degree distribution. Given a set of randomized versions of the graph, we can then easily compute the $z$-score and $p$-value of the triad in the real graph to decide whether its frequency is statistically significant or if it can be explained by the reference model.

% Lack of NULL model
A good null model should reproduce all we know about the properties of the graph so that the motifs that are detected are those that cannot be explained by our current knowledge. Some null models are, for instance, the rewiring of the edges that preserve the degree distribution \citep{Wernicke2006} or random graphs that keep the same blockmodel structure \citep{Birmele2010a}. 

It is important to note that conversation trees are acyclic graphs, and therefore some randomisation strategies cannot be applied. A valid strategy would be to re-build the tree sequentially; for every new vertex, choose a parent vertex uniformly from the vertex that are already in the tree. We may respect the degree distribution (e.g., \cite{Dorat2007}) or give a probability to each parent according to some thread growth model such as \citep{Gomez2012}.

There are only three possible triads in a tree (012, 021U, and 021C) and therefore a triad analysis in a tree is less verbose than a triad analysis in an interaction graph. The obvious solution is to convert the tree into an interactions graph and do the triad analysis there. However, we would be losing the conversation structure; a chain in the interaction graph (triad 021C) does not necessarily correspond to a chain in the tree graph. In chapter 3, we propose a definition of \textit{structural neighbourhood} in order to allow a richer analysis of tree-shaped conversations.

\subsection{Other methods}
There are other approaches that fall off the above categories. Some authors, for instance, have proposed different visualizations to spot interesting roles. \cite{Viegas2004} designed a visualisation tool to plot some aspects of authors posting behavior, and used this tool to visually identify some roles.  \cite{Fisher2006} proposed using egonets of second degree and their degree distribution. An analysis of different visualisation methods and their utility to find roles can be found in \cite{Welser2007}.

Of course, the concept of role is not limited to discussion forums. They have also been studied in Q\&A forums \cite{Furtado2013}, a feature-based approach that uses features like the number of questions, the number of answers, and the scores to their questions and answers given by the community. \cite{Maia2008} build a social graph for the case of YouTube where edges mean subscriptions, and use features from the egonet (e.g.: in-degree, out-degree, reciprocity, and clustering coefficient) and activity features (e.g.: number of uploads, number of watched videos) to cluster users. \cite{Labatut2014} define social capitalists as users trying to increase their number of followers and interactions by any means possible, and identify different types of social capitalists clustering users over structural features. \cite{Zhang2007} classify users of a Java forum in terms of expertise with an adapted PageRank algorithm. \cite{Welser2011} apply their methodology \citep{Glea2009} to find roles in Wikipedia. 

Although language is certainly a major indicator of a user role, it has been barely used for the automatic detection of roles. In \cite{Golder2004}, they describe a flamer as someone who tries to trigger long debates (flame wars). For instance, a flamer in Usenet wrote this:
\begin{quotation}
\texttt{Look moms (and dads), you little babies ARE NOT cute. Sorry to break the news, they are UGLY. So stop bringing them into the office so that fat old bags (who couldn't if they tried) can get up off their fat arses and say things "oh isn't that cute". YUCK.}
\end{quotation}
Detecting this is a flamer requires knowing a cultural context where every one says babies are cute, something that is out beyond the current state of the art of  Natural Language Processing. One might instead apply sentiment analysis over posts to detect the positiveness or negativeness of a user and add this feature to the role analysis. Topic analysis was used by \cite{McCallum2007}, who mixed topic analysis and blockmodeling over the e-mails between Enron employees to detect their role (position) in the company, exploiting the fact people in the same position write about the same things to people in the same other positions. It is not clear, however, how this assumption would map into online forums where the community is already, by definition, a topic-based community.

Other authors have analysed community-based roles in social graphs, such as bridges or hubs. \cite{Scripps2007, Chou2010, Gliwa2013, Henderson2012} and their evolution over time \cite{Rossi2013}.

\section{Summary and contributions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The aim of this thesis is to detect roles of users in online forums. Sociologists have defined roles as behaviours attached to social positions. This dual nature of a role has been intentionally exploited by blockmodeling, a technique that finds structural positions in order to later analyse their behaviours (this is actually called \textit{positional analysis}). Although more implicitly or even unintentionally, this has also been exploited by feature-based methods: some features are behaviours (e.g.: number of posts) or proxies to behaviours (a highly scored post reflects a proper behaviour, whilst a negative score reflects either a newbie or a troll), and other features could be related to social positions (e.g.: centrality metrics).

Nonetheless, behaviours that are captured by most features are aggregated statistics that miss information about behaviours at the conversation level. We think that this level of analysis is necessary for a better understanding of online roles. Indeed, the analysis of conversations has been the central tool in the seminal ethnological analysis of \cite{Golder2004}.

Once the level of analysis was set, we had two aspects that could be analysed: the language and the structure. This thesis is centred in the structure of conversations. This means that we are ignoring the textual content and we are modelling conversations as tree graphs. We do not claim that a textual analysis would not be a good complement. Actually, we have some ideas for future research concerning this approach that we will discuss in the last chapter.  

The general hypothesis of this thesis is that users can be divided into different roles and that every role is a behavioural function that models the user behaviour in the conversation. Thus, we will attempt to find clusters of users according to different kinds of conversational behaviours.

In Chapter 2, we present our forum datasets and extract some general statistics in order to gain some insight. Besides, some findings will help us contextualise the results of the following chapters.

In Chapter 3, we describe a behaviour as a tendency towards some type of conversation. We find the structural neighbourhood around every post and we cluster together users that participate in the same kind of structures.

In Chapter 4, we describe a behaviour as a tendency to reply to some kind of post. We use a growth model for threads and find different growth parameters for different users, clustering together users that tend to reply to the same type of posts. We analyse this in the context of recommender systems.

In Chapter 5, we propose a dual-view model that clusters users based on both their features and their behaviours. The model exploits the idea that users with similar features have similar behaviours. In the context of roles, if features represent the social position of the individual, the model may be an excellent tool to take into account the two dimensions of roles at the same time.

We conclude in Chapter 6 with our conclusions, open questions and possible lines of research.