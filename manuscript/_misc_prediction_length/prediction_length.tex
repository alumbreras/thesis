\documentclass[../main/main.tex]{subfiles}

\begin{document}

\chapter{Predicting the success of a thread}
In this chapter, I explore whether the introduction of roles can be used to improve the predictions of the success of a thread. In the first part, I will create a predictive model that will serve as a baseline. In the second part, I discuss how to introduce roles in the predictive model and compare the role-based prediction with the baseline.

\section{Baseline predictions}

\subsection{Metrics}
In this section I propose some metrics and analyze their predictive power. I look for metrics that, taken at the beginning of the thread, can help to predict its final length.

\subsubsection{Initial speed}
Initial speed is measured as the number of posts per hour. Intuitively, the higher the initial speed, the most interest the thread attracted.
\[
S = \frac{|P|}{t_P - t_0}
\]
where $|P|$ is the number of initial posts, and $t_i$ is the time stamp of post $i$.
\subsubsection{Replies to root}
Replies to root are measured as the proportion of posts that reply to the post who opened the thread. Intuitively, if the root attracts most of posts, it means it is a very query-answer thread and therefore little discussion will happen between users.
\[
R = \frac{\deg(0)}{|P|-1}
\]

\subsubsection{Number of users}
Number of users is an indicator of the diversity of commentators. Intuitively, the more users participate in the first posts, the most people the thread is attracting.
\[
D = \frac{|U|}{|P|}
\]
where $|U|$ denotes the number of different users and $|P|$ denotes the number of posts. 

\subsubsection{Posts length}
Posts length measures the average number of words in the first posts. Intuitively, the longer the posts, the more discussion-based the thread is, which might be and indicator of a long debate starting to born.
\[
W = \frac{\sum_i^P |p_i|}{|P|}
\]
where $|p_i|$ is the number of words pf post $p_i$ and $|P|$ is the number of posts.

\subsubsection{Ramification ratio length}
Ramification is the proportion of posts that attract more than one reply or, in other words, the probability of a leaf to create a new branch. Intuitively, a high ramification between the first posts indicates that parallel discussions are created from the original one, and therefore the chances of the thread to be longer might grow. 
\[
B = \frac{\sum_i [|children(p_i)|> 1]}{|P|}
\]

\begin{figure}[!t]
\centering
\includegraphics[width=1\textwidth]{metrics_scatter.png}
\caption{Scatter plots of proposed metrics against thread final length}
\label{metrics_scatter}
\end{figure}

\begin{figure}[!t]
\centering
\includegraphics[width=1\textwidth]{triads_scatter.png}
\caption{Scatter plots of proposed metrics against thread final length}
\label{triads_scatter_1}
\end{figure}

\subsection{Experiments}

We take the most promising metrics and try to combine them to make the prdictions of the thread length. The two models tested are a linear regression and a SVM for regression.


\begin{figure}[!t]
\centering
\includegraphics[width=1\textwidth]{linear_regression_number_attributes.png}
\caption{Sqqq}
\label{triads_scatter_1}
\end{figure}

\begin{figure}[!t]
\centering
\includegraphics[width=0.5\textwidth]{linear_regression_confusion_matrix.png}
\caption{qqq}
\label{triads_scatter_1}
\end{figure}

\begin{figure}[!t]
\centering
\includegraphics[width=1\textwidth]{SVR_number_attributes.png}
\caption{Scatter plots of proposed metrics against thread final length}
\label{triads_scatter_1}
\end{figure}

\begin{figure}[!t]
\centering
\includegraphics[width=0.5\textwidth]{SVR_confusion_matrix.png}
\caption{Scatter plots of proposed metrics against thread final length}
\label{triads_scatter_1}
\end{figure}

\section{Role-based predictions}



\subsection{Motivation}

According to the Team Role Theory \citep{Meredith}, a role balance is necessary for a team to succeed in a project. In his studies, Belbin found seven complementary roles: plant, resource investigator, co-ordinator, shaper, monitor evaluator, teamworker, implementer and specialist. Discussion forums can be seen as team projects and therefore examined through the lens of Team Role Theory. The members of the team are the active participants in the discussion; the common goal is to create a successful discussion. A successful discussion is, for instance, one with a lot of comments or one where many different users participated.

\subsection{Naive model}
Let $\mathcal{T}$ be the collection of $T$ discussion threads $t_1$,...,$t_T$ in the corpus. Let $\mathbf{s}$ be a vector containing the success level of every thread in $\mathcal{T}$; success levels can be represented, for instance, by number of posts, number of users or any metric of success chosen by the researcher. Let $\mathcal{U}$ a collections of $U$ users. Let $P$ be a $U\times T$ binary matrix where $P_{i,j}$ is 1 if user $i$ participated in thread $j$ and 0 otherwise. Let $ \mathbf{z}$ be a vector containing the role of every user. \marginnote{notation} 	Let C, the role census matrix, be a $T\times Z$ matrix where $T_{ij}$ indicates the proportion of users with role $j$ in thread $j$. We assume there are only three possible roles.

Our first model assumes that the number of threads is a linear function of the role census, that is:

\begin{equation}
\marginnote{model}
y^{(i)} = w_0 + w_1 c_a^{(i)} + w_2 c_b^{(i)} + w_3 c_c^{(i)} + \epsilon
\label{basic_belbin}
\end{equation}

where $\epsilon$ is a zero-mean gaussian noise with $\sigma^2$ variance,

\begin{equation*}
\epsilon \sim \mathcal{N}(0, \sigma)
\end{equation*}
This model assumes that roles are statistically independent, that is, if $w_i$ is positive, the more users in $w_i$, the bigger $y^{(i)}$ will be. However, we could think of situations where the increase of some role with coefficient $w_i$ decreases or increases the importance of another coefficient $w_{j\neq i}$. We will address this issue in future models.

\subsection{Generative model}
$P$ and $\mathbf{y}$ are observed variables. We model the weights $\mathbf{w}$ and the role assignments $\mathbf{z}$ as random variables, drawn respectively from a Gaussian and Multimonial distribution. The complete generative model can be written as:

\begin{align*}
y^{(i)} &\sim \mathcal{N}(\mathbf{c}_{(i)}^T \mathbf{w}, \sigma) \\
w^{(i)} &\sim \mathcal{N}(0, \Sigma_0)\\
z_n &\sim Mult(\pi)\\
\pi &\sim Dirichlet(\alpha)
\end{align*}
where the vector $\mathbf{c}$ is computed from the roles $z_n$ of the participants in thread $i$. To keep the notation close to that of regressions, the census matrix $C$ and the census vectors $\mathbf{c}^{(i)}$ will be often referred as $X$ and $\mathbf{x}^{(i)}$.

\subsection{Joint probability}
The joint probability of our model is:
\begin{align}
p(\boldsymbol{y}, X, \boldsymbol{w}, \boldsymbol{z}, \pi; \alpha)
=
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
\left(p(\pi | \alpha) \prod_{u=1}^U p(z_u | \pi) \right)
p(w) 
\end{align}
the random variables are $y$, $z$, $w$ and $\pi$. Since we are not interested on $\pi$ we integrate it out:

\begin{align*}
p(\boldsymbol{y}, X, \boldsymbol{w}, \boldsymbol{z}, \alpha)
=&
\int_\pi \left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
\left(p(\pi | \alpha) \prod_{u=1}^U p(z_u | \pi) \right)
p(w)
\\=&
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
\left(\int_\pi p(\pi | \alpha) \prod_{u=1}^U p(z_u | \pi) \right)
p(w)
\label{success_joint_prob}
\end{align*}
The integral factor can be worked out because the distributions $p(z_n | \pi)$ and $p(\pi | \alpha)$ are conjugate:
\begin{align*}
\int_\pi p(\pi | \alpha) \prod_{u=1}^U p(z_u | \pi) 
=&
\int_\pi 
\frac{\Gamma( R \alpha)}{\left(\Gamma(\alpha)\right)^R}
\prod_{j}^{R} 
\pi_j^{\alpha-1}
\prod_{u=1}^U \prod_{i}^{R} \pi_i^{[i=z_u]}
\\=&
\int_\pi 
\frac{\Gamma( R \alpha)}{\left(\Gamma(\alpha)\right)^R}
\prod_{i}^{R} 
\pi_i^{N_i + \alpha-1}
\\=&
\frac{\Gamma( R \alpha)}{\left(\Gamma(\alpha)\right)^R}
\int_\pi
\prod_{i}^{R} 
\pi_i^{N_i + \alpha-1}
\\=&
\frac{\Gamma( R \alpha)}{\left(\Gamma(\alpha)\right)^R}
\frac{\prod_{j=1}^{R} \Gamma(N_j + \alpha)}{\Gamma\left(\sum_{j=1}^{R} N_j + \alpha\right)}
\int_\pi
\frac{\Gamma\left(\sum_{j=1}^{R} N_j + \alpha\right)}
{\prod_{j=1}^{R} \Gamma(N_j + \alpha)}
\prod_{i}^{R} 
\pi_i^{N_i + \alpha-1}
\\=&
\frac{\Gamma( R \alpha)}{\left(\Gamma(\alpha)\right)^R}
\frac{\prod_{j=1}^{R} \Gamma(N_j + \alpha)}{\Gamma\left(\sum_{j=1}^{R} N_j + \alpha \right)}
\\\propto&
\frac{\prod_{j=1}^{R} \Gamma(N_j + \alpha)}{\Gamma\left(\sum_{j=1}^{R} N_j + \alpha \right)}
\end{align*}

and then we can express the joint probability distribution marginalized over $\pi$ as:
\begin{align}
p(\boldsymbol{y}, X, \boldsymbol{w}, \boldsymbol{z}, \alpha)
\propto&
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)\frac{\prod_{j=1}^{R} \Gamma(N_j + \alpha)}{\Gamma\left(\sum_{j=1}^{R} N_j + \alpha \right)}
p(w)
\end{align}


\subsection{Inference: Gibbs sampling}
We use Gibbs sampling to infer the weights $\mathbf{w}$ and the role assignments $\mathbf{z}$. Gibbs sampling iteratively samples $\mathbf{w}$ and $\mathbf{z}$ from their conditional distributions $p(\boldsymbol{w} | \boldsymbol{y}, X, \boldsymbol{z}, \alpha)$ and $p(z_u = x | \boldsymbol{z}_{-u}, \boldsymbol{y}, X, \boldsymbol{w}, \alpha)$. The result of the sampler is a posterior distribution of our variables of interest.

\subsubsection{Conditional distribution of $\mathbf{w}$}
The conditional probability of the weights is:
\begin{align*}
p(\boldsymbol{w} | \boldsymbol{y}, X, \boldsymbol{z}, \alpha)
=&
\frac{p(\boldsymbol{y}, X, \boldsymbol{w}, \boldsymbol{z}, \alpha)}
{\int_w p(\boldsymbol{y}, X, \boldsymbol{w}, \boldsymbol{z}, \alpha)}
\\\propto&
\frac{
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)\frac{\prod_{j=1}^{R} \Gamma(N_j + \alpha)}{\Gamma\left(\sum_{j=1}^{R} N_j + \alpha \right)}
p(w)
}
{\int_w
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)\frac{\prod_{j=1}^{R} \Gamma(N_j + \alpha)}{\Gamma\left(\sum_{j=1}^{R} N_j + \alpha \right)}
p(w)
}
\\=&
\frac{
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
p(w)
}
{\int_w
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
p(w)
}
\\=&
\frac{
\left(\prod_{t=1}^T
\mathcal{N}(y^{(i)} | \boldsymbol{x}_i w, \sigma)
\right)
\mathcal{N}(w | 0, \Sigma_0)
}
{\int_w
\left(
\prod_{t=1}^T
\mathcal{N}(y^{(i)} | \boldsymbol{x}_i w, \sigma)
\right)
\mathcal{N}(w | 0, \Sigma_0)
}
\end{align*}

Normal distributions are conjugate distributions. Then, we can easily multiply them, getting a new normal distribution:
\begin{align*}
&\left(\prod_{t=1}^T 
\mathcal{N}(y^{(i)} | \boldsymbol{x}_i^T w, \sigma)
\right)
\mathcal{N}(w | 0, \Sigma_0)
\\
=\exp
&\left\lbrace
\frac{1}{\sigma}\sum_{i=1}^T(y^{(i)}-\boldsymbol{x}_i^T w)^2
+ 
w^T \Sigma_0^{-1}w
\right\rbrace
\\=
\exp
&\left\lbrace
\frac{1}{\sigma}(\boldsymbol{y}-X^T w)^T (\boldsymbol{y}-X^T w)
+ 
w^T \Sigma_0^{-1}w
\right\rbrace
\\=
\exp
&\left\lbrace
\frac{1}{\sigma}\left(\boldsymbol{y}^T\boldsymbol{y} + (X^T \boldsymbol{w})^T (X^T \boldsymbol{w}) - \boldsymbol{y}^T X^T \boldsymbol{w}-(X^T w)^T \boldsymbol{y} \right)
+ 
w^T \Sigma_0^{-1}w
\right\rbrace
\\=
\exp
&\left\lbrace
\frac{1}{\sigma}\left(\boldsymbol{y}^T\boldsymbol{y} + \boldsymbol{w}^T X X^T \boldsymbol{w} - 2\boldsymbol{w}^T X \boldsymbol{y} \right)
+ 
w^T \Sigma_0^{-1}w
\right\rbrace
\\=
\exp
&\left\lbrace
\frac{1}{\sigma}\boldsymbol{y}^T\boldsymbol{y} + 
\boldsymbol{w}^T \left(\frac{X X^T}{\sigma}+\Sigma_0^{-1}\right)\boldsymbol{w} - 2\boldsymbol{w}^T X \boldsymbol{y}
\right\rbrace
\end{align*}

Since we know the result is another normal distribution with a exponent of the form:
\begin{align}
(w-w_p)^T \Sigma_p^{-1}(w-w_p)
&=
w^T \Sigma_p^{-1} w +
w_p^T\Sigma_p^{-1}w_p -
2w^T\Sigma_p^{-1}w_p
\end{align}
we can try to express our equation in this form by identification of the factors in both equations. This is called \textit{completing the square}. the variance $\Sigma_p$ can be identified in equation 4.20 as:
\begin{align}
\Sigma_p^{-1} = \Sigma_0^{-1} + \frac{XX^T}{\sigma}
\end{align}

and for the mean:
\begin{align*}
2 \boldsymbol{w}^T X \boldsymbol{y} &= 2\boldsymbol{w}^T \Sigma_p^{-1} \boldsymbol{w}_p \\
X \boldsymbol{y} &= \Sigma_p^{-1} \boldsymbol{w}_p \\
\Sigma_p X \boldsymbol{y} &= \boldsymbol{w}_p
\end{align*}
therefore:
\begin{align}
\boldsymbol{w}_p &= \Sigma_p X \boldsymbol{y}
\end{align}

Getting back to the conditional distribution, replacing terms we have:
\begin{align}
p(\boldsymbol{w} | \boldsymbol{y}, X, \boldsymbol{z}, \alpha)
\propto&
\frac{\mathcal{N}(\mu_p, \Sigma_p)}{\int_w \mathcal{N}(\mu_p, \Sigma_p)}
\end{align}
since the integral of a probability distribution is one, we finally have a simple expression for the posterior of $\mathbf{w}$:
\begin{align}
p(\boldsymbol{w} | \boldsymbol{y}, X, \boldsymbol{z}, \alpha)
=&
\mathcal{N}(\mu_p, \Sigma_p)
= \mathcal{N}((\Sigma_0^{-1} + \frac{XX^T}{\sigma}) X \boldsymbol{y}, (\Sigma_0^{-1} + \frac{XX^T}{\sigma})^{-1})
\end{align}

\subsubsection{Conditional distribution of $\mathbf{z_n}$}
Now we derive the conditional probability of a role assignment:
\begin{align*}
p(z_u = x | \boldsymbol{z}_{-u}, \boldsymbol{y}, X, \boldsymbol{w}, \alpha) =&
\frac{p(\boldsymbol{y}, X, \boldsymbol{w}, \boldsymbol{z}, \alpha)}
{\sum_{z_u=1}^{R} p(\boldsymbol{y}, X, \boldsymbol{w}, \boldsymbol{z}, \alpha)}
\\
\propto&
\frac{
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)\frac{\prod_{j=1}^{R} \Gamma(N_j + \alpha)}{\Gamma\left(\sum_{j=1}^{R} N_j + \alpha \right)}
p(w)
}
{\left[
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)\frac{\prod_{j=1}^{R} \Gamma(N_j + \alpha)}{\Gamma\left(\sum_{j=1}^{R} N_j + \alpha \right)}
p(w)
\right]^{(-u)}}
\\
=&
\frac{
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)\frac{\prod_{j=1}^{R} \Gamma(N_j + \alpha)}{\Gamma\left(\sum_{j=1}^{R} N_j + \alpha \right)}
}
{\left[
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)\frac{\prod_{j=1}^{R} \Gamma(N_j + \alpha)}{\Gamma\left(\sum_{j=1}^{R} N_j + \alpha \right)}
\right]^{(-u)}}
\\=&
\frac{
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
\prod_{j=1}^{R} \Gamma(N_j + \alpha)
}
{\left[
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
\prod_{j=1}^{R} \Gamma(N_j + \alpha)
\right]^{(-u)}}
\\=&
\frac{
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
\prod_{j=1}^{R} \Gamma(N_j + \alpha)
}
{\left[
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
\prod_{j=1}^{R} \Gamma(N_j + \alpha)
\right]^{(-u)}}
\\=&
\frac{
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
\Gamma(N_x + \alpha)
}
{\left[
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
\right]^{(-u)}
\Gamma(N_x + \alpha - 1)
}
\\=&
\frac{
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
}
{\left[
\left(\prod_{t=1}^T p(y^{(i)} | X, \boldsymbol{z}, w) \right)
\right]^{(-u)}
}
(N_x+\alpha-1)
\\=&
\frac{
\prod_{t=1}^T e^{\frac{\left(y^{(i)}-M_{(i)}^T w\right)^2}{\sigma}}
}
{\left[
\prod_{t=1}^T e^{\frac{\left(y^{(i)}-M_{(i)}^T w\right)^2}{\sigma}}
\right]^{(-u)}
}
(N_x + \alpha - 1)
\end{align*}
So:
\begin{equation}
p(z_u = x | \boldsymbol{z}_{-u}, \boldsymbol{y}, X, \boldsymbol{w}, \alpha) =
\frac{
\prod_{t=1}^T e^{\frac{\left(y^{(i)}-M_{(i)}^T w\right)^2}{\sigma}}
}
{\left[
\prod_{t=1}^T e^{\frac{\left(y^{(i)}-M_{(i)}^T w\right)^2}{\sigma}}
\right]^{(-u)}
}
(N_x + \alpha - 1)
\end{equation}

\end{document}